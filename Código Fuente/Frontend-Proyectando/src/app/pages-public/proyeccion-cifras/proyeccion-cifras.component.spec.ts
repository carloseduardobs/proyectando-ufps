import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProyeccionCifrasComponent } from './proyeccion-cifras.component';

describe('ProyeccionCifrasComponent', () => {
  let component: ProyeccionCifrasComponent;
  let fixture: ComponentFixture<ProyeccionCifrasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProyeccionCifrasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProyeccionCifrasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
