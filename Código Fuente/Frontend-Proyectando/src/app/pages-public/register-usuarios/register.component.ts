import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormBuilder } from '@angular/forms';
import { NotificacionService } from 'src/app/services/notificacion.service';
import { AutenticacionService } from '../../services/autenticacion.service';
import { UsuarioService } from '../../services/usuario.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {

  hide = true;
  show = true;
  form;
  rols:any;
  planesEst = [
    {value: 1, viewValue: 'Ingenieria de Sistemas'}
  ];

  user_registro = {
    nombres: "",
    apellidos: "",
    contrasena: "",
    correo: "",
    idPlanEstudio: 0,
    idRol: 0
  }

  constructor( private formBuilder: FormBuilder, 
               private autenticacionService: AutenticacionService,
               private notificacionService: NotificacionService,
               private route:Router,
               private rutaActiva: ActivatedRoute ) {
                  
    this.form = this.formBuilder.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required], 
      password2: ['', Validators.required], 
      rol: ['', Validators.required],
      planEst: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    this.autenticacionService.getUserRoles().subscribe(resp =>{
      this.rols = resp;
    })
  }

  registro() {
    if (this.form.valid) {

      var dominioEmail = this.form.value.email.split('@');      

      if(this.form.value.password === this.form.value.password2 && dominioEmail[1] === 'ufps.edu.co'){
        
        this.user_registro.nombres = this.form.value.firstname;
        this.user_registro.apellidos = this.form.value.lastname;
        this.user_registro.correo = this.form.value.email;
        this.user_registro.contrasena = this.form.value.password;
        this.user_registro.idRol = this.form.value.rol;
        this.user_registro.idPlanEstudio = this.form.value.planEst;

        console.log(this.user_registro)
 

        this.autenticacionService.postRegistro(this.user_registro).subscribe(resp => {  
          console.log(resp);                 
          this.notificacionService.mostrarNotificacion("Usuario registrado exitosamente", "success");    
        }, error => {
          console.log(error.status);
          console.log(error.error);
          this.notificacionService.mostrarNotificacion(error.error, "error");
        });
      }
      else {
        if(dominioEmail[1] !== 'ufps.edu.co'){
          this.notificacionService.mostrarNotificacion("Debes registrarte con el correo Institucional", "error");
        }else{
        this.notificacionService.mostrarNotificacion("Las contraseñas deben ser iguales", "error");
        }
      }      
    }
    else{
      // alert("Todos los campos son requeridos")
      this.notificacionService.mostrarNotificacion("Todos los campos son requeridos", "error");
    }
  }
  irAtras() {
    this.route.navigate(['/login']);
  }
}
