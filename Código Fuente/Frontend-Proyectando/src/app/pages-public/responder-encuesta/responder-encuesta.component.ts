import { Component, OnInit } from '@angular/core';
import { EncuestaService } from 'src/app/services/encuesta.service';

import { Router } from '@angular/router';

import Swal from 'sweetalert2';


@Component({
  selector: 'app-responder-encuesta',
  templateUrl: './responder-encuesta.component.html',
  styleUrls: ['./responder-encuesta.component.css']
})
export class ResponderEncuestaComponent implements OnInit {
  nombreEncuesta: string;
  respuestas: any = [];
  preguntas: any;
  encuesta: any;
  idEncuesta: any;
  encuestaModificada: any;
  preguntasM: any;
  respuestasM: any = [];

  constructor(private route:Router,
    private encuestaService: EncuestaService) {

    }

  ngOnInit(): void {

    this.encuesta = this.encuestaService.encuestaResponder;

    this.encuestaModificada = this.encuesta;
    this.preguntasM = this.encuestaModificada.preguntas;

    // this.encuesta = this.encuestaService.encuestaResponder;

    this.preguntas = this.encuesta.preguntas;
    this.nombreEncuesta = this.encuesta.nombre;
    this.idEncuesta = this.encuesta.id;
    this.extraerRespuesta();
  }

  extraerRespuesta() {
    console.log("preguntas ", this.preguntas);
    for (let index = 0; index < this.preguntas.length; index++) {

      this.respuestasM.push({
        idPregunta: this.preguntas[index].id,
        respuestas: ""
      });

      if (this.preguntas[index].tipoPregunta === "UNICA") {
        this.respuestas.push(JSON.parse(this.preguntas[index].opciones));
      } else {
        this.respuestas.push("");
      }
    }
  }

  respM(res: any, index: any) {
    this.respuestasM[index].respuestas = res;
  }

  enviarEncuesta() {
    console.log(this.respuestasM);
    let respuestaEncuesta: any = this.encuesta.respuestaEncuesta;
    respuestaEncuesta.respuestas = this.respuestasM;
    console.log('respuestaEncuesta', respuestaEncuesta);

    this.encuestaService.postResponderEncuesta(respuestaEncuesta).subscribe(resp => {
      console.log(resp);
      Swal.fire({
        icon: 'success',
        title: 'Encuestas',
        text: "Se ha enviado con exito tus respuestas",
        confirmButtonColor: '#F44336'
      })
      this.irAtras()
    }, error => {
      Swal.fire({
          icon: 'error',
          title: 'Error',
          text: error.error,
          confirmButtonColor: '#F44336'
        })
    })        
  }

  irAtras() {
    this.route.navigate(['/']);
  }
}
