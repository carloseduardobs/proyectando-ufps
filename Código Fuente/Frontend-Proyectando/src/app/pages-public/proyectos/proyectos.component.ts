import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LandingPageService } from '../../services/landing-page.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-proyectos',
  templateUrl: './proyectos.component.html',
  styleUrls: ['./proyectos.component.css']
})
export class ProyectosLandingComponent implements OnInit {

  params: any = "";
  content: any = "";
  listaProyectos: any = [];
  isLoading: boolean = true;
  responsiveOptions = [
    {
        breakpoint: '1024px',
        numVisible: 1,
        numScroll: 1
    },
    {
        breakpoint: '768px',
        numVisible: 1,
        numScroll: 1
    },
    {
        breakpoint: '560px',
        numVisible: 1,
        numScroll: 1
    }
];

evidenciaNoFound = [
  {
    archivo: 'https://proyectando-img.s3.us-east-2.amazonaws.com/image-not-found.png',
    nombre: 'Evidencias not Found',
    extension: 'png'
  }
]

  

  constructor(private route: Router,
    private rutaActiva: ActivatedRoute,
    private LandingPageService: LandingPageService) { }

  ngOnInit(): void {
    this.params = localStorage.getItem('ples');
    this.LandingPageService.getContent(this.params, 'proyectos').subscribe(resp => {
      this.content = resp;
    },
      error => {
        console.log(error);
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: error.error,
          confirmButtonColor: '#F44336'
        });
        this.route.navigate(['/login']);
        localStorage.clear();

      });
    this.LandingPageService.getProyectos(this.params).subscribe(resp => {
      this.listaProyectos = resp;
      this.isLoading = false;
    },
      error => {
        console.log(error);
        this.isLoading = false;
      });
  }

  verEvidencia(data: String) {
    if (data.indexOf('pdf')  >= 0){
      this.verConvenio(data)
    }
    let archivo: any = data;
    let pdfWindow: any = window.open("");
    pdfWindow.document.write("<iframe width='100%' height='100%' src='" + encodeURI(archivo) + "'></iframe>")
  }

  verConvenio(data: String) {
    let array = data.split(",");
    console.log('array', array[1]);
    let pdfWindow: any = window.open("");
    pdfWindow.document.write("<iframe width='100%' height='100%' src='data:application/pdf;base64, " + encodeURI(array[1]) + "'></iframe>")
  }

}
