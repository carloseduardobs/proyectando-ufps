import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { NotificacionService } from '../../services/notificacion.service';
import Swal from 'sweetalert2';
import { AutenticacionService } from '../../services/autenticacion.service';
import { EncuestaService } from 'src/app/services/encuesta.service';

@Component({
  selector: 'app-login-encuesta',
  templateUrl: './login-encuesta.component.html',
  styleUrls: ['./login-encuesta.component.css']
})
export class LoginEncuestaComponent implements OnInit {

  hide = true;
  form;
  encuesta_response:any;

  encuesta_login = {
    contrasena: "",
    correo: ""
  }

  constructor(private formBuilder: FormBuilder,
              private encuestaService:EncuestaService,
              private router:Router,
              private notificacionService: NotificacionService)
  {
    this.form = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

  ngOnInit(): void {
  }

  submit() {
    if (this.form.valid) {
      console.log(this.form.value)
      this.encuesta_login.correo = this.form.value.email;
      this.encuesta_login.contrasena = this.form.value.password;
      
      this.encuestaService.postValidarEncuesta(this.encuesta_login).subscribe(resp => {
        this.encuesta_response = resp;
        // console.log(resp);
        // localStorage.setItem('token', this.login_response.accessToken);
        // localStorage.setItem('userId', this.login_response.id);
        // localStorage.setItem('rol', this.login_response.roles[0]);
        localStorage.setItem('email', this.encuesta_login.correo);
        // localStorage.setItem('name', this.login_response.name);
        // localStorage.setItem('planEst', this.login_response.idPlanEstudios);
        this.encuestaService.encuestaResponder = this.encuesta_response;

        this.router.navigate(['/responder-encuesta']);

      }, error => {
        console.log(error.status);
        console.log(error.error);
        this.notificacionService.mostrarNotificacion(error.error, "error");
      });
    }
    else{
      // alert("Todos los campos son requeridos")
      this.notificacionService.mostrarNotificacion("Todos los campos son requeridos", "error");
    }
  }


  

}
