import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginEncuestaComponent } from './login-encuesta.component';

describe('LoginEncuestaComponent', () => {
  let component: LoginEncuestaComponent;
  let fixture: ComponentFixture<LoginEncuestaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoginEncuestaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginEncuestaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
