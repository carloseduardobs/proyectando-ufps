import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LandingPageService } from '../../services/landing-page.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-politicas',
  templateUrl: './politicas.component.html',
  styleUrls: ['./politicas.component.css']
})
export class PoliticasComponent implements OnInit {

  params: any = "";
  content: any = "";

  constructor(private route:Router,
    private rutaActiva: ActivatedRoute,
    private LandingPageService: LandingPageService) { }

  ngOnInit(): void {
    this.params = localStorage.getItem('ples');
    this.LandingPageService.getContent(this.params, 'politicas').subscribe(resp => {
      console.log(resp);
      this.content = resp;
    },
    error => {
      console.log(error);
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: error.error,
        confirmButtonColor: '#F44336'
      });
      this.route.navigate(['/login']);
      localStorage.clear();
      
    });
  }

}
