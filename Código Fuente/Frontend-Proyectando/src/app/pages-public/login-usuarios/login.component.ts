import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, Validators} from '@angular/forms';

import { Router } from '@angular/router';
import { NotificacionService } from '../../services/notificacion.service';
import Swal from 'sweetalert2';
import { AutenticacionService } from '../../services/autenticacion.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  hide = true; 
  form;
  login_response:any;

  user_login = {
    contrasena: "",
    contraseña: "",
    correo: ""
  }
      
  constructor(private formBuilder: FormBuilder, 
              private autenticacionService:AutenticacionService, 
              private router:Router, 
              private notificacionService: NotificacionService) 
  { 
    this.form = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    localStorage.clear()
  }

  submit() {
    if (this.form.valid) {
      console.log(this.form.value)
      this.user_login.correo = this.form.value.email;
      this.user_login.contrasena = this.form.value.password;
      this.user_login.contraseña = this.form.value.password;
      this.autenticacionService.postLogin(this.user_login).subscribe(resp => {
        this.login_response = resp;
        console.log(resp);
        localStorage.setItem('token', this.login_response.accessToken);
        localStorage.setItem('userId', this.login_response.id);
        localStorage.setItem('rol', this.login_response.roles[0]);
        localStorage.setItem('email', this.login_response.email);
        localStorage.setItem('name', this.login_response.name);
        localStorage.setItem('planEst', this.login_response.idPlanEstudios);


        this.router.navigate(['/dashboard']);
  
      }, error => {
        console.log(error.status);
        console.log(error.error);
        this.notificacionService.mostrarNotificacion(error.error, "error");
      });
    }
    else{
      // alert("Todos los campos son requeridos")
      this.notificacionService.mostrarNotificacion("Todos los campos son requeridos", "error");
    }
  }

  validarContrasena(){

  }

  async recordarContrasena(){
    const { value: email } = await Swal.fire({
      title: 'Se enviará una contraseña temporal a tu  correo electronico',
      input: 'email',
      inputLabel: 'Ingresar dirección de correo electronico',
      inputPlaceholder: 'Enter your email address',
      confirmButtonColor: '#F44336'
    })
    
    if (email) {
      this.autenticacionService.getValidarEmail(email).subscribe(resp => {
        console.log(resp);
        Swal.fire({
              icon: 'success',
              title: `Se ha enviado una contraseña temporal al correo:`,
              text:  email,
              confirmButtonColor: '#F44336'
            });
      },
      error => {
        console.log(error);
        
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: error.error,
            confirmButtonColor: '#F44336'
          });
        
        
      });
      
    }
  }

}
