import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// MODULOS ANGULAR MATERIAL
import { MatSliderModule } from '@angular/material/slider';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule} from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatInputModule } from '@angular/material/input';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { MatTableModule } from '@angular/material/table';
import { MatListModule } from '@angular/material/list';
import { MatCardModule } from '@angular/material/card';
import { MatSelectModule } from '@angular/material/select';
import { MatMenuModule } from '@angular/material/menu';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatNativeDateModule } from '@angular/material/core';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatTabsModule} from '@angular/material/tabs';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatExpansionModule} from '@angular/material/expansion';

import {CarouselModule} from 'primeng/carousel';
import {GalleriaModule} from 'primeng/galleria';
import { MatTableExporterModule } from 'mat-table-exporter';



import { LoginComponent } from './pages-public/login-usuarios/login.component';
import { DashboardComponent } from './pages-internal/dashboard/dashboard.component';
import { FooterComponent } from './reusable-components/footer/footer.component';
import { HeaderComponent } from './reusable-components/header/header.component';
import { PaginaInicioComponent } from './pages-public/pagina-inicio/pagina-inicio.component';
import { CrearProyectoComponent } from './pages-internal/crear-proyecto/crear-proyecto.component';
import { ConsultarProyectosComponent } from './pages-internal/consultar-proyectos/consultar-proyectos.component';
import { ConsultarActividadesComponent } from './pages-internal/consultar-actividades/consultar-actividades.component';
import { CrearActividadComponent } from './pages-internal/crear-actividad/crear-actividad.component';
import { ConsultarUsuariosComponent } from './pages-internal/consultar-usuarios/consultar-usuarios.component';
import { RegisterComponent } from './pages-public/register-usuarios/register.component';
import { SidenavComponent } from './reusable-components/sidenav/sidenav.component';
import { ProyectoComponent } from './pages-internal/proyecto/proyecto.component';
import { ConsultarConveniosComponent } from './pages-internal/consultar-convenios/consultar-convenios.component';
import { PerfilUsuarioComponent } from './pages-internal/perfil-usuario/perfil-usuario.component';
import { CambioContrasenaComponent } from './pages-internal/cambio-contrasena/cambio-contrasena.component';
import { PoliticasComponent } from './pages-public/politicas/politicas.component';
import { ContactoComponent } from './pages-public/contacto/contacto.component';
import { PrincipiosComponent } from './pages-public/principios/principios.component';
import { CrearEncuestaComponent } from './pages-internal/crear-encuesta/crear-encuesta.component';
import { InicioDashboardComponent } from './pages-internal/inicio-dashboard/inicio-dashboard.component';
import { InicioComponent } from './pages-public/inicio/inicio.component';
import { PracticasComponent } from './pages-public/practicas/practicas.component';
import { ProyectosLandingComponent } from './pages-public/proyectos/proyectos.component';
import { EventosComponent } from './pages-public/eventos/eventos.component';
import { ProyeccionCifrasComponent } from './pages-public/proyeccion-cifras/proyeccion-cifras.component';
import { CrearConvenioComponent } from './pages-internal/crear-convenio/crear-convenio.component';
import { ActividadComponent } from './pages-internal/actividad/actividad.component';
import { ConsultarEncuentasComponent } from './pages-internal/consultar-encuentas/consultar-encuentas.component';
import { EncuestaComponent } from './pages-internal/encuesta/encuesta.component';
import { EvidenciaActividadComponent } from './pages-internal/evidencia-actividad/evidencia-actividad.component';
import { EvidenciaProyectoComponent } from './pages-internal/evidencia-proyecto/evidencia-proyecto.component';
import { LoginEncuestaComponent } from './pages-public/login-encuesta/login-encuesta.component';
import { ResponderEncuestaComponent } from './pages-public/responder-encuesta/responder-encuesta.component';
import { EnviarEncuestaComponent } from './pages-internal/enviar-encuesta/enviar-encuesta.component';
import { UsuarioComponent } from './pages-internal/usuario/usuario.component';
import { ConvenioComponent } from './pages-internal/convenio/convenio.component';
import { LandingConfigComponent } from './pages-internal/landing-config/landing-config.component';
import { QuillModule } from 'ngx-quill';
import {MatRadioModule} from '@angular/material/radio';
import { RespuestasEncuestasComponent } from './pages-internal/respuestas-encuestas/respuestas-encuestas.component';
import { RespuestaEncuestaComponent } from './pages-internal/respuesta-encuesta/respuesta-encuesta.component';
import { InformesComponent } from './pages-internal/informes/informes.component';
import { InformesProyectosComponent } from './pages-internal/informes-proyectos/informes-proyectos.component';
import { InformesActividadesComponent } from './pages-internal/informes-actividades/informes-actividades.component';
import { InformesConveniosComponent } from './pages-internal/informes-convenios/informes-convenios.component';
import { InformesUsuariosComponent } from './pages-internal/informes-usuarios/informes-usuarios.component';
import { InformesEncuestasComponent } from './pages-internal/informes-encuestas/informes-encuestas.component';


@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    PaginaInicioComponent,
    LoginComponent,
    DashboardComponent,
    CrearProyectoComponent,
    ConsultarProyectosComponent,
    ConsultarActividadesComponent,
    CrearActividadComponent,
    ConsultarUsuariosComponent,
    RegisterComponent,
    SidenavComponent,
    ProyectoComponent,
    ConsultarConveniosComponent,
    PerfilUsuarioComponent,
    CambioContrasenaComponent,
    PoliticasComponent,
    ContactoComponent,
    PrincipiosComponent,
    CrearEncuestaComponent,
    InicioDashboardComponent,
    InicioComponent,
    PracticasComponent,
    ProyectosLandingComponent,
    EventosComponent,
    ProyeccionCifrasComponent,
    CrearConvenioComponent,
    ActividadComponent,
    ConsultarEncuentasComponent,
    EncuestaComponent,
    EvidenciaActividadComponent,
    EvidenciaProyectoComponent,
    LoginEncuestaComponent,
    ResponderEncuestaComponent,
    EnviarEncuestaComponent,
    UsuarioComponent,
    ConvenioComponent,
    LandingConfigComponent,
    RespuestasEncuestasComponent,
    RespuestaEncuestaComponent,
    InformesComponent,
    InformesProyectosComponent,
    InformesActividadesComponent,
    InformesConveniosComponent,
    InformesUsuariosComponent,
    InformesEncuestasComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    FormsModule, 
    ReactiveFormsModule,
    MatSliderModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    MatInputModule,
    MatTableModule,
    MatListModule,
    MatCardModule,
    MatSelectModule,
    MatMenuModule,
    MatDatepickerModule,
    MatSnackBarModule,
    MatNativeDateModule,
    MatSlideToggleModule,
    MatPaginatorModule,
    MatSortModule,
    MatDialogModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    MatCheckboxModule,
    MatTabsModule,
    MatFormFieldModule,
    MatExpansionModule,
    CarouselModule,
    MatRadioModule,
    GalleriaModule,
    MatTableExporterModule,
    QuillModule.forRoot({
      customOptions: [{
        import: 'formats/font',
        whitelist: ['mirza', 'roboto', 'aref', 'serif', 'sansserif', 'monospace']
      }]
})
  ],
  entryComponents: [],
  providers: [
    { provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: { appearance: 'fill' } },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
