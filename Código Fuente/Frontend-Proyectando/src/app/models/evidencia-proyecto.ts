﻿import { EvidenciaI } from './evidencia';

export interface EvidenciaProyectoI {
    
    aprobar?: string;
    divulgar?: string;
    evidencias?: EvidenciaI [];
    fechaRealizacion?: string;
    id?: number;
    idProyecto?: number;
    nombre?: string;
    registradoPor?: number
}