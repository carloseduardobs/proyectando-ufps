﻿export interface TipoProyectoI {
    descripcion: string;
    id: number;
    nombre: string;
}