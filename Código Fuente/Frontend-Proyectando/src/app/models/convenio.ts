﻿export interface ConvenioI {
    divulgar: string;
    evidencia: string;
    extEvidencia: string;
    fechaRealizacion: string;
    id?: number;
    idProyecto: number;
    nombre: string;
    nombreEvidencia: string;
    registradoPor: number;
    representante: string;
    aprobar: string;
}