﻿export interface ActividadI {    
    aprobar: string;
    beneficiario: string;
    ciudad: string;
    descripcion: string;
    divulgar: string;
    fechaRealizacion: string;
    id?: number;
    idProyecto: number;
    nombre: string;
    pais: string;
    registradoPor: number; 
    tipo: string;    
}