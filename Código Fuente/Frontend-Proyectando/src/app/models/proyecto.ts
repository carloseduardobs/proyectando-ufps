﻿export interface ProyectoI {    
    aprobar: string;
    beneficiario: string;
    descripcion: string;
    divulgar: string;
    fechaFin: string;
    fechaInicio: string;
    id?: number;
    idPlanEstudios: number;
    idProcesoMisional: number;
    idTipoProyecto: number;
    monto: string;
    nombre: string;
    registradoPor?: number;      
}