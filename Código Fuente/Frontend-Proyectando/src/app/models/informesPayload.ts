export interface InformesPayloadI {
    tipoInforme: string,
    fechaInicio: string,
    fechaFin: string,
    aprobados: string,
    encuesta: string
}