﻿export interface EvidenciaI {
    archivo: string;
            extension: string;
            id?: number;
            idEvidenciaActividad?: number;
            idEvidenciaProyecto?: number;
            nombre: string;
}