import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ProyectoI } from '../../models/proyecto';
import { ProyectoService } from '../../services/proyecto.service';
import { TipoProyectoI } from '../../models/tipoProyecto';
import { NotificacionService } from 'src/app/services/notificacion.service';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-crear-proyecto',
  templateUrl: './crear-proyecto.component.html',
  styleUrls: ['./crear-proyecto.component.css']
})
export class CrearProyectoComponent implements OnInit {

  userID:any;
  proyecto: ProyectoI = {
    nombre:"",
    aprobar: "",
    beneficiario: "",
    descripcion: "",
    divulgar: "",
    fechaFin: "",
    fechaInicio: "",
    idPlanEstudios: 0,
    idProcesoMisional: 0,
    idTipoProyecto: 0,
    monto: "",
    registradoPor: 0
  };
  form;
  tiposProyecto:any;
  procesosMisionales:any;
  isLoading = false;

  constructor(private formBuilder: FormBuilder, 
              public dialog:MatDialog, 
              private proyectoService:ProyectoService,
              private notificacionService:NotificacionService,
              private route:Router,
              private rutaActiva: ActivatedRoute) { 

    this.form = this.formBuilder.group({
      nombre: ['', Validators.required],
      descripcion: ['', Validators.required],
      procesoMisional: ['', Validators.required],
      tipoProyecto: ['', Validators.required], 
      fechaInicio: ['', Validators.required], 
      fechaFin: ['', Validators.required],
      monto: ['', Validators.required],
      beneficiarios: ['', Validators.required],
      aprobarProyecto: ['NO'],
      mostrarProyecto: ['NO']
    });
  }

  ngOnInit(): void {   

    this.userID = localStorage.getItem("userId")
    this.proyectoService.getProcesosMisionales().subscribe(resp => {
      this.procesosMisionales = resp;
      console.log('procesos misionales' , this.procesosMisionales);
    }, error => {
      console.log(error.error);

    });

    this.proyectoService.getTipoProyecto().subscribe(resp => {
      this.tiposProyecto=resp;      
    }, error => {
      console.log(error.error);

    });
  }

 

  crearProyecto(){
    if(this.form.valid){
      this.isLoading = true;
      this.proyecto.nombre = this.form.value.nombre;    
      this.proyecto.idProcesoMisional = this.form.value.procesoMisional;
      this.proyecto.idTipoProyecto = this.form.value.tipoProyecto;
      this.proyecto.fechaInicio = this.form.value.fechaInicio;
      this.proyecto.fechaFin = this.form.value.fechaFin;
      this.proyecto.monto = this.form.value.monto;
      this.proyecto.beneficiario = this.form.value.beneficiarios;
      this.proyecto.aprobar = this.form.value.aprobarProyecto;
      this.proyecto.divulgar = this.form.value.mostrarProyecto;
      this.proyecto.descripcion = this.form.value.descripcion;
      this.proyecto.idPlanEstudios = 1;
      this.proyecto.registradoPor = parseInt(this.userID, 10);
  
      this.proyectoService.postCrearProyecto(this.proyecto).subscribe(resp => {
        console.log(resp);
        Swal.fire({
          icon: 'success',
          title: 'Proyectos',
          text: "Se ha creado el proyecto con exito",
          confirmButtonColor: '#F44336'
        })
        this.isLoading = false;
      }, error => {
        console.log(error.error);
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: error.error,
          confirmButtonColor: '#F44336'
        })
        this.isLoading = false;
      })
      console.log(this.proyecto);
    }else{
      this.notificacionService.mostrarNotificacion("Faltan Campos Obligatorios por diligenciar", "error");
    }
    
  }

  irAtras() {
    this.route.navigate(['/dashboard/proyectos']);
  }

}
