import { AfterViewInit, Component, ViewChild, OnInit } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { Router, RouterLink } from '@angular/router';
import { UserI } from '../../models/user';
import { UsuarioService } from '../../services/usuario.service';



@Component({
  selector: 'app-consultar-usuarios',
  styleUrls: ['./consultar-usuarios.component.css'],
  templateUrl: './consultar-usuarios.component.html',  
  
})

export class ConsultarUsuariosComponent implements OnInit, AfterViewInit{

  columnas: string[] = ['Nombres', 'Apellidos', 'Correo', 'Rol', 'Estado'];
  columnsToDisplay: string[] = ['nombres', 'apellidos', 'correo', 'idRol', 'estado'];
  data_usuarios:any = [];
  dataSource = new MatTableDataSource();
  expandedElement: UserI | null;

  @ViewChild(MatPaginator, {static:true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static:true} ) sort: MatSort;

  constructor( private usuarioService:UsuarioService, private route:Router ){}

  ngOnInit(): void {    
    
    this.usuarioService.getUsuarios().subscribe(resp => {
      console.log(resp);
      this.data_usuarios = resp;      
      this.dataSource.data = this.data_usuarios;
    }, error => {
      if(error.status === 401){
        this.route.navigate(['/login']);
        localStorage.clear();
      }
    });    
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  verUsuario(usuario:UserI){
    this.usuarioService.usuarioSelect = usuario;
    this.route.navigate(['/dashboard/usuario']);    
  }

}

