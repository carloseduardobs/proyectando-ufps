import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UsuarioService } from '../../services/usuario.service';
import { NotificacionService } from 'src/app/services/notificacion.service';
import Swal from 'sweetalert2';
import { EncuestaService } from 'src/app/services/encuesta.service';
import { ProyectoService } from '../../services/proyecto.service';

@Component({
  selector: 'app-enviar-encuesta',
  templateUrl: './enviar-encuesta.component.html',
  styleUrls: ['./enviar-encuesta.component.css']
})
export class EnviarEncuestaComponent implements OnInit {

  form;
  encuestas:any;
  envioEncuesta = {
    correo: "",
    idEncuesta: 0,
    idProyecto: 0,
    urlResponse: ""
  }
  fechaFin:string;
  fechaActual = new Date().toISOString();
  idProyecto:any;

  constructor(private formBuilder: FormBuilder,
    private encuestaService: EncuestaService, 
    private proyectoService:ProyectoService,
    private notificacionService: NotificacionService,
    public dialog: MatDialogRef<EnviarEncuestaComponent>,
    @Inject(MAT_DIALOG_DATA) public userId: string) {

    this.form = this.formBuilder.group({
      correo: ['', Validators.required],
      encuesta: ['', Validators.required,]

    });

  }

  ngOnInit(): void {
    this.fechaFin = this.proyectoService.proyectoSelect.fechaFin;
    this.idProyecto = this.proyectoService.proyectoSelect.id;

    this.encuestaService.getEncuestasActivas().subscribe(resp => {
      console.log(resp);
      this.encuestas = resp;
    })
  }

  enviarEncuesta() {
    if (this.form.valid) {
      let aux1, aux2;
      aux1 = this.fechaFin.split("T");
      aux2 = this.fechaActual.split("T");

      if(aux2[0] >= aux1[0]) {
        this.envioEncuesta.correo = this.form.value.correo;
        this.envioEncuesta.idEncuesta = this.form.value.encuesta;
        this.envioEncuesta.idProyecto = this.idProyecto;
        let url = window.location.origin
        this.envioEncuesta.urlResponse = url;
        this.encuestaService.postEnviarEncuesta(this.envioEncuesta).subscribe(resp => {
          console.log(resp);
          Swal.fire({
            icon: 'success',
            title: 'Enviar Encuesta',
            text: "Se ha enviado la encuesta con exito",
            confirmButtonColor: '#F44336'
          })
        }, error => {
          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: error.error,
            confirmButtonColor: '#F44336'
          })
        })

      }else{
        this.notificacionService.mostrarNotificacion("La encuesta solo puede ser enviada despues de finalizar el proyecto", "error");
      }      
    } else {
      this.notificacionService.mostrarNotificacion("Tienes que diligenciar todos los campos", "error");
    }

  }

}


