import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EnviarEncuestaComponent } from './enviar-encuesta.component';

describe('EnviarEncuestaComponent', () => {
  let component: EnviarEncuestaComponent;
  let fixture: ComponentFixture<EnviarEncuestaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EnviarEncuestaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EnviarEncuestaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
