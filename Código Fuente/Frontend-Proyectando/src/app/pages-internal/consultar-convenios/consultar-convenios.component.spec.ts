import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultarConveniosComponent } from './consultar-convenios.component';

describe('ConsultarConveniosComponent', () => {
  let component: ConsultarConveniosComponent;
  let fixture: ComponentFixture<ConsultarConveniosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConsultarConveniosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultarConveniosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
