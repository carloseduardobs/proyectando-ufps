import { AfterViewInit, Component, ViewChild, OnInit } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { Router } from '@angular/router';
import { ConveniosService } from 'src/app/services/convenios.service';
import { ConvenioI } from '../../models/convenio';

@Component({
  selector: 'app-consultar-convenios',
  templateUrl: './consultar-convenios.component.html',
  styleUrls: ['./consultar-convenios.component.css']
})
export class ConsultarConveniosComponent implements OnInit, AfterViewInit {

  columnas: string[] = ['Nombre', 'Aprobado', 'Fecha Realizacion', 'Representante'];
  columnsToDisplay: string[] = ['nombre', 'aprobar', 'fechaRealizacion', 'representante'];
  data_convenios:any = [];
  dataSource = new MatTableDataSource();
  load:boolean;
  btnDisabled = false;

  @ViewChild(MatPaginator, {static:true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static:true} ) sort: MatSort;

  constructor( private conveniosService:ConveniosService, private route:Router ){
    this.load = false;
  }

  ngOnInit(): void {  
    
    if (localStorage.getItem("rol") === "super usuario") {
      this.btnDisabled = true;
    }
    
    this.conveniosService.getConsultarConvenios().subscribe(resp => {
      console.log(resp);
      this.load = true;
      this.data_convenios = resp;      
      this.dataSource.data = this.data_convenios;
    }, error => {
      this.load = true;
      if(error.status === 401){
        this.route.navigate(['/login']);
        localStorage.clear();
      }
    });      
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }


  verConvenio(convenio:ConvenioI){
    this.conveniosService.convenioSelect = convenio;
    this.route.navigate(['/dashboard/convenio']);
  }

  typeOf(elem:any):any{
    let array=[]
    array = elem.split(/T\d/)
    return array[0];
  }

}
