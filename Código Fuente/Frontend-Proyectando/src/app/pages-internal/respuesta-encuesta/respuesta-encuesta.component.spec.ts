import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RespuestaEncuestaComponent } from './respuesta-encuesta.component';

describe('RespuestaEncuestaComponent', () => {
  let component: RespuestaEncuestaComponent;
  let fixture: ComponentFixture<RespuestaEncuestaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RespuestaEncuestaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RespuestaEncuestaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
