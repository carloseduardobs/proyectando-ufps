import { Component, OnInit } from '@angular/core';
import { EncuestaService } from 'src/app/services/encuesta.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-respuesta-encuesta',
  templateUrl: './respuesta-encuesta.component.html',
  styleUrls: ['./respuesta-encuesta.component.css']
})
export class RespuestaEncuestaComponent implements OnInit {

  nombreEncuesta: string;
  respuestas: any = [];
  preguntas: any;
  encuesta: any;
  getencuesta: any;
  idEncuesta: any;
  encuestaModificada: any;
  preguntasM: any;
  respuestasM: any = [];

  load:boolean;

  constructor(private route: Router,
    private encuestaService: EncuestaService) {
      this.load = true
  }

  ngOnInit(): void {

    this.encuesta = this.encuestaService.respuestaSelect;

    this.encuestaModificada = this.encuesta;
    this.preguntasM = this.encuestaModificada.preguntas;

    // this.encuesta = this.encuestaService.encuestaResponder;


    this.nombreEncuesta = this.encuesta.nombre;
    this.idEncuesta = this.encuesta.id;

    this.extraerRespuesta();
  }



  async extraerRespuesta() {
    this.preguntas = await this.getRespuestas();
    console.log("preguntas ", this.preguntas);
    for (let index = 0; index < this.preguntas.length; index++) {

      this.respuestasM.push({
        idPregunta: this.preguntas[index].id,
        respuestas: ""
      });

      if (this.preguntas[index].tipoPregunta === "UNICA") {
        this.respuestas.push(JSON.parse(this.preguntas[index].opciones));
      } else {
        this.respuestas.push("");
      }
    }
  }

  irAtras() {
    this.route.navigate(['/dashboard/consultar-respuestas']);
  }

  respM(res: any, index: any) {
    this.respuestasM[index].respuestas = res;
  }

  enviarEncuesta() {
    console.log(this.respuestasM);
    let respuestaEncuesta: any = this.encuesta.respuestaEncuesta;
    respuestaEncuesta.respuestas = this.respuestasM;
    console.log('respuestaEncuesta', respuestaEncuesta);

    this.encuestaService.postResponderEncuesta(respuestaEncuesta).subscribe(resp => {
      console.log(resp);
      Swal.fire({
        icon: 'success',
        title: 'Encuestas',
        text: "Se ha enviado con exito tus respuestas",
        confirmButtonColor: '#F44336'
      })
      this.irAtras()
    }, error => {
      Swal.fire({
        icon: 'error',
        title: 'Error',
        text: error.error,
        confirmButtonColor: '#F44336'
      })
    })
  }

  
    
    
    private getRespuestas() {
      return new Promise((resolve, reject) => {
        let preguntas:any;
        this.encuestaService.getRespuestas(this.encuesta.id).subscribe(resp =>{
          preguntas = resp;
          this.load = false;
          resolve(preguntas);
        },error =>{
          reject("Error");
        })        
      });
    }

}
