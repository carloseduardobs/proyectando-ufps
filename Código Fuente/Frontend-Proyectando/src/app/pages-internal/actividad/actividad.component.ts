import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ProyectoService } from 'src/app/services/proyecto.service';
import Swal from 'sweetalert2';
import { ActividadI } from '../../models/actividad';
import { ActividadService } from '../../services/actividad.service';
import { EvidenciaActividadComponent } from '../evidencia-actividad/evidencia-actividad.component';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificacionService } from 'src/app/services/notificacion.service';

@Component({
  selector: 'app-actividad',
  templateUrl: './actividad.component.html',
  styleUrls: ['./actividad.component.css']
})
export class ActividadComponent implements OnInit {

  userRol = localStorage.getItem('rol');
  form;
  actividad: ActividadI;
  proyectos: any;
  listaTipos = ['Actividad', 'Evento'];
  isLoading = false;

  constructor(private formBuilder: FormBuilder,
              public dialog: MatDialog, 
              private proyectoService: ProyectoService,
              private actividadService:ActividadService,
              private notificacionService:NotificacionService,
              private route:Router,
              private rutaActiva: ActivatedRoute) {

    this.form = this.formBuilder.group({
      beneficiario: ['', Validators.required],
      ciudad: ['', Validators.required],
      descripcion: ['', Validators.required],
      pais: ['', Validators.required],
      fechaRealizacion: ['', Validators.required],
      proyecto: ['', Validators.required],
      nombre: ['', Validators.required],
      divulgar: ['NO'],
      aprobar: ['NO'],
      tipo: ['Actividad']
    });
  }

  ngOnInit(): void {    
    console.log('rol ->',this.userRol)
    this.proyectoService.getProyectos().subscribe(resp => {
      this.proyectos = resp;
    }, error => {
      console.log(error.error);
    });

    this.actividad = this.actividadService.actividadSelect;
    this.iniciarValoresForm();
  }

  private iniciarValoresForm() {
    this.form.patchValue({

      aprobar: this.actividad.aprobar,
      beneficiario: this.actividad.beneficiario,
      ciudad: this.actividad.ciudad,
      descripcion: this.actividad.descripcion,
      divulgar: this.actividad.divulgar,
      fechaRealizacion: this.actividad.fechaRealizacion,
      proyecto: this.actividad.idProyecto,
      nombre: this.actividad.nombre,
      pais: this.actividad.pais,
      tipo: this.actividad.tipo

    })
  }

  guardarActividad() {
    if (this.form.valid) {
      this.isLoading = true;
      this.actividad.aprobar = this.form.value.aprobar;
      this.actividad.beneficiario = this.form.value.beneficiario;
      this.actividad.ciudad = this.form.value.ciudad;
      this.actividad.descripcion = this.form.value.descripcion;
      this.actividad.divulgar = this.form.value.divulgar;
      this.actividad.fechaRealizacion = this.form.value.fechaRealizacion;
      this.actividad.idProyecto = this.form.value.proyecto;
      this.actividad.nombre = this.form.value.nombre;
      this.actividad.pais = this.form.value.pais;
      this.actividad.tipo = this.form.value.tipo;

      console.log(this.actividad);
      this.actividadService.putModificarActividad(this.actividad).subscribe(resp => {
        console.log("Modificado", resp);
        Swal.fire({
          icon: 'success',
          title: 'Proyecto',
          text: "Se ha guardado la actividad con exito",
          confirmButtonColor: '#F44336'
        })
        this.isLoading = false
      }, error => {
        console.log(error.error);
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: error.error,
          confirmButtonColor: '#F44336'
        })
        this.isLoading = false;
      })
    }else{
      this.notificacionService.mostrarNotificacion("Faltan Campos Obligatorios por diligenciar", "error");
    }
  }

  adjuntarEvidencia() {
    this.openDialog();
  }

  openDialog(): void {
    this.actividadService.actividadSelect = this.actividad;
    const dialogRef = this.dialog.open(EvidenciaActividadComponent);
    dialogRef.afterClosed().subscribe(resp => {
      console.log(`Dialog result ${resp}`);
    })
  }

  irAtras() {
    this.route.navigate(['/dashboard/actividades']);
  }

}
