import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InformesConveniosComponent } from './informes-convenios.component';

describe('InformesConveniosComponent', () => {
  let component: InformesConveniosComponent;
  let fixture: ComponentFixture<InformesConveniosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InformesConveniosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InformesConveniosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
