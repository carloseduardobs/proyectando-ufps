import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { EncuestaService } from 'src/app/services/encuesta.service';

@Component({
  selector: 'app-respuestas-encuestas',
  templateUrl: './respuestas-encuestas.component.html',
  styleUrls: ['./respuestas-encuestas.component.css']
})
export class RespuestasEncuestasComponent implements OnInit {

  columnas: string[] = ['Nombre Proyecto','Correo', 'Estado', 'Fecha Realizacion'];
  columnsToDisplay: string[] = ['nombreProyecto','correo', 'estado', 'fechaEnvio'];
  data_encuesta:any = [];
  dataSource = new MatTableDataSource();
  load:boolean;
  encuesta:any;

  @ViewChild(MatPaginator, {static:true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static:true} ) sort: MatSort;

  constructor( private encuestaService:EncuestaService, private route:Router ){
    this.load = false
  }

  ngOnInit(): void {
    this.encuesta = this.encuestaService.encuestaSelect
    this.encuestaService.getRespuestasEncuestas(this.encuesta.id).subscribe(resp => {
      console.log(resp);
      this.load = true;
      this.data_encuesta = resp;
      this.dataSource.data = this.data_encuesta;
    }, error => {
      this.load = true;
      if (error.status === 401) {
        this.route.navigate(['/login']);
        localStorage.clear();
      }
    });

  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }


  verRespuesta(respuesta: any) {    
    this.encuestaService.respuestaSelect = respuesta;    
    this.route.navigate(['/dashboard/consultar-respuestas-encuesta']);
  }

  typeOf(elem: any): any {
    let array = []
    array = elem.split(/T\d/)
    return array[0];
  }

}
