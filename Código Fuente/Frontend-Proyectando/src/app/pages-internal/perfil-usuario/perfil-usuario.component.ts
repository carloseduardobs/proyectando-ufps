import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormBuilder } from '@angular/forms';
import { NotificacionService } from 'src/app/services/notificacion.service';
import { MatDialog } from '@angular/material/dialog';
import { UsuarioService } from '../../services/usuario.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { CambioContrasenaComponent } from '../cambio-contrasena/cambio-contrasena.component';
import { UserI } from '../../models/user';


@Component({
  selector: 'app-perfil-usuario',
  templateUrl: './perfil-usuario.component.html',
  styleUrls: ['./perfil-usuario.component.css']
})
export class PerfilUsuarioComponent implements OnInit {

  userRol = localStorage.getItem('rol');
  userId:any;
  form;
  infoUser:any;
  load:boolean;
  listEstados = [
    {value: 'ACTIVO', viewValue: 'ACTIVO'},
    {value: 'INACTIVO', viewValue: 'INACTIVO'},
  ];
  usuario:UserI;
  isLoading = false;

  constructor(private route:Router, private formBuilder: FormBuilder, public dialog:MatDialog, private usuarioService:UsuarioService,
    private notificacionService:NotificacionService) { 
    this.form = this.formBuilder.group({
      apellidos: ['', Validators.required],
      correo: ['', Validators.required,],
      estado: ['', Validators.required],
      nombres: ['', Validators.required]
    });
    this.load = false;
  }

  ngOnInit(): void {
    this.iniciarValoresForm();   

  }

  private iniciarValoresForm(){
    this.usuarioService.getUsuarioConsultar().subscribe(resp => {
      this.load = true;
      this.infoUser = resp
      this.form.patchValue({
        nombres: this.infoUser.nombres,
        apellidos: this.infoUser.apellidos,
        correo: this.infoUser.correo,
        estado: this.infoUser.estado      
      })
      this.usuario = {
        nombres: this.infoUser.nombres,
        apellidos: this.infoUser.apellidos,
        correo: this.infoUser.correo,
        estado: this.infoUser.estado,
        id: this.infoUser.id,
        idPlanEstudio: this.infoUser.idPlanEstudio,
        idRol: this.infoUser.idRol
      }
      this.userId = this.usuario.id

    }, error => {
      if(error.status === 401){
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: "Error: Su sesion expiro",
          confirmButtonColor: '#F44336'
        }).then(() => {
          this.route.navigate(['/login']);
          localStorage.clear();
        });

        
      }
      
    })
  }

  cambioContrasena(){
    this.openDialog(this.userId);
  }

  openDialog(userId:any):void{

    const dialogRef = this.dialog.open(CambioContrasenaComponent, userId);
    dialogRef.afterClosed().subscribe( resp => {
      console.log(`Dialog result ${resp}`);
    })
  }

  irAtras() {
    this.route.navigate(['/dashboard/inicio']);
  }

  actualizarUser() {
    if (this.form.valid) {
        this.isLoading = true;
        this.usuario.nombres = this.form.value.nombres;
        this.usuario.apellidos = this.form.value.apellidos;
        this.usuario.correo = this.form.value.correo;
        this.usuario.estado = this.form.value.estado;

        console.log(this.usuario)
        this.usuarioService.putModificarUsuario(this.usuario).subscribe(resp => {  
          console.log("Modificado", resp);
          Swal.fire({
          icon: 'success',
          title: 'Proyecto',
          text: "Se ha guardado el usuario con exito",
          confirmButtonColor: '#F44336'
          }) 
          this.isLoading = false;
        }, error => {
          console.log(error.error);
          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: error.error,
            confirmButtonColor: '#F44336'
          })
          this.isLoading = false;
        })
      }      
    
  }

}
