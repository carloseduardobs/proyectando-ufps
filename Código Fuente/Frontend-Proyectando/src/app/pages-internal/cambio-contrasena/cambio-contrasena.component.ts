import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UsuarioService } from '../../services/usuario.service';
import { NotificacionService } from 'src/app/services/notificacion.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-cambio-contrasena',
  templateUrl: './cambio-contrasena.component.html',
  styleUrls: ['./cambio-contrasena.component.css']
})
export class CambioContrasenaComponent implements OnInit {

  hideContrasenaAct= true;
  hideContrasenaNew= true;
  hideConfirmContrasena= true;
  form; 
  correo:any;
  userPassword = {
    contrasena: "",
    contrasenaNueva: "",
    contrasenaVerificacion: "",
    correo: ""
  }

  constructor(private formBuilder: FormBuilder, private usuarioService:UsuarioService, private notificacionService: NotificacionService,
     public dialog: MatDialogRef<CambioContrasenaComponent>,
    @Inject(MAT_DIALOG_DATA) public userId:string) {

      this.form = this.formBuilder.group({
        contrasenaAct: ['', Validators.required],
        contrasenaNew: ['', Validators.required,],
        confirmContrasena: ['', Validators.required],
        email: ['', Validators.required]

      });

     }

  ngOnInit(): void {
    this.correo = localStorage.getItem('email');
  }

  actualizarContrasena(){
    if (this.form.valid) {
      if(this.form.value.contrasenaNew === this.form.value.confirmContrasena){

        this.userPassword.contrasena = this.form.value.contrasenaAct;
        this.userPassword.contrasenaNueva = this.form.value.contrasenaNew;
        this.userPassword.contrasenaVerificacion = this.form.value.confirmContrasena;
        this.userPassword.correo = this.correo;

        this.usuarioService.postCambioContrasena(this.userPassword).subscribe(resp => {
          console.log(resp);
          Swal.fire({
            icon: 'success',
            title: 'Cambio de Contraseña',
            text: "Se ha cambiado la contraseña con exito",
            confirmButtonColor: '#F44336'
          })
        }, error => {
          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: error.error,
            confirmButtonColor: '#F44336'
          })
        })
      }else{
        this.notificacionService.mostrarNotificacion("LA CONTRASEÑA DE CONFIRMACION NO ES IGUAL", "error");
      }
      
    }
      
  }

}
