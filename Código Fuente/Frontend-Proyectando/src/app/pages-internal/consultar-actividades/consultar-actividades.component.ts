import { AfterViewInit, Component, ViewChild, OnInit } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { Router } from '@angular/router';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { ActividadI } from 'src/app/models/actividad';
import { ActividadService } from 'src/app/services/actividad.service';


@Component({
  selector: 'app-consultar-actividades',
  templateUrl: './consultar-actividades.component.html',
  styleUrls: ['./consultar-actividades.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ConsultarActividadesComponent implements OnInit, AfterViewInit {

  columns: string[] = ['Nombre', 'Beneficiario', 'Fecha Realizacion'];
  columnsToDisplay: string[] = ['nombre', 'beneficiario', 'fechaRealizacion'];
  data_proyectos:any = [];
  dataSource = new MatTableDataSource();
  expandedElement: ActividadI | null;
  load:boolean;
  btnDisabled = false;

  @ViewChild(MatPaginator, {static:true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static:true} ) sort: MatSort;

  constructor( private actividadService:ActividadService, private route:Router ){
    this.load = false;
  }

  ngOnInit(): void {

    if (localStorage.getItem("rol") === "super usuario") {
      this.btnDisabled = true;
    }
    
    this.actividadService.getActividades().subscribe(resp => {
      this.load = true;
      console.log(resp);
      this.data_proyectos = resp;      
      this.dataSource.data = this.data_proyectos;
    }, error => {
      this.load = true;
      if(error.status === 401){
        this.route.navigate(['/login']);
        localStorage.clear();
      }
    });   
    
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  verActividad(actividad:ActividadI){
    this.actividadService.actividadSelect = actividad;
    this.route.navigate(['/dashboard/actividad']);
  }

  typeOf(elem:any):any{
    let array=[]
    array = elem.split(/T\d/)
    return array[0];
  }

}
