import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { Router, ActivatedRoute } from '@angular/router';
import { EncuestaService } from 'src/app/services/encuesta.service';
import { NotificacionService } from 'src/app/services/notificacion.service';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { InformesProyectosComponent } from '../informes-proyectos/informes-proyectos.component'
import { InformesPayloadI } from '../../models/informesPayload';
import { InformesService } from '../../services/informes.service';
@Component({
  selector: 'app-informes',
  templateUrl: './informes.component.html',
  styleUrls: ['./informes.component.css']
})
export class InformesComponent implements OnInit {
  form: InformesPayloadI = FORM;
  showFormulario = false;
  isLoading = false;
  listaTipos = ['Proyectos', 'Actividades', 'Convenios', 'Encuestas', 'Usuarios']
  listaAprobados = [
    {value: 'SI', text: 'SI'},
    {value: 'NO', text: 'NO'},
    {value: 'TODOS', text: 'TODOS'}
  ]
  listaEncuestas: any = [];
  fechaMaxima: Date;
  fechaMinima: Date;



  constructor(private route: Router,
    private notificacionService: NotificacionService,
    private rutaActiva: ActivatedRoute,
    private encuestaService: EncuestaService,
    private informesService: InformesService) {

  }

  ngOnInit(): void {
    this.isLoading = true
    this.encuestaService.getEncuestasActivas().subscribe(resp => {
      this.listaEncuestas = resp;
      this.isLoading = false;
    })
  }
  setFecha(tipo: string, event: MatDatepickerInputEvent<Date>) {
    this.showFormulario = false;
    if (tipo == 'inicio') {
      this.form.fechaInicio = event.value + '';
      this.fechaMinima = new Date(this.form.fechaInicio);
      this.form.fechaInicio = this.formatFecha(this.fechaMinima);
    }
    if (tipo == 'fin') {
      this.form.fechaFin = event.value + '';
      this.fechaMaxima = new Date(this.form.fechaFin);
      this.form.fechaFin = this.formatFecha(this.fechaMaxima);
    }
  }

  validarForm() {
    if (this.form.tipoInforme != '') {
      if (this.form.fechaInicio == '') {
        this.notificacionService.mostrarNotificacion("El campo Fecha inicio es obligatorio", "error");
        return false;
      }
      if (this.form.fechaFin == '') {
        this.notificacionService.mostrarNotificacion("El campo Fecha fin es obligatorio", "error");
        return false;
      }
      if (this.form.tipoInforme == 'Encuestas') {
        if (this.form.encuesta == '') {
          this.notificacionService.mostrarNotificacion("El campo Encuesta es obligatorio", "error");
          return false;
        }
      } else {
        if (this.form.aprobados == '') {
          this.notificacionService.mostrarNotificacion("El campo" + this.form.tipoInforme == 'Usuarios' ? "Usuarios Activos" : this.form.tipoInforme + " Aprobados" + "es obligatorio", "error");
          return false;
        }
      }
    }
    return true;
  }

  generarDatos() {
    if (this.validarForm()) {
      this.showFormulario = true;
      this.informesService.setPayload(this.form);
    }
  }

  changeShowFormulario() {
    this.showFormulario = false;
  }

  formatFecha(date:Date) {
    var dd = String(date.getDate()).padStart(2, '0');
    var mm = String(date.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = date.getFullYear();
    return yyyy + '-' + mm + '-' + dd;
  }
}


const FORM: InformesPayloadI = {
  tipoInforme: '',
  fechaInicio: '',
  fechaFin: '',
  aprobados: 'TODOS',
  encuesta: ''
};