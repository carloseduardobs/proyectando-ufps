import { Component, OnInit } from '@angular/core';
import { EditorChangeContent, EditorChangeSelection } from 'ngx-quill';
import { LandingPageService } from '../../services/landing-page.service';
import Swal from 'sweetalert2';
import { NotificacionService } from 'src/app/services/notificacion.service';

@Component({
  selector: 'app-landing-config',
  templateUrl: './landing-config.component.html',
  styleUrls: ['./landing-config.component.css']
})
export class LandingConfigComponent implements OnInit {

  nombreImagen = "";
  imgBase64:any;
  archivoCargado:any;
  landingInfo: any;
  editorText1 = "";
  editorText2 = "";
  editorText3 = "";
  editorText4 = "";
  editorText5 = "";
  editorText6 = "";
  title1 = "";
  title2 = "";
  title3 = "";
  title4 = "";
  title5 = "";
  title6 = "";
  title7 = "";
  expand = false;
  expand2 = false;
  expand3 = false;
  expand4 = false;
  expand5 = false;
  expand6 = false;
  expand7 = false;
  panelOpenState = false;
  step = 0;
  setStep(index: number) {
    this.step = index;
  }
  nextStep() {
    this.step++;
  }
  prevStep() {
    this.step--;
  }

  idPlanEst:any;

  params: any = {
    divulgar: "SI",
    id: 0,
    idPlanEstudio: 0,
    informacion: "",
    key: "",
    titulo: ""
  };
  content: any;

  editorContent1: any = '';
  editorContent2: any = '';
  editorContent3: any = '';
  editorContent4: any = '';
  editorContent5: any = '';
  editorContent6: any = '';

  constructor(private landingPageService: LandingPageService, private notificacionService:NotificacionService,) { }

  ngOnInit(): void {
    this.landingPageService.getInfoLanding().subscribe(resp => {
      this.landingInfo = resp;
      console.log(this.landingInfo);
      this.editorContent1 = this.landingInfo[0].informacion;
      this.editorContent2 = this.landingInfo[1].informacion;
      this.editorContent3 = this.landingInfo[2].informacion;
      this.editorContent4 = this.landingInfo[3].informacion;
      this.editorContent5 = this.landingInfo[4].informacion;
      this.editorContent6 = this.landingInfo[5].informacion;
      this.title1 = this.landingInfo[0].titulo;
      this.title2 = this.landingInfo[1].titulo;
      this.title3 = this.landingInfo[2].titulo;
      this.title4 = this.landingInfo[3].titulo;
      this.title5 = this.landingInfo[4].titulo;
      this.title6 = this.landingInfo[5].titulo;
      this.title7 = this.landingInfo[6].titulo;
    }, error => {
      console.log(error);
    })
    this.idPlanEst = localStorage.getItem('planEst');
    // this.landingPageService.getContent(this.params, 'politicas').subscribe(resp => {
    //   this.content = resp;
    //   console.log(this.content);
    // })
  }

  changedEditor(event: EditorChangeContent | EditorChangeSelection, id: number) {
    // console.log("cambios ", event);

    switch (id) {
      case 1:
        this.editorText1 = event['editor']['root']['innerHTML'];
        break;
      case 2:
        this.editorText2 = event['editor']['root']['innerHTML'];
        break;
      case 3:
        this.editorText3 = event['editor']['root']['innerHTML'];
        break;
      case 4:
        this.editorText4 = event['editor']['root']['innerHTML'];
        break;
      case 5:
        this.editorText5 = event['editor']['root']['innerHTML'];
        break;
      case 6:
        this.editorText6 = event['editor']['root']['innerHTML'];
        break;

    }
    // if(id===1){      
    //     this.editorText1 = event['editor']['root']['innerHTML'];      
    // }
    // if(id===2){
    //   this.editorText2 = event['editor']['root']['innerHTML'];
    // }
    // if(id===3){
    //   this.editorText3 = event['editor']['root']['innerHTML'];
    // }

  }

  handleArchivo(e: any): void {     
    if (e.target.files[0].type === 'image/jpeg' || e.target.files[0].type === 'image/png') {
      this.archivoCargado = e.target.files[0];
      this.nombreImagen = this.archivoCargado.name;
      console.log(this.archivoCargado);
      this.logoBase64();      
    } else {
      this.notificacionService.mostrarNotificacion(
        'El archivo adjunto debe ser formato jpeg, jpg o png',
        'error'
      );
    }
  }

  private base64() {
    return new Promise((resolve, reject) => {
      let reader = new FileReader();
      reader.readAsDataURL(this.archivoCargado as Blob);
      reader.onload = () => {
        resolve(reader.result as string);
      };
      reader.onerror = reject;
    });
  }

  async logoBase64() {      
    this.imgBase64 = await this.base64(); 
    
  }

  

  

  guardar(id: number) {

    switch (id) {
      case 1:        
        this.params.id = 1;
        this.params.key = this.landingInfo[0].key;
        this.params.titulo = this.title1;
        this.params.informacion = this.editorText1;
        this.params.idPlanEstudio = this.idPlanEst;        
        break;
      case 2:
        this.params.id = 2;
        this.params.key = this.landingInfo[1].key;
        this.params.titulo = this.title2;
        this.params.informacion = this.editorText2;
        this.params.idPlanEstudio = this.idPlanEst;
        break;
      case 3:
        this.params.id = 3;
        this.params.key = this.landingInfo[2].key;
        this.params.titulo = this.title3;
        this.params.informacion = this.editorText3;
        this.params.idPlanEstudio = this.idPlanEst;        
        break;
      case 4:
        this.params.id = 4;
        this.params.key = this.landingInfo[3].key;
        this.params.titulo = this.title4;
        this.params.informacion = this.editorText4;
        this.params.idPlanEstudio = this.idPlanEst;
        break;
      case 5:
        this.params.id = 5;
        this.params.key = this.landingInfo[4].key;
        this.params.titulo = this.title5;
        this.params.informacion = this.editorText5;
        this.params.idPlanEstudio = this.idPlanEst;
        break;
      case 6:
        this.params.id = 6;
        this.params.key = this.landingInfo[5].key;
        this.params.titulo = this.title6;
        this.params.informacion = this.editorText6;
        this.params.idPlanEstudio = this.idPlanEst;
        break;
        case 7:        
        this.params.id = 7;
        this.params.key = this.landingInfo[6].key;
        this.params.titulo = this.title7;
        this.params.informacion = this.imgBase64;
        this.params.idPlanEstudio = this.idPlanEst;
        break;

    }
    console.log(this.params);
    this.landingPageService.modificarInfoLanding(this.params).subscribe(
      (resp) => {
        console.log(resp);
        Swal.fire({
          icon: 'success',
          title: 'Información',
          text: "Se ha cargado la información correctamente.",
          confirmButtonColor: '#F44336'
        })
      },
      (error) => {
        console.log(error);
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: error.error,
          confirmButtonColor: '#F44336'
        })
      }
      );

    // if (id === 1) {
    //   console.log(this.editorText1);
    // }
    // if (id === 2) {
    //   console.log(this.editorText2);
    // }
    // if (id === 3) {
    //   console.log(this.editorText3);
    // }
  }

  

  editorModules = {
    toolbar: [
      ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
      ['blockquote', 'code-block'],

      //[{ 'header': 1 }, { 'header': 2 }],               // custom button values
      [{ 'list': 'ordered' }, { 'list': 'bullet' }],
      //[{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
      //[{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
      //[{ 'direction': 'rtl' }],                         // text direction

      [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
      [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

      [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
      [{ 'font': [] }],
      [{ 'align': [] }],

      ['clean'],                                         // remove formatting button

      //['link', 'image', 'video']                         // link and image, video
      ['link', 'image']                         // link and image, video
    ]
  };

}
