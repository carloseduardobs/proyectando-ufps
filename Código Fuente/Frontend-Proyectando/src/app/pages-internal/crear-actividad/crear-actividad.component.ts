import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActividadI } from 'src/app/models/actividad';
import { ActividadService } from 'src/app/services/actividad.service';
import { NotificacionService } from 'src/app/services/notificacion.service';
import { ProyectoService } from 'src/app/services/proyecto.service';
import Swal from 'sweetalert2';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-crear-actividad',
  templateUrl: './crear-actividad.component.html',
  styleUrls: ['./crear-actividad.component.css']
})
export class CrearActividadComponent implements OnInit {

  userID:any;
  form;
  actividad: ActividadI = {
    aprobar: "",
    beneficiario: "",
    ciudad: "",
    descripcion: "",
    divulgar: "",
    fechaRealizacion: "",
    idProyecto: 0,
    nombre: "",
    pais: "",
    registradoPor: 0,
    tipo: "Actividad"
  }
  listaTipos = ['Actividad', 'Evento'];
  proyectos: any;
  isLoading = false;

  constructor(private formBuilder: FormBuilder,
    private proyectoService: ProyectoService,
    private actividadService: ActividadService,
    private notificacionService:NotificacionService,
    private route:Router,
    private rutaActiva: ActivatedRoute) {

    this.form = this.formBuilder.group({
      beneficiario: ['', Validators.required],
      ciudad: ['', Validators.required],
      descripcion: ['', Validators.required],
      pais: ['', Validators.required],
      fechaRealizacion: ['', Validators.required],
      proyecto: ['', Validators.required],
      nombre: ['', Validators.required],
      tipo: ['Actividad']
    });


  }

  ngOnInit(): void {
    this.userID = localStorage.getItem("userId")

    this.proyectoService.getProyectos().subscribe(resp => {
      this.proyectos = resp;
    }, error => {
      console.log(error.error);
    });
  }

  crearActividad(){
    if (this.form.valid) {
      this.isLoading = true;
      this.actividad.aprobar = "NO";
      this.actividad.beneficiario = this.form.value.beneficiario;
      this.actividad.ciudad = this.form.value.ciudad;
      this.actividad.descripcion = this.form.value.descripcion;
      this.actividad.divulgar = "NO";
      this.actividad.fechaRealizacion = this.form.value.fechaRealizacion;
      this.actividad.idProyecto = this.form.value.proyecto;
      this.actividad.nombre = this.form.value.nombre;
      this.actividad.pais = this.form.value.pais;
      this.actividad.tipo = this.form.value.tipo;
      this.actividad.registradoPor = parseInt(this.userID, 10);

      console.log(this.actividad);
      this.actividadService.postInsertarActividad(this.actividad).subscribe(resp => {
        console.log(resp);
        Swal.fire({
          icon: 'success',
          title: 'Proyectos',
          text: "Se ha creado la actividad con exito",
          confirmButtonColor: '#F44336'
        })
        this.isLoading = false;
      }, error => {
        console.log(error.error);
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: error.error,
          confirmButtonColor: '#F44336'
        })
        this.isLoading = false;
      })
    }else{
      this.notificacionService.mostrarNotificacion("Faltan Campos Obligatorios por diligenciar", "error");
    }
  }

  irAtras() {
    this.route.navigate(['/dashboard/actividades']);
  }

}
