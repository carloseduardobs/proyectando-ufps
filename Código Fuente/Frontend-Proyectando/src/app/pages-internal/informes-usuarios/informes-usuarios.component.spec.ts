import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InformesUsuariosComponent } from './informes-usuarios.component';

describe('InformesUsuariosComponent', () => {
  let component: InformesUsuariosComponent;
  let fixture: ComponentFixture<InformesUsuariosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InformesUsuariosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InformesUsuariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
