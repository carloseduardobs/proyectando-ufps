import { AfterViewInit, Component, ViewChild, OnInit } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { InformesService } from '../../services/informes.service';
import { Router } from '@angular/router';
import { ProyectoI } from '../../models/proyecto';
import { InformesPayloadI } from 'src/app/models/informesPayload';

@Component({
  selector: 'app-informes-usuarios',
  templateUrl: './informes-usuarios.component.html',
  styleUrls: ['./informes-usuarios.component.css']
})
export class InformesUsuariosComponent implements OnInit {
  hiddenColumn: boolean[] = [true,	true,	true,	true,	true,	false];
  columns: string[] = ['Correo',	'Nombres',	'Apellidos',	'Estado',	'Rol',	'Fecha Registro'];
  columnsToDisplay: string[] = ['correo',	'nombres',	'apellidos',	'estado',	'rol',	'fechaRegistro'];
  data_proyectos:any = [];
  dataSource = new MatTableDataSource();
  load:boolean;

  @ViewChild(MatPaginator, {static:true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static:true} ) sort: MatSort;

  constructor( private informesService:InformesService, private route:Router ){
    this.load = false;
  }

  ngOnInit(): void {
    let payload:InformesPayloadI = this.informesService.getPayload()
    this.informesService.getUsuarios(payload).subscribe(resp => {
      this.load = true;
      console.log(resp);
      this.data_proyectos = resp;      
      this.dataSource.data = this.data_proyectos;
    }, error => {
      if(error.status === 401){
        this.route.navigate(['/login']);
        localStorage.clear();
      }
    });    
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  typeOf(elem:any):any{
    elem = elem + ''
    let array=[]
    array = elem.split(/T\d/)
    return array[0];
  }
}
