import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { EvidenciasService } from 'src/app/services/evidencias.service';
import { NotificacionService } from '../../services/notificacion.service';
import { ConveniosService } from '../../services/convenios.service';
import { ProyectoService } from 'src/app/services/proyecto.service';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-crear-convenio',
  templateUrl: './crear-convenio.component.html',
  styleUrls: ['./crear-convenio.component.css']
})
export class CrearConvenioComponent implements OnInit {

  userRol: any;
  form;
  convenioCargado: any;
  convenio: any;
  proyectos: any;
  isLoading = false;

  constructor(private formBuilder: FormBuilder,
    private notificacionService: NotificacionService,
    private conveniosService:ConveniosService,
    private proyectoService:ProyectoService,
    private route:Router,
    private rutaActiva: ActivatedRoute) {

    this.form = this.formBuilder.group({
      nombre: ['', Validators.required],
      fechaRealizacion: ['', Validators.required],
      representante: ['', Validators.required],
      proyecto: ['', Validators.required],
      aprobarConvenio: ['NO'],
      mostrarConvenio: ['NO']
    });
  }

  ngOnInit(): void {
    this.proyectoService.getProyectos().subscribe(resp => {
      this.proyectos = resp;
    }, error => {
      console.log(error.error);
    });
  }

 
  async crearConvenio() {
    try {
      if(this.form.valid){
        this.isLoading = true;
        let array: any = await this.base64(this.convenioCargado);
        console.log(array);

        this.convenio = {
          evidencia: array,
          extEvidencia: this.convenioCargado.type as string,
          fechaRealizacion: this.form.value.fechaRealizacion,
          nombre: this.form.value.nombre,
          idProyecto: this.form.value.proyecto,
          nombreEvidencia: this.convenioCargado.name as string,
          representante: this.form.value.representante
        };

        console.log(this.convenio);
        this.conveniosService
          .postRegistrarConvenios(this.convenio)
          .subscribe(
            (resp) => {
              Swal.fire({
                icon: 'success',
                title: 'Proyectos',
                text: "Se ha creado el convenio con exito",
                confirmButtonColor: '#F44336'
              })
              this.isLoading = false
              console.log(resp);
            },
            (error) => {
              console.log(error);
              Swal.fire({
                icon: 'error',
                title: 'Error',
                text: error.error,
                confirmButtonColor: '#F44336'
              })
              this.isLoading = false
            }
          );
      } else {
        this.notificacionService.mostrarNotificacion("Faltan Campos Obligatorios por diligenciar", "error");
      }
    } catch (err) {
      console.log(err);
    }
  }

  handleConvenio(e: any): void {
    if (e.target.files[0].type === 'application/pdf') {
      this.convenioCargado = e.target.files[0];
    } else {
      this.notificacionService.mostrarNotificacion(
        'El archivo adjunto debe ser formato PDF',
        'error'
      );
    }
  }

  private base64(archivosCargados: any) {
    return new Promise((resolve, reject) => {
      let reader = new FileReader();
      reader.readAsDataURL(archivosCargados as Blob);
      reader.onload = () => {
        resolve(reader.result as string);
      };
      reader.onerror = reject;
    });
  }

  irAtras() {
    this.route.navigate(['/dashboard/consultar-convenios']);
  }

}
