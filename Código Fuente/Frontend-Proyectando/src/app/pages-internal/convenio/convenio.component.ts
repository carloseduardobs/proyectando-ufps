import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ConvenioI } from 'src/app/models/convenio';
import { ConveniosService } from 'src/app/services/convenios.service';
import { ProyectoService } from 'src/app/services/proyecto.service';
import { NotificacionService } from '../../services/notificacion.service';
import Swal from 'sweetalert2';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-convenio',
  templateUrl: './convenio.component.html',
  styleUrls: ['./convenio.component.css']
})
export class ConvenioComponent implements OnInit {

  userRol = localStorage.getItem('rol');
  form;
  convenio: ConvenioI;
  proyectos: any; 
  archivoConvenio:any;
  convenioCargado:any;
  isLoading = false;

  constructor(private formBuilder: FormBuilder,
              private proyectoService: ProyectoService,
              private conveniosService:ConveniosService,
              private notificacionService:NotificacionService,
              private route:Router,
              private rutaActiva: ActivatedRoute) {

          this.form = this.formBuilder.group({
            nombre: ['', Validators.required],
            fechaRealizacion: ['', Validators.required],
            representante: ['', Validators.required],
            proyecto: ['', Validators.required],
            aprobarConvenio: [''],
            mostrarConvenio: ['']
          })
  }

  ngOnInit(): void {    

    this.proyectoService.getProyectos().subscribe(resp => {
      this.proyectos = resp;
    }, error => {
      console.log(error.error);
    });

    this.convenio = this.conveniosService.convenioSelect;
    this.archivoConvenio = this.convenio.evidencia;
    this.iniciarValoresForm();
  }

  private iniciarValoresForm() {
    this.form.patchValue({

      nombre: this.convenio.nombre,
      fechaRealizacion: this.convenio.fechaRealizacion,
      proyecto: this.convenio.idProyecto,
      representante: this.convenio.representante,
      aprobarConvenio: this.convenio.aprobar,
      mostrarConvenio: this.convenio.divulgar

    })
  }

  async guardarConvenio() {
    if (this.form.valid) {
      this.isLoading = true;
      this.convenio.nombre = this.form.value.nombre;
      this.convenio.fechaRealizacion = this.form.value.fechaRealizacion;
      this.convenio.idProyecto = this.form.value.proyecto;
      this.convenio.representante = this.form.value.representante;
      this.convenio.aprobar = this.form.value.aprobarConvenio;
      this.convenio.divulgar = this.form.value.mostrarConvenio;
      this.convenio.evidencia = this.archivoConvenio;

      console.log(this.convenio);
      this.conveniosService.putModificarConvenio(this.convenio).subscribe(resp => {
        console.log("Modificado", resp);
        Swal.fire({
          icon: 'success',
          title: 'Proyecto',
          text: "Se ha guardado la actividad con exito",
          confirmButtonColor: '#F44336'
        })
        this.isLoading = false;
      }, error => {
        console.log(error.error);
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: error.error,
          confirmButtonColor: '#F44336'
        })
        this.isLoading = false;
      })
    } else {
      this.notificacionService.mostrarNotificacion("Faltan Campos Obligatorios por diligenciar", "error");
    }
  }

  verConvenio(){
    console.log(this.archivoConvenio);
    let pdfWindow:any = window.open("");
    pdfWindow.document.write(`<iframe width='100%' height='100%' src=${this.archivoConvenio}></iframe>`)
  }

  handleConvenio(e: any): void {
    if (e.target.files[0].type === 'application/pdf') {
      this.convenioCargado = e.target.files[0];
    } else {
      this.notificacionService.mostrarNotificacion(
        'El archivo adjunto debe ser formato PDF',
        'error'
      );
    }
  }

  private base64(archivosCargados: any) {
    return new Promise((resolve, reject) => {
      let reader = new FileReader();
      reader.readAsDataURL(archivosCargados as Blob);
      reader.onload = () => {
        resolve(reader.result as string);
      };
      reader.onerror = reject;
    });
  }

  irAtras() {
    this.route.navigate(['/dashboard/consultar-convenios']);
  }


}
