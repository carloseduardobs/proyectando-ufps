import { Component, OnInit } from '@angular/core';
import { ProyectoService } from '../../services/proyecto.service';
import { ActividadService } from '../../services/actividad.service';
import { ConveniosService } from 'src/app/services/convenios.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-inicio-dashboard',
  templateUrl: './inicio-dashboard.component.html',
  styleUrls: ['./inicio-dashboard.component.css']
})
export class InicioDashboardComponent implements OnInit {

  proyectos: any = [];
  actividades: any = [];
  convenios: any = [];

  constructor(private proyectoService: ProyectoService,
    private actividadService:ActividadService,
    private conveniosService:ConveniosService,
    private route:Router) { }

  ngOnInit(): void {
    this.proyectoService.getProyectos().subscribe(resp => {
      console.log(resp);
      this.proyectos = resp;    
    }, error => {
      console.log(error.error);
    });
    this.actividadService.getActividades().subscribe(resp => {
      console.log(resp);
      this.actividades = resp;    
    }, error => {
      console.log(error.error);
    });
    this.conveniosService.getConsultarConvenios().subscribe(resp => {
      console.log(resp);
      this.convenios = resp;    
    }, error => {
      console.log(error.error);
    });
  }
  irProyectos() {
    this.route.navigate(['/dashboard/proyectos']);
  }
  irConvenios() {
    this.route.navigate(['/dashboard/consultar-convenios']);
  }
  irActividades() {
    this.route.navigate(['/dashboard/actividades']);
  }
}
