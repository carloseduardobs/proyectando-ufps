import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InformesProyectosComponent } from './informes-proyectos.component';

describe('InformesProyectosComponent', () => {
  let component: InformesProyectosComponent;
  let fixture: ComponentFixture<InformesProyectosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InformesProyectosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InformesProyectosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
