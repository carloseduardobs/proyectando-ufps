import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EvidenciaProyectoI } from '../../models/evidencia-proyecto';
import { ProyectoService } from '../../services/proyecto.service';
import { EvidenciasService } from '../../services/evidencias.service';
import { NotificacionService } from '../../services/notificacion.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConveniosService } from '../../services/convenios.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-evidencia-proyecto',
  templateUrl: './evidencia-proyecto.component.html',
  styleUrls: ['./evidencia-proyecto.component.css']
})
export class EvidenciaProyectoComponent implements OnInit {

  evidenciaProyecto: any;
  fileBase64: string = '';
  evidencias2: any[] = [];
  archivosCargados: any[] = [];
  convenioCargado:any;
  form;
  id: any;
  showConvenio: boolean;
  convenio:any;
  title = '';
  userRol = localStorage.getItem('rol');

  constructor(
    private proyectoService: ProyectoService,
    private evidenciasService: EvidenciasService,
    private notificacionService: NotificacionService,
    private conveniosService:ConveniosService,
    public dialog: MatDialogRef<EvidenciaProyectoComponent>,
    private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public userId: string
  ) {
    this.form = this.formBuilder.group({
      fechaRealizacion: ['', Validators.required],
      nombre: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    this.id = this.proyectoService.proyectoSelect.id;    
  }

  handleArchivos(e: any): void {
    Array.from(e.target.files).forEach((element) => {
      this.archivosCargados.push(element);
    });
  }
  
  private base64(archivosCargados: any) {
    return new Promise((resolve, reject) => {
      let reader = new FileReader();
      reader.readAsDataURL(archivosCargados as Blob);
      reader.onload = () => {
        resolve(reader.result as string);
      };
      reader.onerror = reject;
    });
  }

  async saveEvidencia() {
    try {
      for (let index = 0; index < this.archivosCargados.length; index++) {
        let array: any = await this.base64(this.archivosCargados[index]);
        this.evidencias2.push({
          archivo: array,
          extension: this.archivosCargados[index].type as string,
          nombre: this.archivosCargados[index].name as string,
        });
      }

      this.evidenciaProyecto = {
        fechaRealizacion: this.form.value.fechaRealizacion,
        nombre: this.form.value.nombre,
        idProyecto: this.id,
        evidencias: this.evidencias2,
      };

      console.log(this.evidenciaProyecto);
      this.evidenciasService
        .postCrearEvidenciaProyecto(this.evidenciaProyecto)
        .subscribe(
          (resp) => {
            console.log(resp);
            Swal.fire({
              icon: 'success',
              title: 'Evidencia',
              text: "Se ha cargado la evidencia al proyecto.",
              confirmButtonColor: '#F44336'
            })
          },
          (error) => {
            console.log(error);
            Swal.fire({
              icon: 'error',
              title: 'Error',
              text: error.error,
              confirmButtonColor: '#F44336'
            })
          }
        );
    } catch (err) {
      console.log(err);
      Swal.fire({
        icon: 'error',
        title: 'Error',
        text: err.error,
        confirmButtonColor: '#F44336'
      })
    }
  }

  borrarEvidencia(item: any) {
    this.archivosCargados.splice(item, 1);
    console.log(this.archivosCargados);
  }

}
