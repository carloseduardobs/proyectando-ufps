import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EvidenciaProyectoComponent } from './evidencia-proyecto.component';

describe('EvidenciaProyectoComponent', () => {
  let component: EvidenciaProyectoComponent;
  let fixture: ComponentFixture<EvidenciaProyectoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EvidenciaProyectoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EvidenciaProyectoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
