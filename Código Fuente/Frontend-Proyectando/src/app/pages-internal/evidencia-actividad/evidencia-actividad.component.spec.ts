import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EvidenciaActividadComponent } from './evidencia-actividad.component';

describe('EvidenciaActividadComponent', () => {
  let component: EvidenciaActividadComponent;
  let fixture: ComponentFixture<EvidenciaActividadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EvidenciaActividadComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EvidenciaActividadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
