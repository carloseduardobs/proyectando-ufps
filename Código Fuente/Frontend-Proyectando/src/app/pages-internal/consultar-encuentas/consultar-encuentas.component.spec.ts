import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultarEncuentasComponent } from './consultar-encuentas.component';

describe('ConsultarEncuentasComponent', () => {
  let component: ConsultarEncuentasComponent;
  let fixture: ComponentFixture<ConsultarEncuentasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConsultarEncuentasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultarEncuentasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
