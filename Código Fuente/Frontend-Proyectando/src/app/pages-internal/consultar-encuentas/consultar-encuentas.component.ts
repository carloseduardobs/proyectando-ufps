import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { EncuestaService } from 'src/app/services/encuesta.service';

@Component({
  selector: 'app-consultar-encuentas',
  templateUrl: './consultar-encuentas.component.html',
  styleUrls: ['./consultar-encuentas.component.css']
})
export class ConsultarEncuentasComponent implements OnInit {

  columnas: string[] = ['Nombre', 'Estado', 'Fecha Realizacion'];
  columnsToDisplay: string[] = ['nombre', 'estado', 'fechaRealizacion', 'select'];
  data_encuesta: any = [];
  dataSource = new MatTableDataSource();
  load: boolean;
  btnDisabled = false;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private encuestaService: EncuestaService, private route: Router) {
    this.load = false
  }

  ngOnInit(): void {

    if (localStorage.getItem("rol") === "super usuario") {
      this.btnDisabled = true;
    }

    this.encuestaService.getEncuestas().subscribe(resp => {
      console.log(resp);
      this.load = true;
      this.data_encuesta = resp;
      this.dataSource.data = this.data_encuesta;
    }, error => {
      this.load = true;
      if (error.status === 401) {
        this.route.navigate(['/login']);
        localStorage.clear();
      }
    });

  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }


  verEncuesta(encuesta: any) {
    this.encuestaService.encuestaSelect = encuesta;
    this.route.navigate(['/dashboard/encuesta']);
  }

  verRespuesta(encuesta: any) {
    console.log(encuesta.id);
    this.encuestaService.encuestaSelect = encuesta;
    this.route.navigate(['/dashboard/consultar-respuestas']);
  }

  typeOf(elem: any): any {
    let array = []
    array = elem.split(/T\d/)
    return array[0];
  }

}
