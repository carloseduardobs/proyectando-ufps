import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AutenticacionService } from 'src/app/services/autenticacion.service';
import { NotificacionService } from 'src/app/services/notificacion.service';
import { UserI } from '../../models/user';
import { UsuarioService } from '../../services/usuario.service';
import Swal from 'sweetalert2';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {

  form;
  rols:any;
  planesEst = [
    {id: 1, nombre: 'Ingenieria de Sistemas'}
  ];
  isLoading = false;

  usuario:UserI;

  constructor( private formBuilder: FormBuilder, 
               private notificacionService: NotificacionService,
               private usuarioService:UsuarioService,
               private autenticacionService:AutenticacionService,
               private route:Router,
               private rutaActiva: ActivatedRoute ) {
                  
    this.form = this.formBuilder.group({
      nombre: ['', Validators.required],
      apellido: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      rol: ['', Validators.required],
      planEst: ['', Validators.required],
      estado: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    this.autenticacionService.getUserRoles().subscribe(resp =>{
      this.rols = resp;
    })
this.usuario = this.usuarioService.usuarioSelect;
    this.iniciarValoresForm();
  }

  private iniciarValoresForm() {
    this.form.patchValue({
      nombre: this.usuario.nombres,
      apellido: this.usuario.apellidos,
      email: this.usuario.correo,
      estado: this.usuario.estado,
      rol: this.usuario.idRol,
      planEst: this.usuario.idPlanEstudio

    })
  }

  actualizarUser() {
    if (this.form.valid) {
        this.isLoading = true;
        this.usuario.nombres = this.form.value.nombre;
        this.usuario.apellidos = this.form.value.apellido;
        this.usuario.correo = this.form.value.email;
        this.usuario.estado = this.form.value.estado;
        this.usuario.idRol = this.form.value.rol;
        this.usuario.idPlanEstudio = this.form.value.planEst;

        console.log(this.usuario)
 
        this.usuarioService.putModificarUsuario(this.usuario).subscribe(resp => {  
          console.log("Modificado", resp);
          Swal.fire({
          icon: 'success',
          title: 'Proyecto',
          text: "Se ha guardado el usuario con exito",
          confirmButtonColor: '#F44336'
          })  
          this.isLoading = false;
        }, error => {
          console.log(error.error);
          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: error.error,
            confirmButtonColor: '#F44336'
          })
          this.isLoading = false;
        })
      }      
    
  }

  irAtras() {
    this.route.navigate(['/dashboard/usuarios']);
  }
}
