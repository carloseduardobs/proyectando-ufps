import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormBuilder } from '@angular/forms';
import { NotificacionService } from 'src/app/services/notificacion.service';
import { MatDialog } from '@angular/material/dialog';
import { ProyectoService } from '../../services/proyecto.service';
import { ProyectoI } from '../../models/proyecto';
import Swal from 'sweetalert2';

import { EvidenciasService } from '../../services/evidencias.service';
import { EvidenciaProyectoComponent } from '../evidencia-proyecto/evidencia-proyecto.component';
import { EnviarEncuestaComponent } from '../enviar-encuesta/enviar-encuesta.component';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-proyecto',
  templateUrl: './proyecto.component.html',
  styleUrls: ['./proyecto.component.css']
})
export class ProyectoComponent implements OnInit {

  userRol = localStorage.getItem('rol');
  form;
  proyecto: ProyectoI;
  procesosMisionales: any;
  tiposProyecto: any;
  userID:any;
  isLoading = false;

  constructor(private formBuilder: FormBuilder, 
    public dialog: MatDialog, 
    private proyectoService: ProyectoService,
    private evidenciasService:EvidenciasService,
    private notificacionService:NotificacionService,
    private route:Router,
    private rutaActiva: ActivatedRoute) {

    this.form = this.formBuilder.group({
      nombre: ['', Validators.required],
      descripcion: ['', Validators.required],
      procesoMisional: [1, Validators.required],
      tipoProyecto: [2, Validators.required],
      fechaInicio: ['', Validators.required],
      fechaFin: ['', Validators.required],
      monto: ['', Validators.required],
      beneficiarios: ['', Validators.required],
      aprobarProyecto: [''],
      mostrarProyecto: ['']
    });
  }

  ngOnInit(): void {
    this.proyectoService.getProcesosMisionales().subscribe(resp => {
      this.procesosMisionales = resp;
    }, error => {
      console.log(error.error);
    });

    this.proyectoService.getTipoProyecto().subscribe(resp => {
      this.tiposProyecto = resp;
    }, error => {
      console.log(error.error);
    });

    this.proyecto = this.proyectoService.proyectoSelect;
    this.iniciarValoresForm();
  }

  private iniciarValoresForm() {
    this.form.patchValue({
      nombre: this.proyecto.nombre,
      procesoMisional: this.proyecto.idProcesoMisional,
      tipoProyecto: this.proyecto.idTipoProyecto,
      fechaInicio: this.proyecto.fechaInicio,
      fechaFin: this.proyecto.fechaFin,
      monto: this.proyecto.monto,
      beneficiarios: this.proyecto.beneficiario,
      aprobarProyecto: this.proyecto.aprobar,
      mostrarProyecto: this.proyecto.divulgar,
      descripcion: this.proyecto.descripcion,

    })
  }

  guardarProyecto() {
    if (this.form.valid) {
      this.isLoading = true;
      this.proyecto.nombre = this.form.value.nombre;
      this.proyecto.idProcesoMisional = this.form.value.procesoMisional;
      this.proyecto.idTipoProyecto = this.form.value.tipoProyecto;
      this.proyecto.fechaInicio = this.form.value.fechaInicio;
      this.proyecto.fechaFin = this.form.value.fechaFin;
      this.proyecto.monto = this.form.value.monto;
      this.proyecto.beneficiario = this.form.value.beneficiarios;
      this.proyecto.aprobar = this.form.value.aprobarProyecto;
      this.proyecto.divulgar = this.form.value.mostrarProyecto;
      this.proyecto.descripcion = this.form.value.descripcion;

      console.log(this.proyecto);
      this.proyectoService.putModificarProyecto(this.proyecto).subscribe(resp => {
        console.log("Modificado", resp);
        Swal.fire({
          icon: 'success',
          title: 'Proyecto',
          text: "Se ha guardado el proyecto con exito",
          confirmButtonColor: '#F44336'
        })
        this.isLoading = false;
      }, error => {
        console.log(error.error);
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: error.error,
          confirmButtonColor: '#F44336'
        })
        this.isLoading = false;
      })
    } else {
      this.notificacionService.mostrarNotificacion("Faltan Campos Obligatorios por diligenciar", "error");
    }
  }

  adjuntarEvidencia() {
    this.openDialogEvidencia();
  }

  openDialogEvidencia(): void {
    const dialogRef = this.dialog.open(EvidenciaProyectoComponent);
    dialogRef.afterClosed().subscribe(resp => {
      console.log(`Dialog result ${resp}`);
    })
  }

  adjuntarEncuesta() {
    this.openDialogEncuesta();
  }

  openDialogEncuesta(): void {
    this.proyectoService.proyectoSelect = this.proyecto;
    const dialogRef = this.dialog.open(EnviarEncuestaComponent);
    dialogRef.afterClosed().subscribe(resp => {
      console.log(`Dialog result ${resp}`);
    })
  }

  irAtras() {
    this.route.navigate(['/dashboard/proyectos']);
  }
}
