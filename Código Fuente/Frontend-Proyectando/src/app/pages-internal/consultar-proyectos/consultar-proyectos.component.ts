import { AfterViewInit, Component, ViewChild, OnInit } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { ProyectoService } from '../../services/proyecto.service';
import { Router } from '@angular/router';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { ProyectoI } from '../../models/proyecto';

@Component({
  selector: 'app-consultar-proyectos',
  templateUrl: './consultar-proyectos.component.html',
  styleUrls: ['./consultar-proyectos.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ConsultarProyectosComponent implements AfterViewInit, OnInit {

  columnsToDisplay: string[] = ['nombre', 'monto', 'aprobar'];
  data_proyectos:any = [];
  dataSource = new MatTableDataSource();
  expandedElement: ProyectoI | null;
  load:boolean;
  btnDisabled = false;

  @ViewChild(MatPaginator, {static:true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static:true} ) sort: MatSort;

  constructor( private proyectoService:ProyectoService, private route:Router ){
    this.load = false;
  }

  ngOnInit(): void {

    if (localStorage.getItem("rol") === "super usuario") {
      this.btnDisabled = true;
    }
    
    this.proyectoService.getProyectos().subscribe(resp => {
      this.load = true;
      console.log(resp);
      this.data_proyectos = resp;      
      this.dataSource.data = this.data_proyectos;
    }, error => {
      if(error.status === 401){
        this.route.navigate(['/login']);
        localStorage.clear();
      }
    });    
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  verProyecto(proyecto:ProyectoI){
    this.proyectoService.proyectoSelect = proyecto;
    this.route.navigate(['/dashboard/proyecto']);
  }



}
