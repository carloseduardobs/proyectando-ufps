import { Component, OnInit } from '@angular/core';
import { EncuestaService } from 'src/app/services/encuesta.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-encuesta',
  templateUrl: './encuesta.component.html',
  styleUrls: ['./encuesta.component.css']
})
export class EncuestaComponent implements OnInit {

  nombreEncuesta:string;
  respuestas:any = [];
  preguntas: any;
  encuesta:any;
  idEncuesta:any;
  encuestaModificada:any;
  isLoading = false;

  constructor(private route:Router,private encuestaService:EncuestaService) { }

  ngOnInit(): void {
    
    this.encuesta = this.encuestaService.encuestaSelect;
    console.log(this.encuesta);
    this.preguntas = this.encuesta.preguntas;
    this.nombreEncuesta = this.encuesta.nombre;
    this.idEncuesta = this.encuesta.id;
    this.extraerRespuesta();
  }

  extraerRespuesta(){
    console.log("preguntas ", this.preguntas);
    for (let index = 0; index < this.preguntas.length; index++) {
      if(this.preguntas[index].tipoPregunta === "UNICA"){
        this.respuestas.push(JSON.parse(this.preguntas[index].opciones));
      }else{
        this.respuestas.push("");
      }     
    }   
    console.log(this.respuestas);
  }

  delete(index:any):void{
    this.respuestas.splice(index, 1);
    this.preguntas.splice(index, 1);
    console.log(this.respuestas);
  }

  eliminarEncuesta(){
    this.isLoading = true;
    this.encuestaService.deleteEncuesta(this.idEncuesta).subscribe( resp => {
      Swal.fire({
        icon: 'success',
        title: 'Proyecto',
        text: "Se ha eliminado la encuesta con exito",
        confirmButtonColor: '#F44336'
      })
      this.isLoading = false
      this.irAtras();
    }, error => {
      console.log(error.error);
      Swal.fire({
        icon: 'error',
        title: 'Error',
        text: error.error,
        confirmButtonColor: '#F44336'
      })
      this.isLoading = false;
    })

  }

  actualizarEncuesta(){
    this.encuestaService.putEncuesta(this.encuesta).subscribe( resp => {
      console.log(resp);
    }, error => {
      console.log(error);
    })
  }
  irAtras() {
    this.route.navigate(['/dashboard/consultar-encuestas']);
  }

}
