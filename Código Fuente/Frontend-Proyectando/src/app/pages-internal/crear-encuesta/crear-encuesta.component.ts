import { Component, OnInit } from '@angular/core';
import { RouterLink, Router } from '@angular/router';
import { EncuestaService } from '../../services/encuesta.service';
import { NotificacionService } from 'src/app/services/notificacion.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-crear-encuesta',
  templateUrl: './crear-encuesta.component.html',
  styleUrls: ['./crear-encuesta.component.css']
})
export class CrearEncuestaComponent implements OnInit {

  
  nombreEncuesta:string = "";
  pregunta:string = "";
  respuesta1:string = "";
  respuesta2:string = "";
  respuesta3:string = "";
  respuesta4:string = "";

  checked = false;
  idPlanEst:any;

  encuesta = {
    estado: "",
    fechaREalizacion: "",
    idPlanEstudio: "",
    nombre: "",
    preguntas: []
  }

  respuestas:any =[];
  preguntas:any = [];

  isLoading = false;

  constructor(private encuestaService:EncuestaService,
    private route:Router,
    private notificacionService:NotificacionService) { }

  ngOnInit(): void {
    this.idPlanEst = localStorage.getItem("planEst");
  }

  agregarPregunta(){
    let preguntaOM :any;
    if(this.checked){
      preguntaOM = {
        enunciado: this.pregunta,
        opciones: `[{"value":"${this.respuesta1}", "label": "Opcion 1"},
                    {"value": "${this.respuesta2}", "label": "Opcion 2"},
                    {"value":"${this.respuesta3}", "label": "Opcion 3"},
                    {"value": "${this.respuesta4}", "label": "Opcion 4"}]`,
        tipoPregunta: "UNICA"
      }
      this.respuestas.push(JSON.parse(preguntaOM.opciones));
    }else{
      preguntaOM= {
        enunciado: this.pregunta,
        opciones: "",
        tipoPregunta: "ABIERTA"
      }
      this.respuestas.push(preguntaOM.opciones);
    }   
    
    this.preguntas.push(preguntaOM);

    this.limpiarPreguntas();
    console.log(this.preguntas);
    console.log(this.respuestas);
  }

  private limpiarPreguntas(){
    this.pregunta = "";
    this.respuesta1= "";
    this.respuesta2= "";
    this.respuesta3= "";
    this.respuesta4= "";
    this.checked = false;

  }
  guardar(){
    if(this.preguntas.length !== 0){
      this.isLoading = true;
      let encuesta = {
        estado: "PENDIENTE",
        fechaRealizacion: new Date().toISOString(),
        idPlanEstudio: this.idPlanEst,
        nombre: this.nombreEncuesta,
        preguntas: this.preguntas
      }
  
      this.encuestaService.postInsertarEncuestas(encuesta).subscribe( resp => {
        console.log(resp);
        Swal.fire({
          icon: 'success',
          title: 'Encuestas',
          text: "Se ha creado la encuesta con exito",
          confirmButtonColor: '#F44336'
        })
        this.isLoading = false;
      }, error => {
        console.log(error);
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: error.error,
          confirmButtonColor: '#F44336'
        })
        this.isLoading = false;
      })
    }else{
      this.notificacionService.mostrarNotificacion("No se han agregado preguntas", "error");
    }
    
  }

  delete(index:any):void{
    this.preguntas.splice(index, 1);
    console.log(this.preguntas);
  }

  irAtras() {
    this.route.navigate(['/dashboard/consultar-encuestas']);
  }
}
