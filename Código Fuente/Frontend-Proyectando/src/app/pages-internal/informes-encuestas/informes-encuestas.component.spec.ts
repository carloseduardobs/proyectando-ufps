import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InformesEncuestasComponent } from './informes-encuestas.component';

describe('InformesEncuestasComponent', () => {
  let component: InformesEncuestasComponent;
  let fixture: ComponentFixture<InformesEncuestasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InformesEncuestasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InformesEncuestasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
