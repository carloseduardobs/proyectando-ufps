import { AfterViewInit, Component, ViewChild, OnInit } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { InformesService } from '../../services/informes.service';
import { Router } from '@angular/router';
import { ProyectoI } from '../../models/proyecto';
import { InformesPayloadI } from '../../models/informesPayload';

@Component({
  selector: 'app-informes-encuestas',
  templateUrl: './informes-encuestas.component.html',
  styleUrls: ['./informes-encuestas.component.css']
})
export class InformesEncuestasComponent implements OnInit {

  hiddenColumn: boolean[] = [true, false, true, true, false, true, false, false];
  columns: string[] = ['Nombre', 'Descripción', 'Fecha Inicio', 'Fecha Fin', 'Monto', 'Beneficiario', 'Aprobado', 'Divulgado', 'Registrado Por'];
  columnsToDisplay: string[] = ['nombre', 'descripcion', 'fechaInicio', 'fechaFin', 'monto', 'beneficiario', 'aprobar', 'divulgar', 'registradoPor'];
  data_proyectos:any = [];
  dataSource = new MatTableDataSource();
  load:boolean;

  @ViewChild(MatPaginator, {static:true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static:true} ) sort: MatSort;

  constructor( private informesService:InformesService, private route:Router ){
    this.load = false;
  }

  ngOnInit(): void {
    let payload:InformesPayloadI = this.informesService.getPayload()
    this.informesService.getRespuestasEncuestas(payload).subscribe(resp => {
      this.load = true;
      console.log(resp);
      this.data_proyectos = resp;
      this.columns = this.data_proyectos.columns;
      this.hiddenColumn = this.data_proyectos.hiddenColumn;
      this.columnsToDisplay = this.data_proyectos.columnsToDisplay; 
      this.dataSource.data = this.data_proyectos.rows;
    }, error => {
      if(error.status === 401){
        this.route.navigate(['/login']);
        localStorage.clear();
      }
    });    
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  typeOf(elem:any):any{
    elem = elem + ''
    let array=[]
    array = elem.split(/T\d/)
    return array[0];
  }

}
