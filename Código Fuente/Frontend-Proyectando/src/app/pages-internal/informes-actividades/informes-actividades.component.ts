import { AfterViewInit, Component, ViewChild, OnInit } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { InformesService } from '../../services/informes.service';
import { Router } from '@angular/router';
import { ProyectoI } from '../../models/proyecto';
import { InformesPayloadI } from '../../models/informesPayload';


@Component({
  selector: 'app-informes-actividades',
  templateUrl: './informes-actividades.component.html',
  styleUrls: ['./informes-actividades.component.css']
})
export class InformesActividadesComponent implements OnInit {
  hiddenColumn: boolean[] = [false, true, false, true, true, false, false, true, false, false, false, false, false, false];
  columns: string[] = ['Proyecto', 'Nombre', 'Descripción', 'Fecha Realizacion', 'Beneficiario', 'Pais', 'Ciudad', 'Tipo', 'Aprobar', 'Divulgar', 'Correo', 'Nombres', 'Apellidos', 'Rol'];
  columnsToDisplay: string[] = ['proyecto', 'nombre', 'descripcion', 'fechaRealizacion', 'beneficiario', 'pais', 'ciudad', 'tipo', 'aprobar', 'divulgar', 'correo', 'nombres', 'apellidos', 'rol'];
  data_proyectos:any = [];
  dataSource = new MatTableDataSource();
  load:boolean;

  @ViewChild(MatPaginator, {static:true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static:true} ) sort: MatSort;

  constructor( private informesService:InformesService, private route:Router ){
    this.load = false;
  }

  ngOnInit(): void {
    let payload:InformesPayloadI = this.informesService.getPayload()
    this.informesService.getActividades(payload).subscribe(resp => {
      this.load = true;
      console.log(resp);
      this.data_proyectos = resp;      
      this.dataSource.data = this.data_proyectos;
    }, error => {
      if(error.status === 401){
        this.route.navigate(['/login']);
        localStorage.clear();
      }
    });    
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  typeOf(elem:any):any{
    elem = elem + ''
    let array=[]
    array = elem.split(/T\d/)
    return array[0];
  }
  
}
