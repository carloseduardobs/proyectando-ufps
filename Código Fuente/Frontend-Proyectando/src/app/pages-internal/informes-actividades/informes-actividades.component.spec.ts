import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InformesActividadesComponent } from './informes-actividades.component';

describe('InformesActividadesComponent', () => {
  let component: InformesActividadesComponent;
  let fixture: ComponentFixture<InformesActividadesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InformesActividadesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InformesActividadesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
