import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PaginaInicioComponent } from './pages-public/pagina-inicio/pagina-inicio.component';
import { LoginComponent } from './pages-public/login-usuarios/login.component';
import { DashboardComponent } from './pages-internal/dashboard/dashboard.component';
import { RegisterComponent } from './pages-public/register-usuarios/register.component';
import { ConsultarUsuariosComponent } from './pages-internal/consultar-usuarios/consultar-usuarios.component';
import { CrearProyectoComponent } from './pages-internal/crear-proyecto/crear-proyecto.component';
import { ConsultarActividadesComponent } from './pages-internal/consultar-actividades/consultar-actividades.component';
import { CrearActividadComponent } from './pages-internal/crear-actividad/crear-actividad.component';
import { ConsultarProyectosComponent } from './pages-internal/consultar-proyectos/consultar-proyectos.component';
import { ProyectoComponent } from './pages-internal/proyecto/proyecto.component';
import { PerfilUsuarioComponent } from './pages-internal/perfil-usuario/perfil-usuario.component';
import { ConsultarConveniosComponent } from './pages-internal/consultar-convenios/consultar-convenios.component';
import { PoliticasComponent } from './pages-public/politicas/politicas.component';
import { ContactoComponent } from './pages-public/contacto/contacto.component';
import { PrincipiosComponent } from './pages-public/principios/principios.component';
import { CrearEncuestaComponent } from './pages-internal/crear-encuesta/crear-encuesta.component';
import { InicioDashboardComponent } from './pages-internal/inicio-dashboard/inicio-dashboard.component';
import { InicioComponent } from './pages-public/inicio/inicio.component';
import { PracticasComponent } from './pages-public/practicas/practicas.component';
import { CrearConvenioComponent } from './pages-internal/crear-convenio/crear-convenio.component';
import { ConsultarEncuentasComponent } from './pages-internal/consultar-encuentas/consultar-encuentas.component';
import { ActividadComponent } from './pages-internal/actividad/actividad.component';
import { EncuestaComponent } from './pages-internal/encuesta/encuesta.component';
import { UsuarioComponent } from './pages-internal/usuario/usuario.component';
import { ConvenioComponent } from './pages-internal/convenio/convenio.component';
import { ProyectosLandingComponent } from './pages-public/proyectos/proyectos.component';
import { EventosComponent } from './pages-public/eventos/eventos.component';
import { LoginEncuestaComponent } from './pages-public/login-encuesta/login-encuesta.component';
import { ResponderEncuestaComponent } from './pages-public/responder-encuesta/responder-encuesta.component';
import { LandingConfigComponent } from './pages-internal/landing-config/landing-config.component';
import { RespuestaEncuestaComponent } from './pages-internal/respuesta-encuesta/respuesta-encuesta.component';
import { RespuestasEncuestasComponent } from './pages-internal/respuestas-encuestas/respuestas-encuestas.component';
import { InformesComponent } from './pages-internal/informes/informes.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'proyectando/:parametro', component: PaginaInicioComponent,
    children:[
      { path: '', redirectTo: 'inicio', pathMatch: 'full' },
      { path: 'inicio', component: InicioComponent },
      { path: "politicas", component: PoliticasComponent},
      { path: "contacto", component: ContactoComponent},
      { path: "principios", component: PrincipiosComponent},
      { path: "practicas", component: PracticasComponent},
      { path: "proyectos", component: ProyectosLandingComponent},
      { path: "eventos", component: EventosComponent}

    ]},  
  { path: 'login', component: LoginComponent},
  { path: 'register', component: RegisterComponent},
  { path: 'login-encuesta', component: LoginEncuestaComponent},
  { path: 'responder-encuesta', component: ResponderEncuestaComponent},
  { path: 'dashboard', component: DashboardComponent,
    children:[
      { path: '', redirectTo: 'inicio', pathMatch: 'full' },
      { path: 'inicio', component: InicioDashboardComponent },
      { path: 'usuarios', component: ConsultarUsuariosComponent },
      { path: 'crear-proyecto', component: CrearProyectoComponent },
      { path: 'crear-actividad', component: CrearActividadComponent },      
      { path: 'crear-convenio', component: CrearConvenioComponent },      
      { path: 'crear-encuesta', component: CrearEncuestaComponent },
      { path: 'actividades', component: ConsultarActividadesComponent },
      { path: 'proyectos', component: ConsultarProyectosComponent },
      { path: 'proyecto', component: ProyectoComponent },
      { path: 'actividad', component: ActividadComponent },
      { path: 'encuesta', component: EncuestaComponent },
      { path: 'usuario', component: UsuarioComponent },
      { path: 'convenio', component: ConvenioComponent },
      { path: 'perfil-usuario', component: PerfilUsuarioComponent },
      { path: 'consultar-convenios', component: ConsultarConveniosComponent },
      { path: 'consultar-encuestas', component: ConsultarEncuentasComponent },
      { path: 'consultar-respuestas', component: RespuestasEncuestasComponent },
      { path: 'consultar-respuestas-encuesta', component: RespuestaEncuestaComponent },
      { path: 'landing-config', component: LandingConfigComponent },
      { path: 'informes', component: InformesComponent },
    ]},

  { path: '**', redirectTo: 'proyectando' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
