import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { InformesPayloadI } from '../models/informesPayload';

@Injectable({
  providedIn: 'root'
})
export class InformesService {
  private url:string = environment.url;

  httpHeaders = {
    headers: new HttpHeaders({
      Authorization: 'Bearer ' + localStorage.getItem('token')
    })
  };

  private payload: InformesPayloadI;

  constructor(public http:HttpClient) { }

  public getProyectos(informePayload:InformesPayloadI){
    return this.http.post(`${this.url}informes/proyectos`, informePayload, this.httpHeaders)
    .pipe(
      catchError(this.handleError)
    )
  }

  public getActividades(informePayload:InformesPayloadI){
    return this.http.post(`${this.url}informes/actividades`, informePayload, this.httpHeaders)
    .pipe(
      catchError(this.handleError)
    )
  }

  public getConsultarConvenios(informePayload:InformesPayloadI){
    return this.http.post(`${this.url}informes/convenios`, informePayload, this.httpHeaders)
    .pipe(
      catchError(this.handleError)
    )
  }

  public getUsuarios(informePayload:InformesPayloadI){
    return this.http.post(`${this.url}informes/usuarios`, informePayload, this.httpHeaders)
    .pipe(
      catchError(this.handleError)
    )
  }

  public getRespuestasEncuestas(informePayload:InformesPayloadI){
    return this.http.post(`${this.url}informes/encuestas`, informePayload, this.httpHeaders)
    .pipe(
      catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // Return an observable with a user-facing error message.
    return throwError(error);
  }

  public setPayload(informe:InformesPayloadI) {
    this.payload = informe;
  }

  public getPayload() {
    return this.payload;
  }
}
