import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { ActividadI } from '../models/actividad';
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class ActividadService {

  private url:string = environment.url;

  actividadSelect:ActividadI;

  httpHeaders = {
    headers: new HttpHeaders({
      Authorization: 'Bearer ' + localStorage.getItem('token')
    })
  };

  constructor(public http:HttpClient) { }

  public getActividades(){
    return this.http.get(`${this.url}actividad`, this.httpHeaders)
    .pipe(
      catchError(this.handleError)
    )
  }

  public putModificarActividad(actividad:ActividadI){
    return this.http.put(`${this.url}actividad/modificar`, actividad, this.httpHeaders)
    .pipe(
      catchError(this.handleError)
    )
  }

  public postInsertarActividad(actividad:ActividadI){
    return this.http.post(`${this.url}actividad/insertar`, actividad, this.httpHeaders)
    .pipe(
      catchError(this.handleError)
    )
  }

  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // Return an observable with a user-facing error message.
    return throwError(error);
  }
}
