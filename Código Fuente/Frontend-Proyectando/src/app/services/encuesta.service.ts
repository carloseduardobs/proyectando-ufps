import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EncuestaService {

  private url:string = environment.url;

  encuestaSelect:any;
  encuestaResponder:any;
  respuestaSelect:any;

  httpHeaders = {
    headers: new HttpHeaders({
      Authorization: 'Bearer ' + localStorage.getItem('token')
    })
  };

  constructor(public http:HttpClient) { }


  public getEncuestas(){
    return this.http.get(`${this.url}encuesta`, this.httpHeaders)
    .pipe(
      catchError(this.handleError)
    );
  }

  public getRespuestasEncuestas(id:any){
    return this.http.get(`${this.url}encuesta/listar-respuestas/${id}`, this.httpHeaders)
    .pipe(
      catchError(this.handleError)
    );
  }

  public getRespuestas(id:any){
    return this.http.get(`${this.url}encuesta/cargar-respuesta/${id}`, this.httpHeaders)
    .pipe(
      catchError(this.handleError)
    );
  }

  

  public getEncuestasActivas(){
    return this.http.get(`${this.url}encuesta/activas`, this.httpHeaders)
    .pipe(
      catchError(this.handleError)
    );
  }

  public postInsertarEncuestas(encuesta:any){
    return this.http.post(`${this.url}encuesta/insertar`, encuesta, this.httpHeaders)
    .pipe(
      catchError(this.handleError)
    );
  }

  public deleteEncuesta(id:any){
    return this.http.delete(`${this.url}encuesta/eliminar/${id}`, this.httpHeaders)
    .pipe(
      catchError(this.handleError)
    );
  }

  public putEncuesta(encuesta:any){
    return this.http.put(`${this.url}encuesta/modificar`, encuesta, this.httpHeaders)
    .pipe(
      catchError(this.handleError)
    );
  }

  // Metodos para el manejo del envio y respuesta de encuestas

  public postEnviarEncuesta(enviarEncuesta:any){
    return this.http.post(`${this.url}respuesta-encuesta/enviar`, enviarEncuesta, this.httpHeaders)
    .pipe(
      catchError(this.handleError)
    );
  }

  public postResponderEncuesta(respuestaEncuesta:any){
    return this.http.post(`${this.url}respuesta-encuesta/responder`, respuestaEncuesta, this.httpHeaders)
    .pipe(
      catchError(this.handleError)
    );
  }

  public postValidarEncuesta(credenciales:any){
    return this.http.post(`${this.url}respuesta-encuesta/validar`, credenciales, this.httpHeaders)
    .pipe(
      catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // Return an observable with a user-facing error message.
    return throwError(error);
  }
}
