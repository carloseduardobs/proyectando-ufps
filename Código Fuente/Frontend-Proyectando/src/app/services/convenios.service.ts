import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { ConvenioI } from '../models/convenio';

@Injectable({
  providedIn: 'root'
})
export class ConveniosService {

  private url:string = environment.url;

  convenioSelect:ConvenioI

  httpHeaders = {
    headers: new HttpHeaders({
      Authorization: 'Bearer ' + localStorage.getItem('token')
    })
  };

  constructor(public http:HttpClient) { }

  public getConsultarConvenios(){
    return this.http.get(`${this.url}convenio`, this.httpHeaders)
    .pipe(
      catchError(this.handleError)
    )
  }

  public postRegistrarConvenios(convenio:any){
    return this.http.post(`${this.url}convenio/insertar`, convenio, this.httpHeaders)
    .pipe(
      catchError(this.handleError)
    )
  }

  public putModificarConvenio(convenio:ConvenioI){
    return this.http.put(`${this.url}convenio/modificar`, convenio, this.httpHeaders)
    .pipe(
      catchError(this.handleError)
    )
  }

  

  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // Return an observable with a user-facing error message.
    return throwError(error);
  }
}
