import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LandingPageService {

  private url:string = environment.url;


  httpHeaders = {
    headers: new HttpHeaders({
      Authorization: 'Bearer ' + localStorage.getItem('token')
    })
  };

  constructor(public http:HttpClient) { }


  public getNav(url:string){
    return this.http.get(`${this.url}dashboard/nav/${url}`)
    .pipe(
      catchError(this.handleError)
    )
  }

  public getLogo(url:string){
    return this.http.get(`${this.url}dashboard/logo/${url}`)
    .pipe(
      catchError(this.handleError)
    )
  }
  
  public getContent(url:string, path:string){
    return this.http.get(`${this.url}dashboard/content/${url}/${path}`)
    .pipe(
      catchError(this.handleError)
    )
  }

  public getProyectos(key:string){
    return this.http.get(`${this.url}dashboard/proyectos/${key}`)
    .pipe(
      catchError(this.handleError)
    )
  }

  public getEventos(key:string){
    return this.http.get(`${this.url}dashboard/eventos/${key}`)
    .pipe(
      catchError(this.handleError)
    )
  }

  public getInfoLanding(){
    return this.http.get(`${this.url}informacion-institucional/`, this.httpHeaders)
    .pipe(
      catchError(this.handleError)
    )
  }

  public modificarInfoLanding(param:any){
    return this.http.put(`${this.url}informacion-institucional/modificar`,param, this.httpHeaders)
    .pipe(
      catchError(this.handleError)
    )
  }
  
  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // Return an observable with a user-facing error message.
    return throwError(error);
  }
}
