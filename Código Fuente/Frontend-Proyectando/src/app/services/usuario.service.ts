import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { UserI } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  private url:string = environment.url;

  usuarioSelect:UserI;

  httpHeaders = {
    headers: new HttpHeaders({
      Authorization: 'Bearer ' + localStorage.getItem('token')
    })
  };

  constructor(public http:HttpClient) { }

  public postCambioContrasena(cambioContrasena:any){    
    return this.http.post(`${this.url}usuario/cambio-contrasena`, cambioContrasena, this.httpHeaders)
    .pipe(
      catchError(this.handleError)
    );
  }

  public putModificarUsuario(usuario:UserI){
    return this.http.put(`${this.url}usuario/modificar`, usuario, this.httpHeaders)
    .pipe(
      catchError(this.handleError)
    )
  }

  public getUsuarios(){
    return this.http.get(`${this.url}usuario`, this.httpHeaders)
    .pipe(
      catchError(this.handleError)
    )
  }

  public getUsuarioById(id:string){
    return this.http.get(`${this.url}usuario/${id}`, this.httpHeaders)
    .pipe(
      catchError(this.handleError)
    )
  }

  public getUsuarioConsultar(){
    return this.http.get(`${this.url}usuario/cosultar`, this.httpHeaders)
    .pipe(
      catchError(this.handleError)
    )
  }

  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // Return an observable with a user-facing error message.
    return throwError(error);
  }

}
