import { TestBed } from '@angular/core/testing';

import { EncuentaService } from './encuesta.service';

describe('EncuentaService', () => {
  let service: EncuentaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EncuentaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
