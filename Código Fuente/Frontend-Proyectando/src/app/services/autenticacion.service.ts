import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AutenticacionService {

  private url:string = environment.url;

  constructor(public http:HttpClient) { }

  public postLogin(login:any){    
    return this.http.post(`${this.url}authenticate/login`, login)
    .pipe(
      catchError(this.handleError)
    );
  }

  public postRegistro(registro:any){
    return this.http.post(`${this.url}authenticate/registrar`, registro)
    .pipe(
      catchError(this.handleError)
    )
  }

  public getValidarEmail(email:string){
    return this.http.get(`${this.url}authenticate/validar/${email}`)
    .pipe(
      catchError(this.handleError)
    )
  }

  public getUserRoles(){
    return this.http.get(`${this.url}rol`)
    .pipe(
      catchError(this.handleError)
    )
  }


  
  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // Return an observable with a user-facing error message.
    return throwError(error);
  }
}
