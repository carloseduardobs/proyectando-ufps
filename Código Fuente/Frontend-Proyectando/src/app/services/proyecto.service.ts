import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { ProyectoI } from '../models/proyecto';


@Injectable({
  providedIn: 'root'
})
export class ProyectoService {

  private url:string = environment.url;

  httpHeaders = {
    headers: new HttpHeaders({
      Authorization: 'Bearer ' + localStorage.getItem('token')
    })
  };

  proyectoSelect:ProyectoI;

  constructor(public http:HttpClient) { }

  public postCrearProyecto(proyecto:ProyectoI){    
    return this.http.post(`${this.url}proyecto/insertar`, proyecto, this.httpHeaders)
    .pipe(
      catchError(this.handleError)
    );
  }

  public getProyectos(){
    return this.http.get(`${this.url}proyecto`, this.httpHeaders)
    .pipe(
      catchError(this.handleError)
    )
  }

  public getProyectosById(id:number){
    return this.http.get(`${this.url}proyecto/${id}`, this.httpHeaders)
    .pipe(
      catchError(this.handleError)
    )
  }

  public putModificarProyecto(proyecto:ProyectoI){
    return this.http.put(`${this.url}proyecto/modificar`, proyecto, this.httpHeaders)
    .pipe(
      catchError(this.handleError)
    )
  }

  public getProcesosMisionales(){
    return this.http.get(`${this.url}proceso-misional`, this.httpHeaders)
    .pipe(
      catchError(this.handleError)
    )
  }

  public getTipoProyecto(){
    return this.http.get(`${this.url}tipo-proyecto`, this.httpHeaders)
    .pipe(
      catchError(this.handleError)
    )
  }

  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // Return an observable with a user-facing error message.
    return throwError(error);
  }
}
