import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { EvidenciaProyectoI } from '../models/evidencia-proyecto';

@Injectable({
  providedIn: 'root'
})
export class EvidenciasService {

  private url:string = environment.url;

  showConvenio:boolean;

  httpHeaders = {
    headers: new HttpHeaders({
      Authorization: 'Bearer ' + localStorage.getItem('token')
    })
  };

  constructor(public http:HttpClient) { }

 // Insertar las evidencias de los proyectos
  public postCrearEvidenciaProyecto(evidencia:any){

    return this.http.post(`${this.url}evidencia-proyecto/insertar`, evidencia, this.httpHeaders)
    .pipe(
      catchError(this.handleError)
    );
  }

  // Insertar las evidencias de las actividades
  public postCrearEvidenciaActividad(evidencia:any){

    return this.http.post(`${this.url}evidencia-actividad/insertar`, evidencia, this.httpHeaders)
    .pipe(
      catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // Return an observable with a user-facing error message.
    return throwError(error);
  }

  
}
