import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';


@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnInit {

  userName:any;
  name:any;
  userRol:any;

  isHandset: Observable<BreakpointState> = this.breakpointObserver.observe(Breakpoints.Handset);
  constructor( private breakpointObserver: BreakpointObserver, private route:Router) { }  

  ngOnInit(){
    this.userName = localStorage.getItem('email');
    this.name = localStorage.getItem('name');
    this.userRol = localStorage.getItem('rol');
  }
  
  verPerfil():void{
    this.route.navigate(['/dashboard/perfil-usuario']);
  }

  logout():void {
    localStorage.clear();
    this.route.navigate(['/login']);
  }

}
