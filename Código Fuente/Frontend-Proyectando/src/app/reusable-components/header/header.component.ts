import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import { LandingPageService } from '../../services/landing-page.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

 
  opened: boolean = false;
  menuProyeccion: boolean = true
  listaNavegacion: any = [];
  params: any = "";
  logo: any = "";
  isLoading: boolean = true;
 
  constructor( private breakpointObserver: BreakpointObserver,
    private route:Router,
    private rutaActiva: ActivatedRoute,
    private LandingPageService: LandingPageService) { }  

  ngOnInit(){
    this.params = this.rutaActiva.snapshot.params
    localStorage.setItem('ples', this.params.parametro);
    this.LandingPageService.getNav(this.params.parametro).subscribe(resp => {
      this.listaNavegacion = resp;
      localStorage.setItem('ples', this.params.parametro);
      this.isLoading = false
    },
    error => {
      console.log(error);
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: error.error,
        confirmButtonColor: '#F44336'
      });
      this.route.navigate(['/login']);
      localStorage.clear();
      
    });
    this.LandingPageService.getLogo(this.params.parametro).subscribe(resp => {
      this.logo = resp;
    },
    error => {
      console.log(error);      
    });
  }
  
  

}
