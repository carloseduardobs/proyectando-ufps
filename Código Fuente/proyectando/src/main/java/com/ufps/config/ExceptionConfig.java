package com.ufps.config;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.ufps.exceptions.BadRequestException;
import com.ufps.exceptions.NotFoundException;
import com.ufps.utilities.ApiError;

@ControllerAdvice(annotations = RestController.class)
public class ExceptionConfig extends ResponseEntityExceptionHandler {

	@ExceptionHandler(NotFoundException.class)
	public ResponseEntity<?> noFoundException(Exception e) {
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
	}

	@ExceptionHandler(BadRequestException.class)
	public ResponseEntity<?> badRequestException(Exception e) {
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
	}

	/**
	 * Método que intercepta los errores de atributos marcados con @valid 
	 */
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(
			MethodArgumentNotValidException ex, 
			HttpHeaders headers, 
			HttpStatus status, 
			WebRequest request) {
		List<String> errors = new ArrayList<String>();
		for (FieldError error : ex.getBindingResult().getFieldErrors()) {
			errors.add(error.getField() + ": " + error.getDefaultMessage());
		}
		for (ObjectError error : ex.getBindingResult().getGlobalErrors()) {
			errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
		}

		ApiError apiError = 
				new ApiError(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), errors);
		return ResponseEntity.status(apiError.getStatus()).body(errors);
	}

	/**
	 * Método que intercepta los errores de parámetros requeridos
	 */
	@Override
	protected ResponseEntity<Object> handleMissingServletRequestParameter(
			MissingServletRequestParameterException ex, HttpHeaders headers, 
			HttpStatus status, WebRequest request) {
		String error = "El parámetro \"" + ex.getParameterName() + "\" es requerido";

		ApiError apiError = 
				new ApiError(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), error);
		return ResponseEntity.status(apiError.getStatus()).body(error);
	}

	/**
	 * Método que intercepta los errores de constraint en bd
	 */
	@ExceptionHandler({ ConstraintViolationException.class })
	public ResponseEntity<Object> handleConstraintViolation(
			ConstraintViolationException ex, WebRequest request) {
		List<String> errors = new ArrayList<String>();
		for (ConstraintViolation<?> violation : ex.getConstraintViolations()) {
			errors.add(violation.getRootBeanClass().getName() + " " + 
					violation.getPropertyPath() + ": " + violation.getMessage());
		}

		ApiError apiError = 
				new ApiError(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), errors);
		return ResponseEntity.status(apiError.getStatus()).body(errors);
	}

	/**
	 * Método que intercepta los errores de tipo de parámetro
	 */
	@ExceptionHandler({ MethodArgumentTypeMismatchException.class })
	public ResponseEntity<Object> handleMethodArgumentTypeMismatch(
			MethodArgumentTypeMismatchException ex, WebRequest request) {
		String error = "El parámetro \""
				+ ex.getValue().toString()
				+ "\" debe ser de tipo "
				+ ex.getRequiredType().getName();

		ApiError apiError = 
				new ApiError(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), error);
		return ResponseEntity.status(apiError.getStatus()).body(error);
	}

}
