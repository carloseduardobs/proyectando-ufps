package com.ufps.security.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ufps.entity.UsuarioEntity;
import com.ufps.repository.UsuarioRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	@Autowired
	UsuarioRepository userRepository;

	@Override
	@Transactional
	public UserDetailsImpl loadUserByUsername(String correo) throws UsernameNotFoundException {
		UsuarioEntity user = userRepository.findByCorreo(correo)
				.orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + correo));

		return UserDetailsImpl.build(user);
	}

}
