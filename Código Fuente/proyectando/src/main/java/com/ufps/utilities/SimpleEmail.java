package com.ufps.utilities;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.amazonaws.services.simpleemail.model.Body;
import com.amazonaws.services.simpleemail.model.Content;
import com.amazonaws.services.simpleemail.model.Destination;
import com.amazonaws.services.simpleemail.model.Message;
import com.amazonaws.services.simpleemail.model.SendEmailRequest;

public class SimpleEmail {

	private static final Logger logger = LoggerFactory.getLogger(SimpleEmail.class);

	public SimpleEmail() {
		
	}

	public boolean sendEmail(Map<String, String> parameters) {
		boolean resul = true;
		final String FROM = Constantes.FROM_EMAIL;
		final String TO = parameters.get("to");
		final String SUBJECT = parameters.get("subject");
		final String HTMLBODY = parameters.get("message");
		final String TEXTBODY = "This email was sent through Amazon SES " + "using the AWS SDK for Java.";
		try {
			AmazonSimpleEmailService client = AmazonSimpleEmailServiceClientBuilder.standard()
					.withRegion(Regions.US_EAST_2).build();
			SendEmailRequest request = new SendEmailRequest().withDestination(new Destination().withToAddresses(TO))
					.withMessage(new Message()
							.withBody(new Body().withHtml(new Content().withCharset("UTF-8").withData(HTMLBODY))
									.withText(new Content().withCharset("UTF-8").withData(TEXTBODY)))
							.withSubject(new Content().withCharset("UTF-8").withData(SUBJECT)))
					.withSource(FROM);
			client.sendEmail(request);
			resul = true;
		} catch (Exception ex) {
			logger.error("The email was not sent. Error message: " + ex.getMessage());
			resul = false;
		}
		return resul;
	}

}
