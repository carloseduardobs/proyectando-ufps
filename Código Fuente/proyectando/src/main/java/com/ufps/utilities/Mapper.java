package com.ufps.utilities;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.ufps.entity.ActividadEntity;
import com.ufps.entity.ConvenioEntity;
import com.ufps.entity.EncuestaEntity;
import com.ufps.entity.EvidenciaActividadEntity;
import com.ufps.entity.EvidenciaEntity;
import com.ufps.entity.EvidenciaProyectoEntity;
import com.ufps.entity.InformacionInstitucionalEntity;
import com.ufps.entity.PlanEstudiosEntity;
import com.ufps.entity.PreguntaEntity;
import com.ufps.entity.ProyectoEntity;
import com.ufps.entity.RespuestaEncuestaEntity;
import com.ufps.entity.RespuestaPreguntaEntity;
import com.ufps.entity.UsuarioEntity;
import com.ufps.payload.UsuarioRegistroPayload;
import com.ufps.payload.ActividadDashboardPayload;
import com.ufps.payload.ActividadPayload;
import com.ufps.payload.ActividadRegistroPayload;
import com.ufps.payload.ConvenioPayload;
import com.ufps.payload.ConvenioRegistroPayload;
import com.ufps.payload.ConveniosDashboardPayload;
import com.ufps.payload.EncuestaPayload;
import com.ufps.payload.EncuestaRegistroPayload;
import com.ufps.payload.EncuestaResponsePayload;
import com.ufps.payload.EvidenciaActividadDashboardPayload;
import com.ufps.payload.EvidenciaActividadPayload;
import com.ufps.payload.EvidenciaActividadRegistroPayload;
import com.ufps.payload.EvidenciaPayload;
import com.ufps.payload.EvidenciaProyectoDashboardPayload;
import com.ufps.payload.EvidenciaProyectoPayload;
import com.ufps.payload.EvidenciaProyectoRegistroPayload;
import com.ufps.payload.EvidenciaRegistroPayload;
import com.ufps.payload.InformacionInstitucionalPayload;
import com.ufps.payload.InformacionInstitucionalRegistroPayload;
import com.ufps.payload.InformeActividadPayload;
import com.ufps.payload.InformeConvenioPayload;
import com.ufps.payload.InformeEncuestaPayload;
import com.ufps.payload.InformeProyectoPayload;
import com.ufps.payload.InformeUsuarioPayload;
import com.ufps.payload.PlanEstudiosPayload;
import com.ufps.payload.PlanEstudiosRegistroPayload;
import com.ufps.payload.PreguntaPayload;
import com.ufps.payload.PreguntaRegistroPayload;
import com.ufps.payload.ProyectoDashboardPayload;
import com.ufps.payload.ProyectoPayload;
import com.ufps.payload.ProyectoRegistroPayload;
import com.ufps.payload.ProyectoReport;
import com.ufps.payload.RespuestaEncuestaPayload;
import com.ufps.payload.RespuestaEncuestaRegistroPayload;
import com.ufps.payload.RespuestaPreguntaPayload;
import com.ufps.payload.UsuarioConsultarPayload;
import com.ufps.payload.UsuarioPayload;

public class Mapper {


	public static UsuarioEntity usuarioRegistryPayloadToEntity(UsuarioRegistroPayload user) {
		if (user == null) {
			return null;
		}
		UsuarioEntity entity = new UsuarioEntity();
		entity.setApellidos(user.getApellidos());
		entity.setContrasena(user.getContrasena());
		entity.setCorreo(user.getCorreo());
		entity.setIdPlanEstudio(user.getIdPlanEstudio());
		entity.setNombres(user.getNombres());
		entity.setEstado(Constantes.ACTIVO);
		return entity;
	}

	public static UsuarioEntity usuarioPayloadToEntity(UsuarioEntity entity, UsuarioPayload user) {
		if (user == null) {
			return null;
		}
		if (entity == null) {
			entity = new UsuarioEntity();
		}
		entity.setId(user.getId());
		entity.setApellidos(user.getApellidos());
		entity.setCorreo(user.getCorreo());
		entity.setIdPlanEstudio(user.getIdPlanEstudio());
		entity.setNombres(user.getNombres());
		entity.setEstado(user.getEstado());
		return entity;		
	}

	public static UsuarioConsultarPayload entityToUsuarioConsultarPayload(UsuarioEntity entity) {
		if (entity == null) {
			return null;
		}
		UsuarioConsultarPayload user = new UsuarioConsultarPayload();
		user.setId(entity.getId());
		user.setApellidos(entity.getApellidos());
		user.setCorreo(entity.getCorreo());
		user.setIdPlanEstudio(entity.getIdPlanEstudio());
		user.setNombres(entity.getNombres());
		user.setEstado(entity.getEstado());
		List<Integer> roles = entity.getRoles().stream()
				.map(item -> item.getId())
				.collect(Collectors.toList());

		user.setIdRol(roles.get(0));
		return user;		
	}

	public static UsuarioPayload entityToUsuarioPayload(UsuarioEntity entity) {
		if (entity == null) {
			return null;
		}
		UsuarioPayload user = new UsuarioPayload();
		user.setId(entity.getId());
		user.setApellidos(entity.getApellidos());
		user.setCorreo(entity.getCorreo());
		user.setIdPlanEstudio(entity.getIdPlanEstudio());
		user.setNombres(entity.getNombres());
		user.setEstado(entity.getEstado());
		List<Integer> roles = entity.getRoles().stream()
				.map(item -> item.getId())
				.collect(Collectors.toList());

		user.setIdRol(roles.get(0));
		return user;		
	}

	public static ProyectoEntity proyectoRegistroPayloadToEntity(ProyectoRegistroPayload proyecto) {
		if (proyecto == null) {
			return null;
		}
		ProyectoEntity entity = new ProyectoEntity();
		entity.setNombre(proyecto.getNombre());
		entity.setDescripcion(proyecto.getDescripcion());
		entity.setAprobar(Constantes.NOAPROBAR);
		entity.setBeneficiario(proyecto.getBeneficiario());
		entity.setDivulgar(Constantes.NODIVULGAR);
		entity.setFechaFin(proyecto.getFechaFin());
		entity.setFechaInicio(proyecto.getFechaInicio());
		entity.setIdPlanEstudios(proyecto.getIdPlanEstudios());
		entity.setIdProcesoMisional(proyecto.getIdProcesoMisional());
		entity.setIdTipoProyecto(proyecto.getIdTipoProyecto());
		entity.setMonto(proyecto.getMonto());
		return entity;
	}

	public static ProyectoEntity proyectoPayloadToEntity(ProyectoPayload proyecto) {
		if (proyecto == null) {
			return null;
		}
		ProyectoEntity entity = new ProyectoEntity();
		entity.setNombre(proyecto.getNombre());
		entity.setDescripcion(proyecto.getDescripcion());
		entity.setAprobar(proyecto.getAprobar());
		entity.setBeneficiario(proyecto.getBeneficiario());
		entity.setDivulgar(proyecto.getDivulgar());
		entity.setFechaFin(proyecto.getFechaFin());
		entity.setFechaInicio(proyecto.getFechaInicio());
		entity.setIdPlanEstudios(proyecto.getIdPlanEstudios());
		entity.setIdProcesoMisional(proyecto.getIdProcesoMisional());
		entity.setIdTipoProyecto(proyecto.getIdTipoProyecto());
		entity.setMonto(proyecto.getMonto());
		entity.setRegistradoPor(proyecto.getRegistradoPor());
		entity.setId(proyecto.getId());
		return entity;
	}

	public static ProyectoPayload proyectoEntityToPayload(ProyectoEntity entity) {
		if (entity == null) {
			return null;
		}
		ProyectoPayload proyecto = new ProyectoPayload();
		proyecto.setNombre(entity.getNombre());
		proyecto.setDescripcion(entity.getDescripcion());
		proyecto.setAprobar(entity.getAprobar());
		proyecto.setBeneficiario(entity.getBeneficiario());
		proyecto.setDivulgar(entity.getDivulgar());
		proyecto.setFechaFin(entity.getFechaFin());
		proyecto.setFechaInicio(entity.getFechaInicio());
		proyecto.setIdPlanEstudios(entity.getIdPlanEstudios());
		proyecto.setIdProcesoMisional(entity.getIdProcesoMisional());
		proyecto.setIdTipoProyecto(entity.getIdTipoProyecto());
		proyecto.setMonto(entity.getMonto());
		proyecto.setRegistradoPor(entity.getRegistradoPor());
		proyecto.setId(entity.getId());
		return proyecto;
	}

	public static ProyectoReport proyectoEntityToReport(ProyectoEntity entity) {
		if (entity == null) {
			return null;
		}
		ProyectoReport proyecto = new ProyectoReport();
		proyecto.setNombre(entity.getNombre());
		proyecto.setDescripcion(entity.getDescripcion());
		proyecto.setBeneficiario(entity.getBeneficiario());
		proyecto.setFechafin(entity.getFechaFin().toString());
		proyecto.setFechainicio(entity.getFechaInicio().toString());
		proyecto.setId(entity.getId());
		return proyecto;
	}

	public static ActividadEntity actividadRegistroPayloadToEntity(ActividadRegistroPayload actividad) {
		if (actividad == null) {
			return null;
		}
		ActividadEntity entity = new ActividadEntity();
		entity.setFechaRealizacion(actividad.getFechaRealizacion());
		entity.setNombre(actividad.getNombre());
		entity.setDescripcion(actividad.getDescripcion());
		entity.setAprobar(Constantes.NOAPROBAR);
		entity.setDivulgar(Constantes.NODIVULGAR);
		entity.setIdProyecto(actividad.getIdProyecto());
		entity.setCiudad(actividad.getCiudad());
		entity.setPais(actividad.getPais());
		entity.setBeneficiario(actividad.getBeneficiario());
		entity.setTipo(actividad.getTipo());
		return entity;
	}

	public static ActividadEntity actividadPayloadToEntity(ActividadPayload actividad) {
		if (actividad == null) {
			return null;
		}
		ActividadEntity entity = new ActividadEntity();
		entity.setFechaRealizacion(actividad.getFechaRealizacion());
		entity.setNombre(actividad.getNombre());
		entity.setDescripcion(actividad.getDescripcion());
		entity.setAprobar(actividad.getAprobar());
		entity.setDivulgar(actividad.getDivulgar());
		entity.setIdProyecto(actividad.getIdProyecto());
		entity.setCiudad(actividad.getCiudad());
		entity.setPais(actividad.getPais());
		entity.setRegistradoPor(actividad.getRegistradoPor());
		entity.setId(actividad.getId());
		entity.setBeneficiario(actividad.getBeneficiario());
		entity.setTipo(actividad.getTipo());
		return entity;
	}

	public static ActividadPayload actividadEntityToPayload(ActividadEntity entity) {
		if (entity == null) {
			return null;
		}
		ActividadPayload actividad = new ActividadPayload();
		actividad.setFechaRealizacion(entity.getFechaRealizacion());
		actividad.setNombre(entity.getNombre());
		actividad.setDescripcion(entity.getDescripcion());
		actividad.setAprobar(entity.getAprobar());
		actividad.setDivulgar(entity.getDivulgar());
		actividad.setIdProyecto(entity.getIdProyecto());
		actividad.setCiudad(entity.getCiudad());
		actividad.setPais(entity.getPais());
		actividad.setRegistradoPor(entity.getRegistradoPor());
		actividad.setId(entity.getId());
		actividad.setBeneficiario(entity.getBeneficiario());
		actividad.setTipo(entity.getTipo());
		return actividad;
	}

	public static ConvenioEntity convenioRegistroPayloadToEntity(ConvenioRegistroPayload convenio) {
		if (convenio == null) {
			return null;
		}
		ConvenioEntity entity = new ConvenioEntity();
		entity.setEvidencia(convenio.getEvidencia());
		entity.setExtEvidencia(convenio.getExtEvidencia());
		entity.setFechaRealizacion(convenio.getFechaRealizacion());
		entity.setIdProyecto(convenio.getIdProyecto());
		entity.setNombre(convenio.getNombre());
		entity.setNombreEvidencia(convenio.getNombreEvidencia());
		entity.setRepresentante(convenio.getRepresentante());
		entity.setAprobar(Constantes.NOAPROBAR);
		entity.setDivulgar(Constantes.NODIVULGAR);
		return entity;
	}

	public static ConvenioEntity convenioPayloadToEntity(ConvenioPayload convenio) {
		if (convenio == null) {
			return null;
		}
		ConvenioEntity entity = new ConvenioEntity();
		entity.setEvidencia(convenio.getEvidencia());
		entity.setExtEvidencia(convenio.getExtEvidencia());
		entity.setFechaRealizacion(convenio.getFechaRealizacion());
		entity.setIdProyecto(convenio.getIdProyecto());
		entity.setNombre(convenio.getNombre());
		entity.setNombreEvidencia(convenio.getNombreEvidencia());
		entity.setRepresentante(convenio.getRepresentante());
		entity.setAprobar(convenio.getAprobar());
		entity.setDivulgar(convenio.getDivulgar());
		entity.setId(convenio.getId());
		entity.setRegistradoPor(convenio.getRegistradoPor());
		return entity;
	}

	public static ConvenioPayload convenioEntityToPayload(ConvenioEntity entity) {
		if (entity == null) {
			return null;
		}
		ConvenioPayload convenio = new ConvenioPayload();
		convenio.setEvidencia(entity.getEvidencia());
		convenio.setExtEvidencia(entity.getExtEvidencia());
		convenio.setFechaRealizacion(entity.getFechaRealizacion());
		convenio.setIdProyecto(entity.getIdProyecto());
		convenio.setNombre(entity.getNombre());
		convenio.setNombreEvidencia(entity.getNombreEvidencia());
		convenio.setRepresentante(entity.getRepresentante());
		convenio.setAprobar(entity.getAprobar());
		convenio.setDivulgar(entity.getDivulgar());
		convenio.setId(entity.getId());
		convenio.setRegistradoPor(entity.getRegistradoPor());
		return convenio;
	}

	public static EvidenciaEntity evidenciaRegistroPayloadToEntity(EvidenciaRegistroPayload evidencia) {
		if (evidencia == null) {
			return null;
		}
		EvidenciaEntity entity = new EvidenciaEntity();
		entity.setArchivo(evidencia.getArchivo());
		entity.setExtension(evidencia.getExtension());	
		entity.setNombre(evidencia.getNombre());
		return entity;
	}

	public static EvidenciaEntity evidenciaPayloadToEntity(EvidenciaPayload evidencia) {
		if (evidencia == null) {
			return null;
		}
		EvidenciaEntity entity = new EvidenciaEntity();
		if ((Integer) evidencia.getId() != null && evidencia.getId() != 0) {
			entity.setId(evidencia.getId());	
		} 
		entity.setArchivo(evidencia.getArchivo());
		entity.setExtension(evidencia.getExtension());
		if ((Integer) evidencia.getIdEvidenciaActividad() != null && evidencia.getIdEvidenciaActividad() != 0) {
			entity.setIdEvidenciaActividad(evidencia.getIdEvidenciaActividad());	
		} 
		if ((Integer) evidencia.getIdEvidenciaProyecto() != null && evidencia.getIdEvidenciaProyecto() != 0) {
			entity.setIdEvidenciaProyecto(evidencia.getIdEvidenciaProyecto());	
		}
		entity.setNombre(evidencia.getNombre());
		return entity;
	}

	public static EvidenciaPayload evidenciaEntityToPayload(EvidenciaEntity entity) {
		if (entity == null) {
			return null;
		}
		EvidenciaPayload evidencia = new EvidenciaPayload();
		evidencia.setId(entity.getId());
		evidencia.setArchivo(entity.getArchivo());
		evidencia.setExtension(entity.getExtension());
		if (entity.getIdEvidenciaActividad() != null) {
			evidencia.setIdEvidenciaActividad(entity.getIdEvidenciaActividad());	
		} 
		if (entity.getIdEvidenciaProyecto() != null) {
			evidencia.setIdEvidenciaProyecto(entity.getIdEvidenciaProyecto());	
		}
		evidencia.setNombre(entity.getNombre());
		return evidencia;
	}

	public static EvidenciaProyectoEntity evidenciaProyectoRegistroPayloadToEntity(EvidenciaProyectoRegistroPayload evidencia) {
		if (evidencia == null) {
			return null;
		}
		EvidenciaProyectoEntity entity = new EvidenciaProyectoEntity();
		entity.setAprobar(Constantes.NOAPROBAR);
		entity.setDivulgar(Constantes.NODIVULGAR);
		entity.setFechaRealizacion(evidencia.getFechaRealizacion());
		entity.setIdProyecto(evidencia.getIdProyecto());
		entity.setNombre(evidencia.getNombre());
		return entity;
	}

	public static EvidenciaProyectoEntity evidenciaProyectoPayloadToEntity(EvidenciaProyectoPayload evidencia) {
		if (evidencia == null) {
			return null;
		}
		EvidenciaProyectoEntity entity = new EvidenciaProyectoEntity();
		entity.setAprobar(evidencia.getAprobar());
		entity.setDivulgar(evidencia.getDivulgar());
		entity.setFechaRealizacion(evidencia.getFechaRealizacion());
		entity.setIdProyecto(evidencia.getIdProyecto());
		entity.setNombre(evidencia.getNombre());
		entity.setId(evidencia.getId());
		entity.setRegistradoPor(evidencia.getRegistradoPor());
		return entity;
	}

	public static EvidenciaProyectoPayload evidenciaProyectoEntityToPayload(EvidenciaProyectoEntity entity) {
		if (entity == null) {
			return null;
		}
		EvidenciaProyectoPayload evidencia = new EvidenciaProyectoPayload();
		evidencia.setAprobar(entity.getAprobar());
		evidencia.setDivulgar(entity.getDivulgar());
		evidencia.setFechaRealizacion(entity.getFechaRealizacion());
		evidencia.setIdProyecto(entity.getIdProyecto());
		evidencia.setNombre(entity.getNombre());
		evidencia.setId(entity.getId());
		evidencia.setRegistradoPor(entity.getRegistradoPor());
		return evidencia;
	}

	public static EvidenciaActividadEntity evidenciaActividadRegistroPayloadToEntity(EvidenciaActividadRegistroPayload evidencia) {
		if (evidencia == null) {
			return null;
		}
		EvidenciaActividadEntity entity = new EvidenciaActividadEntity();
		entity.setAprobar(Constantes.NOAPROBAR);
		entity.setDivulgar(Constantes.NODIVULGAR);
		entity.setFechaRealizacion(evidencia.getFechaRealizacion());
		entity.setIdActividad(evidencia.getIdActividad());
		entity.setNombre(evidencia.getNombre());
		return entity;
	}

	public static EvidenciaActividadEntity evidenciaActividadPayloadToEntity(EvidenciaActividadPayload evidencia) {
		if (evidencia == null) {
			return null;
		}
		EvidenciaActividadEntity entity = new EvidenciaActividadEntity();
		entity.setAprobar(evidencia.getAprobar());
		entity.setDivulgar(evidencia.getDivulgar());
		entity.setFechaRealizacion(evidencia.getFechaRealizacion());
		entity.setIdActividad(evidencia.getIdActividad());
		entity.setNombre(evidencia.getNombre());
		entity.setId(evidencia.getId());
		entity.setRegistradoPor(evidencia.getRegistradoPor());
		return entity;
	}

	public static EvidenciaActividadPayload evidenciaActividadEntityToPayload(EvidenciaActividadEntity entity) {
		if (entity == null) {
			return null;
		}
		EvidenciaActividadPayload evidencia = new EvidenciaActividadPayload();
		evidencia.setAprobar(entity.getAprobar());
		evidencia.setDivulgar(entity.getDivulgar());
		evidencia.setFechaRealizacion(entity.getFechaRealizacion());
		evidencia.setIdActividad(entity.getIdActividad());
		evidencia.setNombre(entity.getNombre());
		evidencia.setId(entity.getId());
		evidencia.setRegistradoPor(entity.getRegistradoPor());
		return evidencia;
	}
	
	public static PlanEstudiosEntity planEstudioPayloadToEntity(PlanEstudiosPayload payload) {
		if (payload == null) {
			return null;
		}
		PlanEstudiosEntity entity = new PlanEstudiosEntity();
		entity.setId(payload.getId());
		entity.setNombre(payload.getNombre());
		entity.setUrl(payload.getUrl());
		return entity;
	}

	public static PlanEstudiosEntity planEstudioRegistroPayloadToEntity(PlanEstudiosRegistroPayload payload) {
		if (payload == null) {
			return null;
		}
		PlanEstudiosEntity entity = new PlanEstudiosEntity();
		entity.setNombre(payload.getNombre());
		entity.setUrl(payload.getUrl());
		return entity;
	}
	
	public static PlanEstudiosPayload planEstudioEntityToPayload(PlanEstudiosEntity entity) {
		if (entity == null) {
			return null;
		}
		PlanEstudiosPayload payload = new PlanEstudiosPayload();
		payload.setId(entity.getId());
		payload.setNombre(entity.getNombre());
		payload.setUrl(entity.getUrl());
		return payload;
	}
	
	public static InformacionInstitucionalEntity informacionInstitucionalPayloadToEntity(InformacionInstitucionalPayload payload) {
		if (payload == null) {
			return null;
		}
		InformacionInstitucionalEntity entity = new InformacionInstitucionalEntity();
		entity.setDivulgar(payload.getDivulgar());
		entity.setId(payload.getId());
		entity.setIdPlanEstudio(payload.getIdPlanEstudio());
		entity.setInformacion(payload.getInformacion());
		entity.setKey(payload.getKey());
		entity.setTitulo(payload.getTitulo());
		return entity;
	}

	public static InformacionInstitucionalEntity informacionInstitucionalRegistroPayloadToEntity(InformacionInstitucionalRegistroPayload payload) {
		if (payload == null) {
			return null;
		}
		InformacionInstitucionalEntity entity = new InformacionInstitucionalEntity();
		entity.setDivulgar(Constantes.NODIVULGAR);
		entity.setIdPlanEstudio(payload.getIdPlanEstudio());
		entity.setInformacion(payload.getInformacion());
		entity.setKey(payload.getKey());
		entity.setTitulo(payload.getTitulo());
		return entity;
	}
	
	public static InformacionInstitucionalPayload informacionInstitucionalEntityToPayload(InformacionInstitucionalEntity entity) {
		if (entity == null) {
			return null;
		}
		InformacionInstitucionalPayload payload = new InformacionInstitucionalPayload();
		payload.setDivulgar(entity.getDivulgar());
		payload.setId(entity.getId());
		payload.setIdPlanEstudio(entity.getIdPlanEstudio());
		payload.setInformacion(entity.getInformacion());
		payload.setKey(entity.getKey());
		payload.setTitulo(entity.getTitulo());
		return payload;
	}

	public static ProyectoDashboardPayload proyectoDashboard(ProyectoEntity entity) {
		if (entity == null) {
			return null;
		}
		ProyectoDashboardPayload payload = new ProyectoDashboardPayload();
		payload.setId(entity.getId());
		payload.setNombre(entity.getNombre());
		payload.setDescripcion(entity.getDescripcion());
		payload.setFechaFin(entity.getFechaFin());
		payload.setFechaInicio(entity.getFechaInicio());
		payload.setBeneficiario(entity.getBeneficiario());
		return payload;
	}

	public static ActividadDashboardPayload actividadDashboard(ActividadEntity entity) {
		if (entity == null) {
			return null;
		}
		ActividadDashboardPayload payload = new ActividadDashboardPayload();
		payload.setId(entity.getId());
		payload.setNombre(entity.getNombre());
		payload.setDescripcion(entity.getDescripcion());
		return payload;
	}

	public static ConveniosDashboardPayload convenioDashboard(ConvenioEntity entity) {
		if (entity == null) {
			return null;
		}
		ConveniosDashboardPayload payload = new ConveniosDashboardPayload();
		payload.setNombre(entity.getNombre());
		payload.setNombreEvidencia(entity.getNombreEvidencia());
		payload.setEvidencia(entity.getEvidencia());
		payload.setExtEvidencia(entity.getExtEvidencia());
		return payload;
	}

	public static EvidenciaProyectoDashboardPayload evidenciaProyectoDashboard(EvidenciaProyectoEntity entity) {
		if (entity == null) {
			return null;
		}
		EvidenciaProyectoDashboardPayload payload = new EvidenciaProyectoDashboardPayload();
		payload.setId(entity.getId());
		payload.setFechaRealizacion(entity.getFechaRealizacion());
		payload.setNombre(entity.getNombre());
		return payload;
	}

	public static EvidenciaActividadDashboardPayload evidenciaActividadDashboard(EvidenciaActividadEntity entity) {
		if (entity == null) {
			return null;
		}
		EvidenciaActividadDashboardPayload payload = new EvidenciaActividadDashboardPayload();
		payload.setId(entity.getId());
		payload.setFechaRealizacion(entity.getFechaRealizacion());
		payload.setNombre(entity.getNombre());
		return payload;
	}

	public static EncuestaEntity encuestaPayloadToEntity(EncuestaPayload payload) {
		if (payload == null) {
			return null;
		}
		EncuestaEntity entity = new EncuestaEntity();
		entity.setEstado(payload.getEstado());
		entity.setFechaRealizacion(payload.getFechaRealizacion());
		entity.setId(payload.getId());
		entity.setIdPlanEstudio(payload.getIdPlanEstudio());
		entity.setNombre(payload.getNombre());
		return entity;
	}

	public static EncuestaEntity encuestaRegistroPayloadToEntity(EncuestaRegistroPayload payload) {
		if (payload == null) {
			return null;
		}
		EncuestaEntity entity = new EncuestaEntity();
		entity.setEstado(Constantes.ACTIVO);
		entity.setFechaRealizacion(payload.getFechaRealizacion());
		entity.setNombre(payload.getNombre());
		return entity;
	}
	
	public static EncuestaPayload encuestaEntityToPayload(EncuestaEntity entity) {
		if (entity == null) {
			return null;
		}
		EncuestaPayload payload = new EncuestaPayload();
		payload.setEstado(entity.getEstado());
		payload.setFechaRealizacion(entity.getFechaRealizacion());
		payload.setId(entity.getId());
		payload.setIdPlanEstudio(entity.getIdPlanEstudio());
		payload.setNombre(entity.getNombre());
		return payload;
	}
	
	public static EncuestaResponsePayload encuestaEntityToResponsePayload(EncuestaEntity entity) {
		if (entity == null) {
			return null;
		}
		EncuestaResponsePayload payload = new EncuestaResponsePayload();
		payload.setEstado(entity.getEstado());
		payload.setFechaRealizacion(entity.getFechaRealizacion());
		payload.setId(entity.getId());
		payload.setIdPlanEstudio(entity.getIdPlanEstudio());
		payload.setNombre(entity.getNombre());
		return payload;
	}

	public static PreguntaEntity preguntaPayloadToEntity(PreguntaPayload payload) {
		if (payload == null) {
			return null;
		}
		PreguntaEntity entity = new PreguntaEntity();
		entity.setId(payload.getId());
		entity.setEnunciado(payload.getEnunciado());
		entity.setIdEncuesta(payload.getIdEncuesta());
		entity.setOpciones(payload.getOpciones());
		entity.setTipoPregunta(payload.getTipoPregunta());
		return entity;
	}

	public static PreguntaEntity preguntaRegistroPayloadToEntity(PreguntaRegistroPayload payload) {
		if (payload == null) {
			return null;
		}
		PreguntaEntity entity = new PreguntaEntity();
		entity.setEnunciado(payload.getEnunciado());
		entity.setOpciones(payload.getOpciones());
		entity.setTipoPregunta(payload.getTipoPregunta());
		return entity;
	}
	
	public static PreguntaPayload preguntaEntityToPayload(PreguntaEntity entity) {
		if (entity == null) {
			return null;
		}
		PreguntaPayload payload = new PreguntaPayload();
		payload.setId(entity.getId());
		payload.setEnunciado(entity.getEnunciado());
		payload.setIdEncuesta(entity.getIdEncuesta());
		payload.setOpciones(entity.getOpciones());
		payload.setTipoPregunta(entity.getTipoPregunta());
		return payload;
	}

	public static RespuestaEncuestaEntity respuestaEncuestaPayloadToEntity(RespuestaEncuestaPayload payload) {
		if (payload == null) {
			return null;
		}
		RespuestaEncuestaEntity entity = new RespuestaEncuestaEntity();
		entity.setCorreo(payload.getCorreo());
		entity.setEstado(payload.getEstado());
		entity.setFechaEnvio(payload.getFechaEnvio());
		entity.setFechaRealizacion(payload.getFechaRealizacion());
		entity.setId(payload.getId());
		entity.setIdEncuesta(payload.getIdEncuesta());
		entity.setIdProyecto(payload.getIdProyecto());
		entity.setKey(payload.getKey());
		return entity;
	}

	public static RespuestaEncuestaEntity respuestaEncuestaRegistroPayloadToEntity(RespuestaEncuestaRegistroPayload payload) {
		if (payload == null) {
			return null;
		}
		RespuestaEncuestaEntity entity = new RespuestaEncuestaEntity();
		entity.setCorreo(payload.getCorreo());
		entity.setEstado(Constantes.PENDIENTE);
		entity.setFechaEnvio(new Date());
		entity.setIdEncuesta(payload.getIdEncuesta());
		entity.setIdProyecto(payload.getIdProyecto());
		return entity;
	}
	
	public static RespuestaEncuestaPayload respuestaEncuestaEntityToPayload(RespuestaEncuestaEntity entity) {
		if (entity == null) {
			return null;
		}
		RespuestaEncuestaPayload payload = new RespuestaEncuestaPayload();
		payload.setCorreo(entity.getCorreo());
		payload.setEstado(entity.getEstado());
		payload.setFechaEnvio(entity.getFechaEnvio());
		payload.setFechaRealizacion(entity.getFechaRealizacion());
		payload.setId(entity.getId());
		payload.setIdEncuesta(entity.getIdEncuesta());
		payload.setIdProyecto(entity.getIdProyecto());
		payload.setKey(entity.getKey());
		return payload;
	}

	public static RespuestaPreguntaEntity respuestaPreguntaPayloadToEntity(RespuestaPreguntaPayload payload) {
		if (payload == null) {
			return null;
		}
		RespuestaPreguntaEntity entity = new RespuestaPreguntaEntity();
		entity.setIdPregunta(payload.getIdPregunta());
		entity.setIdRespuestaEncuesta(payload.getIdRespuestaEncuesta());
		entity.setRespuestas(payload.getRespuestas());
		return entity;
	}

	
	public static RespuestaPreguntaPayload respuestaPreguntaEntityToPayload(RespuestaPreguntaEntity entity) {
		if (entity == null) {
			return null;
		}
		RespuestaPreguntaPayload payload = new RespuestaPreguntaPayload();
		payload.setIdPregunta(entity.getIdPregunta());
		payload.setIdRespuestaEncuesta(entity.getIdRespuestaEncuesta());
		payload.setRespuestas(entity.getRespuestas());
		return payload;
	}
	
	public static InformeProyectoPayload objectToInformeProyectoPayload(Object[] obj) {
		if (obj == null) {
			return null;
		}
		InformeProyectoPayload info = new InformeProyectoPayload();
		info.setNombre(obj[0].toString());
		info.setDescripcion(obj[1].toString());
		info.setMonto(obj[2].toString());
		info.setBeneficiario(obj[3].toString());
		info.setAprobar(obj[4].toString());
		info.setDivulgar(obj[5].toString());
		info.setFechaInicio(obj[6].toString());
		info.setFechaFin(obj[7].toString());
		info.setTipoProyecto(obj[8].toString());
		info.setProcesoMisional(obj[9].toString());
		info.setCorreo(obj[10].toString());
		info.setNombres(obj[11].toString());
		info.setApellidos(obj[12].toString());
		info.setRol(obj[13].toString());
		return info;
	}

	public static InformeActividadPayload objectToInformeActividadPayload(Object[] obj) {
		if (obj == null) {
			return null;
		}
		InformeActividadPayload info = new InformeActividadPayload();
		info.setProyecto(obj[0].toString());
		info.setNombre(obj[1].toString());
		info.setDescripcion(obj[2].toString());
		info.setFechaRealizacion(obj[3].toString());
		info.setBeneficiario(obj[4].toString());
		info.setPais(obj[5].toString());
		info.setCiudad(obj[6].toString());
		info.setTipo(obj[7].toString());
		info.setAprobar(obj[8].toString());
		info.setDivulgar(obj[9].toString());
		info.setCorreo(obj[10].toString());
		info.setNombres(obj[11].toString());
		info.setApellidos(obj[12].toString());
		info.setRol(obj[13].toString()); 
		return info;
	}

	public static InformeConvenioPayload objectToInformeConvenioPayload(Object[] obj) {
		if (obj == null) {
			return null;
		}
		InformeConvenioPayload info = new InformeConvenioPayload();
		info.setProyecto(obj[0].toString());
		info.setNombre(obj[1].toString());
		info.setRepresentante(obj[2].toString());
		info.setFechaRealizacion(obj[3].toString());
		info.setCorreo(obj[4].toString());
		info.setNombres(obj[5].toString());
		info.setApellidos(obj[6].toString());
		info.setRol(obj[7].toString()); 
		return info;
	}

	public static InformeUsuarioPayload objectToInformeUsuariosPayload(Object[] obj) {
		if (obj == null) {
			return null;
		}
		InformeUsuarioPayload info = new InformeUsuarioPayload();
		info.setCorreo(obj[0].toString());
		info.setNombres(obj[1].toString());
		info.setApellidos(obj[2].toString());
		info.setEstado(obj[3].toString());
		info.setRol(obj[4].toString()); 
		info.setFechaRegistro(obj[5].toString()); 
		return info;
	}

	public static InformeEncuestaPayload objectToInformeEncuestaPayload(Object[] obj) {
		if (obj == null) {
			return null;
		}
		InformeEncuestaPayload info = new InformeEncuestaPayload();
		info.setProyecto(obj[0].toString());
		info.setCorreo(obj[1].toString());
		info.setFechaEnvio(obj[2].toString());
		info.setFechaRealizacion(obj[3].toString());
		info.setIdEncuesta(obj[4].toString());
		info.setIdRespuestaEncuesta(obj[5].toString());
		return info;
	}
}
