package com.ufps.utilities;

import com.ufps.payload.InformacionInstitucionalPayload;
import com.ufps.payload.InformacionInstitucionalRegistroPayload;

public class UtilInformacionInstitucional {

	public static InformacionInstitucionalRegistroPayload getPoliticas(int idPlanEstudios) {
		InformacionInstitucionalRegistroPayload payload = new InformacionInstitucionalRegistroPayload();
		payload.setIdPlanEstudio(idPlanEstudios);
		payload.setKey(Constantes.POLITICAS);
		payload.setTitulo("Politicas");
		payload.setInformacion("<p style=\"text-align: justify;\">&nbsp;</p>"
				+ "<p>Aca va la información relacionada a las Politicas<p>"
				+ "        <p>&nbsp;</p>"
				+ "        <p style=\"text-align: center;\">&nbsp;</p>"
				+ "        <div style=\"clear:both; min-height:30px;\"></div>");
		return payload;
	}

	public static InformacionInstitucionalRegistroPayload getInicio(int idPlanEstudios) {
		InformacionInstitucionalRegistroPayload payload = new InformacionInstitucionalRegistroPayload();
		payload.setIdPlanEstudio(idPlanEstudios);
		payload.setKey(Constantes.INICIO);
		payload.setTitulo("Proyección Social");
		payload.setInformacion("<p style=\"text-align: justify;\">&nbsp;</p>"
				+ "<p>Aca va la información relacionada a al inicio<p>"
				+ "        <p>&nbsp;</p>"
				+ "        <p style=\"text-align: center;\">&nbsp;</p>"
				+ "        <div style=\"clear:both; min-height:30px;\"></div>");
		return payload;
	}

	public static InformacionInstitucionalRegistroPayload getExtension(int idPlanEstudios) {
		InformacionInstitucionalRegistroPayload payload = new InformacionInstitucionalRegistroPayload();
		payload.setIdPlanEstudio(idPlanEstudios);
		payload.setKey(Constantes.PRINCIPIOS);
		payload.setTitulo("Extensión");
		payload.setInformacion("<p style=\"text-align: justify;\">&nbsp;</p>"
				+ "<p>Aca va la información relacionada a la Extensión<p>"
				+ "        <p>&nbsp;</p>"
				+ "        <p style=\"text-align: center;\">&nbsp;</p>"
				+ "        <div style=\"clear:both; min-height:30px;\"></div>");
		return payload;
	}

	public static InformacionInstitucionalRegistroPayload getEventos(int idPlanEstudios) {
		InformacionInstitucionalRegistroPayload payload = new InformacionInstitucionalRegistroPayload();
		payload.setIdPlanEstudio(idPlanEstudios);
		payload.setKey(Constantes.EVENTOS);
		payload.setTitulo("Eventos");
		payload.setInformacion("<p style=\"text-align: justify;\">&nbsp;</p>"
				+ " <p>Aca va la información relacionada a los Eventos. Se cargara los proyectos de tipo evento que esten marcados como divulgados<p>"
				+ "        <p>&nbsp;</p>"
				+ "        <p style=\"text-align: center;\">&nbsp;</p>"
				+ "        <div style=\"clear:both; min-height:30px;\"></div>");
		return payload;
	}

	public static InformacionInstitucionalRegistroPayload getProyectos(int idPlanEstudios) {
		InformacionInstitucionalRegistroPayload payload = new InformacionInstitucionalRegistroPayload();
		payload.setIdPlanEstudio(idPlanEstudios);
		payload.setKey(Constantes.PROYECTOS);
		payload.setTitulo("Proyectos");
		payload.setInformacion("<p style=\"text-align: justify;\">&nbsp;</p>"
				+ "<p>Aca va la información relacionada a los Proyectos. Se cargara los proyectos que esten marcados como divulgar<p>"
				+ "        <p>&nbsp;</p>"
				+ "        <p style=\"text-align: center;\">&nbsp;</p>"
				+ "        <div style=\"clear:both; min-height:30px;\"></div>");
		return payload;
	}

	public static InformacionInstitucionalRegistroPayload getPracticas(int idPlanEstudios) {
		InformacionInstitucionalRegistroPayload payload = new InformacionInstitucionalRegistroPayload();
		payload.setIdPlanEstudio(idPlanEstudios);
		payload.setKey(Constantes.PRACTICAS);
		payload.setTitulo("Prácticas Empresariales");
		payload.setInformacion(""
				+ "<p style=\"text-align: justify;\">&nbsp;</p><p>Aca va la información relacionada a las Prácticas Empresariales.<p>"
				+ "        <p>&nbsp;</p>"
				+ "        <p style=\"text-align: center;\">&nbsp;</p>"
				+ "        <div style=\"clear:both; min-height:30px;\"></div>");
		return payload;
	}
	
	public static InformacionInstitucionalRegistroPayload getCifras(int idPlanEstudios) {
		InformacionInstitucionalRegistroPayload payload = new InformacionInstitucionalRegistroPayload();
		payload.setIdPlanEstudio(idPlanEstudios);
		payload.setKey(Constantes.CIFRAS);
		payload.setTitulo("Proyección social en cifras");
		payload.setInformacion("<p>Aca va la información relacionada a las Proyección social en cifras.<p>"
				+ "        <p>&nbsp;</p>"
				+ "        <p style=\"text-align: center;\">&nbsp;</p>"
				+ "        <div style=\"clear:both; min-height:30px;\"></div>");
		return payload;
	}

	public static InformacionInstitucionalRegistroPayload getLogo(int idPlanEstudios) {
		InformacionInstitucionalRegistroPayload payload = new InformacionInstitucionalRegistroPayload();
		payload.setIdPlanEstudio(idPlanEstudios);
		payload.setKey(Constantes.LOGO);
		payload.setTitulo("Logo Pie de Página UFPS");
		payload.setInformacion("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAACdCAYAAADxGzfaAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAEaVJREFUeNrsXeF14roSFvfc/8+vgngreN4K4q0gbAUhFSypYEkFJBVAKoBUgFMB3ArwVgC3Aj+UHYVhVpJlsI1Jvu8czm5AlkYajTQzmpH/UgAAOPEXhgAAICAAAAEBAAgIAEBAAAACAgAQEACAgAAABAQAPh/+/gydLIoi2f0zBrtrxX2v11tBQD4Got0nxZyufUyhYgEAbJBPil5F7B751mDdD+zxb72OoEqfISAAACP982FnxA92/8SeItPdYpq3XTc9e3umYXne0TWFgACKJqHPiM92n/wMdcdndC68YlpAxQIACAgAQEAAAAICABAQAICAAMAlAG7ey8Z093luqO4cwwsBuXT86vV6GYYBAgJ4UBRFvPtn0FD1VxAQ4NKhBeQnhgFGOgBAQAAAAgIAEBAAgIAAZ4Z29SJ7EAICAK0Dbt7Lxm1RFNc11PMprvCBgHw+xMqfzhuKCEMJFQsAICAAAAEBANggnUSuDi94AyAggAHdXzXCSEBAgEMkRVEc+yw8RBCQD4+uvjphqurJKMQZCATkQwIZhRAQoAw1ZhReYTQhIB8RWkCQUdgAcA4CABAQAICAAABskIZwTjfn1vNbrn6/P8T3OwABaRa9Xu++o3RN1e+zDgAqFgBgB+kUihPiRxrGeEfatkJ5hLNAQD4VEgwBBKQLyDpES6z26bOrEuO9TUQQ2I+vSqWFBR2jccRIS7s+dl2iEUY6AEDFOusqOT6jGuG8cmdH10D9fs/6OfBMbmYICPAmHOdSGaISu+RcdL1iWkDFAgAICABAQAAANsjZoI3VX0c+e6vquR7UhqxBWyED2yEgoXg+NvebLpduSkBed3SNwB4ISCewm+zBp8ptXqZQha4jkEBAgCqTZRFYttdRugAY6QAAAQEACAgAQEAAAEY60CB0sGNTb6jVDoAxBAS4WPR6PZ1clTVRd3ezkiEgXURjE/FIXOtEq4bqznAxNgSk6krdpCpzDFLVbDj8pxcQGOkAgB3kZCw+uy6OHQQAAOwgF4a8JTsgxVBDQC7RKTBVLdzNW0B/hIoFANhBmoO+/b2pVySszuQU0Occ38BaCEgtkxiHZhAQoFxXbzJz70rsKk3fzYt3o0NAakdbmXv32LFgpAMAdhDgJJVuoM53Ny8AAek8YoVDPKhYAAABAQAICADABvmIeFb+O27zM9GVnXFMMkwLCMgbuvomJToLwUSFgJwHR7yIsq17apMOBdjibt5PjK7eZztWAIx0AMAO0g1oI/sB7K59TAEAgIoFAAAEBAAgIAAAAQEACMjF4ogDyJA6E0r5BTrA4IX5OH4fsDLJkW2M6flBh8ch2n1mRGca2KeiznumaKw3VG3qEZ5FyKfj8y5m4510mdDCx2h95T4rkh5Rf8qe33R4HIaMzkXgwlK3gJSOtRhPLzouILyvk8+sYuVqf2tHl2/XWAmagebnhcE/XSDoLCfpvV4v360QX9XvlNLOCoiOpiU6owu6ZeRiL4TTUdW78V51abxrExCmguhbCPu7zzX7+WX3mepXhVkuIlhp3VMPinmeXlTD656QMGnckYClVE9sa8fYBGofjapDTX6wdnKi9ac6jFjVbT9R2YTrxzws3tL+VtBi7tEairH4g07eBtHD66zVMCfdfszoiAR9z/Q374um40X0n/Nbj8UNK69zax4t/UtozBKxazy4xpvG5IeNR3oesPJ9osE3H+T4OnlRuw0SoP4uLc8WpLfz78YWA9RgbbENJNbGwOM2gQULZvxKTCzPjoThXKrrk9HpHA/ulQqpM9AGWVSw/yQ2Fv4UNt6E8Fv0b1zS7h/jTeO38dFCjhQfn2cB47txOQWaskG0NE7V4c3kCe0eWurl9snLSa/WD/b/B1opxuLZB6a/6tXBZeDpdh+pbM5W5xXV8Ui0P9F3K8sE64v6V/TcyqZK0vdzqn/KbK/E9JWYMxGr6qM6Llkqqei92lJbc/bJ2bjOWdkhrcI+ftv6N6Sd1GAu6jbjLe28gYdHz/T9TO1vftkyug0/nmhhmXjmTET1tLKDrD0r48zSxsKoUey7AVsd5O6xdq2oYiXpi79nnj6MhFpkW21H9N3S9lyoF0vsiKbvM5f3Rqy8R3uxPGWTgPnAd+yhhd+xbzcTu8DAMh6RjSfi74HkkfSGyr6wuTnz8Izzs9/GDpJzfU6ks/r06Sf2/1vLbvJMHY65x4MGKaXfeF65ZPyDx2PykwQ09umiNBES1s9RoG9/Yhix+2rJfjZ94Yy5F1X8e8TunVk+NsylvccWtZlZjEhvd/Ew57aANK6JL++7gExv1u17xpzzaEKLBecRXzCeZF8YLXx8Hz3zLumEF8vhwdDGekadTplxxtWAREyuRZX6LR6TayaE+t8B0fDdwbS4ituXVj2fPz8WKktWyVh0uKYreLH+sdA8sai5pzgKEmEQV/VqcR4NSc2b0kJyLdTnkLHZeoTwui0b5FhwaZ6xCTl1dCzzfPIABtxpYRD2Q6pqSMWliT8RK9c3+uQ1Tb5aQQI9YIvSPdF7f0K1J/WPeHQneDRQzaRLbyvvILSlycl23dAuMt+1l5NgJA7B4avB9xLab0Pa1KoGqQIzYmji0Pe3jt3EBv68dnne2+wc2jnfn9G/1bCLHAvuqv1uVJQTD+AzpqJdHzkv3hwAzEFiXkORiZ0q80x8w1c5volvR3XtINxzMROG61AwP6+ZSdJWmBoBJYaZzvUdBl9aYcXsC311HqCmbZl6NPGslpHnHEL+xldHeWHDVYsCEjn4ysf0P1VVPl6PhWepz1EgeDQX9a2EHZlYbKlIeuLEQvWzopr2myjHuYHVx2zzSjm8Youy8uS52ri8LBY//ZK5M9+9XWVeJeYhW5MRPZHtOrxYI8u5y0J41wrhrSocbRREx8BxLrBu+Bxk5PGYrenvmZwHVfltOQMxPFuKciPLOYjkEUcsvFAFmwtrVk9iaV/yoprKVnKwYwiJ6hYQMVCuaOJJCW1JgID4DhsnvskU0H5RUm5jOfAqrbMFAfEdzG2EC70qvyclB3VxiZu3sB3c0gKzDDigHoQeagbZIFpn3j30rP4MDdDb2oslVubBo3LZfvOVn5K++uAy3Ii2G0bblty8c2YzOa8U3ZV5JHuHhyfIvuWMhky0/8Ke3bJ2IlHulcpFVN8L1TWmZ189fXqhZ5ISdTYvGU9X2UyMiQ7h+UJG8A0bkyei/yd7rhK/S3hmwpAy4dHLiEfXLh6RPfGVVLdrcQzwatzK5BHLqG/XtjIAAFQEMgoBwINKB4Xk2zeRug+2U9gafPE2L09ucTU3DtJLB032l9sG5C3akhqxOqO7t1Nokw+nEnpSJmFgGwuPITdpMy9bGLRN9TexeKuscUttTcaupbu2wYePoGKZVWT9US4voB15odyHjtuW6dHeu43C5dkXIyAmNOORTRZ5uHPJ6DN1crr7/HenPvQUhVbQwVibGEAkKtggtNXySMirQBuFR28ezWTmbs3IrWr86omHRi1Ic2mz0K6T2GwaojsWbYboxX2x+lftL98Jn43NYUIrHOObhrTJbLkV9Tt1jY8Ymze6mCqTWzL3ZEbg3JEdGVN7uRK5Hb5xssyjUBslCqRJ0bjwZ7LgNF/SQydVDq08B4vrKjptyeGePKH1ZewNPXrsyGVbheq+lhPm0kOnEptuUfZclTZ5ZEGZfVOSTzIKKLeRuRSsbxvH4ePE0cdxxXk3cNTvo6mwPDOqomKNxXabKc/lCnQaPGQrFC//pmefajeICW9W2lztM9EkjWNH9ltdeFGHuRc8ky5UBeS7hGb8ssQIffW0OXQ8Ezt4OGY8MXUq8XemDg8CV+xf/puuZ+LZJSP1ZybpwLKIjUQ/ViXzjgcvyj6+ZQl6FmdOk0mnCPaqWLO0bNltYhWX2YRD14oe6MUy4QYTz+oXS0GytdnADhJZdt21jAcL6O/AEcaTOFQJV5tLxzhKHs482X2l4SoWGia2MfJkbCaOUBQZg5c6xoh/v3H0se+IFxy52q5igxxktwmf878l5bVeyd+vx5+9CZbSPWwr8VSGBojbS1w6fq1gN7TcsHYisWqH1GNCIMZsLFPadb/x8S9p07VSrgQPn1g7x+ywCaURxJYxdo13xvqwoqt9JL0pe/5R2AO5zfWrDvPVud1k7J5YHUYiczzwca0iIP+zbKmhhubQs9WfCnPVy9Szxfr6Uqc7NCKHQVKTsGlmficGG2E3Fwl8aaLNI/s9UP4MyUSVpAx43NeJUCVLBVUIVxowP20OoMperOgEP/zK88zrEROnV1KEC8ej2qd0LhreQYaMQRmtyrrft6e4Sik4T7u1l7T6afU1oR3gp6PNcRtCIzIkt2p/cwi/Z6suVJ13ufIHctbq5n1l0pja3I0ePLUVGSm2WJmxV7W6qklAJhp0y/O/d+3+qEl1e2bqpVShctFmW4eJXJW+M27aGq/7zYU6XrbCc+1mzvlfJ/4qaXgsjFObyjJ3lScDvtVTWY8zgE+ka351TOCqH/uMVvo3PYLepUkOYvTcetTciLUZN7R7xKINn63xo6Y2uUAMhQMhLdFWhsIhE5FDKWpqki2rJO9YvEwb3/1VoV6sgLIbcRYg6V6L8mtPElCZF2vm6O/GET/WD6C/XzLG44ptJpZxXHj6NArg+8LifSocGY8Th8codfBY0jauOO+GjnO3St7KqjuIxjfHFrd1qAV3wkMVqQbvl+VeNnV4FpCoQ59+LFaie9GHSO2jZ8u2/T6b9Pfq0N+eWnbT28A+uFSkR6E2hLR5agjOnYfHK8HjVO0vHzd0DU48e3pwGPkumh7pmSqXaVRTeUtWOB6ikKnDC5pXjqN8PlFXVW/p5uHuIc8y1SYhRmXMHaosLuE/ynOBtlx8ZkJZ5pZLtY33JKd2zaXaibKEuwTQr3hdHturtE02jltOty/khv1uQmhy6SYW4S4m68+EeqzYTSixa664aHO1wZwBrnln+sTn6soXanJBt/UDQHeBjEIAgIAAwCcSEHO5ssUjtCQvTcjVNyP6f1Gx7ZEr8tNR3hZZHLNYs1J6K3oA0xbGf+S42R6JVh0REHPpGw+MnJDbrzRllMrELQnIxhJIOGYvq6ktxbVFAYnFKw/6H1U4LnIHIe/EXB2e7vbpu/fQB2Icv6VwzTw+A8F0kxu+pGcGTJjen1fs/lo20ZeePJCVqd9CqxL0LtiNg+/5DIy2hXiDFqd5bTw0UojNTsVoNmMxE31cmhsV2bMzVt58P1D7l+Po7/T5R2pyWmhXXDPalsWFpklfsg1iLm4zQYu2m080475TaMZ35feR6xNhHSqjX9r5Ve1PiPUE0C7DL/Q9P2HW7X6hZzLlPpE3F/AZWrXQ/HL1i+q7YzToiXxH/bhT+5go/f0DlS/rnwk21LFdX6g/EbvRXY/fV/reTP4hLUim76k4qEvEGLyofVxWRGNvxmYIAWkXc2JYRILyLNUAYnpGTDZnHi68HSLSyjtRe7+7rueJ7VzP9H8tjHrCDmkVHSj3geicCc+N8rwngw6/FIt1MmcGxm5KGW2K3RxY1j/T9hN7VofTT6nNF9ppZqwfN7zvJECZ2Amf2XnDVO3PIvj5yb+XOsn+vlTC6TDQqFn683BilXpimPcT6nqXZYa+WcHLJiYd5mW0WqckjKErqi0bL6vYtytWl1xEjMp0w/riupk+DqQXNkiH1Cy9ev9x8kx/R0z35ietNqSkxmTqz0sJ3u0RtQ8h0eXN6mtLAPLRuq2wEGS0i23Z6x8mQlBtUQ9XjOY+o4GHwIzpGXMh25zqN315VYfBiBPRT93OLbMvhiosHwQC0tIuMmeMt0Hr6zNyo86UPzdAr55L8jjdkro1oO9v6Pul2sf9aHWiz75XyhN/xUJeXo7oqt5xzNtqF9QvX//eBJbRljOaI+Zw2BJdetcc83oo8PCRyhsnwMFtJOydKmtq61qd9jYqoE3wm0JYznOEkQE+vA0SiFfaFXJjbOO+WwA43EWMXx47BwAAAIx0AICAAAAEBAAgIAAAAQEACAgAQEAAACD8X4ABALy1pPSIZq3uAAAAAElFTkSuQmCC");
		return payload;
	}

	public static InformacionInstitucionalPayload getLogoDefault() {
		InformacionInstitucionalPayload payload = new InformacionInstitucionalPayload();
		payload.setKey(Constantes.LOGO);
		payload.setTitulo("Logo Pie de Página UFPS");
		payload.setInformacion("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAACdCAYAAADxGzfaAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAEaVJREFUeNrsXeF14roSFvfc/8+vgngreN4K4q0gbAUhFSypYEkFJBVAKoBUgFMB3ArwVgC3Aj+UHYVhVpJlsI1Jvu8czm5AlkYajTQzmpH/UgAAOPEXhgAAICAAAAEBAAgIAEBAAAACAgAQEACAgAAABAQAPh/+/gydLIoi2f0zBrtrxX2v11tBQD4Got0nxZyufUyhYgEAbJBPil5F7B751mDdD+zxb72OoEqfISAAACP982FnxA92/8SeItPdYpq3XTc9e3umYXne0TWFgACKJqHPiM92n/wMdcdndC68YlpAxQIACAgAQEAAAAICABAQAICAAMAlAG7ey8Z093luqO4cwwsBuXT86vV6GYYBAgJ4UBRFvPtn0FD1VxAQ4NKhBeQnhgFGOgBAQAAAAgIAEBAAgIAAZ4Z29SJ7EAICAK0Dbt7Lxm1RFNc11PMprvCBgHw+xMqfzhuKCEMJFQsAICAAAAEBANggnUSuDi94AyAggAHdXzXCSEBAgEMkRVEc+yw8RBCQD4+uvjphqurJKMQZCATkQwIZhRAQoAw1ZhReYTQhIB8RWkCQUdgAcA4CABAQAICAAABskIZwTjfn1vNbrn6/P8T3OwABaRa9Xu++o3RN1e+zDgAqFgBgB+kUihPiRxrGeEfatkJ5hLNAQD4VEgwBBKQLyDpES6z26bOrEuO9TUQQ2I+vSqWFBR2jccRIS7s+dl2iEUY6AEDFOusqOT6jGuG8cmdH10D9fs/6OfBMbmYICPAmHOdSGaISu+RcdL1iWkDFAgAICABAQAAANsjZoI3VX0c+e6vquR7UhqxBWyED2yEgoXg+NvebLpduSkBed3SNwB4ISCewm+zBp8ptXqZQha4jkEBAgCqTZRFYttdRugAY6QAAAQEACAgAQEAAAEY60CB0sGNTb6jVDoAxBAS4WPR6PZ1clTVRd3ezkiEgXURjE/FIXOtEq4bqznAxNgSk6krdpCpzDFLVbDj8pxcQGOkAgB3kZCw+uy6OHQQAAOwgF4a8JTsgxVBDQC7RKTBVLdzNW0B/hIoFANhBmoO+/b2pVySszuQU0Occ38BaCEgtkxiHZhAQoFxXbzJz70rsKk3fzYt3o0NAakdbmXv32LFgpAMAdhDgJJVuoM53Ny8AAek8YoVDPKhYAAABAQAICADABvmIeFb+O27zM9GVnXFMMkwLCMgbuvomJToLwUSFgJwHR7yIsq17apMOBdjibt5PjK7eZztWAIx0AMAO0g1oI/sB7K59TAEAgIoFAAAEBAAgIAAAAQEACMjF4ogDyJA6E0r5BTrA4IX5OH4fsDLJkW2M6flBh8ch2n1mRGca2KeiznumaKw3VG3qEZ5FyKfj8y5m4510mdDCx2h95T4rkh5Rf8qe33R4HIaMzkXgwlK3gJSOtRhPLzouILyvk8+sYuVqf2tHl2/XWAmagebnhcE/XSDoLCfpvV4v360QX9XvlNLOCoiOpiU6owu6ZeRiL4TTUdW78V51abxrExCmguhbCPu7zzX7+WX3mepXhVkuIlhp3VMPinmeXlTD656QMGnckYClVE9sa8fYBGofjapDTX6wdnKi9ac6jFjVbT9R2YTrxzws3tL+VtBi7tEairH4g07eBtHD66zVMCfdfszoiAR9z/Q374um40X0n/Nbj8UNK69zax4t/UtozBKxazy4xpvG5IeNR3oesPJ9osE3H+T4OnlRuw0SoP4uLc8WpLfz78YWA9RgbbENJNbGwOM2gQULZvxKTCzPjoThXKrrk9HpHA/ulQqpM9AGWVSw/yQ2Fv4UNt6E8Fv0b1zS7h/jTeO38dFCjhQfn2cB47txOQWaskG0NE7V4c3kCe0eWurl9snLSa/WD/b/B1opxuLZB6a/6tXBZeDpdh+pbM5W5xXV8Ui0P9F3K8sE64v6V/TcyqZK0vdzqn/KbK/E9JWYMxGr6qM6Llkqqei92lJbc/bJ2bjOWdkhrcI+ftv6N6Sd1GAu6jbjLe28gYdHz/T9TO1vftkyug0/nmhhmXjmTET1tLKDrD0r48zSxsKoUey7AVsd5O6xdq2oYiXpi79nnj6MhFpkW21H9N3S9lyoF0vsiKbvM5f3Rqy8R3uxPGWTgPnAd+yhhd+xbzcTu8DAMh6RjSfi74HkkfSGyr6wuTnz8Izzs9/GDpJzfU6ks/r06Sf2/1vLbvJMHY65x4MGKaXfeF65ZPyDx2PykwQ09umiNBES1s9RoG9/Yhix+2rJfjZ94Yy5F1X8e8TunVk+NsylvccWtZlZjEhvd/Ew57aANK6JL++7gExv1u17xpzzaEKLBecRXzCeZF8YLXx8Hz3zLumEF8vhwdDGekadTplxxtWAREyuRZX6LR6TayaE+t8B0fDdwbS4ituXVj2fPz8WKktWyVh0uKYreLH+sdA8sai5pzgKEmEQV/VqcR4NSc2b0kJyLdTnkLHZeoTwui0b5FhwaZ6xCTl1dCzzfPIABtxpYRD2Q6pqSMWliT8RK9c3+uQ1Tb5aQQI9YIvSPdF7f0K1J/WPeHQneDRQzaRLbyvvILSlycl23dAuMt+1l5NgJA7B4avB9xLab0Pa1KoGqQIzYmji0Pe3jt3EBv68dnne2+wc2jnfn9G/1bCLHAvuqv1uVJQTD+AzpqJdHzkv3hwAzEFiXkORiZ0q80x8w1c5volvR3XtINxzMROG61AwP6+ZSdJWmBoBJYaZzvUdBl9aYcXsC311HqCmbZl6NPGslpHnHEL+xldHeWHDVYsCEjn4ysf0P1VVPl6PhWepz1EgeDQX9a2EHZlYbKlIeuLEQvWzopr2myjHuYHVx2zzSjm8Youy8uS52ri8LBY//ZK5M9+9XWVeJeYhW5MRPZHtOrxYI8u5y0J41wrhrSocbRREx8BxLrBu+Bxk5PGYrenvmZwHVfltOQMxPFuKciPLOYjkEUcsvFAFmwtrVk9iaV/yoprKVnKwYwiJ6hYQMVCuaOJJCW1JgID4DhsnvskU0H5RUm5jOfAqrbMFAfEdzG2EC70qvyclB3VxiZu3sB3c0gKzDDigHoQeagbZIFpn3j30rP4MDdDb2oslVubBo3LZfvOVn5K++uAy3Ii2G0bblty8c2YzOa8U3ZV5JHuHhyfIvuWMhky0/8Ke3bJ2IlHulcpFVN8L1TWmZ189fXqhZ5ISdTYvGU9X2UyMiQ7h+UJG8A0bkyei/yd7rhK/S3hmwpAy4dHLiEfXLh6RPfGVVLdrcQzwatzK5BHLqG/XtjIAAFQEMgoBwINKB4Xk2zeRug+2U9gafPE2L09ucTU3DtJLB032l9sG5C3akhqxOqO7t1Nokw+nEnpSJmFgGwuPITdpMy9bGLRN9TexeKuscUttTcaupbu2wYePoGKZVWT9US4voB15odyHjtuW6dHeu43C5dkXIyAmNOORTRZ5uHPJ6DN1crr7/HenPvQUhVbQwVibGEAkKtggtNXySMirQBuFR28ezWTmbs3IrWr86omHRi1Ic2mz0K6T2GwaojsWbYboxX2x+lftL98Jn43NYUIrHOObhrTJbLkV9Tt1jY8Ymze6mCqTWzL3ZEbg3JEdGVN7uRK5Hb5xssyjUBslCqRJ0bjwZ7LgNF/SQydVDq08B4vrKjptyeGePKH1ZewNPXrsyGVbheq+lhPm0kOnEptuUfZclTZ5ZEGZfVOSTzIKKLeRuRSsbxvH4ePE0cdxxXk3cNTvo6mwPDOqomKNxXabKc/lCnQaPGQrFC//pmefajeICW9W2lztM9EkjWNH9ltdeFGHuRc8ky5UBeS7hGb8ssQIffW0OXQ8Ezt4OGY8MXUq8XemDg8CV+xf/puuZ+LZJSP1ZybpwLKIjUQ/ViXzjgcvyj6+ZQl6FmdOk0mnCPaqWLO0bNltYhWX2YRD14oe6MUy4QYTz+oXS0GytdnADhJZdt21jAcL6O/AEcaTOFQJV5tLxzhKHs482X2l4SoWGia2MfJkbCaOUBQZg5c6xoh/v3H0se+IFxy52q5igxxktwmf878l5bVeyd+vx5+9CZbSPWwr8VSGBojbS1w6fq1gN7TcsHYisWqH1GNCIMZsLFPadb/x8S9p07VSrgQPn1g7x+ywCaURxJYxdo13xvqwoqt9JL0pe/5R2AO5zfWrDvPVud1k7J5YHUYiczzwca0iIP+zbKmhhubQs9WfCnPVy9Szxfr6Uqc7NCKHQVKTsGlmficGG2E3Fwl8aaLNI/s9UP4MyUSVpAx43NeJUCVLBVUIVxowP20OoMperOgEP/zK88zrEROnV1KEC8ej2qd0LhreQYaMQRmtyrrft6e4Sik4T7u1l7T6afU1oR3gp6PNcRtCIzIkt2p/cwi/Z6suVJ13ufIHctbq5n1l0pja3I0ePLUVGSm2WJmxV7W6qklAJhp0y/O/d+3+qEl1e2bqpVShctFmW4eJXJW+M27aGq/7zYU6XrbCc+1mzvlfJ/4qaXgsjFObyjJ3lScDvtVTWY8zgE+ka351TOCqH/uMVvo3PYLepUkOYvTcetTciLUZN7R7xKINn63xo6Y2uUAMhQMhLdFWhsIhE5FDKWpqki2rJO9YvEwb3/1VoV6sgLIbcRYg6V6L8mtPElCZF2vm6O/GET/WD6C/XzLG44ptJpZxXHj6NArg+8LifSocGY8Th8codfBY0jauOO+GjnO3St7KqjuIxjfHFrd1qAV3wkMVqQbvl+VeNnV4FpCoQ59+LFaie9GHSO2jZ8u2/T6b9Pfq0N+eWnbT28A+uFSkR6E2hLR5agjOnYfHK8HjVO0vHzd0DU48e3pwGPkumh7pmSqXaVRTeUtWOB6ikKnDC5pXjqN8PlFXVW/p5uHuIc8y1SYhRmXMHaosLuE/ynOBtlx8ZkJZ5pZLtY33JKd2zaXaibKEuwTQr3hdHturtE02jltOty/khv1uQmhy6SYW4S4m68+EeqzYTSixa664aHO1wZwBrnln+sTn6soXanJBt/UDQHeBjEIAgIAAwCcSEHO5ssUjtCQvTcjVNyP6f1Gx7ZEr8tNR3hZZHLNYs1J6K3oA0xbGf+S42R6JVh0REHPpGw+MnJDbrzRllMrELQnIxhJIOGYvq6ktxbVFAYnFKw/6H1U4LnIHIe/EXB2e7vbpu/fQB2Icv6VwzTw+A8F0kxu+pGcGTJjen1fs/lo20ZeePJCVqd9CqxL0LtiNg+/5DIy2hXiDFqd5bTw0UojNTsVoNmMxE31cmhsV2bMzVt58P1D7l+Po7/T5R2pyWmhXXDPalsWFpklfsg1iLm4zQYu2m080475TaMZ35feR6xNhHSqjX9r5Ve1PiPUE0C7DL/Q9P2HW7X6hZzLlPpE3F/AZWrXQ/HL1i+q7YzToiXxH/bhT+5go/f0DlS/rnwk21LFdX6g/EbvRXY/fV/reTP4hLUim76k4qEvEGLyofVxWRGNvxmYIAWkXc2JYRILyLNUAYnpGTDZnHi68HSLSyjtRe7+7rueJ7VzP9H8tjHrCDmkVHSj3geicCc+N8rwngw6/FIt1MmcGxm5KGW2K3RxY1j/T9hN7VofTT6nNF9ppZqwfN7zvJECZ2Amf2XnDVO3PIvj5yb+XOsn+vlTC6TDQqFn683BilXpimPcT6nqXZYa+WcHLJiYd5mW0WqckjKErqi0bL6vYtytWl1xEjMp0w/riupk+DqQXNkiH1Cy9ev9x8kx/R0z35ietNqSkxmTqz0sJ3u0RtQ8h0eXN6mtLAPLRuq2wEGS0i23Z6x8mQlBtUQ9XjOY+o4GHwIzpGXMh25zqN315VYfBiBPRT93OLbMvhiosHwQC0tIuMmeMt0Hr6zNyo86UPzdAr55L8jjdkro1oO9v6Pul2sf9aHWiz75XyhN/xUJeXo7oqt5xzNtqF9QvX//eBJbRljOaI+Zw2BJdetcc83oo8PCRyhsnwMFtJOydKmtq61qd9jYqoE3wm0JYznOEkQE+vA0SiFfaFXJjbOO+WwA43EWMXx47BwAAAIx0AICAAAAEBAAgIAAAAQEACAgAQEAAACD8X4ABALy1pPSIZq3uAAAAAElFTkSuQmCC");
		return payload;
	}

}
