package com.ufps.utilities;

public class Constantes {

	//ROlES
	public static String SUPER_USUARIO = "SUPER USUARIO";
	public static String ADMINISTRADOR = "ADMINISTRADOR";
	public static String DIRECTOR_PROYECTO = "DIRECTOR DE PROYECTO";
	public static String ESTUDIANTE = "ESTUDIANTE";

	//ESTADOS USUARIO
	public static String INACTIVO = "INACTIVO";
	public static String ACTIVO = "ACTIVO";

	//DIVULGAR
	public static String DIVULGAR = "SI";
	public static String NODIVULGAR = "NO";

	//APROBAR
	public static String APROBAR = "SI";
	public static String NOAPROBAR = "NO";

	//ESTADOS RESPUESTA ENCUESTA
	public static String PENDIENTE = "PENDIENTE";
	public static String CONTESTADO = "CONTESTADO";
	
	//TIPO PREGUNTA
	public static String ABIERTA = "ABIERTA";
	public static String UNICA = "UNICA";
	
	// key información institucional
	public static String INICIO = "/inicio";
	public static String POLITICAS = "/politicas";
	public static String PRINCIPIOS = "/principios";
	public static String EVENTOS = "/eventos";
	public static String PROYECTOS = "/proyectos";
	public static String PRACTICAS = "/practicas";
	public static String CIFRAS = "/cifras";
	public static String LOGO = "/logo";
	
	//Email desde el cual se envian los correos
	public static String FROM_EMAIL = "proyectandoufps@gmail.com";
	public static String DIRECCION = "Avenida Gran Colombia No. 12E-96 Barrio Colsag, San José de Cúcuta - Colombia.";
	public static String TELEFONO = "Teléfono (057)(7) 5776655";

	//EMAIL SOLICITUD CAMBIO CONTRASENA
	public static String ARCHIVO = "template.html";
	public static String CONTENIDO_HTML = "Apreciado(a) usuario(a)."
			+ "<br>${content-user}"
			+ "<br><br>"
			+ "<p>Se ha creado o modificado una cuenta de usuario</p>"
			+ "<p>Nombre de usuario: <b>${content-email}</b>"
			+ "<br>Contraseña temporal: <b>${content-pass}</b></p>"
			+ "<p>Una vez que hayan iniciado sesión con su contraseña temporal, podrán crear "
			+ "una propia desde el menú cambio de contraseña</p>";
	public static String SUBJECT = "Solicitud de cambio de contraseña";

	//EMAIL CAMBIO CONTRASENA 
	public static String CONTENIDO_HTML_CAMBIO = "Apreciado(a) usuario(a)."
			+ "<br>${content-user}"
			+ "<br><br>"
			+ "<p>Se ha cambiado tu contrasena</p>"
			+ "<p>Nombre de usuario: <b>${content-email}</b>"
			+ "<br>Nueva contraseña: <b>${content-pass}</b></p>";
	public static String SUBJECT_CAMBIO = "Cambio de contraseña";

	// EMAIL REGISTRO
	public static String CONTENIDO_HTML_REGISTRO = "<p class=\"title\">Bienvenido(a) a Proyectando-UFPS</p>"
			+ "Apreciado(a) usuario(a)."
			+ "<br>${content-user}"
			+ "<br><br>"
			+ "<p>Te has registrado en proyectando-UFPS. Este sistema permite gestionar la proyeccion social en la UFPS</p>"
			+ "<p>Nombre de usuario: <b>${content-email}</b>"
			+ "<br>Contraseña temporal: <b>${content-pass}</b></p>";
	public static String SUBJECT_REGISTRO = "Bienvenido(a) Proyectando-UFPS";
	
	// EMAIL Encuesta
	public static String CONTENIDO_HTML_ENCUESTA = "Apreciado(a) usuario(a)."
			+ "<br><br>"
			+ "<p>Con el fin del mejoramiento de la proyección social en nuestra carrera te hemos enviado una encuesta</p>"
			+ "<p>usuario: <b>${content-email}</b>"
			+ "<br>contraseña: <b>${content-pass}</b></p>"
			+ "<br><p>Para responder la encuesta has clic en el botón \"Responder Encuesta\", ingresa con el usuario y contraseña suministrados en este correo para poder contestar nuesta encuesta.</p>"
			+ "<br>"
			+ "<div align=\"center\" class=\"button-container\""
			+ " style=\"padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;\">"
			+ " <a href=\"${content-url}\""
			+ " style=\"-webkit-text-size-adjust: none; text-decoration: none; display: inline-block; color: #ffffff; background-color: #AA1916; border-radius: 20px; -webkit-border-radius: 20px; -moz-border-radius: 20px; width: auto; width: auto; border-top: 1px solid #AA1916; border-right: 1px solid #AA1916; border-bottom: 1px solid #AA1916; border-left: 1px solid #AA1916; padding-top: 5px; padding-bottom: 5px; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; text-align: center; mso-border-alt: none; word-break: keep-all;\""
			+ " target=\"_blank\"><span style=\"padding-left:20px;padding-right:20px;font-size:11px;display:inline-block;\">"
			+ " <span style=\"font-size: 16px; line-height: 2; mso-line-height-alt: 32px;\"><span"
			+ "				style=\"font-size: 11px; line-height: 22px;\"><strong>Responder Encuesta</strong></span></span>"
			+ " </span></a>"
		    + " </div>";
	public static String SUBJECT_ENCUESTA = "Encuesta de Satisfacción";
	
	public static String URL = "/login-encuesta";




}
