package com.ufps.utilities;

import java.util.List;

public interface BaseServiceInterface<T> {
	public List<T> getAll();
	
	public T getById(int id);
	
	public T save(T entity);
	
	public T update(T entity);
	
	public void delete(int id);
}
