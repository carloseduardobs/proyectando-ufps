package com.ufps.utilities;

import com.ufps.entity.UsuarioEntity;
import com.ufps.payload.InformacionInstitucionalRegistroPayload;
import com.ufps.payload.UsuarioRegistroPayload;

import java.io.InputStream;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;

public class Util {

	private static final Logger logger = LoggerFactory.getLogger(Util.class);

	public Util() {

	}

	public void passwordEncrypted(UsuarioRegistroPayload user, UsuarioEntity entity) {
		logger.info("Encriptando password.");
		String contrasena = "";
		String salt = generateRandomSalt();
		entity.setSalt(salt);
		contrasena = getHash(user.getContrasena() + salt);
		entity.setContrasena(contrasena);
	}

	public String generateRandomSalt() {
		String salt = String.valueOf((int) (Math.random() * (999999999 - 100000000 + 1) + 100000000));
		return getHash(salt);
	}

	private String getHash(String txt) {
		try {
			MessageDigest md = java.security.MessageDigest.getInstance("MD5");
			byte[] array = md.digest(txt.getBytes());
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < array.length; ++i) {
				sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
			}
			return sb.toString();
		} catch (java.security.NoSuchAlgorithmException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	public boolean validatePassword(UsuarioEntity entity, String password) {
		logger.info("Validando password.");
		boolean resul = false;
		String contrasena = getHash(password + entity.getSalt());
		if (contrasena.equalsIgnoreCase(entity.getContrasena())) {
			resul = true;
		}
		return resul;
	}

	public String contentMensaje(String archivo, String contenido) {
		String msj = "";
		try {
			// Read File Content
			ClassPathResource classPathResource = new ClassPathResource("template/" + archivo);
			InputStream input = classPathResource.getInputStream();
			Scanner sc = new Scanner(input);
			// Reading line by line from scanner to StringBuffer
			StringBuffer sb = new StringBuffer();
			while (sc.hasNext()) {
				sb.append(sc.nextLine());
			}

			msj = sb.toString();
			msj = msj.replace("${content-message}", contenido);
			msj = msj.replace("${content-direccion}", Constantes.DIRECCION);
			msj = msj.replace("${content-telefono}", Constantes.TELEFONO);
			// logger.info("url -> " + msj);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return msj;
	}

	public String AjustarContenidoHtml(String html, Map<String, String> parametros) {
		String msj = "";
		try {
			msj = html;
			if (parametros.containsKey("content-user")) {
				msj = msj.replace("${content-user}", parametros.get("content-user"));
			}
			if (parametros.containsKey("content-email")) {
				msj = msj.replace("${content-email}", parametros.get("content-email"));
			}
			if (parametros.containsKey("content-pass")) {
				msj = msj.replace("${content-pass}", parametros.get("content-pass"));
			}
			if (parametros.containsKey("content-url")) {
				msj = msj.replace("${content-url}", parametros.get("content-url"));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return msj;
	}

	public List<InformacionInstitucionalRegistroPayload> cargarInformacionInstitucionalDefault(int idPlanEstudios) {
		List<InformacionInstitucionalRegistroPayload> lista = new ArrayList<>();
		lista.add(UtilInformacionInstitucional.getInicio(idPlanEstudios));
		lista.add(UtilInformacionInstitucional.getPoliticas(idPlanEstudios));
		lista.add(UtilInformacionInstitucional.getCifras(idPlanEstudios));
		lista.add(UtilInformacionInstitucional.getEventos(idPlanEstudios));
		lista.add(UtilInformacionInstitucional.getExtension(idPlanEstudios));
		lista.add(UtilInformacionInstitucional.getLogo(idPlanEstudios));
		lista.add(UtilInformacionInstitucional.getPracticas(idPlanEstudios));
		lista.add(UtilInformacionInstitucional.getProyectos(idPlanEstudios));
		return lista;
	}


}
