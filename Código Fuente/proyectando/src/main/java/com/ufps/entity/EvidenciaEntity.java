package com.ufps.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name = "Evidencia")
public class EvidenciaEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idEvidencia")
	private Integer id;
	@Column(name = "archivo")
	private String archivo;
	@Column(name = "nombre")
	private String nombre;
	@Column(name = "extension")
	@Lob
	private String extension;
	@Column(name = "idEvidenciaActividad")
	private Integer idEvidenciaActividad;
	@Column(name = "idEvidenciaProyecto")
	private Integer idEvidenciaProyecto;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getArchivo() {
		return archivo;
	}

	public void setArchivo(String archivo) {
		this.archivo = archivo;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public Integer getIdEvidenciaActividad() {
		return idEvidenciaActividad;
	}

	public void setIdEvidenciaActividad(int idEvidenciaActividad) {
		this.idEvidenciaActividad = idEvidenciaActividad;
	}

	public Integer getIdEvidenciaProyecto() {
		return idEvidenciaProyecto;
	}

	public void setIdEvidenciaProyecto(int idEvidenciaProyecto) {
		this.idEvidenciaProyecto = idEvidenciaProyecto;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}
