package com.ufps.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Proyecto")
public class ProyectoEntity {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "idProyecto")
	private int id;
	@Column(name = "nombre")
	private String nombre;
	@Column(name = "descripcion")
	private String descripcion;
	@Column(name = "monto")
	private String monto;
	@Column(name = "beneficiario")
	private String beneficiario;
	@Column(name = "aprobar")
	private String aprobar;
	@Column(name = "divulgar")
	private String divulgar;
	@Column(name = "registradopor")
	private int registradoPor;
	@Column(name = "idProcesoMisional")
	private int idProcesoMisional;
	@Column(name = "idTipoProyecto")
	private int idTipoProyecto;
	@Column(name = "idProgramaAcademico")
	private int idPlanEstudios;
	@Column(name = "fechaInicio")
	private Date fechaInicio;
	@Column(name = "fechaFin")
	private Date fechaFin;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getMonto() {
		return monto;
	}

	public void setMonto(String monto) {
		this.monto = monto;
	}

	public String getBeneficiario() {
		return beneficiario;
	}

	public void setBeneficiario(String beneficiario) {
		this.beneficiario = beneficiario;
	}

	public String getAprobar() {
		return aprobar;
	}

	public void setAprobar(String aprobar) {
		this.aprobar = aprobar;
	}

	public String getDivulgar() {
		return divulgar;
	}

	public void setDivulgar(String divulgar) {
		this.divulgar = divulgar;
	}

	public int getRegistradoPor() {
		return registradoPor;
	}

	public void setRegistradoPor(int registradoPor) {
		this.registradoPor = registradoPor;
	}

	public int getIdProcesoMisional() {
		return idProcesoMisional;
	}

	public void setIdProcesoMisional(int idProcesoMisional) {
		this.idProcesoMisional = idProcesoMisional;
	}

	public int getIdTipoProyecto() {
		return idTipoProyecto;
	}

	public void setIdTipoProyecto(int idTipoProyecto) {
		this.idTipoProyecto = idTipoProyecto;
	}

	public int getIdPlanEstudios() {
		return idPlanEstudios;
	}

	public void setIdPlanEstudios(int idPlanEstudios) {
		this.idPlanEstudios = idPlanEstudios;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

}
