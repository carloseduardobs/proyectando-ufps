package com.ufps.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Encuesta")
public class EncuestaEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idEncuesta")
	private Integer id;
	@Column(name = "nombre")
	private String nombre;
	@Column(name = "fechaRealizacion")
	private Date fechaRealizacion;
	@Column(name = "estado")
	private String estado;
	@Column(name = "idProgramaAcademico")
	private Integer idPlanEstudio;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getFechaRealizacion() {
		return fechaRealizacion;
	}

	public void setFechaRealizacion(Date fechaRealizacion) {
		this.fechaRealizacion = fechaRealizacion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Integer getIdPlanEstudio() {
		return idPlanEstudio;
	}

	public void setIdPlanEstudio(Integer idPlanEstudio) {
		this.idPlanEstudio = idPlanEstudio;
	}

}
