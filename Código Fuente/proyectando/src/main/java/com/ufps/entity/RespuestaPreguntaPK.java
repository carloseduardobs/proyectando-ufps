package com.ufps.entity;

import java.io.Serializable;

public class RespuestaPreguntaPK implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer idRespuestaEncuesta;
	private Integer idPregunta;

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idPregunta == null) ? 0 : idPregunta.hashCode());
		result = prime * result + ((idRespuestaEncuesta == null) ? 0 : idRespuestaEncuesta.hashCode());
		return result;
	}

    @Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RespuestaPreguntaPK other = (RespuestaPreguntaPK) obj;
		if (idPregunta == null) {
			if (other.idPregunta != null)
				return false;
		} else if (!idPregunta.equals(other.idPregunta))
			return false;
		if (idRespuestaEncuesta == null) {
			if (other.idRespuestaEncuesta != null)
				return false;
		} else if (!idRespuestaEncuesta.equals(other.idRespuestaEncuesta))
			return false;
		return true;
	}

	public Integer getIdRespuestaEncuesta() {
		return idRespuestaEncuesta;
	}

	public void setIdRespuestaEncuesta(Integer idRespuestaEncuesta) {
		this.idRespuestaEncuesta = idRespuestaEncuesta;
	}

	public Integer getIdPregunta() {
		return idPregunta;
	}

	public void setIdPregunta(Integer idPregunta) {
		this.idPregunta = idPregunta;
	}

    
}
