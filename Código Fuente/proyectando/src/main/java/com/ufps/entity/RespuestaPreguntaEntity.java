package com.ufps.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "RespuestaPregunta")
@IdClass(value = RespuestaPreguntaPK.class)
public class RespuestaPreguntaEntity {

	@Id
	@Column(name = "idRespuestaEncuesta")
	private Integer idRespuestaEncuesta;
	@Id
	@Column(name = "idPregunta")
	private Integer idPregunta;
	@Column(name = "respuestas")
	private String respuestas;

	public Integer getIdRespuestaEncuesta() {
		return idRespuestaEncuesta;
	}

	public void setIdRespuestaEncuesta(Integer idRespuestaEncuesta) {
		this.idRespuestaEncuesta = idRespuestaEncuesta;
	}

	public Integer getIdPregunta() {
		return idPregunta;
	}

	public void setIdPregunta(Integer idPregunta) {
		this.idPregunta = idPregunta;
	}

	public String getRespuestas() {
		return respuestas;
	}

	public void setRespuestas(String respuestas) {
		this.respuestas = respuestas;
	}

}
