package com.ufps.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name = "InformacionInstitucional")
public class InformacionInstitucionalEntity {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "idInformacionInstitucional")
	private int id;
	@Column(name = "keyInformacionInstitucional")
	private String key;
	@Column(name = "titulo")
	private String titulo;
	@Column(name = "informacion")
	@Lob
	private String informacion;
	@Column(name = "divulgar")
	private String divulgar;
	@Column(name = "idProgramaAcademico")
	private int idPlanEstudio;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getInformacion() {
		return informacion;
	}

	public void setInformacion(String informacion) {
		this.informacion = informacion;
	}

	public String getDivulgar() {
		return divulgar;
	}

	public void setDivulgar(String divulgar) {
		this.divulgar = divulgar;
	}

	public int getIdPlanEstudio() {
		return idPlanEstudio;
	}

	public void setIdPlanEstudio(int idPlanEstudio) {
		this.idPlanEstudio = idPlanEstudio;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

}
