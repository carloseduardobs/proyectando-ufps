package com.ufps.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "EvidenciaActividad")
public class EvidenciaActividadEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idEvidenciaActividad")
	private int id;
	@Column(name = "fechaRealizacion")
	private Date fechaRealizacion;
	@Column(name = "nombre")
	private String nombre;
	@Column(name = "registradopor")
	private int registradoPor;
	@Column(name = "aprobar")
	private String aprobar;
	@Column(name = "divulgar")
	private String divulgar;
	@Column(name = "idActividad")
	private int idActividad;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getFechaRealizacion() {
		return fechaRealizacion;
	}

	public void setFechaRealizacion(Date fechaRealizacion) {
		this.fechaRealizacion = fechaRealizacion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getRegistradoPor() {
		return registradoPor;
	}

	public void setRegistradoPor(int registradoPor) {
		this.registradoPor = registradoPor;
	}

	public String getAprobar() {
		return aprobar;
	}

	public void setAprobar(String aprobar) {
		this.aprobar = aprobar;
	}

	public String getDivulgar() {
		return divulgar;
	}

	public void setDivulgar(String divulgar) {
		this.divulgar = divulgar;
	}

	public int getIdActividad() {
		return idActividad;
	}

	public void setIdActividad(int idActividad) {
		this.idActividad = idActividad;
	}

}
