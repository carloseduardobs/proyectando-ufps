package com.ufps.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name = "Convenio")
public class ConvenioEntity {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "idConvenio")
	private int id;
	@Column(name = "fechaRealizacion")
	private Date fechaRealizacion;
	@Column(name = "representante")
	private String representante;
	@Column(name = "nombre")
	private String nombre;
	@Column(name = "evidencia")
	@Lob
	private String evidencia;
	@Column(name = "extEvidencia")
	private String extEvidencia;
	@Column(name = "nombreEvidencia")
	private String nombreEvidencia;
	@Column(name = "registradopor")
	private int registradoPor;
	@Column(name = "aprobar")
	private String aprobar;
	@Column(name = "divulgar")
	private String divulgar;
	@Column(name = "idProyecto")
	private int idProyecto;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getFechaRealizacion() {
		return fechaRealizacion;
	}

	public void setFechaRealizacion(Date fechaRealizacion) {
		this.fechaRealizacion = fechaRealizacion;
	}

	public String getEvidencia() {
		return evidencia;
	}

	public void setEvidencia(String evidencia) {
		this.evidencia = evidencia;
	}

	public String getExtEvidencia() {
		return extEvidencia;
	}

	public void setExtEvidencia(String extEvidencia) {
		this.extEvidencia = extEvidencia;
	}

	public int getRegistradoPor() {
		return registradoPor;
	}

	public void setRegistradoPor(int registradoPor) {
		this.registradoPor = registradoPor;
	}

	public String getAprobar() {
		return aprobar;
	}

	public void setAprobar(String aprobar) {
		this.aprobar = aprobar;
	}

	public String getDivulgar() {
		return divulgar;
	}

	public void setDivulgar(String divulgar) {
		this.divulgar = divulgar;
	}

	public int getIdProyecto() {
		return idProyecto;
	}

	public void setIdProyecto(int idProyecto) {
		this.idProyecto = idProyecto;
	}

	public String getRepresentante() {
		return representante;
	}

	public void setRepresentante(String representante) {
		this.representante = representante;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNombreEvidencia() {
		return nombreEvidencia;
	}

	public void setNombreEvidencia(String nombreEvidencia) {
		this.nombreEvidencia = nombreEvidencia;
	}

}
