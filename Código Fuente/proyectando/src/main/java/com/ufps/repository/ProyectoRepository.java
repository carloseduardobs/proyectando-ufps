package com.ufps.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.ufps.entity.ProyectoEntity;

public interface ProyectoRepository extends CrudRepository<ProyectoEntity, Integer> {

	List<ProyectoEntity> findAllByIdPlanEstudios(int idPlanEstudio);

	List<ProyectoEntity> findAllByRegistradoPor(int registradoPor);

	@Query(value = "select proy.*" + " from Actividad acti"
			+ " inner join Proyecto proy on(proy.idProyecto = acti.idProyecto)"
			+ " inner join ProgramaAcademico ples on(proy.idProgramaAcademico = ples.idProgramaAcademico)"
			+ " where ples.urlProgramaAcademico = ? and proy.divulgar = 'SI' and acti.tipo = 'Evento'", nativeQuery = true)
	List<ProyectoEntity> findAllDivulgadosAndEventosByUrl(String url);

	@Query(value = "select proy.*" + " from TipoProyecto tipr"
			+ " inner join Proyecto proy on(tipr.idTipoProyecto = proy.idTipoProyecto)"
			+ " inner join ProgramaAcademico ples on(proy.idProgramaAcademico = ples.idProgramaAcademico)"
			+ " where ples.urlProgramaAcademico = ? and proy.divulgar = 'SI'", nativeQuery = true)
	List<ProyectoEntity> findAllDivulgadosByUrl(String url);

	@Query(value = "SELECT p.nombre, p.descripcion, p.monto, p.beneficiario, p.aprobar, p.divulgar, CAST(p.fechaInicio as date) as fechaInicio, CAST(p.fechaFin as date) as fechaFin,"
			+ " tp.nombre as tipoProyecto, pm.nombre as procesoMisional, u.correo, u.nombres, u.apellidos, r.nombre as rol"
			+ " FROM Proyecto p" + " INNER JOIN TipoProyecto tp on (tp.idTipoProyecto = p.idTipoProyecto)"
			+ " INNER JOIN ProcesoMisional pm  on (pm.idProcesoMisional = p.idProcesoMisional)"
			+ " INNER JOIN Usuario u on (u.idUsuario = p.registradopor) "
			+ " INNER JOIN UsuarioRol ur on (u.idUsuario = ur.idUsuario)" + " INNER JOIN Rol r on (ur.idRol = r.idRol)"
			+ " WHERE" + " CAST(p.fechaInicio AS date) >= ? and CAST(p.fechaFin AS date) <= ? and p.aprobar = ?"
			+ " and p.idProgramaAcademico = ?" + " ORDER by fechaInicio", nativeQuery = true)
	public List<Object[]> getInformeProyecto(Date fechaInicio, Date fechaFin, String aprobado, int idProgramaAcademico);

	@Query(value = "SELECT p.nombre, p.descripcion, p.monto, p.beneficiario, p.aprobar, p.divulgar, CAST(p.fechaInicio as date) as fechaInicio, CAST(p.fechaFin as date) as fechaFin,"
			+ " tp.nombre as tipoProyecto, pm.nombre as procesoMisional, u.correo, u.nombres, u.apellidos, r.nombre as rol"
			+ " FROM Proyecto p" + " INNER JOIN TipoProyecto tp on (tp.idTipoProyecto = p.idTipoProyecto)"
			+ " INNER JOIN ProcesoMisional pm  on (pm.idProcesoMisional = p.idProcesoMisional)"
			+ " INNER JOIN Usuario u on (u.idUsuario = p.registradopor) "
			+ " INNER JOIN UsuarioRol ur on (u.idUsuario = ur.idUsuario)" + " INNER JOIN Rol r on (ur.idRol = r.idRol)"
			+ " WHERE" + " CAST(p.fechaInicio AS date) >= ? and CAST(p.fechaFin AS date) <= ?"
			+ " and p.idProgramaAcademico = ?" + " ORDER by fechaInicio", nativeQuery = true)
	public List<Object[]> getInformeProyecto(Date fechaInicio, Date fechaFin, int idProgramaAcademico);
}
