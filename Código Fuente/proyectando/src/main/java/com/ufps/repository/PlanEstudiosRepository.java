package com.ufps.repository;

import org.springframework.data.repository.CrudRepository;

import com.ufps.entity.PlanEstudiosEntity;

public interface PlanEstudiosRepository extends CrudRepository<PlanEstudiosEntity, Integer>  {

	Boolean existsByUrl(String url);
}
