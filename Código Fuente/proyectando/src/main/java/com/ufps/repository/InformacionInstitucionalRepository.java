package com.ufps.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.ufps.entity.InformacionInstitucionalEntity;

public interface InformacionInstitucionalRepository extends CrudRepository<InformacionInstitucionalEntity, Integer> {

	List<InformacionInstitucionalEntity> findAllByIdPlanEstudio(int idPlanEstudio);

	@Query(value ="select inin.*"
			+ " from InformacionInstitucional inin"
			+ " inner join ProgramaAcademico ples on(ples.idProgramaAcademico = inin.idProgramaAcademico)"
			+ " where inin.divulgar = 'SI' and  ples.urlProgramaAcademico = ? and (inin.keyInformacionInstitucional <> ? and inin.keyInformacionInstitucional <> ?) "
			+ " order by inin.idInformacionInstitucional ",
			nativeQuery = true)
	List<InformacionInstitucionalEntity> findAllByUrl(String url, String logo, String inicio);

	@Query(value ="select inin.*"
			+ " from InformacionInstitucional inin"
			+ " inner join ProgramaAcademico ples on(ples.idProgramaAcademico = inin.idProgramaAcademico)"
			+ " where ples.urlProgramaAcademico = ? and inin.keyInformacionInstitucional = ?"
			+ " order by inin.idInformacionInstitucional ",
			nativeQuery = true)
	InformacionInstitucionalEntity getLogo(String url, String logo);
	
	@Query(value ="select inin.*"
			+ " from InformacionInstitucional inin"
			+ " inner join ProgramaAcademico ples on(ples.idProgramaAcademico = inin.idProgramaAcademico)"
			+ " where inin.divulgar = 'SI' and  ples.urlProgramaAcademico = ? and inin.keyInformacionInstitucional = ? "
			+ " order by inin.idInformacionInstitucional ",
			nativeQuery = true)
	InformacionInstitucionalEntity findAllByUrlAndPath(String url, String path);
}
