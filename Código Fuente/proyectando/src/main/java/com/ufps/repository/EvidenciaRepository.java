package com.ufps.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.ufps.entity.EvidenciaEntity;

public interface EvidenciaRepository extends CrudRepository<EvidenciaEntity, Integer> {

	List<EvidenciaEntity> findAllByIdEvidenciaActividad(int idEvidenciaActividad);

	List<EvidenciaEntity> findAllByIdEvidenciaProyecto(int idEvidenciaProyecto);

}
