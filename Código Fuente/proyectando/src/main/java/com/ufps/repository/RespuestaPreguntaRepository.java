package com.ufps.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.ufps.entity.RespuestaPreguntaEntity;

public interface RespuestaPreguntaRepository extends CrudRepository<RespuestaPreguntaEntity, Integer> {

	Optional<RespuestaPreguntaEntity> findByIdRespuestaEncuestaAndIdPregunta(Integer idRespuestaEncuesta, Integer idPregunta);
	
	List<RespuestaPreguntaEntity> findAllByIdRespuestaEncuesta(Integer idRespuestaEncuesta);
}
