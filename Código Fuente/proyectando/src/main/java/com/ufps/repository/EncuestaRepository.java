package com.ufps.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.ufps.entity.EncuestaEntity;


public interface EncuestaRepository  extends CrudRepository<EncuestaEntity, Integer>  {

	List<EncuestaEntity> findAllByIdPlanEstudio(Integer idPlanEstudio);

	List<EncuestaEntity> findAllByIdPlanEstudioAndEstado(Integer idPlanEstudio, String estado);
}
