package com.ufps.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.ufps.entity.ActividadEntity;

public interface ActividadRepository extends CrudRepository<ActividadEntity, Integer> {

	List<ActividadEntity> findAllByRegistradoPor(int registradoPor);

	List<ActividadEntity> findAllByIdProyecto(int idProyecto);

	@Query(value = "select acti.*"
			+ " from Proyecto proy inner join Actividad acti on(proy.idProyecto = acti.idProyecto)"
			+ " where proy.idProgramaAcademico = ?;", nativeQuery = true)
	List<ActividadEntity> findAllByIdPlanEstudios(int idPlanEstudios);

	@Query(value = "select acti.*" + " from Proyecto proy"
			+ " inner join Actividad acti on(proy.idProyecto = acti.idProyecto)"
			+ " where acti.divulgar = 'SI' and proy.idProyecto = ? and acti.tipo <> 'Evento'", nativeQuery = true)
	List<ActividadEntity> findAllByProyectoAndDivulgado(int idProyecto);

	@Query(value = "select acti.*" + " from Proyecto proy"
			+ " inner join Actividad acti on(proy.idProyecto = acti.idProyecto)"
			+ " where acti.divulgar = 'SI' and proy.idProyecto = ? and acti.tipo = 'Evento'", nativeQuery = true)
	List<ActividadEntity> findAllByProyectoAndDivulgadoEventos(int idProyecto);

	@Query(value = "SELECT p.nombre as proyecto, a.nombre, a.descripcion, CAST(a.fechaRealizacion as date) as fechaRealizacion,"
			+ " a.beneficiario, a.pais , a.ciudad, a.tipo, a.aprobar, a.divulgar, u.correo, u.nombres, u.apellidos, r.nombre as rol"
			+ " FROM Proyecto p" + " INNER JOIN Actividad a on (p.idProyecto = a.idProyecto)"
			+ " INNER JOIN Usuario u on (u.idUsuario = a.registradopor)"
			+ " INNER JOIN UsuarioRol ur on (u.idUsuario = ur.idUsuario)" + " INNER JOIN Rol r on (ur.idRol = r.idRol)"
			+ " WHERE" + " CAST(a.fechaRealizacion AS date) >= ?" + " and CAST(a.fechaRealizacion AS date) <= ?"
			+ " and a.aprobar = ?" + " and p.idProgramaAcademico = ?"
			+ " ORDER by a.fechaRealizacion ", nativeQuery = true)
	public List<Object[]> getInformeActividad(Date fechaInicio, Date fechaFin, String aprobado,
			int idProgramaAcademico);

	@Query(value = "SELECT p.nombre as proyecto, a.nombre, a.descripcion, CAST(a.fechaRealizacion as date) as fechaRealizacion,"
			+ " a.beneficiario, a.pais , a.ciudad, a.tipo, a.aprobar, a.divulgar, u.correo, u.nombres, u.apellidos, r.nombre as rol"
			+ " FROM Proyecto p" + " INNER JOIN Actividad a on (p.idProyecto = a.idProyecto)"
			+ " INNER JOIN Usuario u on (u.idUsuario = a.registradopor)"
			+ " INNER JOIN UsuarioRol ur on (u.idUsuario = ur.idUsuario)" + " INNER JOIN Rol r on (ur.idRol = r.idRol)"
			+ " WHERE" + " CAST(a.fechaRealizacion AS date) >= ?" + " and CAST(a.fechaRealizacion AS date) <= ?"
			+ " and p.idProgramaAcademico = ?" + " ORDER by a.fechaRealizacion ", nativeQuery = true)
	public List<Object[]> getInformeActividad(Date fechaInicio, Date fechaFin, int idProgramaAcademico);
}
