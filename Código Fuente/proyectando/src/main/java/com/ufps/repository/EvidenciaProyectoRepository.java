package com.ufps.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.ufps.entity.EvidenciaProyectoEntity;

public interface EvidenciaProyectoRepository extends CrudRepository<EvidenciaProyectoEntity, Integer>  {

	List<EvidenciaProyectoEntity> findAllByRegistradoPor(int registradoPor);

	List<EvidenciaProyectoEntity> findAllByIdProyecto(int idProyecto);

	@Query(value = "select evpr.*"
			+ " from EvidenciaProyecto evpr "
			+ " inner join Proyecto proy on(proy.idProyecto = evpr.idProyecto)"
			+ " where proy.idProgramaAcademico = ?", nativeQuery = true)
	List<EvidenciaProyectoEntity> findAllByIdPlanEstudios(int idPlanEstudios);
	
	@Query(value="select evpr.*"
			+ " from Proyecto proy"
			+ " inner join EvidenciaProyecto evpr on(proy.idProyecto = evpr.idProyecto)"
			+ " where evpr.divulgar = 'SI' and proy.idProyecto = ?",
			nativeQuery = true)
	List<EvidenciaProyectoEntity> findAllByProyectoAndDivulgado(int idProyecto);
}
