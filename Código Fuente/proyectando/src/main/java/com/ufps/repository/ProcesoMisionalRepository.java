package com.ufps.repository;

import org.springframework.data.repository.CrudRepository;

import com.ufps.entity.ProcesoMisionalEntity;

public interface ProcesoMisionalRepository extends CrudRepository<ProcesoMisionalEntity, Integer> {

}
