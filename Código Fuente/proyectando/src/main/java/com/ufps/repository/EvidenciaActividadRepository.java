package com.ufps.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.ufps.entity.EvidenciaActividadEntity;

public interface EvidenciaActividadRepository extends CrudRepository<EvidenciaActividadEntity, Integer> {

	List<EvidenciaActividadEntity> findAllByRegistradoPor(int registradoPor);

	List<EvidenciaActividadEntity> findAllByIdActividad(int idActividad);

	@Query(value = "select evac.*"
			+ " from EvidenciaActividad evac"
			+ " inner join Actividad acti on(acti.idActividad = evac.idActividad)"
			+ " inner join Proyecto proy on(proy.idProyecto = acti.idProyecto)"
			+ " where proy.idProgramaAcademico = ?", nativeQuery = true)
	List<EvidenciaActividadEntity> findAllByIdPlanEstudios(int idPlanEstudios);

	@Query(value="select evac.*"
			+ " from  EvidenciaActividad evac"
			+ " inner join Actividad acti on(acti.idActividad = evac.idActividad)"
			+ " where evac.divulgar = 'SI' and acti.idActividad = ?",
			nativeQuery = true)
	List<EvidenciaActividadEntity> findAllByActividadAndDivulgado(int idActividad);
}
