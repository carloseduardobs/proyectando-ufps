package com.ufps.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.ufps.entity.RespuestaEncuestaEntity;


public interface RespuestaEncuestaRepository extends CrudRepository<RespuestaEncuestaEntity, Integer> {


	List<RespuestaEncuestaEntity> findAllByIdEncuesta(Integer idEncuesta);
	
	@Query(value ="select reen.*"
			+ "from RespuestaEncuesta reen"
			+ "where reen.idEncuesta = ? and reen.estado = 'CONTESTADO';",
			nativeQuery = true)
	List<RespuestaEncuestaEntity> findAllByIdEncuestaContestada(int idEncuesta);
	
	Optional<RespuestaEncuestaEntity> findByCorreoAndKeyAndEstado(String correo, String key, String estado);

	@Query(value = "SELECT p.nombre as proyecto, re.correo, CAST(re.fechaEnvio AS date) as fechaEnvio, CAST(re.fechaRealizacion AS date) as fechaRealizacion"
			+ " , re.idEncuesta, re.idRespuestaEncuesta"
			+ " FROM RespuestaEncuesta re"
			+ " INNER JOIN Proyecto p on(re.idProyecto = p.idProyecto)"
			+ " WHERE"
			+ " CAST(re.fechaRealizacion AS date) >= ?"
			+ " and CAST(re.fechaRealizacion AS date) <= ?"
			+ " and p.idProgramaAcademico = ?"
			+ " and re.idEncuesta = ?"
			+ " and re.estado = 'CONTESTADO'"
			+ " ORDER by re.fechaRealizacion ", nativeQuery = true)
	public List<Object[]> getInformeEncuesta(Date fechaInicio, Date fechaFin, int idProgramaAcademico, int idEncuesta);

}
