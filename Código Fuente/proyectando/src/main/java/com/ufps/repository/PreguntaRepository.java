package com.ufps.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.ufps.entity.PreguntaEntity;

public interface PreguntaRepository extends CrudRepository<PreguntaEntity, Integer> {

	List<PreguntaEntity> findAllByIdEncuesta(Integer idEncuesta);
}
