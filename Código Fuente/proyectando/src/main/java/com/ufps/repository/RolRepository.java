package com.ufps.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.ufps.entity.RolEntity;

public interface RolRepository extends CrudRepository<RolEntity, Integer> {

	Optional<RolEntity> findBynombre(String name);
}
