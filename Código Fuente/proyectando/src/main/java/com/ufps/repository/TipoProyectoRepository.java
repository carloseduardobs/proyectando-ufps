package com.ufps.repository;

import org.springframework.data.repository.CrudRepository;

import com.ufps.entity.TipoProyectoEntity;

public interface TipoProyectoRepository extends CrudRepository<TipoProyectoEntity, Integer> {

}
