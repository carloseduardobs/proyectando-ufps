package com.ufps.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.ufps.entity.UsuarioEntity;

public interface UsuarioRepository extends CrudRepository<UsuarioEntity, Integer> {

	Optional<UsuarioEntity> findByCorreo(String correo);

	Boolean existsByCorreo(String correo);
	
	@Query(value ="select usua.* "
			+ "from Usuario usua inner join UsuarioRol usro on (usua.idUsuario = usro.idUsuario) "
			+ "where usua.idProgramaAcademico = ? and usro.idRol = ?",
			nativeQuery = true)
	Optional<UsuarioEntity> findByidPlanEstudioandidRol(int idPlanEstudio, int idRol);

	List<UsuarioEntity> findAllByIdPlanEstudio(int idPlanEstudio);

	@Query(value = "SELECT u.correo, u.nombres, u.apellidos, u.estado, r.nombre as rol, CAST(ur.fechaRegistro AS date) as fechaRegistro"
			+ " FROM Usuario u"
			+ " INNER JOIN UsuarioRol ur on (u.idUsuario = ur.idUsuario)"
			+ " INNER JOIN Rol r on (ur.idRol = r.idRol)"
			+ " WHERE"
			+ " CAST(ur.fechaRegistro AS date) >= ?"
			+ " and CAST(ur.fechaRegistro AS date) <= ?"
			+ " and u.estado = ?"
			+ " and u.idProgramaAcademico = ?"
			+ " and r.nombre <> 'super usuario'"
			+ " ORDER by ur.fechaRegistro ", nativeQuery = true)
	public List<Object[]> getInformeUsuarios(Date fechaInicio, Date fechaFin, String aprobado,
			int idProgramaAcademico);

	@Query(value = "SELECT u.correo, u.nombres, u.apellidos, u.estado, r.nombre as rol, CAST(ur.fechaRegistro AS date) as fechaRegistro"
			+ " FROM Usuario u"
			+ " INNER JOIN UsuarioRol ur on (u.idUsuario = ur.idUsuario)"
			+ " INNER JOIN Rol r on (ur.idRol = r.idRol)"
			+ " WHERE"
			+ " CAST(ur.fechaRegistro AS date) >= ?"
			+ " and CAST(ur.fechaRegistro AS date) <= ?"
			+ " and u.idProgramaAcademico = ?"
			+ " and r.nombre <> 'super usuario'"
			+ " ORDER by ur.fechaRegistro ", nativeQuery = true)
	public List<Object[]> getInformeUsuarios(Date fechaInicio, Date fechaFin, int idProgramaAcademico);
}
