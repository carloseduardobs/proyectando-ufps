package com.ufps.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.ufps.entity.ConvenioEntity;

public interface ConvenioRepository extends CrudRepository<ConvenioEntity, Integer> {

	List<ConvenioEntity> findAllByRegistradoPor(int registradoPor);
	
	List<ConvenioEntity> findAllByIdProyecto(int idProyecto);
	
	Boolean existsByIdProyecto(int idProyecto);

	@Query(value ="select conv.*"
			+ " from Proyecto proy inner join Convenio conv on(proy.idProyecto = conv.idProyecto)"
			+ " where proy.idProgramaAcademico = ?;",
			nativeQuery = true)
	List<ConvenioEntity> findAllByIdPlanEstudios(int idPlanEstudios);

	@Query(value="select conv.*"
			+ " from Proyecto proy"
			+ " inner join Convenio conv on(proy.idProyecto = conv.idProyecto)"
			+ " where conv.divulgar = 'SI' and proy.idProyecto = ?",
			nativeQuery = true)
	ConvenioEntity findAllByProyectoAndDivulgado(int idProyecto);

	@Query(value = "SELECT p.nombre as proyecto, c.nombre , c.representante,  CAST(c.fechaRealizacion as date) as fechaRealizacion,"
			+ " u.correo, u.nombres, u.apellidos, r.nombre as rol "
			+ " FROM Proyecto p"
			+ " INNER JOIN Convenio c on(p.idProyecto = c.idProyecto)"
			+ " INNER JOIN Usuario u on (u.idUsuario = c.registradopor)"
			+ " INNER JOIN UsuarioRol ur on (u.idUsuario = ur.idUsuario)"
			+ " INNER JOIN Rol r on (ur.idRol = r.idRol)"
			+ " WHERE"
			+ " CAST(c.fechaRealizacion AS date) >= ?"
			+ " and CAST(c.fechaRealizacion AS date) <= ?"
			+ " and p.idProgramaAcademico = ?"
			+ " ORDER by c.fechaRealizacion ", nativeQuery = true)
	public List<Object[]> getInformeConvenios(Date fechaInicio, Date fechaFin, int idProgramaAcademico);

}
