package com.ufps.repository;

import org.springframework.data.repository.CrudRepository;

import com.ufps.entity.UsuarioRolEntity;

public interface UsuarioRolRepository extends CrudRepository<UsuarioRolEntity, Integer> {

}
