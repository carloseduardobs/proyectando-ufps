package com.ufps.payload;

public class ConveniosDashboardPayload {
	private String nombre;
	private String evidencia;
	private String extEvidencia;
	private String nombreEvidencia;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEvidencia() {
		return evidencia;
	}

	public void setEvidencia(String evidencia) {
		this.evidencia = evidencia;
	}

	public String getExtEvidencia() {
		return extEvidencia;
	}

	public void setExtEvidencia(String extEvidencia) {
		this.extEvidencia = extEvidencia;
	}

	public String getNombreEvidencia() {
		return nombreEvidencia;
	}

	public void setNombreEvidencia(String nombreEvidencia) {
		this.nombreEvidencia = nombreEvidencia;
	}

}
