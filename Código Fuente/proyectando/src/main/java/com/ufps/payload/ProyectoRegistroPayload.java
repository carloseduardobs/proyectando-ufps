package com.ufps.payload;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

public class ProyectoRegistroPayload {

	@NotNull
	@NotBlank(message = "El nombre del proyecto es requerido.")
	private String nombre;
	@NotNull
	@NotBlank(message = "La Descripción del proyecto es requerida.")
	private String descripcion;
	private String monto;
	@NotNull
	@NotBlank(message = "El beneficiario del proyecto es requerido.")
	private String beneficiario;
	@NotNull(message = "El proyecto debe pertenecer aun proceso misional.")
	private int idProcesoMisional;
	@NotNull(message = "El proyecto debe de ser de un Tipo.")
	private int idTipoProyecto;
	@NotNull(message = "El proyecto debe pertenecer a un plan de estudios.")
	private int idPlanEstudios;
	@NotNull(message = "La fecha de inicio del proyecto es requerida.")
	@DateTimeFormat(pattern = "dd.MM.yyyy")
	private Date fechaInicio;
	@NotNull(message = "La fecha de final del proyecto es requerida.")
	@DateTimeFormat(pattern = "dd.MM.yyyy")
	private Date fechaFin;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getMonto() {
		return monto;
	}

	public void setMonto(String monto) {
		this.monto = monto;
	}

	public String getBeneficiario() {
		return beneficiario;
	}

	public void setBeneficiario(String beneficiario) {
		this.beneficiario = beneficiario;
	}

	public int getIdProcesoMisional() {
		return idProcesoMisional;
	}

	public void setIdProcesoMisional(int idProcesoMisional) {
		this.idProcesoMisional = idProcesoMisional;
	}

	public int getIdTipoProyecto() {
		return idTipoProyecto;
	}

	public void setIdTipoProyecto(int idTipoProyecto) {
		this.idTipoProyecto = idTipoProyecto;
	}

	public int getIdPlanEstudios() {
		return idPlanEstudios;
	}

	public void setIdPlanEstudios(int idPlanEstudios) {
		this.idPlanEstudios = idPlanEstudios;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

}
