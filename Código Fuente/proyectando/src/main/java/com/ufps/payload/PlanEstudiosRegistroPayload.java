package com.ufps.payload;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class PlanEstudiosRegistroPayload {

	@NotNull
	@NotBlank(message = "El nombre del plan de estudios es requerido")
	private String nombre;
	@NotNull
	@NotBlank(message = "La url del plan de estudios es requerida")
	private String url;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
