package com.ufps.payload;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class EvidenciaPayload {

	@NotNull(message = "La evidencia debe tener Id.")
	private int id;
	@NotNull(message = "debes cargar el archivo.")
	@NotBlank(message = "debes cargar el archivo.")
	private String archivo;
	@NotNull(message = "La evidencia debe tener extensión.")
	@NotBlank(message = "La evidencia debe tener extensión.")
	private String extension;
	@NotNull(message = "La evidencia evidencia debe tener un nombre.")
	@NotBlank(message = "La evidencia evidencia debe tener un nombre.")
	private String nombre;
	private int idEvidenciaActividad;
	private int idEvidenciaProyecto;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getArchivo() {
		return archivo;
	}

	public void setArchivo(String archivo) {
		this.archivo = archivo;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getIdEvidenciaActividad() {
		return idEvidenciaActividad;
	}

	public void setIdEvidenciaActividad(int idEvidenciaActividad) {
		this.idEvidenciaActividad = idEvidenciaActividad;
	}

	public int getIdEvidenciaProyecto() {
		return idEvidenciaProyecto;
	}

	public void setIdEvidenciaProyecto(int idEvidenciaProyecto) {
		this.idEvidenciaProyecto = idEvidenciaProyecto;
	}

}
