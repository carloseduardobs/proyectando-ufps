package com.ufps.payload;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class RespuestaPreguntaPayload {

	@NotNull
	private Integer idRespuestaEncuesta;
	@NotNull
	private Integer idPregunta;
	@NotNull
	@NotBlank
	private String respuestas;

	public Integer getIdRespuestaEncuesta() {
		return idRespuestaEncuesta;
	}

	public void setIdRespuestaEncuesta(Integer idRespuestaEncuesta) {
		this.idRespuestaEncuesta = idRespuestaEncuesta;
	}

	public Integer getIdPregunta() {
		return idPregunta;
	}

	public void setIdPregunta(Integer idPregunta) {
		this.idPregunta = idPregunta;
	}

	public String getRespuestas() {
		return respuestas;
	}

	public void setRespuestas(String respuestas) {
		this.respuestas = respuestas;
	}

}
