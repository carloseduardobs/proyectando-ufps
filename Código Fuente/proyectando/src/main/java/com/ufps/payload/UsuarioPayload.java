package com.ufps.payload;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class UsuarioPayload {

	@NotNull(message = "Id de usuario es requerido")
	private int id;
	@NotNull
	@Email(message = "Debe ser un email válido")
	@NotBlank(message = "Correo es requerido")
	private String correo;
	@NotNull
	@NotBlank(message = "Nombres son requeridos")
	private String nombres;
	@NotNull
	@NotBlank(message = "Apellidos son requeridos")
	private String apellidos;
	@NotNull
	@NotBlank(message = "Estado es requerido")
	private String estado;
	@NotNull(message = "Plan de estudio requerido")
	private int idPlanEstudio;
	@NotNull(message = "Rol es requerido")
	private int idRol;
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public int getIdPlanEstudio() {
		return idPlanEstudio;
	}

	public void setIdPlanEstudio(int idPlanEstudio) {
		this.idPlanEstudio = idPlanEstudio;
	}

	public int getIdRol() {
		return idRol;
	}

	public void setIdRol(int idRol) {
		this.idRol = idRol;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

}
