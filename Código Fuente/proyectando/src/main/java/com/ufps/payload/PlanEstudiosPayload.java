package com.ufps.payload;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;


public class PlanEstudiosPayload extends PlanEstudiosRegistroPayload {

	@NotNull
	@Positive(message = "El valor debe ser mayor a 0")
	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	
}
