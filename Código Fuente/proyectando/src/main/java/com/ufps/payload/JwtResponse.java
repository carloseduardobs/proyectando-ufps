package com.ufps.payload;

import java.util.List;

public class JwtResponse {
	private String token;
	private String type = "Bearer";
	private Long id;
	private String email;
	private String name;
	private int idPlanEstudios;
	private List<String> roles;

	public JwtResponse(String accessToken, Long id, String email, int idPlanEstudios, List<String> roles, String name) {
		this.token = accessToken;
		this.id = id;
		this.email = email;
		this.idPlanEstudios = idPlanEstudios;
		this.roles = roles;
		this.name = name;
	}

	public String getAccessToken() {
		return token;
	}

	public void setAccessToken(String accessToken) {
		this.token = accessToken;
	}

	public String getTokenType() {
		return type;
	}

	public void setTokenType(String tokenType) {
		this.type = tokenType;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getIdPlanEstudios() {
		return idPlanEstudios;
	}

	public void setIdPlanEstudios(int idPlanEstudios) {
		this.idPlanEstudios = idPlanEstudios;
	}

	public List<String> getRoles() {
		return roles;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
