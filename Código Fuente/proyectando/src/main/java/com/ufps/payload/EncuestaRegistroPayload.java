package com.ufps.payload;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class EncuestaRegistroPayload {
	@NotNull
	@NotBlank
	private String nombre;
	@NotNull
	@NotBlank
	private Date fechaRealizacion;
	@NotNull
	private List<PreguntaRegistroPayload> preguntas;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getFechaRealizacion() {
		return fechaRealizacion;
	}

	public void setFechaRealizacion(Date fechaRealizacion) {
		this.fechaRealizacion = fechaRealizacion;
	}

	public List<PreguntaRegistroPayload> getPreguntas() {
		return preguntas;
	}

	public void setPreguntas(List<PreguntaRegistroPayload> preguntas) {
		this.preguntas = preguntas;
	}

}
