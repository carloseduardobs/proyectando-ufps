package com.ufps.payload;

import java.util.List;

public class UsuarioConsultarPayload extends UsuarioPayload {

	List<ProyectoPayload> proyectos;
	List<ActividadPayload> actividades;
	List<ConvenioPayload> convenios;
	List<EvidenciaActividadPayload> evidenciasActividad;
	List<EvidenciaProyectoPayload> evidenciasProyecto;

	public List<ProyectoPayload> getProyectos() {
		return proyectos;
	}

	public void setProyectos(List<ProyectoPayload> proyectos) {
		this.proyectos = proyectos;
	}

	public List<ActividadPayload> getActividades() {
		return actividades;
	}

	public void setActividades(List<ActividadPayload> actividades) {
		this.actividades = actividades;
	}

	public List<ConvenioPayload> getConvenios() {
		return convenios;
	}

	public void setConvenios(List<ConvenioPayload> convenios) {
		this.convenios = convenios;
	}

	public List<EvidenciaActividadPayload> getEvidenciasActividad() {
		return evidenciasActividad;
	}

	public void setEvidenciasActividad(List<EvidenciaActividadPayload> evidenciasActividad) {
		this.evidenciasActividad = evidenciasActividad;
	}

	public List<EvidenciaProyectoPayload> getEvidenciasProyecto() {
		return evidenciasProyecto;
	}

	public void setEvidenciasProyecto(List<EvidenciaProyectoPayload> evidenciasProyecto) {
		this.evidenciasProyecto = evidenciasProyecto;
	}

}
