package com.ufps.payload;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class InformacionInstitucionalRegistroPayload {

	@NotNull
	@NotBlank
	private String key;
	@NotNull
	@NotBlank
	private String titulo;
	@NotNull
	@NotBlank
	private String informacion;
	@NotNull
	private int idPlanEstudio;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getInformacion() {
		return informacion;
	}

	public void setInformacion(String informacion) {
		this.informacion = informacion;
	}

	public int getIdPlanEstudio() {
		return idPlanEstudio;
	}

	public void setIdPlanEstudio(int idPlanEstudio) {
		this.idPlanEstudio = idPlanEstudio;
	}

}
