package com.ufps.payload;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


public class RespuestaEncuestaRegistroPayload {

	@NotNull
	@NotBlank
	private String correo;
	@NotNull
	private Integer idProyecto;
	@NotNull
	private Integer idEncuesta;
	@NotNull
	@NotBlank
	private String urlResponse;

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public Integer getIdProyecto() {
		return idProyecto;
	}

	public void setIdProyecto(Integer idProyecto) {
		this.idProyecto = idProyecto;
	}

	public Integer getIdEncuesta() {
		return idEncuesta;
	}

	public void setIdEncuesta(Integer idEncuesta) {
		this.idEncuesta = idEncuesta;
	}

	public String getUrlResponse() {
		return urlResponse;
	}

	public void setUrlResponse(String urlResponse) {
		this.urlResponse = urlResponse;
	}

}
