package com.ufps.payload;

import java.util.List;

public class ActividadDashboardPayload {

	private int id;
	private String nombre;
	private String descripcion;
	private List<EvidenciaActividadDashboardPayload> evidencias;
	List<EvidenciaPayload> evidenciasActividad;
	private String nombreProyecto;

	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<EvidenciaActividadDashboardPayload> getEvidencias() {
		return evidencias;
	}

	public void setEvidencias(List<EvidenciaActividadDashboardPayload> evidencias) {
		this.evidencias = evidencias;
	}

	public List<EvidenciaPayload> getEvidenciasActividad() {
		return evidenciasActividad;
	}

	public void setEvidenciasActividad(List<EvidenciaPayload> evidenciasActividad) {
		this.evidenciasActividad = evidenciasActividad;
	}

	public String getNombreProyecto() {
		return nombreProyecto;
	}

	public void setNombreProyecto(String nombreProyecto) {
		this.nombreProyecto = nombreProyecto;
	}

	
}
