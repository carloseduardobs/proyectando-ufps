package com.ufps.payload;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class EvidenciaActividadRegistroPayload {

	@NotNull(message = "La fecha de realización de la evidencia es requerida.")
	private Date fechaRealizacion;
	@NotNull(message = "El nombre de la evidencia es requerida.")
	@NotBlank(message = "El nombre de la evidencia es requerida.")
	private String nombre;
	@NotNull(message = "La actividad a la cual pertenece la evidencia es requerida.")
	private int idActividad;
	@NotNull(message = "Debes subir las evidencias")
	private List<EvidenciaRegistroPayload> evidencias;

	public Date getFechaRealizacion() {
		return fechaRealizacion;
	}

	public void setFechaRealizacion(Date fechaRealizacion) {
		this.fechaRealizacion = fechaRealizacion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getIdActividad() {
		return idActividad;
	}

	public void setIdActividad(int idActividad) {
		this.idActividad = idActividad;
	}

	public List<EvidenciaRegistroPayload> getEvidencias() {
		return evidencias;
	}

	public void setEvidencias(List<EvidenciaRegistroPayload> evidencias) {
		this.evidencias = evidencias;
	}

}
