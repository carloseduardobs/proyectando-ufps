package com.ufps.payload;

import java.util.Date;
import java.util.List;

public class EvidenciaProyectoDashboardPayload {

	private int id;
	private Date fechaRealizacion;
	private String nombre;
	List<EvidenciaPayload> evidencia;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getFechaRealizacion() {
		return fechaRealizacion;
	}

	public void setFechaRealizacion(Date fechaRealizacion) {
		this.fechaRealizacion = fechaRealizacion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<EvidenciaPayload> getEvidencia() {
		return evidencia;
	}

	public void setEvidencia(List<EvidenciaPayload> evidencia) {
		this.evidencia = evidencia;
	}
}
