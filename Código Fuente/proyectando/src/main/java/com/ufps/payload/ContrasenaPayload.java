package com.ufps.payload;

import javax.validation.constraints.NotBlank;

import com.sun.istack.NotNull;

public class ContrasenaPayload {

	@NotNull
	@NotBlank(message = "Correo es requerido")
	private String correo;
	@NotNull
	@NotBlank(message = "Contraseña actual es requerida")
	private String contrasena;
	@NotNull
	@NotBlank(message = "Contraseña nueva es requerida")
	private String contrasenaNueva;
	@NotNull
	@NotBlank(message = "Contraseña nueva es requerida")
	private String contrasenaVerificacion;

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getContrasena() {
		return contrasena;
	}

	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

	public String getContrasenaNueva() {
		return contrasenaNueva;
	}

	public void setContrasenaNueva(String contrasenaNueva) {
		this.contrasenaNueva = contrasenaNueva;
	}

	public String getContrasenaVerificacion() {
		return contrasenaVerificacion;
	}

	public void setContrasenaVerificacion(String contrasenaVerificacion) {
		this.contrasenaVerificacion = contrasenaVerificacion;
	}

}
