package com.ufps.payload;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class InformacionInstitucionalPayload extends InformacionInstitucionalRegistroPayload {

	@NotNull
	private int id;
	@NotNull
	@NotBlank
	private String divulgar;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDivulgar() {
		return divulgar;
	}

	public void setDivulgar(String divulgar) {
		this.divulgar = divulgar;
	}

}
