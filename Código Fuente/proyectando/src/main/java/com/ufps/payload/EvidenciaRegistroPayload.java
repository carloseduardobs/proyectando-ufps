package com.ufps.payload;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class EvidenciaRegistroPayload {

	@NotNull(message = "debes cargar el archivo.")
	@NotBlank(message = "debes cargar el archivo.")
	private String archivo;
	@NotNull(message = "La evidencia debe tener extensión.")
	@NotBlank(message = "La evidencia debe tener extensión.")
	private String extension;
	@NotNull(message = "La evidencia evidencia debe tener un nombre.")
	@NotBlank(message = "La evidencia evidencia debe tener un nombre.")
	private String nombre;

	public String getArchivo() {
		return archivo;
	}

	public void setArchivo(String archivo) {
		this.archivo = archivo;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
