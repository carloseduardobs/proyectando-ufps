package com.ufps.payload;

import java.util.Date;

public class InformesPayload {

	private String tipoInforme;
	private Date fechaInicio;
	private Date fechaFin;
	private String aprobados;
	private String encuesta;

	public String getTipoInforme() {
		return tipoInforme;
	}

	public void setTipoInforme(String tipoInforme) {
		this.tipoInforme = tipoInforme;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public String getAprobados() {
		return aprobados;
	}

	public void setAprobados(String aprobados) {
		this.aprobados = aprobados;
	}

	public String getEncuesta() {
		return encuesta;
	}

	public void setEncuesta(String encuesta) {
		this.encuesta = encuesta;
	}

}
