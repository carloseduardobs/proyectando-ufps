package com.ufps.payload;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class ConvenioPayload {

	@NotNull(message = "El id del convenio es requerido.")
	private int id;
	@NotNull(message = "La fecha de realización del convenio es requerida.")
	private Date fechaRealizacion;
	@NotNull(message = "El convenio debe tener un representante.")
	@NotBlank(message = "El convenio debe tener un representante.")
	private String representante;
	@NotNull(message = "El convenio debe tener un nombre.")
	@NotBlank(message = "El convenio debe tener un nombre.")
	private String nombre;
	@NotNull(message = "El convenio debe tener una evidencia.")
	@NotBlank(message = "El convenio debe tener una evidencia.")
	private String evidencia;
	@NotNull(message = "La evidencia del convenio debe tener una extensi�n.")
	@NotBlank(message = "La evidencia del convenio debe tener una extensi�n.")
	private String extEvidencia;
	@NotNull(message = "La evidencia del convenio debe tener un nombre.")
	@NotBlank(message = "La evidencia del convenio debe tener un nombre.")
	private String nombreEvidencia;
	@NotNull(message = "La persona que registra el convenio es requerida.")
	private int registradoPor;
	@NotNull(message = "El campo aprobar debe ser requerido.")
	@NotBlank(message = "El campo aprobar debe ser requerido.")
	private String aprobar;
	@NotNull(message = "El campo divulgar debe ser requerido.")
	@NotBlank(message = "El campo divulgar debe ser requerido.")
	private String divulgar;
	@NotNull(message = "El proyecto al cual pertenece el convenio es requerido.")
	private int idProyecto;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getFechaRealizacion() {
		return fechaRealizacion;
	}

	public void setFechaRealizacion(Date fechaRealizacion) {
		this.fechaRealizacion = fechaRealizacion;
	}

	public String getRepresentante() {
		return representante;
	}

	public void setRepresentante(String representante) {
		this.representante = representante;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEvidencia() {
		return evidencia;
	}

	public void setEvidencia(String evidencia) {
		this.evidencia = evidencia;
	}

	public String getExtEvidencia() {
		return extEvidencia;
	}

	public void setExtEvidencia(String extEvidencia) {
		this.extEvidencia = extEvidencia;
	}

	public String getNombreEvidencia() {
		return nombreEvidencia;
	}

	public void setNombreEvidencia(String nombreEvidencia) {
		this.nombreEvidencia = nombreEvidencia;
	}

	public int getRegistradoPor() {
		return registradoPor;
	}

	public void setRegistradoPor(int registradoPor) {
		this.registradoPor = registradoPor;
	}

	public String getAprobar() {
		return aprobar;
	}

	public void setAprobar(String aprobar) {
		this.aprobar = aprobar;
	}

	public String getDivulgar() {
		return divulgar;
	}

	public void setDivulgar(String divulgar) {
		this.divulgar = divulgar;
	}

	public int getIdProyecto() {
		return idProyecto;
	}

	public void setIdProyecto(int idProyecto) {
		this.idProyecto = idProyecto;
	}

}
