package com.ufps.payload;

import java.util.Date;
import java.util.List;

public class ProyectoDashboardPayload {

	private int id;
	private String nombre;
	private Date fechaInicio;
	private Date fechaFin;
	private String descripcion;
	private String beneficiario;
	List<ActividadDashboardPayload> actividadesProyecto;
	List<EvidenciaProyectoDashboardPayload> evidenciasProyecto;
	List<EvidenciaPayload> evidencias;
	ConveniosDashboardPayload convenio;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<ActividadDashboardPayload> getActividadesProyecto() {
		return actividadesProyecto;
	}

	public void setActividadesProyecto(List<ActividadDashboardPayload> actividadesProyecto) {
		this.actividadesProyecto = actividadesProyecto;
	}

	public List<EvidenciaProyectoDashboardPayload> getEvidenciasProyecto() {
		return evidenciasProyecto;
	}

	public void setEvidenciasProyecto(List<EvidenciaProyectoDashboardPayload> evidenciasProyecto) {
		this.evidenciasProyecto = evidenciasProyecto;
	}

	public ConveniosDashboardPayload getConvenio() {
		return convenio;
	}

	public void setConvenio(ConveniosDashboardPayload convenio) {
		this.convenio = convenio;
	}

	public String getBeneficiario() {
		return beneficiario;
	}

	public void setBeneficiario(String beneficiario) {
		this.beneficiario = beneficiario;
	}

	public List<EvidenciaPayload> getEvidencias() {
		return evidencias;
	}

	public void setEvidencias(List<EvidenciaPayload> evidencias) {
		this.evidencias = evidencias;
	}

}
