package com.ufps.payload;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class EncuestaPayload {

	@NotNull
	private Integer id;
	@NotNull
	@NotBlank
	private String estado;
	@NotNull
	@NotBlank
	private Integer idPlanEstudio;
	@NotNull
	private List<PreguntaPayload> preguntas;
	@NotNull
	@NotBlank
	private String nombre;
	@NotNull
	@NotBlank
	private Date fechaRealizacion;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Integer getIdPlanEstudio() {
		return idPlanEstudio;
	}

	public void setIdPlanEstudio(Integer idPlanEstudio) {
		this.idPlanEstudio = idPlanEstudio;
	}

	public List<PreguntaPayload> getPreguntas() {
		return preguntas;
	}

	public void setPreguntas(List<PreguntaPayload> preguntas) {
		this.preguntas = preguntas;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getFechaRealizacion() {
		return fechaRealizacion;
	}

	public void setFechaRealizacion(Date fechaRealizacion) {
		this.fechaRealizacion = fechaRealizacion;
	}

}
