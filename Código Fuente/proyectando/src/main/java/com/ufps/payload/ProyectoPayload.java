package com.ufps.payload;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class ProyectoPayload {

	@NotBlank(message = "El Id del proyecto es requerido.")
	private int id;
	@NotNull
	@NotBlank(message = "El nombre del proyecto es requerido.")
	private String nombre;
	@NotNull
	@NotBlank(message = "La Descripción del proyecto es requerida.")
	private String descripcion;
	@NotNull
	@NotBlank(message = "El monto del proyecto es requerido.")
	private String monto;
	@NotNull
	@NotBlank(message = "El beneficiario del proyecto es requerido.")
	private String beneficiario;
	@NotBlank(message = "El proyecto debe pertenecer aun proceso misional.")
	private int idProcesoMisional;
	@NotBlank(message = "El proyecto debe de ser de un Tipo.")
	private int idTipoProyecto;
	@NotBlank(message = "El proyecto debe pertenecer a un plan de estudios.")
	private int idPlanEstudios;
	@NotNull
	@NotBlank(message = "La fecha de inicio del proyecto es requerida.")
	private Date fechaInicio;
	@NotNull
	@NotBlank(message = "La fecha de final del proyecto es requerida.")
	private Date fechaFin;
	@NotNull
	@NotBlank(message = "El campo aprobar debe ser requerido.")
	private String aprobar;
	@NotNull
	@NotBlank(message = "El campo divulgar debe ser requerido.")
	private String divulgar;
	@NotNull(message = "El campo registrado por es requerido.")
	private int registradoPor;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getMonto() {
		return monto;
	}

	public void setMonto(String monto) {
		this.monto = monto;
	}

	public String getBeneficiario() {
		return beneficiario;
	}

	public void setBeneficiario(String beneficiario) {
		this.beneficiario = beneficiario;
	}

	public int getIdProcesoMisional() {
		return idProcesoMisional;
	}

	public void setIdProcesoMisional(int idProcesoMisional) {
		this.idProcesoMisional = idProcesoMisional;
	}

	public int getIdTipoProyecto() {
		return idTipoProyecto;
	}

	public void setIdTipoProyecto(int idTipoProyecto) {
		this.idTipoProyecto = idTipoProyecto;
	}

	public int getIdPlanEstudios() {
		return idPlanEstudios;
	}

	public void setIdPlanEstudios(int idPlanEstudios) {
		this.idPlanEstudios = idPlanEstudios;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public String getAprobar() {
		return aprobar;
	}

	public void setAprobar(String aprobar) {
		this.aprobar = aprobar;
	}

	public String getDivulgar() {
		return divulgar;
	}

	public void setDivulgar(String divulgar) {
		this.divulgar = divulgar;
	}

	public int getRegistradoPor() {
		return registradoPor;
	}

	public void setRegistradoPor(int registradoPor) {
		this.registradoPor = registradoPor;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
