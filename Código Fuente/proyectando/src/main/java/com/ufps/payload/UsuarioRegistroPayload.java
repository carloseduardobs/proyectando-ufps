package com.ufps.payload;

import javax.validation.constraints.*;


public class UsuarioRegistroPayload {

	@NotNull
	@Email(message = "Debe ser un email válido")
	@NotBlank(message = "Correo es requerido")
	private String correo;
	@NotNull
	@NotBlank(message = "Contraseña es requerida")
	private String contrasena;
	@NotNull
	@NotBlank(message = "Nombres son requeridos")
	private String nombres;
	@NotNull
	@NotBlank(message = "Apellidos son requeridos")
	private String apellidos;
	@NotNull(message = "Plan de estudio requerido")
	private int idPlanEstudio;
	@NotNull(message = "Rol es requerido")
	private int idRol;

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getContrasena() {
		return contrasena;
	}

	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public int getIdPlanEstudio() {
		return idPlanEstudio;
	}

	public void setIdPlanEstudio(int idPlanEstudio) {
		this.idPlanEstudio = idPlanEstudio;
	}

	public int getIdRol() {
		return idRol;
	}

	public void setIdRol(int idRol) {
		this.idRol = idRol;
	}

}
