package com.ufps.payload;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class ActividadPayload {

	@NotNull(message = "El id de la actividad es requerido.")
	private int id;
	@NotNull(message = "La fecha de realización de la actividad es requerida.")
	private Date fechaRealizacion;
	@NotBlank(message = "El nombre de la actividad es requerido.")
	@NotNull(message = "El nombre de la actividad es requerido.")
	private String nombre;
	@NotBlank(message = "La descripción de la actividad es requerida.")
	@NotNull(message = "La descripción de la actividad es requerida.")
	private String descripcion;
	@NotNull(message = "La persona que registra la actividad es requerida.")
	private int registradoPor;
	@NotNull(message = "El campo aprobar debe ser requerido.")
	@NotBlank(message = "El campo aprobar debe ser requerido.")
	private String aprobar;
	@NotNull(message = "El campo divulgar debe ser requerido.")
	@NotBlank(message = "El campo divulgar debe ser requerido.")
	private String divulgar;
	@NotNull(message = "El proyecto de la actividad es requerido.")
	private int idProyecto;
	@NotBlank(message = "La ciudad de la actividad es requerida.")
	@NotNull(message = "La ciudad de la actividad es requerida.")
	private String ciudad;
	@NotBlank(message = "El pais de la actividad es requerido.")
	@NotNull(message = "El pais de la actividad es requerido.")
	private String pais;
	@NotBlank(message = "El beneficiario de la actividad es requerido.")
	@NotNull(message = "El beneficiario de la actividad es requerido.")
	private String beneficiario;
	private String tipo;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getFechaRealizacion() {
		return fechaRealizacion;
	}

	public void setFechaRealizacion(Date fechaRealizacion) {
		this.fechaRealizacion = fechaRealizacion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getRegistradoPor() {
		return registradoPor;
	}

	public void setRegistradoPor(int registradoPor) {
		this.registradoPor = registradoPor;
	}

	public String getAprobar() {
		return aprobar;
	}

	public void setAprobar(String aprobar) {
		this.aprobar = aprobar;
	}

	public String getDivulgar() {
		return divulgar;
	}

	public void setDivulgar(String divulgar) {
		this.divulgar = divulgar;
	}

	public int getIdProyecto() {
		return idProyecto;
	}

	public void setIdProyecto(int idProyecto) {
		this.idProyecto = idProyecto;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getBeneficiario() {
		return beneficiario;
	}

	public void setBeneficiario(String beneficiario) {
		this.beneficiario = beneficiario;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

}
