package com.ufps.payload;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class EvidenciaProyectoPayload {

	@NotNull(message = "El id de la evidencia es requerido.")
	private int id;
	@NotNull(message = "La fecha de realización de la evidencia es requerida.")
	private Date fechaRealizacion;
	@NotNull(message = "El nombre de la evidencia es requerida.")
	@NotBlank(message = "El nombre de la evidencia es requerida.")
	private String nombre;
	@NotNull(message = "La persona que registra la evidencia es requerida.")
	private int registradoPor;
	@NotNull(message = "El campo aprobar debe ser requerido.")
	@NotBlank(message = "El campo aprobar debe ser requerido.")
	private String aprobar;
	@NotNull(message = "El campo divulgar debe ser requerido.")
	@NotBlank(message = "El campo divulgar debe ser requerido.")
	private String divulgar;
	@NotNull(message = "El proyecto al cual pertenece la evidencia es requerido.")
	private int idProyecto;
	@NotNull(message = "Debes subir las evidencias")
	private List<EvidenciaPayload> evidencias;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getFechaRealizacion() {
		return fechaRealizacion;
	}

	public void setFechaRealizacion(Date fechaRealizacion) {
		this.fechaRealizacion = fechaRealizacion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getRegistradoPor() {
		return registradoPor;
	}

	public void setRegistradoPor(int registradoPor) {
		this.registradoPor = registradoPor;
	}

	public String getAprobar() {
		return aprobar;
	}

	public void setAprobar(String aprobar) {
		this.aprobar = aprobar;
	}

	public String getDivulgar() {
		return divulgar;
	}

	public void setDivulgar(String divulgar) {
		this.divulgar = divulgar;
	}

	public int getIdProyecto() {
		return idProyecto;
	}

	public void setIdProyecto(int idProyecto) {
		this.idProyecto = idProyecto;
	}

	public List<EvidenciaPayload> getEvidencias() {
		return evidencias;
	}

	public void setEvidencias(List<EvidenciaPayload> evidencias) {
		this.evidencias = evidencias;
	}
}
