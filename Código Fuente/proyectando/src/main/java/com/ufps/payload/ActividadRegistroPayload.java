package com.ufps.payload;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class ActividadRegistroPayload {

	@NotNull(message = "La fecha de realización de la actividad es requerida.")
	private Date fechaRealizacion;
	@NotBlank(message = "El nombre de la actividad es requerido.")
	@NotNull(message = "El nombre de la actividad es requerido.")
	private String nombre;
	@NotBlank(message = "La descripcón de la actividad es requerida.")
	@NotNull(message = "La descripcón de la actividad es requerida.")
	private String descripcion;
	@NotNull(message = "El proyecto de la actividad es requerido.")
	private int idProyecto;
	@NotBlank(message = "La ciudad de la actividad es requerida.")
	@NotNull(message = "La ciudad de la actividad es requerida.")
	private String ciudad;
	@NotBlank(message = "El pais de la actividad es requerido.")
	@NotNull(message = "El pais de la actividad es requerido.")
	private String pais;
	@NotBlank(message = "El beneficiario de la actividad es requerido.")
	@NotNull(message = "El beneficiario de la actividad es requerido.")
	private String beneficiario;
	private String tipo;
	

	public Date getFechaRealizacion() {
		return fechaRealizacion;
	}

	public void setFechaRealizacion(Date fechaRealizacion) {
		this.fechaRealizacion = fechaRealizacion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getIdProyecto() {
		return idProyecto;
	}

	public void setIdProyecto(int idProyecto) {
		this.idProyecto = idProyecto;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getBeneficiario() {
		return beneficiario;
	}

	public void setBeneficiario(String beneficiario) {
		this.beneficiario = beneficiario;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

}
