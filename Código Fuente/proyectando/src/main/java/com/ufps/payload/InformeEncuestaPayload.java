package com.ufps.payload;


public class InformeEncuestaPayload {
	private String proyecto;
	private String correo;
	private String fechaEnvio;
	private String fechaRealizacion;
	private String idEncuesta;
	private String idRespuestaEncuesta;

	public String getProyecto() {
		return proyecto;
	}

	public void setProyecto(String proyecto) {
		this.proyecto = proyecto;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getFechaEnvio() {
		return fechaEnvio;
	}

	public void setFechaEnvio(String fechaEnvio) {
		this.fechaEnvio = fechaEnvio;
	}

	public String getFechaRealizacion() {
		return fechaRealizacion;
	}

	public void setFechaRealizacion(String fechaRealizacion) {
		this.fechaRealizacion = fechaRealizacion;
	}

	public String getIdEncuesta() {
		return idEncuesta;
	}

	public void setIdEncuesta(String idEncuesta) {
		this.idEncuesta = idEncuesta;
	}

	public String getIdRespuestaEncuesta() {
		return idRespuestaEncuesta;
	}

	public void setIdRespuestaEncuesta(String idRespuestaEncuesta) {
		this.idRespuestaEncuesta = idRespuestaEncuesta;
	}

}
