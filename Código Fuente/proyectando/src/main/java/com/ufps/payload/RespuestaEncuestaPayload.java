package com.ufps.payload;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class RespuestaEncuestaPayload extends RespuestaEncuestaRegistroPayload {

	@NotNull
	private Integer id;
	@NotNull
	@NotBlank
	private Date fechaEnvio;
	@NotNull
	@NotBlank
	private Date fechaRealizacion;
	@NotNull
	@NotBlank
	private String key;
	@NotNull
	@NotBlank
	private String estado;
	@NotNull
	private List<RespuestaPreguntaPayload> respuestas;
	private String nombreProyecto;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getFechaEnvio() {
		return fechaEnvio;
	}

	public void setFechaEnvio(Date fechaEnvio) {
		this.fechaEnvio = fechaEnvio;
	}

	public Date getFechaRealizacion() {
		return fechaRealizacion;
	}

	public void setFechaRealizacion(Date fechaRealizacion) {
		this.fechaRealizacion = fechaRealizacion;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public List<RespuestaPreguntaPayload> getRespuestas() {
		return respuestas;
	}

	public void setRespuestas(List<RespuestaPreguntaPayload> respuestas) {
		this.respuestas = respuestas;
	}

	public String getNombreProyecto() {
		return nombreProyecto;
	}

	public void setNombreProyecto(String nombreProyecto) {
		this.nombreProyecto = nombreProyecto;
	}

}
