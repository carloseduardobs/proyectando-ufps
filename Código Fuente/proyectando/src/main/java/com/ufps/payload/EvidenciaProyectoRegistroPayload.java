package com.ufps.payload;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class EvidenciaProyectoRegistroPayload {

	@NotNull(message = "La fecha de realización de la evidencia es requerida.")
	private Date fechaRealizacion;
	@NotNull(message = "El nombre de la evidencia es requerida.")
	@NotBlank(message = "El nombre de la evidencia es requerida.")
	private String nombre;
	@NotNull(message = "El proyecto al cual pertenece la evidencia es requerido.")
	private int idProyecto;
	@NotNull(message = "Debes subir las evidencias")
	private List<EvidenciaRegistroPayload> evidencias;

	public Date getFechaRealizacion() {
		return fechaRealizacion;
	}

	public void setFechaRealizacion(Date fechaRealizacion) {
		this.fechaRealizacion = fechaRealizacion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getIdProyecto() {
		return idProyecto;
	}

	public void setIdProyecto(int idProyecto) {
		this.idProyecto = idProyecto;
	}

	public List<EvidenciaRegistroPayload> getEvidencias() {
		return evidencias;
	}

	public void setEvidencias(List<EvidenciaRegistroPayload> evidencias) {
		this.evidencias = evidencias;
	}

}
