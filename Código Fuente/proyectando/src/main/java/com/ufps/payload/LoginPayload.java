package com.ufps.payload;

import javax.validation.constraints.NotBlank;

import com.sun.istack.NotNull;

public class LoginPayload {

	@NotNull
	@NotBlank(message = "Correo es requerido")
	private String correo;

	@NotNull
	@NotBlank(message = "Contraseña es requerida")
	private String contrasena;

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getContrasena() {
		return contrasena;
	}

	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

}
