package com.ufps.payload;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class ConvenioRegistroPayload {

	@NotNull(message = "La fecha de realización del convenio es requerida.")
	private Date fechaRealizacion;
	@NotNull(message = "El convenio debe tener un representante.")
	@NotBlank(message = "El convenio debe tener un representante.")
	private String representante;
	@NotNull(message = "El convenio debe tener un nombre.")
	@NotBlank(message = "El convenio debe tener un nombre.")
	private String nombre;
	@NotNull(message = "El convenio debe tener una evidencia.")
	@NotBlank(message = "El convenio debe tener una evidencia.")
	private String evidencia;
	@NotNull(message = "La evidencia del convenio debe tener una extensi�n.")
	@NotBlank(message = "La evidencia del convenio debe tener una extensi�n.")
	private String extEvidencia;
	@NotNull(message = "La evidencia del convenio debe tener un nombre.")
	@NotBlank(message = "La evidencia del convenio debe tener un nombre.")
	private String nombreEvidencia;
	@NotNull(message = "El proyecto al cual pertenece el convenio es requerido.")
	private int idProyecto;

	public Date getFechaRealizacion() {
		return fechaRealizacion;
	}

	public void setFechaRealizacion(Date fechaRealizacion) {
		this.fechaRealizacion = fechaRealizacion;
	}

	public String getRepresentante() {
		return representante;
	}

	public void setRepresentante(String representante) {
		this.representante = representante;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEvidencia() {
		return evidencia;
	}

	public void setEvidencia(String evidencia) {
		this.evidencia = evidencia;
	}

	public String getExtEvidencia() {
		return extEvidencia;
	}

	public void setExtEvidencia(String extEvidencia) {
		this.extEvidencia = extEvidencia;
	}

	public String getNombreEvidencia() {
		return nombreEvidencia;
	}

	public void setNombreEvidencia(String nombreEvidencia) {
		this.nombreEvidencia = nombreEvidencia;
	}

	public int getIdProyecto() {
		return idProyecto;
	}

	public void setIdProyecto(int idProyecto) {
		this.idProyecto = idProyecto;
	}

}
