package com.ufps.service;


import java.util.List;
import java.util.Optional;

import com.ufps.entity.RespuestaPreguntaEntity;
import com.ufps.utilities.BaseServiceInterface;

public interface RespuestaPreguntaInterface extends BaseServiceInterface<RespuestaPreguntaEntity> {

	Optional<RespuestaPreguntaEntity> findByIdRespuestaEncuestaAndIdPregunta(int idRespuestaEncuesta, int idPregunta);

	List<RespuestaPreguntaEntity> findAllByIdRespuestaEncuesta(Integer idRespuestaEncuesta);
}
