package com.ufps.service;

import java.util.Date;
import java.util.List;


import com.ufps.entity.ProyectoEntity;
import com.ufps.utilities.BaseServiceInterface;

public interface ProyectoInterface extends BaseServiceInterface<ProyectoEntity> {

	public List<ProyectoEntity> getAllIdPlanEstudios(int idPlanEstudios); 
	
	public List<ProyectoEntity> findAllByRegistradoPor(int registradoPor);

	public List<ProyectoEntity> findAllDivulgadosByUrl(String url);

	public List<ProyectoEntity> findAllDivulgadosAndEventosByUrl(String url);

	public List<Object[]> getInformeProyecto(Date fechaInicio, Date fechaFin, String aprobado, int idProgramaAcademico);

	public List<Object[]> getInformeProyecto(Date fechaInicio, Date fechaFin, int idProgramaAcademico);
}
