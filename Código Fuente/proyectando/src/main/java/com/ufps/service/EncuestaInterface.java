package com.ufps.service;


import java.util.List;

import com.ufps.entity.EncuestaEntity;
import com.ufps.utilities.BaseServiceInterface;


public interface EncuestaInterface extends BaseServiceInterface<EncuestaEntity> {

	List<EncuestaEntity> findAllByIdPlanEstudio(Integer idPlanEstudio);
	
	List<EncuestaEntity> findAllByIdPlanEstudioAndEstado(Integer idPlanEstudio, String estado);
}
