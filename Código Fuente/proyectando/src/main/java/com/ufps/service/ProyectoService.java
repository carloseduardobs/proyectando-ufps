package com.ufps.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ufps.entity.ProyectoEntity;
import com.ufps.repository.ProyectoRepository;


@Service
public class ProyectoService implements ProyectoInterface {

	@Autowired
	private ProyectoRepository repository;

	@Override
	public List<ProyectoEntity> getAll() {
		// TODO Auto-generated method stub
		return (List<ProyectoEntity>) repository.findAll();
	}

	@Override
	public ProyectoEntity getById(int id) {
		// TODO Auto-generated method stub
		return repository.findById(id).isPresent() ?
				repository.findById(id).orElse(null) : null;
	}

	@Override
	public ProyectoEntity save(ProyectoEntity entity) {
		// TODO Auto-generated method stub
		return repository.save(entity);
	}

	@Override
	public ProyectoEntity update(ProyectoEntity entity) {
		// TODO Auto-generated method stub
		return repository.findById(entity.getId()).isPresent() ?
				repository.save(entity) : null;
	}

	@Override
	public void delete(int id) {
		repository.deleteById(id);		
	}

	@Override
	public List<ProyectoEntity> getAllIdPlanEstudios(int idPlanEstudios) {
		// TODO Auto-generated method stub
		return repository.findAllByIdPlanEstudios(idPlanEstudios);
	}

	@Override
	public List<ProyectoEntity> findAllByRegistradoPor(int registradoPor) {
		return repository.findAllByRegistradoPor(registradoPor);
	}

	@Override
	public List<ProyectoEntity> findAllDivulgadosAndEventosByUrl(String url) {
		// TODO Auto-generated method stub
		return repository.findAllDivulgadosAndEventosByUrl(url);
	}

	@Override
	public List<ProyectoEntity> findAllDivulgadosByUrl(String url) {
		// TODO Auto-generated method stub
		return repository.findAllDivulgadosByUrl(url);
	}

	@Override
	public List<Object[]> getInformeProyecto(Date fechaInicio, Date fechaFin, String aprobado, int idProgramaAcademico) {
		// TODO Auto-generated method stub
		return repository.getInformeProyecto(fechaInicio, fechaFin, aprobado, idProgramaAcademico);
	}

	@Override
	public List<Object[]> getInformeProyecto(Date fechaInicio, Date fechaFin, int idProgramaAcademico) {
		// TODO Auto-generated method stub
		return repository.getInformeProyecto(fechaInicio, fechaFin, idProgramaAcademico);
	}
}
