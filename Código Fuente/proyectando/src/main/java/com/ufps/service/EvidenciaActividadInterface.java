package com.ufps.service;

import java.util.List;

import com.ufps.entity.EvidenciaActividadEntity;
import com.ufps.utilities.BaseServiceInterface;

public interface EvidenciaActividadInterface extends BaseServiceInterface<EvidenciaActividadEntity> {

	public List<EvidenciaActividadEntity> findAllByRegistradoPor(int registradoPor);

	public List<EvidenciaActividadEntity> findAllByIdActividad(int idActividad);

	public List<EvidenciaActividadEntity> getAllIdPlanEstudios(int idPlanEstudios);

	public List<EvidenciaActividadEntity> findAllByActividadAndDivulgado(int idActividad);
}
