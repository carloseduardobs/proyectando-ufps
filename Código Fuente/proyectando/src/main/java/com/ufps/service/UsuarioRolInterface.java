package com.ufps.service;

import com.ufps.entity.UsuarioRolEntity;
import com.ufps.utilities.BaseServiceInterface;

public interface UsuarioRolInterface extends BaseServiceInterface<UsuarioRolEntity> {

}
