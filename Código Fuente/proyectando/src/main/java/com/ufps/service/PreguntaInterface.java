package com.ufps.service;

import java.util.List;

import com.ufps.entity.PreguntaEntity;
import com.ufps.utilities.BaseServiceInterface;

public interface PreguntaInterface extends BaseServiceInterface<PreguntaEntity> {

	List<PreguntaEntity> findAllByIdEncuesta(int idEncuesta);
}
