package com.ufps.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ufps.entity.InformacionInstitucionalEntity;
import com.ufps.repository.InformacionInstitucionalRepository;


@Service
public class InformacionInstitucionalService implements InformacionInstitucionalInterface {

	@Autowired
	private InformacionInstitucionalRepository repository;

	@Override
	public List<InformacionInstitucionalEntity> getAll() {
		// TODO Auto-generated method stub
		return (List<InformacionInstitucionalEntity>) repository.findAll();
	}

	@Override
	public InformacionInstitucionalEntity getById(int id) {
		// TODO Auto-generated method stub
		return repository.findById(id).isPresent() ?
				repository.findById(id).orElse(null) : null;
	}

	@Override
	public InformacionInstitucionalEntity save(InformacionInstitucionalEntity entity) {
		// TODO Auto-generated method stub
		return repository.save(entity);
	}

	@Override
	public InformacionInstitucionalEntity update(InformacionInstitucionalEntity entity) {
		// TODO Auto-generated method stub
		return repository.findById(entity.getId()).isPresent() ?
				repository.save(entity) : null;
	}

	@Override
	public void delete(int id) {
		repository.deleteById(id);		
	}

	@Override
	public List<InformacionInstitucionalEntity> findAllByIdPlanEstudio(int idPlanEstudio) {
		// TODO Auto-generated method stub
		return repository.findAllByIdPlanEstudio(idPlanEstudio);
	}

	@Override
	public List<InformacionInstitucionalEntity> findAllByUrl(String url, String logo, String inicio) {
		// TODO Auto-generated method stub
		return repository.findAllByUrl(url, logo, inicio);
	}

	@Override
	public InformacionInstitucionalEntity getLogo(String url, String logo) {
		// TODO Auto-generated method stub
		return repository.getLogo(url, logo);
	}

	@Override
	public InformacionInstitucionalEntity findAllByUrlAndPath(String url, String path) {
		// TODO Auto-generated method stub
		return repository.findAllByUrlAndPath(url, path);
	}

}
