package com.ufps.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ufps.entity.UsuarioEntity;
import com.ufps.repository.UsuarioRepository;

@Service
public class UsuarioService implements UsuarioInterface {

	@Autowired
	private UsuarioRepository repository;

	
	@Override
	public List<UsuarioEntity> getAll() {
		return (List<UsuarioEntity>) repository.findAll();
	}

	@Override
	public UsuarioEntity getById(int id) {
		// TODO Auto-generated method stub
		return repository.findById(id).isPresent() ? repository.findById(id).orElse(null) : null;
	}

	@Override
	public UsuarioEntity save(UsuarioEntity entity) {
		// TODO Auto-generated method stub
		return repository.save(entity); 
	}

	@Override
	public UsuarioEntity update(UsuarioEntity entity) {
		// TODO Auto-generated method stub
		return repository.findById(entity.getId()).isPresent() ? repository.save(entity) : null;
	}

	@Override
	public void delete(int id) {
		repository.deleteById(id);
	}


	public Optional<UsuarioEntity> findByCorreo(String correo) {
		return repository.findByCorreo(correo);
	}
	
	public boolean existsByCorreo(String correo) {
		return repository.existsByCorreo(correo);
	}

	public Optional<UsuarioEntity> findByidPlanEstudioandidRol(int idPlanEstudio, int idRol) {
		return repository.findByidPlanEstudioandidRol(idPlanEstudio, idRol);
	}

	@Override
	public List<UsuarioEntity> getAllIdPlanEstudios(int idPlanEstudios) {
		// TODO Auto-generated method stub
		return repository.findAllByIdPlanEstudio(idPlanEstudios);
	}

	@Override
	public List<Object[]> getInformeUsuarios(Date fechaInicio, Date fechaFin, String aprobado,
			int idProgramaAcademico) {
		// TODO Auto-generated method stub
		return repository.getInformeUsuarios(fechaInicio, fechaFin, aprobado, idProgramaAcademico);
	}

	@Override
	public List<Object[]> getInformeUsuarios(Date fechaInicio, Date fechaFin, int idProgramaAcademico) {
		// TODO Auto-generated method stub
		return repository.getInformeUsuarios(fechaInicio, fechaFin, idProgramaAcademico);
	}
}
