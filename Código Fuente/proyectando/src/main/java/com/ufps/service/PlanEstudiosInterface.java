package com.ufps.service;

import com.ufps.entity.PlanEstudiosEntity;
import com.ufps.utilities.BaseServiceInterface;

public interface PlanEstudiosInterface extends BaseServiceInterface<PlanEstudiosEntity> {

	boolean existeUrl(String url);
}
