package com.ufps.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ufps.entity.TipoProyectoEntity;
import com.ufps.repository.TipoProyectoRepository;


@Service
public class TipoProyectoService implements TipoProyectoInterface {
	
	@Autowired
	private TipoProyectoRepository repository;

	@Override
	public List<TipoProyectoEntity> getAll() {
		// TODO Auto-generated method stub
		return (List<TipoProyectoEntity>) repository.findAll();
	}

	@Override
	public TipoProyectoEntity getById(int id) {
		// TODO Auto-generated method stub
		return repository.findById(id).isPresent() ?
				repository.findById(id).orElse(null) : null;
	}

	@Override
	public TipoProyectoEntity save(TipoProyectoEntity entity) {
		// TODO Auto-generated method stub
		return repository.save(entity);
	}

	@Override
	public TipoProyectoEntity update(TipoProyectoEntity entity) {
		// TODO Auto-generated method stub
		return repository.findById(entity.getId()).isPresent() ?
				repository.save(entity) : null;
	}

	@Override
	public void delete(int id) {
		repository.deleteById(id);		
	}

}
