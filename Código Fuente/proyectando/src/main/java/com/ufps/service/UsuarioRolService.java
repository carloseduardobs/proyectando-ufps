package com.ufps.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ufps.entity.UsuarioRolEntity;
import com.ufps.repository.UsuarioRolRepository;


@Service
public class UsuarioRolService implements UsuarioRolInterface {

	@Autowired
	private UsuarioRolRepository repository;

	@Override
	public List<UsuarioRolEntity> getAll() {
		// TODO Auto-generated method stub
		return (List<UsuarioRolEntity>) repository.findAll();
	}

	@Override
	public UsuarioRolEntity getById(int id) {
		// TODO Auto-generated method stub
		return repository.findById(id).isPresent() ?
				repository.findById(id).orElse(null) : null;
	}

	@Override
	public UsuarioRolEntity save(UsuarioRolEntity entity) {
		// TODO Auto-generated method stub
		return repository.save(entity);
	}

	@Override
	public UsuarioRolEntity update(UsuarioRolEntity entity) {
		// TODO Auto-generated method stub
		return repository.findById(entity.getIdUsuario()).isPresent() ?
				repository.save(entity) : null;
	}

	@Override
	public void delete(int id) {
		repository.deleteById(id);		
	}

}
