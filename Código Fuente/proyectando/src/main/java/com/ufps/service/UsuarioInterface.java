package com.ufps.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import com.ufps.entity.UsuarioEntity;
import com.ufps.utilities.BaseServiceInterface;

public interface UsuarioInterface extends BaseServiceInterface<UsuarioEntity> {
	
	public Optional<UsuarioEntity> findByCorreo(String correo);
	
	public boolean existsByCorreo(String correo);
	
	public Optional<UsuarioEntity> findByidPlanEstudioandidRol(int idPlanEstudio, int idRol);
	
	public List<UsuarioEntity> getAllIdPlanEstudios(int idPlanEstudios);

	public List<Object[]> getInformeUsuarios(Date fechaInicio, Date fechaFin, String aprobado,
			int idProgramaAcademico);

	public List<Object[]> getInformeUsuarios(Date fechaInicio, Date fechaFin, int idProgramaAcademico);
}
