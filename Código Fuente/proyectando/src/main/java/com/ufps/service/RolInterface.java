package com.ufps.service;

import com.ufps.entity.RolEntity;
import com.ufps.utilities.BaseServiceInterface;

public interface RolInterface extends BaseServiceInterface<RolEntity> {

}
