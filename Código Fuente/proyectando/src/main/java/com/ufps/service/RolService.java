package com.ufps.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ufps.entity.RolEntity;
import com.ufps.repository.RolRepository;


@Service
public class RolService implements RolInterface {

	@Autowired
	private RolRepository repository;

	@Override
	public List<RolEntity> getAll() {
		// TODO Auto-generated method stub
		return (List<RolEntity>) repository.findAll();
	}

	@Override
	public RolEntity getById(int id) {
		// TODO Auto-generated method stub
		return repository.findById(id).isPresent() ?
				repository.findById(id).orElse(null) : null;
	}

	@Override
	public RolEntity save(RolEntity entity) {
		// TODO Auto-generated method stub
		return repository.save(entity);
	}

	@Override
	public RolEntity update(RolEntity entity) {
		// TODO Auto-generated method stub
		return repository.findById(entity.getId()).isPresent() ?
				repository.save(entity) : null;
	}

	@Override
	public void delete(int id) {
		repository.deleteById(id);		
	}

}
