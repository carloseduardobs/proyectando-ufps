package com.ufps.service;

import java.util.List;

import com.ufps.entity.EvidenciaEntity;
import com.ufps.utilities.BaseServiceInterface;

public interface EvidenciaInterface extends BaseServiceInterface<EvidenciaEntity> {

	List<EvidenciaEntity> getAllByIdEvidenciaActividad(int idEvidenciaActividad);

	List<EvidenciaEntity> getAllByIdEvidenciaProyecto(int idEvidenciaProyecto);
}
