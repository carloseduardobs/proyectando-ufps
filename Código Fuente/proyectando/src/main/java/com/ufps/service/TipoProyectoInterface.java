package com.ufps.service;

import com.ufps.entity.TipoProyectoEntity;
import com.ufps.utilities.BaseServiceInterface;

public interface TipoProyectoInterface extends BaseServiceInterface<TipoProyectoEntity> {

}
