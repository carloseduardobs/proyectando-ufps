package com.ufps.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ufps.entity.EvidenciaProyectoEntity;
import com.ufps.repository.EvidenciaProyectoRepository;


@Service
public class EvidenciaProyectoService implements EvidenciaProyectoInterface {

	@Autowired
	private EvidenciaProyectoRepository repository;

	@Override
	public List<EvidenciaProyectoEntity> getAll() {
		// TODO Auto-generated method stub
		return (List<EvidenciaProyectoEntity>) repository.findAll();
	}

	@Override
	public EvidenciaProyectoEntity getById(int id) {
		// TODO Auto-generated method stub
		return repository.findById(id).isPresent() ?
				repository.findById(id).orElse(null) : null;
	}

	@Override
	public EvidenciaProyectoEntity save(EvidenciaProyectoEntity entity) {
		// TODO Auto-generated method stub
		return repository.save(entity);
	}

	@Override
	public EvidenciaProyectoEntity update(EvidenciaProyectoEntity entity) {
		// TODO Auto-generated method stub
		return repository.findById(entity.getId()).isPresent() ?
				repository.save(entity) : null;
	}

	@Override
	public void delete(int id) {
		repository.deleteById(id);		
	}

	@Override
	public List<EvidenciaProyectoEntity> findAllByRegistradoPor(int registradoPor) {
		// TODO Auto-generated method stub
		return repository.findAllByRegistradoPor(registradoPor);
	}

	@Override
	public List<EvidenciaProyectoEntity> findAllByIdProyecto(int idProyecto) {
		// TODO Auto-generated method stub
		return repository.findAllByIdProyecto(idProyecto);
	}

	@Override
	public List<EvidenciaProyectoEntity> getAllIdPlanEstudios(int idPlanEstudios) {
		// TODO Auto-generated method stub
		return repository.findAllByIdPlanEstudios(idPlanEstudios);
	}

	@Override
	public List<EvidenciaProyectoEntity> findAllByProyectoAndDivulgado(int idProyecto) {
		// TODO Auto-generated method stub
		return repository.findAllByProyectoAndDivulgado(idProyecto);
	}


}
