package com.ufps.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ufps.entity.ProcesoMisionalEntity;
import com.ufps.repository.ProcesoMisionalRepository;

@Service
public class ProcesoMisionalService implements ProcesoMisionalInterface {

	@Autowired
	private ProcesoMisionalRepository repository;

	@Override
	public List<ProcesoMisionalEntity> getAll() {
		// TODO Auto-generated method stub
		return (List<ProcesoMisionalEntity>) repository.findAll();
	}

	@Override
	public ProcesoMisionalEntity getById(int id) {
		// TODO Auto-generated method stub
		return repository.findById(id).isPresent() ?
				repository.findById(id).orElse(null) : null;
	}

	@Override
	public ProcesoMisionalEntity save(ProcesoMisionalEntity entity) {
		// TODO Auto-generated method stub
		return repository.save(entity);
	}

	@Override
	public ProcesoMisionalEntity update(ProcesoMisionalEntity entity) {
		// TODO Auto-generated method stub
		return repository.findById(entity.getId()).isPresent() ?
				repository.save(entity) : null;
	}

	@Override
	public void delete(int id) {
		repository.deleteById(id);		
	}

}
