package com.ufps.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ufps.entity.ActividadEntity;
import com.ufps.repository.ActividadRepository;

@Service
public class ActividadService implements ActividadInterface {

	@Autowired
	private ActividadRepository repository;

	@Override
	public List<ActividadEntity> getAll() {
		// TODO Auto-generated method stub
		return (List<ActividadEntity>) repository.findAll();
	}

	@Override
	public ActividadEntity getById(int id) {
		// TODO Auto-generated method stub
		return repository.findById(id).isPresent() ?
				repository.findById(id).orElse(null) : null;
	}

	@Override
	public ActividadEntity save(ActividadEntity entity) {
		// TODO Auto-generated method stub
		return repository.save(entity);
	}

	@Override
	public ActividadEntity update(ActividadEntity entity) {
		// TODO Auto-generated method stub
		return repository.findById(entity.getId()).isPresent() ?
				repository.save(entity) : null;
	}

	@Override
	public void delete(int id) {
		repository.deleteById(id);		
	}

	@Override
	public List<ActividadEntity> findAllByRegistradoPor(int registradoPor) {
		// TODO Auto-generated method stub
		return repository.findAllByRegistradoPor(registradoPor);
	}

	@Override
	public List<ActividadEntity> findAllByIdProyecto(int idProyecto) {
		// TODO Auto-generated method stub
		return repository.findAllByIdProyecto(idProyecto);
	}

	@Override
	public List<ActividadEntity> getAllIdPlanEstudios(int idPlanEstudios) {
		// TODO Auto-generated method stub
		return repository.findAllByIdPlanEstudios(idPlanEstudios);
	}

	@Override
	public List<ActividadEntity> findAllByProyectoAndDivulgado(int idProyecto) {
		// TODO Auto-generated method stub
		return repository.findAllByProyectoAndDivulgado(idProyecto);
	}

	public List<ActividadEntity> findAllByProyectoAndDivulgadoEventos(int idProyecto) {
		// TODO Auto-generated method stub
		return repository.findAllByProyectoAndDivulgadoEventos(idProyecto);
	}

	@Override
	public List<Object[]> getInformeActividad(Date fechaInicio, Date fechaFin, String aprobado,
			int idProgramaAcademico) {
		// TODO Auto-generated method stub
		return repository.getInformeActividad(fechaInicio, fechaFin, aprobado, idProgramaAcademico);
	}

	@Override
	public List<Object[]> getInformeActividad(Date fechaInicio, Date fechaFin, int idProgramaAcademico) {
		// TODO Auto-generated method stub
		return repository.getInformeActividad(fechaInicio, fechaFin, idProgramaAcademico);
	}

	
}
