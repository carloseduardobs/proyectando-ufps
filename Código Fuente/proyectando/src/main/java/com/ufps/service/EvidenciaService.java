package com.ufps.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ufps.entity.EvidenciaEntity;
import com.ufps.repository.EvidenciaRepository;

@Service
public class EvidenciaService implements EvidenciaInterface {

	@Autowired
	private EvidenciaRepository repository;

	@Override
	public List<EvidenciaEntity> getAll() {
		// TODO Auto-generated method stub
		return (List<EvidenciaEntity>) repository.findAll();
	}

	@Override
	public EvidenciaEntity getById(int id) {
		// TODO Auto-generated method stub
		return repository.findById(id).isPresent() ?
				repository.findById(id).orElse(null) : null;
	}

	@Override
	public EvidenciaEntity save(EvidenciaEntity entity) {
		// TODO Auto-generated method stub
		return repository.save(entity);
	}

	@Override
	public EvidenciaEntity update(EvidenciaEntity entity) {
		// TODO Auto-generated method stub
		return repository.findById(entity.getId()).isPresent() ?
				repository.save(entity) : null;
	}

	@Override
	public void delete(int id) {
		repository.deleteById(id);		
	}

	@Override
	public List<EvidenciaEntity> getAllByIdEvidenciaActividad(int idEvidenciaActividad) {
		// TODO Auto-generated method stub
		return repository.findAllByIdEvidenciaActividad(idEvidenciaActividad);
	}

	@Override
	public List<EvidenciaEntity> getAllByIdEvidenciaProyecto(int idEvidenciaProyecto) {
		// TODO Auto-generated method stub
		return repository.findAllByIdEvidenciaProyecto(idEvidenciaProyecto);
	}

}
