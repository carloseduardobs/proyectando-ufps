package com.ufps.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ufps.entity.RespuestaPreguntaEntity;
import com.ufps.exceptions.BadRequestException;
import com.ufps.repository.RespuestaPreguntaRepository;

@Service
public class RespuestaPreguntaService implements RespuestaPreguntaInterface {

	@Autowired
	private RespuestaPreguntaRepository repository;

	@Override
	public List<RespuestaPreguntaEntity> getAll() {
		// TODO Auto-generated method stub
		return (List<RespuestaPreguntaEntity>) repository.findAll();
	}

	@Override
	public RespuestaPreguntaEntity getById(int id) {
		// TODO Auto-generated method stub
		throw new BadRequestException("No implementado.");
	}

	@Override
	public RespuestaPreguntaEntity save(RespuestaPreguntaEntity entity) {
		// TODO Auto-generated method stub
		return repository.save(entity);
	}

	@Override
	public RespuestaPreguntaEntity update(RespuestaPreguntaEntity entity) {
		// TODO Auto-generated method stub
		return repository.findByIdRespuestaEncuestaAndIdPregunta(entity.getIdRespuestaEncuesta(), entity.getIdPregunta()).isPresent() ?
				repository.save(entity) : null;
	}

	@Override
	public void delete(int id) {
		repository.deleteById(id);	
	}

	@Override
	public Optional<RespuestaPreguntaEntity> findByIdRespuestaEncuestaAndIdPregunta(int idRespuestaEncuesta, int idPregunta) {
		// TODO Auto-generated method stub
		return repository.findByIdRespuestaEncuestaAndIdPregunta(idRespuestaEncuesta, idPregunta);
	}

	@Override
	public List<RespuestaPreguntaEntity> findAllByIdRespuestaEncuesta(Integer idRespuestaEncuesta) {
		// TODO Auto-generated method stub
		return repository.findAllByIdRespuestaEncuesta(idRespuestaEncuesta);
	}


}
