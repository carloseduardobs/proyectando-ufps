package com.ufps.service;

import java.util.List;

import com.ufps.entity.EvidenciaProyectoEntity;
import com.ufps.utilities.BaseServiceInterface;

public interface EvidenciaProyectoInterface extends BaseServiceInterface<EvidenciaProyectoEntity> {

	public List<EvidenciaProyectoEntity> findAllByRegistradoPor(int registradoPor);
	
	public List<EvidenciaProyectoEntity> findAllByIdProyecto(int idProyecto);

	public List<EvidenciaProyectoEntity> getAllIdPlanEstudios(int idPlanEstudios);
	
	public List<EvidenciaProyectoEntity> findAllByProyectoAndDivulgado(int idProyecto);
}
