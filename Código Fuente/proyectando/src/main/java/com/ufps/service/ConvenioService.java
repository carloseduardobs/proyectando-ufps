package com.ufps.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ufps.entity.ConvenioEntity;
import com.ufps.repository.ConvenioRepository;

@Service
public class ConvenioService implements ConvenioInterface {

	@Autowired
	private ConvenioRepository repository;

	@Override
	public List<ConvenioEntity> getAll() {
		// TODO Auto-generated method stub
		return (List<ConvenioEntity>) repository.findAll();
	}

	@Override
	public ConvenioEntity getById(int id) {
		// TODO Auto-generated method stub
		return repository.findById(id).isPresent() ?
				repository.findById(id).orElse(null) : null;
	}

	@Override
	public ConvenioEntity save(ConvenioEntity entity) {
		// TODO Auto-generated method stub
		return repository.save(entity);
	}

	@Override
	public ConvenioEntity update(ConvenioEntity entity) {
		// TODO Auto-generated method stub
		return repository.findById(entity.getId()).isPresent() ?
				repository.save(entity) : null;
	}

	@Override
	public void delete(int id) {
		repository.deleteById(id);		
	}

	@Override
	public List<ConvenioEntity> findAllByRegistradoPor(int registradoPor) {
		// TODO Auto-generated method stub
		return repository.findAllByRegistradoPor(registradoPor);
	}

	@Override
	public List<ConvenioEntity> findAllByIdProyecto(int idProyecto) {
		// TODO Auto-generated method stub
		return repository.findAllByIdProyecto(idProyecto);
	}

	@Override
	public List<ConvenioEntity> getAllIdPlanEstudios(int idPlanEstudio) {
		// TODO Auto-generated method stub
		return repository.findAllByIdPlanEstudios(idPlanEstudio);
	}

	@Override
	public boolean existsByIdProyecto(int idProyecto) {
		// TODO Auto-generated method stub
		return repository.existsByIdProyecto(idProyecto);
	}

	@Override
	public ConvenioEntity findAllByProyectoAndDivulgado(int idProyecto) {
		// TODO Auto-generated method stub
		return repository.findAllByProyectoAndDivulgado(idProyecto);
	}

	@Override
	public List<Object[]> getInformeConvenios(Date fechaInicio, Date fechaFin, int idProgramaAcademico) {
		// TODO Auto-generated method stub
		return repository.getInformeConvenios(fechaInicio, fechaFin, idProgramaAcademico);
	}

}
