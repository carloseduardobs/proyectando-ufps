package com.ufps.service;

import java.util.Date;
import java.util.List;

import com.ufps.entity.ConvenioEntity;
import com.ufps.utilities.BaseServiceInterface;

public interface ConvenioInterface extends BaseServiceInterface<ConvenioEntity> {
 
	public List<ConvenioEntity> findAllByRegistradoPor(int registradoPor);

	public List<ConvenioEntity> findAllByIdProyecto(int idProyecto);

	public List<ConvenioEntity> getAllIdPlanEstudios(int idPlanEstudio);
	
	public boolean existsByIdProyecto(int idProyecto);

	public ConvenioEntity findAllByProyectoAndDivulgado(int idProyecto);

	public List<Object[]> getInformeConvenios(Date fechaInicio, Date fechaFin, int idProgramaAcademico);
}
