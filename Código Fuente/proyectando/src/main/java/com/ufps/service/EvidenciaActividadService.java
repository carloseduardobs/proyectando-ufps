package com.ufps.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ufps.entity.EvidenciaActividadEntity;
import com.ufps.repository.EvidenciaActividadRepository;



@Service
public class EvidenciaActividadService implements EvidenciaActividadInterface {

	@Autowired
	private EvidenciaActividadRepository repository;

	@Override
	public List<EvidenciaActividadEntity> getAll() {
		// TODO Auto-generated method stub
		return (List<EvidenciaActividadEntity>) repository.findAll();
	}

	@Override
	public EvidenciaActividadEntity getById(int id) {
		// TODO Auto-generated method stub
		return repository.findById(id).isPresent() ?
				repository.findById(id).orElse(null) : null;
	}

	@Override
	public EvidenciaActividadEntity save(EvidenciaActividadEntity entity) {
		// TODO Auto-generated method stub
		return repository.save(entity);
	}

	@Override
	public EvidenciaActividadEntity update(EvidenciaActividadEntity entity) {
		// TODO Auto-generated method stub
		return repository.findById(entity.getId()).isPresent() ?
				repository.save(entity) : null;
	}

	@Override
	public void delete(int id) {
		repository.deleteById(id);		
	}

	@Override
	public List<EvidenciaActividadEntity> findAllByRegistradoPor(int registradoPor) {
		// TODO Auto-generated method stub
		return repository.findAllByRegistradoPor(registradoPor);
	}

	@Override
	public List<EvidenciaActividadEntity> findAllByIdActividad(int idActividad) {
		// TODO Auto-generated method stub
		return repository.findAllByIdActividad(idActividad);
	}

	@Override
	public List<EvidenciaActividadEntity> getAllIdPlanEstudios(int idPlanEstudios) {
		// TODO Auto-generated method stub
		return repository.findAllByIdPlanEstudios(idPlanEstudios);
	}

	@Override
	public List<EvidenciaActividadEntity> findAllByActividadAndDivulgado(int idActividad) {
		// TODO Auto-generated method stub
		return repository.findAllByActividadAndDivulgado(idActividad);
	}

}
