package com.ufps.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ufps.entity.PreguntaEntity;
import com.ufps.repository.PreguntaRepository;

@Service
public class PreguntaService implements PreguntaInterface {

	@Autowired
	private PreguntaRepository repository;

	@Override
	public List<PreguntaEntity> getAll() {
		// TODO Auto-generated method stub
		return (List<PreguntaEntity>) repository.findAll();
	}

	@Override
	public PreguntaEntity getById(int id) {
		// TODO Auto-generated method stub
		return repository.findById(id).isPresent() ?
				repository.findById(id).orElse(null) : null;
	}

	@Override
	public PreguntaEntity save(PreguntaEntity entity) {
		// TODO Auto-generated method stub
		return repository.save(entity);
	}

	@Override
	public PreguntaEntity update(PreguntaEntity entity) {
		// TODO Auto-generated method stub
		return repository.findById(entity.getId()).isPresent() ?
				repository.save(entity) : null;
	}

	@Override
	public void delete(int id) {
		repository.deleteById(id);	
	}

	@Override
	public List<PreguntaEntity> findAllByIdEncuesta(int idEncuesta) {
		// TODO Auto-generated method stub
		return repository.findAllByIdEncuesta(idEncuesta);
	}
}
