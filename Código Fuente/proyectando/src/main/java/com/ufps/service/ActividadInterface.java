package com.ufps.service;

import java.util.Date;
import java.util.List;

import com.ufps.entity.ActividadEntity;
import com.ufps.utilities.BaseServiceInterface;

public interface ActividadInterface extends BaseServiceInterface<ActividadEntity> {

	public List<ActividadEntity> findAllByRegistradoPor(int registradoPor);
	
	List<ActividadEntity> findAllByIdProyecto(int idProyecto);
	
	List<ActividadEntity> getAllIdPlanEstudios(int idPlanEstudios);

	List<ActividadEntity> findAllByProyectoAndDivulgado(int idProyecto);
	
	List<ActividadEntity> findAllByProyectoAndDivulgadoEventos(int idProyecto);

	public List<Object[]> getInformeActividad(Date fechaInicio, Date fechaFin, String aprobado, int idProgramaAcademico);

	public List<Object[]> getInformeActividad(Date fechaInicio, Date fechaFin, int idProgramaAcademico);
}
