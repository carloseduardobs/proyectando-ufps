package com.ufps.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ufps.entity.PlanEstudiosEntity;
import com.ufps.repository.PlanEstudiosRepository;


@Service
public class PlanEstudiosService implements PlanEstudiosInterface {

	@Autowired
	private PlanEstudiosRepository repository;

	@Override
	public List<PlanEstudiosEntity> getAll() {
		// TODO Auto-generated method stub
		return (List<PlanEstudiosEntity>) repository.findAll();
	}

	@Override
	public PlanEstudiosEntity getById(int id) {
		// TODO Auto-generated method stub
		return repository.findById(id).isPresent() ?
				repository.findById(id).orElse(null) : null;
	}

	@Override
	public PlanEstudiosEntity save(PlanEstudiosEntity entity) {
		// TODO Auto-generated method stub
		return repository.save(entity);
	}

	@Override
	public PlanEstudiosEntity update(PlanEstudiosEntity entity) {
		// TODO Auto-generated method stub
		return repository.findById(entity.getId()).isPresent() ?
				repository.save(entity) : null;
	}

	@Override
	public void delete(int id) {
		repository.deleteById(id);		
	}

	@Override
	public boolean existeUrl(String url) {
		// TODO Auto-generated method stub
		return repository.existsByUrl(url);
	}

}
