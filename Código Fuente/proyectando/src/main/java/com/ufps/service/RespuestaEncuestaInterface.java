package com.ufps.service;

import java.util.Date;
import java.util.List;

import com.ufps.entity.RespuestaEncuestaEntity;
import com.ufps.utilities.BaseServiceInterface;


public interface RespuestaEncuestaInterface extends BaseServiceInterface<RespuestaEncuestaEntity> {

	List<RespuestaEncuestaEntity> findAllByIdEncuesta(int idEncuesta);
	
	List<RespuestaEncuestaEntity> findAllByIdEncuestaContestada(int idEncuesta);
	
	RespuestaEncuestaEntity findByCorreoAndKeyAndEstado(String correo, String key, String estado);

	List<Object[]> getInformeEncuesta(Date fechaInicio, Date fechaFin, int idProgramaAcademico, int idEncuesta);
}
