package com.ufps.service;

import java.util.List;

import com.ufps.entity.InformacionInstitucionalEntity;
import com.ufps.utilities.BaseServiceInterface;

public interface InformacionInstitucionalInterface extends BaseServiceInterface<InformacionInstitucionalEntity> {

	List<InformacionInstitucionalEntity> findAllByIdPlanEstudio(int idPlanEstudio);

	List<InformacionInstitucionalEntity> findAllByUrl(String url, String logo, String inicio);

	InformacionInstitucionalEntity getLogo(String url, String logo);
	
	InformacionInstitucionalEntity findAllByUrlAndPath(String url, String path);

}
