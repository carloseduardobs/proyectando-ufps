package com.ufps.service;

import com.ufps.entity.ProcesoMisionalEntity;
import com.ufps.utilities.BaseServiceInterface;

public interface ProcesoMisionalInterface extends BaseServiceInterface<ProcesoMisionalEntity> {

}
