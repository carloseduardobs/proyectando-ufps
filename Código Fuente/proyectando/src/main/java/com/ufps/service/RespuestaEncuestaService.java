package com.ufps.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ufps.entity.RespuestaEncuestaEntity;
import com.ufps.repository.RespuestaEncuestaRepository;


@Service
public class RespuestaEncuestaService implements RespuestaEncuestaInterface {

	@Autowired
	private RespuestaEncuestaRepository repository;

	@Override
	public List<RespuestaEncuestaEntity> getAll() {
		// TODO Auto-generated method stub
		return (List<RespuestaEncuestaEntity>) repository.findAll();
	}

	@Override
	public RespuestaEncuestaEntity getById(int id) {
		// TODO Auto-generated method stub
		return repository.findById(id).isPresent() ?
				repository.findById(id).orElse(null) : null;
	}

	@Override
	public RespuestaEncuestaEntity save(RespuestaEncuestaEntity entity) {
		// TODO Auto-generated method stub
		return repository.save(entity);
	}

	@Override
	public RespuestaEncuestaEntity update(RespuestaEncuestaEntity entity) {
		// TODO Auto-generated method stub
		return repository.findById(entity.getId()).isPresent() ?
				repository.save(entity) : null;
	}

	@Override
	public void delete(int id) {
		repository.deleteById(id);	
	}

	@Override
	public List<RespuestaEncuestaEntity> findAllByIdEncuesta(int idEncuesta) {
		// TODO Auto-generated method stub
		return repository.findAllByIdEncuesta(idEncuesta);
	}

	@Override
	public RespuestaEncuestaEntity findByCorreoAndKeyAndEstado(String correo, String key, String estado) {
		// TODO Auto-generated method stub
		return repository.findByCorreoAndKeyAndEstado(correo, key, estado).orElse(null);
	}

	@Override
	public List<RespuestaEncuestaEntity> findAllByIdEncuestaContestada(int idEncuesta) {
		// TODO Auto-generated method stub
		return repository.findAllByIdEncuestaContestada(idEncuesta);
	}

	@Override
	public List<Object[]> getInformeEncuesta(Date fechaInicio, Date fechaFin, int idProgramaAcademico, int idEncuesta) {
		// TODO Auto-generated method stub
		return repository.getInformeEncuesta(fechaInicio, fechaFin, idProgramaAcademico, idEncuesta);
	}

}
