package com.ufps.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ufps.entity.EncuestaEntity;
import com.ufps.repository.EncuestaRepository;

@Service
public class EncuestaService implements EncuestaInterface {

	@Autowired
	private EncuestaRepository repository;

	@Override
	public List<EncuestaEntity> getAll() {
		// TODO Auto-generated method stub
		return (List<EncuestaEntity>) repository.findAll();
	}

	@Override
	public EncuestaEntity getById(int id) {
		// TODO Auto-generated method stub
		return repository.findById(id).isPresent() ?
				repository.findById(id).orElse(null) : null;
	}

	@Override
	public EncuestaEntity save(EncuestaEntity entity) {
		// TODO Auto-generated method stub
		return repository.save(entity);
	}

	@Override
	public EncuestaEntity update(EncuestaEntity entity) {
		// TODO Auto-generated method stub
		return repository.findById(entity.getId()).isPresent() ?
				repository.save(entity) : null;
	}

	@Override
	public void delete(int id) {
		repository.deleteById(id);	
	}

	@Override
	public List<EncuestaEntity> findAllByIdPlanEstudio(Integer idPlanEstudio) {
		// TODO Auto-generated method stub
		return repository.findAllByIdPlanEstudio(idPlanEstudio);
	}

	@Override
	public List<EncuestaEntity> findAllByIdPlanEstudioAndEstado(Integer idPlanEstudio, String estado) {
		// TODO Auto-generated method stub
		return repository.findAllByIdPlanEstudioAndEstado(idPlanEstudio, estado);
	}

}
