package com.ufps.api;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ufps.entity.RespuestaEncuestaEntity;
import com.ufps.entity.RespuestaPreguntaEntity;
import com.ufps.exceptions.BadRequestException;
import com.ufps.exceptions.NotFoundException;
import com.ufps.payload.EncuestaPayload;
import com.ufps.payload.EncuestaResponsePayload;
import com.ufps.payload.LoginPayload;
import com.ufps.payload.MessageResponse;
import com.ufps.payload.PreguntaPayload;
import com.ufps.payload.ProyectoPayload;
import com.ufps.payload.RespuestaEncuestaPayload;
import com.ufps.payload.RespuestaEncuestaRegistroPayload;
import com.ufps.payload.RespuestaPreguntaPayload;
import com.ufps.service.EncuestaInterface;
import com.ufps.service.PreguntaInterface;
import com.ufps.service.ProyectoInterface;
import com.ufps.service.RespuestaEncuestaInterface;
import com.ufps.service.RespuestaPreguntaInterface;
import com.ufps.utilities.Constantes;
import com.ufps.utilities.Mapper;
import com.ufps.utilities.SimpleEmail;
import com.ufps.utilities.Util;

@RestController
@CrossOrigin(origins="*", methods= {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
@RequestMapping(value = "/respuesta-encuesta")
public class RespuestaEncuestaApi {

	@Autowired
	private RespuestaEncuestaInterface service;
	
	@Autowired
	private RespuestaPreguntaInterface serviceRespuesta;
	
	@Autowired
	private EncuestaInterface serviceEncuesta;

	@Autowired
	private PreguntaInterface servicePregunta;

	@Autowired
	private ProyectoInterface serviceProyecto;

	@Autowired
	private HttpSession request;
	
	@Transactional
	@PostMapping(path = "/enviar", consumes = "application/json", produces = "application/json")
	public MessageResponse enviar(@RequestBody RespuestaEncuestaRegistroPayload payload) {

		String rol = (String) request.getAttribute("_ROL");
		int idPlanEstudios = (int) request.getAttribute("_IDPLANESTUDIOS");

		if (!rol.equalsIgnoreCase(Constantes.ADMINISTRADOR) && !rol.equalsIgnoreCase(Constantes.DIRECTOR_PROYECTO)) {
			throw new BadRequestException("No tienes permisos para enviar encuestas");
		}
		
		ProyectoPayload proyecto = Mapper.proyectoEntityToPayload(serviceProyecto.getById(payload.getIdProyecto()));
		if (proyecto == null) {
			throw new NotFoundException("Proyecto no encontrado.");
		}
		
		if (proyecto.getIdPlanEstudios() != idPlanEstudios) {
			throw new BadRequestException("No puedes enviar encuestas de otros planes de estudio.");
		}

		EncuestaPayload encuesta = Mapper.encuestaEntityToPayload(serviceEncuesta.getById(payload.getIdEncuesta()));
		if (encuesta == null) {
			throw new NotFoundException("Encuesta no encontrada.");
		}
		
		RespuestaEncuestaEntity entity = Mapper.respuestaEncuestaRegistroPayloadToEntity(payload);
		Util util = new Util();
		entity.setKey(util.generateRandomSalt());
		entity.setFechaEnvio(new Date());

		//Preparamos el cuerpo del mensaje
		Map<String, String> parameterContent = new HashMap<>();
		parameterContent.put("content-email", entity.getCorreo());
		parameterContent.put("content-pass", entity.getKey());
		parameterContent.put("content-url", payload.getUrlResponse() + Constantes.URL);
		String msj = util.AjustarContenidoHtml(Constantes.CONTENIDO_HTML_ENCUESTA, parameterContent);

		RespuestaEncuestaPayload payloadResponse = Mapper.respuestaEncuestaEntityToPayload(service.save(entity));

		//Envio de mensajes 
		SimpleEmail sEmail = new SimpleEmail();
		Map<String, String> parameterEmail = new HashMap<>();
		parameterEmail.put("to", payloadResponse.getCorreo());
		parameterEmail.put("subject", Constantes.SUBJECT_ENCUESTA);
		parameterEmail.put("message", util.contentMensaje(Constantes.ARCHIVO, msj));
		if (!sEmail.sendEmail(parameterEmail)) {
			throw new BadRequestException("Error!!\nEl email no pudo ser enviado");
		}
		return new MessageResponse("Se ha enviado la encuesta al correo " + payloadResponse.getCorreo());
	}

	@PostMapping(path = "/validar", consumes = "application/json", produces = "application/json")
	public EncuestaResponsePayload validar(@RequestBody LoginPayload payload) {
		RespuestaEncuestaPayload reen = Mapper.respuestaEncuestaEntityToPayload(service
				.findByCorreoAndKeyAndEstado(payload.getCorreo(), payload.getContrasena(), Constantes.PENDIENTE));

		if (reen == null ) {
			throw new NotFoundException("No tiene encuestas pendientes.");
		}

		EncuestaResponsePayload payloadResponse = Mapper.encuestaEntityToResponsePayload(serviceEncuesta.getById(reen.getIdEncuesta()));
		if (payloadResponse == null) {
			throw new NotFoundException("Encuesta no encontrada");
		}

		List<PreguntaPayload> preguntas = servicePregunta.findAllByIdEncuesta(payloadResponse.getId())
				.stream()
				.map(pregunta -> Mapper.preguntaEntityToPayload(pregunta))
				.collect(Collectors.toList());
		if (preguntas == null || preguntas.isEmpty()) {
			throw new NotFoundException("Preguntas no encontradas");
		}
		payloadResponse.setPreguntas(preguntas);
		payloadResponse.setRespuestaEncuesta(reen);
		return payloadResponse;
	}

	@Transactional
	@PostMapping(path = "/responder", consumes = "application/json", produces = "application/json")
	public MessageResponse responder(@RequestBody RespuestaEncuestaPayload payload) {
		ProyectoPayload proyecto = Mapper.proyectoEntityToPayload(serviceProyecto.getById(payload.getIdProyecto()));

		RespuestaEncuestaPayload reen = Mapper.respuestaEncuestaEntityToPayload(service
				.findByCorreoAndKeyAndEstado(payload.getCorreo(), payload.getKey(), Constantes.PENDIENTE));

		if (reen == null ) {
			throw new NotFoundException("No tiene encuestas pendientes.");
		}
		
		if (reen.getId() != payload.getId()) {
			throw new NotFoundException("Encuesta no encontrada.");
		}

		if (proyecto == null) {
			throw new NotFoundException("Proyecto no encontrado.");
		}

		EncuestaPayload encuesta = Mapper.encuestaEntityToPayload(serviceEncuesta.getById(payload.getIdEncuesta()));
		if (encuesta == null) {
			throw new NotFoundException("Encuesta no encontrada.");
		}
		
		List<PreguntaPayload> preguntas = servicePregunta.findAllByIdEncuesta(encuesta.getId())
				.stream()
				.map(entity -> Mapper.preguntaEntityToPayload(entity))
				.collect(Collectors.toList());
		if (preguntas == null || preguntas.isEmpty()) {
			throw new NotFoundException("Encuesta no encontrada.");
		}
		
		if (payload.getRespuestas().size() != preguntas.size()) {
			throw new BadRequestException("Debes contestar todas las preguntas");
		}
		
		payload.setFechaRealizacion(new Date());
		payload.setEstado(Constantes.CONTESTADO);
		service.update(Mapper.respuestaEncuestaPayloadToEntity(payload));
		for (RespuestaPreguntaPayload respuesta : payload.getRespuestas()) {
			RespuestaPreguntaEntity entity = null;
			if (servicePregunta.getById(respuesta.getIdPregunta()) == null) {
				throw new NotFoundException("Pregunta no valida");
			}
			respuesta.setIdRespuestaEncuesta(payload.getId());
			if (respuesta.getRespuestas().trim().length() <= 0) {
				throw new NotFoundException("Respuesta de pregunta no encontrada");
			}
			entity = Mapper.respuestaPreguntaPayloadToEntity(respuesta);
			serviceRespuesta.save(entity);
			System.out.print(entity);
		};
		return new MessageResponse("Respuesta enviada con exito");
	}
}
