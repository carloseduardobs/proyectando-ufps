package com.ufps.api;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ufps.entity.ActividadEntity;
import com.ufps.exceptions.BadRequestException;
import com.ufps.exceptions.NotFoundException;
import com.ufps.payload.ActividadPayload;
import com.ufps.payload.ActividadRegistroPayload;
import com.ufps.payload.ProyectoPayload;
import com.ufps.service.ActividadInterface;
import com.ufps.service.EvidenciaActividadInterface;
import com.ufps.service.ProyectoInterface;
import com.ufps.utilities.Constantes;
import com.ufps.utilities.Mapper;

@RestController
@CrossOrigin(origins="*", methods= {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
@RequestMapping(value="/actividad")
public class ActividadApi {

	@Autowired
	private ActividadInterface service;
	
	@Autowired
	private ProyectoInterface proyectoSerivice;

	@Autowired
	private EvidenciaActividadInterface evidencias;

	@Autowired
	private HttpSession request;

	@GetMapping(produces = "application/json")
	public List<ActividadPayload> getAll(){
		String rol = (String) request.getAttribute("_ROL");
		int idPlanEstudios = (int) request.getAttribute("_IDPLANESTUDIOS");

		List<ActividadEntity> lista = null;
		if (!rol.equalsIgnoreCase(Constantes.SUPER_USUARIO)) {
			lista = service.getAllIdPlanEstudios(idPlanEstudios);
		} else {
			lista = service.getAll();
		}
		List<ActividadPayload> listaActividades = new ArrayList<>();
		lista.forEach(entity -> {
			listaActividades.add(Mapper.actividadEntityToPayload(entity));
		});
		
		return listaActividades;
	}
	
	@GetMapping(path="/{id}", produces = "application/json")
	public ResponseEntity<ActividadPayload> getById(@PathVariable("id") int id) {
		ActividadPayload actividad = Mapper.actividadEntityToPayload(service.getById(id));
		if(actividad == null) {
			//throw new RecordNotFoundException("Id: ("  + idOpcion + ") not found");
		}
		 return new ResponseEntity<>(actividad, HttpStatus.OK);
	}

	@PostMapping(path="/insertar", consumes = "application/json", produces = "application/json")
	public ActividadPayload save(@RequestBody ActividadRegistroPayload payload) {

		String rol = (String) request.getAttribute("_ROL");
		int idPlanEstudios = (int) request.getAttribute("_IDPLANESTUDIOS");

		if (!rol.equalsIgnoreCase(Constantes.ADMINISTRADOR) && !rol.equalsIgnoreCase(Constantes.DIRECTOR_PROYECTO)) {
			throw new BadRequestException("No tienes permisos para registrar las actividades");
		}

		ProyectoPayload proyecto = Mapper.proyectoEntityToPayload(proyectoSerivice.getById(payload.getIdProyecto()));
		if (proyecto == null) {
			throw new NotFoundException("El proyecto no fue encontrado.");
		}
		
		if (proyecto.getAprobar().equalsIgnoreCase(Constantes.NOAPROBAR)) {
			throw new BadRequestException("No puedes registrar actividades a proyectos que no esten aprobados.");
		}

		if (proyecto.getIdPlanEstudios() != idPlanEstudios) {
			throw new BadRequestException("No puedes registrar actividades a proyectos de otros planes de estudio");
		}

		ActividadEntity entity =Mapper.actividadRegistroPayloadToEntity(payload);
		long registradoPor =(Long) request.getAttribute("_ID");
		entity.setRegistradoPor((int) registradoPor);
		return Mapper.actividadEntityToPayload(service.save(entity));
	}
	
	@PutMapping("/modificar")
	public ActividadPayload update(@RequestBody ActividadPayload payload) {

		String rol = (String) request.getAttribute("_ROL");
		int idPlanEstudios = (int) request.getAttribute("_IDPLANESTUDIOS");
		long registradoPor =(Long) request.getAttribute("_ID");

		if (!rol.equalsIgnoreCase(Constantes.ADMINISTRADOR) && !rol.equalsIgnoreCase(Constantes.DIRECTOR_PROYECTO)) {
			throw new BadRequestException("No tienes permisos para modificar las actividades");
		}

		ActividadPayload actividad = Mapper.actividadEntityToPayload(service.getById(payload.getId()));
		if (actividad == null) {
			throw new NotFoundException("La actividad no fue encontrada.");
		}
		
		if (payload.getDivulgar().equalsIgnoreCase(Constantes.DIVULGAR) && payload.getAprobar().equalsIgnoreCase(Constantes.NOAPROBAR)) {
			throw new BadRequestException("No puedes divulgar actividades las cuales no hayan sido aprobadas");
		}
		
		ProyectoPayload proyecto = Mapper.proyectoEntityToPayload(proyectoSerivice.getById(payload.getIdProyecto()));
		if (proyecto == null) {
			throw new NotFoundException("El proyecto no fue encontrado.");
		}

		if (proyecto.getIdPlanEstudios() != idPlanEstudios) {
			throw new BadRequestException("No puedes modificar actividades de otros planes de estudio");
		}

		if (actividad.getRegistradoPor() != payload.getRegistradoPor()) {
			throw new BadRequestException("No puedes cambiar la persona que registro la actividad");
		}

		if (rol.equalsIgnoreCase(Constantes.DIRECTOR_PROYECTO)) {
			if (actividad.getRegistradoPor() != (int) registradoPor) {
				throw new BadRequestException("No puedes modificar las actividades que no hayas creado");
			}
			if (!actividad.getAprobar().equalsIgnoreCase(payload.getAprobar())) {
				throw new BadRequestException("No puedes aprobar actividades");
			}
			
			if (!actividad.getDivulgar().equalsIgnoreCase(payload.getDivulgar())) {
				throw new BadRequestException("No puedes divulgar actividades");
			}
		}

		return Mapper.actividadEntityToPayload(service.save(Mapper.actividadPayloadToEntity(payload)));
	}
	
	@DeleteMapping("/eliminar/{id}")
	public void delete(@PathVariable("id") int id) {

		String rol = (String) request.getAttribute("_ROL");
		int idPlanEstudios = (int) request.getAttribute("_IDPLANESTUDIOS");

		if (!rol.equalsIgnoreCase(Constantes.ADMINISTRADOR) && !rol.equalsIgnoreCase(Constantes.SUPER_USUARIO)) {
			throw new BadRequestException("No tienes permisos para eliminar los proyectos");
		}

		ActividadPayload payload = Mapper.actividadEntityToPayload(service.getById(id));
		if (payload == null) {
			throw new NotFoundException("La actividad no fue encontrada.");
		}

		ProyectoPayload proyecto = Mapper.proyectoEntityToPayload(proyectoSerivice.getById(payload.getIdProyecto()));
		if (proyecto == null) {
			throw new NotFoundException("El proyecto no fue encontrado.");
		}

		if (rol.equalsIgnoreCase(Constantes.ADMINISTRADOR)) {
			if (proyecto.getIdPlanEstudios() != idPlanEstudios) {
				throw new BadRequestException("No puedes eliminar actividades de otros planes de estudio");
			}
		}

		if (evidencias.findAllByIdActividad(payload.getId()) != null && !evidencias.findAllByIdActividad(payload.getId()).isEmpty()) {
			throw new BadRequestException("No puedes eliminar el actividades ya que cuenta con evidencias.");
		}

		service.delete(id);
	}

}
