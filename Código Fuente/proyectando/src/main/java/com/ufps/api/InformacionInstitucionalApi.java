package com.ufps.api;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ufps.exceptions.BadRequestException;
import com.ufps.exceptions.NotFoundException;
import com.ufps.payload.InformacionInstitucionalPayload;
import com.ufps.payload.PlanEstudiosPayload;
import com.ufps.service.InformacionInstitucionalInterface;
import com.ufps.service.PlanEstudiosInterface;
import com.ufps.utilities.Constantes;
import com.ufps.utilities.Mapper;

@RestController
@CrossOrigin(origins="*", methods= {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
@RequestMapping(value="/informacion-institucional")
public class InformacionInstitucionalApi {

	@Autowired
	private InformacionInstitucionalInterface service;

	@Autowired
	private PlanEstudiosInterface servicePlanEstudios;

	@Autowired
	private HttpSession request;

	@GetMapping(produces = "application/json")
	public List<InformacionInstitucionalPayload> getAll(){
		String rol = (String) request.getAttribute("_ROL");
		int idPlanEstudios = (int) request.getAttribute("_IDPLANESTUDIOS");

		if (!rol.equalsIgnoreCase(Constantes.ADMINISTRADOR)) {
			throw new BadRequestException("Error!!\\nNo cuentas con los permisos para consultar Información Institucional");
		}

		List<InformacionInstitucionalPayload> lista = service.findAllByIdPlanEstudio(idPlanEstudios)
				.stream()
				.map(entity -> Mapper.informacionInstitucionalEntityToPayload(entity))
				.collect(Collectors.toList());
		return lista;
	}
	
	@GetMapping(path="/{id}", produces = "application/json")
	public InformacionInstitucionalPayload getById(@PathVariable("id") int id) {
		int idPlanEstudios = (int) request.getAttribute("_IDPLANESTUDIOS");
		String rol = (String) request.getAttribute("_ROL");

		if (!rol.equalsIgnoreCase(Constantes.ADMINISTRADOR)) {
			throw new BadRequestException("Error!!\\nNo cuentas con los permisos para consultar Información Institucional");
		}

		InformacionInstitucionalPayload payload = Mapper.informacionInstitucionalEntityToPayload(service.getById(id));
		if(payload == null) {
			throw new NotFoundException("Error!!\nInformación institucional no encontrada.");
		}
		
		if (payload.getIdPlanEstudio() != idPlanEstudios) {
			throw new BadRequestException("Error!!\nNo puedes cargar Información institucional de otros planes de estudio.");
		}

		 return payload;
	}
	
	
	@PutMapping("/modificar")
	public InformacionInstitucionalPayload update(@RequestBody InformacionInstitucionalPayload payload) {
		String rol = (String) request.getAttribute("_ROL");
		int idPlanEstudios = (int) request.getAttribute("_IDPLANESTUDIOS");

		if (!rol.equalsIgnoreCase(Constantes.ADMINISTRADOR)) {
			throw new BadRequestException("Error!!\\nNo cuentas con los permisos para modificar Información Institucional");
		}

		InformacionInstitucionalPayload inin = Mapper.informacionInstitucionalEntityToPayload(service.getById(payload.getId()));
		if (inin == null) {
			throw new NotFoundException("Error!!\\nInformación Institucional no encontrada.");
		}

		PlanEstudiosPayload ples = Mapper.planEstudioEntityToPayload(servicePlanEstudios.getById(payload.getIdPlanEstudio()));
		if (ples == null) {
			throw new NotFoundException("Error!!\\nPlan de estudios no encontrado.");
		}

		if (idPlanEstudios != payload.getIdPlanEstudio() || idPlanEstudios != inin.getIdPlanEstudio()) {
			throw new BadRequestException("Error!!\nNo puedes modificar la Información institucional a otros planes de estudio.");
		}
		
		if (inin.getIdPlanEstudio() != payload.getIdPlanEstudio()) {
			throw new BadRequestException("Error!!\nNo puedes cambiar el plan de estudio de la Información Institucional.");
		}
		
		if (!inin.getKey().equalsIgnoreCase(payload.getKey())) {
			throw new BadRequestException("Error!!\nNo puedes cambiar las keys de la Información Institucional.");
		}
		
		if (payload.getKey()  == Constantes.LOGO) {
			if (!payload.getInformacion().contains("data:image/")) {
				throw new BadRequestException("Error!!\nEL logo debe estar en Base64");
			}
			
			if (payload.getDivulgar().equalsIgnoreCase(Constantes.NODIVULGAR)) {
				throw new BadRequestException("Error!!\nDebes divulgar el logo.");
			}
		}
		
		if (payload.getKey()  == Constantes.INICIO) {
			if (payload.getDivulgar().equalsIgnoreCase(Constantes.NODIVULGAR)) {
				throw new BadRequestException("Error!!\nDebes divulgar el inicio.");
			}
		}

		if (service.findAllByUrl(ples.getUrl(), Constantes.LOGO, Constantes.INICIO) == null || service.findAllByUrl(ples.getUrl(), Constantes.LOGO, Constantes.INICIO).isEmpty()) {
			throw new BadRequestException("Error!!\nNo puedes dejar Información institucional sin divulgar.");
		}

		return Mapper.informacionInstitucionalEntityToPayload(service.update(Mapper.informacionInstitucionalPayloadToEntity(payload)));
		
	}
	

}
