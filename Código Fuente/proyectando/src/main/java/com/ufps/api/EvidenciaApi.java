package com.ufps.api;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ufps.exceptions.BadRequestException;
import com.ufps.service.EvidenciaInterface;
import com.ufps.utilities.Constantes;


@RestController
@CrossOrigin(origins="*", methods= {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
@RequestMapping(value="/evidencia")
public class EvidenciaApi {

	@Autowired
	private EvidenciaInterface service;

	@Autowired
	private HttpSession request;

	@DeleteMapping("/eliminar/{id}")
	public void delete(@PathVariable("id") int id) {
		String rol = (String) request.getAttribute("_ROL");

		if (rol.equalsIgnoreCase(Constantes.SUPER_USUARIO)) {
			throw new BadRequestException("No tienes permisos para eliminar acrchivos de la evidencia.");
		}

		service.delete(id);
	}

}
