package com.ufps.api;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ufps.entity.ProyectoEntity;
import com.ufps.exceptions.BadRequestException;
import com.ufps.exceptions.NotFoundException;
import com.ufps.payload.ProyectoPayload;
import com.ufps.payload.ProyectoRegistroPayload;
import com.ufps.service.ActividadInterface;
import com.ufps.service.ConvenioInterface;
import com.ufps.service.EvidenciaProyectoInterface;
import com.ufps.service.PlanEstudiosInterface;
import com.ufps.service.ProcesoMisionalInterface;
import com.ufps.service.ProyectoInterface;
import com.ufps.service.TipoProyectoInterface;
import com.ufps.utilities.Constantes;
import com.ufps.utilities.Mapper;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT,
		RequestMethod.DELETE })
@RequestMapping(value = "/proyecto")
public class ProyectoApi {

	@Autowired
	private ProyectoInterface service;

	@Autowired
	private PlanEstudiosInterface servicePlanEstudios;

	@Autowired
	private TipoProyectoInterface serviceTipoProyecto;

	@Autowired
	private ProcesoMisionalInterface serviceProcesoMisional;

	@Autowired
	private HttpSession request;

	@Autowired
	private ActividadInterface actividades;

	@Autowired
	private ConvenioInterface convenios;

	@Autowired
	private EvidenciaProyectoInterface evidencias;

	@GetMapping(produces = "application/json")
	public List<ProyectoPayload> getAll() {
		String rol = (String) request.getAttribute("_ROL");
		int idPlanEstudios = (int) request.getAttribute("_IDPLANESTUDIOS");

		List<ProyectoEntity> lista = null;
		if (!rol.equalsIgnoreCase(Constantes.SUPER_USUARIO)) {
			lista = service.getAllIdPlanEstudios(idPlanEstudios);
		} else {
			lista = service.getAll();
		}
		List<ProyectoPayload> listproyecto = new ArrayList<>();
		lista.forEach(entity -> {
			listproyecto.add(Mapper.proyectoEntityToPayload(entity));
		});

		return listproyecto;
	}

	@GetMapping(path = "/{id}", produces = "application/json")
	public ResponseEntity<ProyectoPayload> getById(@PathVariable("id") int id) {
		ProyectoPayload proyecto = Mapper.proyectoEntityToPayload(service.getById(id));
		if (proyecto == null) {
			throw new NotFoundException("Proyecto no encontrado.");
		}
		return new ResponseEntity<>(proyecto, HttpStatus.OK);
	}

	@PostMapping(path = "/insertar", consumes = "application/json", produces = "application/json")
	public ProyectoPayload save(@Valid @RequestBody ProyectoRegistroPayload payload) {

		String rol = (String) request.getAttribute("_ROL");
		int idPlanEstudios = (int) request.getAttribute("_IDPLANESTUDIOS");

		if (!rol.equalsIgnoreCase(Constantes.ADMINISTRADOR) && !rol.equalsIgnoreCase(Constantes.DIRECTOR_PROYECTO)) {
			throw new BadRequestException("No tienes permisos para registrar los proyectos");
		}

		if (servicePlanEstudios.getById(payload.getIdPlanEstudios()) == null) {
			throw new NotFoundException("Plan de estudios no encontrado");
		}

		if (serviceProcesoMisional.getById(payload.getIdProcesoMisional()) == null) {
			throw new NotFoundException("Proceso misional no encontrado");
		}

		if (serviceTipoProyecto.getById(payload.getIdTipoProyecto()) == null) {
			throw new NotFoundException("Tipo de proyecto no encontrado");
		}

		if (idPlanEstudios != payload.getIdPlanEstudios()) {
			throw new BadRequestException("No puedes registrar proyectos a otros planes de estudio");
		}

		ProyectoEntity entity = Mapper.proyectoRegistroPayloadToEntity(payload);
		long registradoPor = (Long) request.getAttribute("_ID");
		entity.setRegistradoPor((int) registradoPor);
		return Mapper.proyectoEntityToPayload(service.save(entity));
	}

	@PutMapping("/modificar")
	public ProyectoPayload update(@RequestBody ProyectoPayload payload) {

		String rol = (String) request.getAttribute("_ROL");
		int idPlanEstudios = (int) request.getAttribute("_IDPLANESTUDIOS");
		long registradoPor = (Long) request.getAttribute("_ID");

		if (idPlanEstudios != payload.getIdPlanEstudios()) {
			throw new BadRequestException("No puedes modificar proyectos de otros planes de estudio");
		}

		if (!rol.equalsIgnoreCase(Constantes.ADMINISTRADOR) && !rol.equalsIgnoreCase(Constantes.DIRECTOR_PROYECTO)) {
			throw new BadRequestException("No tienes permisos para modificar los proyectos");
		}

		ProyectoPayload proyecto = Mapper.proyectoEntityToPayload(service.getById(payload.getId()));

		if (proyecto == null) {
			throw new NotFoundException("El proyecto no fue encontrado.");
		}

		if (rol.equalsIgnoreCase(Constantes.DIRECTOR_PROYECTO)) {
			if (proyecto.getRegistradoPor() != (int) registradoPor) {
				throw new BadRequestException("No puedes modificar los proyectos que no hayas creado");
			}
			if (!proyecto.getAprobar().equalsIgnoreCase(payload.getAprobar())) {
				throw new BadRequestException("No puedes aprobar proyectos");
			}

			if (!proyecto.getDivulgar().equalsIgnoreCase(payload.getDivulgar())) {
				throw new BadRequestException("No puedes divulgar proyectos");
			}
		}
		
		if (!proyecto.getAprobar().equalsIgnoreCase(payload.getAprobar()) && payload.getAprobar().equalsIgnoreCase(Constantes.NOAPROBAR)) {
			if (actividades.findAllByIdProyecto(proyecto.getId()) != null
					&& !actividades.findAllByIdProyecto(proyecto.getId()).isEmpty()) {
				throw new BadRequestException("No puedes marcar como no aprobado el proyecto ya que cuenta con actividades");
			}

			if (evidencias.findAllByIdProyecto(proyecto.getId()) != null
					&& !evidencias.findAllByIdProyecto(proyecto.getId()).isEmpty()) {
				throw new BadRequestException("No puedes marcar como no aprobado el proyecto ya que cuenta con evidencias");
			}

			if (convenios.findAllByIdProyecto(proyecto.getId()) != null
					&& !convenios.findAllByIdProyecto(proyecto.getId()).isEmpty()) {
				throw new BadRequestException("No puedes marcar como no aprobado el proyecto ya que cuenta con convenios");
			}
		}
		
		if (payload.getDivulgar().equalsIgnoreCase(Constantes.DIVULGAR) && payload.getAprobar().equalsIgnoreCase(Constantes.NOAPROBAR)) {
			throw new BadRequestException("No puedes divulgar proyectos los cuales no hayan sido aprobados");
		}

		if (proyecto.getRegistradoPor() != payload.getRegistradoPor()) {
			throw new BadRequestException("No puedes cambiar la persona que registro el proyecto");
		}

		if (servicePlanEstudios.getById(payload.getIdPlanEstudios()) == null) {
			throw new NotFoundException("Plan de estudios no encontrado");
		}

		if (serviceProcesoMisional.getById(payload.getIdProcesoMisional()) == null) {
			throw new NotFoundException("Proceso misional no encontrado");
		}

		if (serviceTipoProyecto.getById(payload.getIdTipoProyecto()) == null) {
			throw new NotFoundException("Tipo de proyecto no encontrado");
		}

		return Mapper.proyectoEntityToPayload(service.update(Mapper.proyectoPayloadToEntity(payload)));
	}

	@DeleteMapping("/eliminar/{id}")
	public void delete(@PathVariable("id") int id) {
		String rol = (String) request.getAttribute("_ROL");
		int idPlanEstudios = (int) request.getAttribute("_IDPLANESTUDIOS");

		if (!rol.equalsIgnoreCase(Constantes.ADMINISTRADOR) && !rol.equalsIgnoreCase(Constantes.SUPER_USUARIO)) {
			throw new BadRequestException("No tienes permisos para eliminar los proyectos");
		}

		ProyectoPayload proyecto = Mapper.proyectoEntityToPayload(service.getById(id));

		if (proyecto == null) {
			throw new NotFoundException("El proyecto no fue encontrado.");
		}

		if (servicePlanEstudios.getById(proyecto.getIdPlanEstudios()) == null) {
			throw new NotFoundException("Plan de estudios no encontrado");
		}

		if (rol.equalsIgnoreCase(Constantes.ADMINISTRADOR)) {
			if (proyecto.getIdPlanEstudios() != idPlanEstudios) {
				throw new BadRequestException("No puedes eliminar proyectos de otros planes de estudio");
			}
		}

		if (actividades.findAllByIdProyecto(proyecto.getId()) != null
				&& !actividades.findAllByIdProyecto(proyecto.getId()).isEmpty()) {
			throw new BadRequestException("No puedes eliminar el proyecto ya que cuenta con actividades");
		}

		if (evidencias.findAllByIdProyecto(proyecto.getId()) != null
				&& !evidencias.findAllByIdProyecto(proyecto.getId()).isEmpty()) {
			throw new BadRequestException("No puedes eliminar el proyecto ya que cuenta con evidencias");
		}

		if (convenios.findAllByIdProyecto(proyecto.getId()) != null
				&& !convenios.findAllByIdProyecto(proyecto.getId()).isEmpty()) {
			throw new BadRequestException("No puedes eliminar el proyecto ya que cuenta con convenios");
		}
		service.delete(id);
	}

}
