package com.ufps.api;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ufps.entity.EncuestaEntity;
import com.ufps.entity.PreguntaEntity;
import com.ufps.entity.ProyectoEntity;
import com.ufps.entity.RespuestaPreguntaEntity;
import com.ufps.exceptions.BadRequestException;
import com.ufps.exceptions.NotFoundException;
import com.ufps.payload.EncuestaPayload;
import com.ufps.payload.EncuestaRegistroPayload;
import com.ufps.payload.PreguntaPayload;
import com.ufps.payload.RespuestaEncuestaPayload;
import com.ufps.service.EncuestaInterface;
import com.ufps.service.PreguntaInterface;
import com.ufps.service.ProyectoInterface;
import com.ufps.service.RespuestaEncuestaInterface;
import com.ufps.service.RespuestaPreguntaInterface;
import com.ufps.utilities.Constantes;
import com.ufps.utilities.Mapper;


@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE })
@RequestMapping(value = "/encuesta")
public class EncuestaApi {

	@Autowired
	private EncuestaInterface service;
	
	@Autowired
	private PreguntaInterface servicePregunta;

	@Autowired
	private RespuestaEncuestaInterface serviceRespuestaEncuesta;
	
	@Autowired
	private RespuestaPreguntaInterface serviceRespuestaPregunta;
	
	@Autowired
	private ProyectoInterface serviceProyecto;

	@Autowired
	private HttpSession request;

	@GetMapping(produces = "application/json")
	public List<EncuestaPayload> getAll() {
		String rol = (String) request.getAttribute("_ROL");
		int idPlanEstudios = (int) request.getAttribute("_IDPLANESTUDIOS");

		if (rol.equalsIgnoreCase(Constantes.ESTUDIANTE)) {
			throw new BadRequestException("No tienes permisos para listar las encuestas");
		}

		List<EncuestaEntity> lista = null;
		if (!rol.equalsIgnoreCase(Constantes.SUPER_USUARIO)) {
			lista = service.findAllByIdPlanEstudio(idPlanEstudios);
		} else {
			lista = service.getAll();
		}
		List<EncuestaPayload> listaEncuesta = new ArrayList<>();
		lista.forEach(entity -> {
			List<PreguntaPayload> listaPreguntas = servicePregunta.findAllByIdEncuesta(entity.getId())
					.stream()
					.map(pregunta -> Mapper.preguntaEntityToPayload(pregunta))
					.collect(Collectors.toList());
					
			EncuestaPayload payload = Mapper.encuestaEntityToPayload(entity);
			payload.setPreguntas(listaPreguntas);
			listaEncuesta.add(payload);
		});

		return listaEncuesta;
	}

	@GetMapping(path = "/activas", produces = "application/json")
	public List<EncuestaPayload> getAllActivas() {
		String rol = (String) request.getAttribute("_ROL");
		int idPlanEstudios = (int) request.getAttribute("_IDPLANESTUDIOS");

		if (!rol.equalsIgnoreCase(Constantes.ADMINISTRADOR) && !rol.equalsIgnoreCase(Constantes.DIRECTOR_PROYECTO)) {
			throw new BadRequestException("No tienes permisos para listar las encuestas");
		}

		List<EncuestaEntity> lista = service.findAllByIdPlanEstudioAndEstado(idPlanEstudios, Constantes.ACTIVO);
		List<EncuestaPayload> listaEncuesta = new ArrayList<>();
		lista.forEach(entity -> {
			List<PreguntaPayload> listaPreguntas = servicePregunta.findAllByIdEncuesta(entity.getId())
					.stream()
					.map(pregunta -> Mapper.preguntaEntityToPayload(pregunta))
					.collect(Collectors.toList());
					
			EncuestaPayload payload = Mapper.encuestaEntityToPayload(entity);
			payload.setPreguntas(listaPreguntas);
			listaEncuesta.add(payload);
		});

		return listaEncuesta;
	}

	@GetMapping(path = "/{id}", produces = "application/json")
	public EncuestaPayload getById(@PathVariable("id") int id) {
		String rol = (String) request.getAttribute("_ROL");

		if (!rol.equalsIgnoreCase(Constantes.ADMINISTRADOR) && !rol.equalsIgnoreCase(Constantes.SUPER_USUARIO)) {
			throw new BadRequestException("No tienes permisos para consultar encuestas");
		}
		EncuestaPayload payload = Mapper.encuestaEntityToPayload(service.getById(id));
		if (payload == null) {
			throw new NotFoundException("Encuesta no encontrada");
		}

		List<PreguntaPayload> preguntas = servicePregunta.findAllByIdEncuesta(payload.getId())
				.stream()
				.map(pregunta -> Mapper.preguntaEntityToPayload(pregunta))
				.collect(Collectors.toList());
		
		payload.setPreguntas(preguntas);
		return payload;
	}

	@Transactional
	@PostMapping(path = "/insertar", consumes = "application/json", produces = "application/json")
	public EncuestaPayload save(@RequestBody EncuestaRegistroPayload payload) {

		String rol = (String) request.getAttribute("_ROL");
		int idPlanEstudios = (int) request.getAttribute("_IDPLANESTUDIOS");

		if (payload.getPreguntas() == null || payload.getPreguntas().isEmpty()) {
			throw new BadRequestException("Debes adjuntar preguntas.");
		}

		if (!rol.equalsIgnoreCase(Constantes.ADMINISTRADOR)) {
			throw new BadRequestException("No tienes permisos para registrar encuestas");
		}
		
		//Registro encuesta
		EncuestaEntity encuesta = Mapper.encuestaRegistroPayloadToEntity(payload);
		encuesta.setIdPlanEstudio(idPlanEstudios);
		EncuestaPayload payloadResponse = Mapper.encuestaEntityToPayload(service.save(encuesta));
		
		//Saco las lista de preguntas y las mapeo a lista de preguntas entity y las voy guardando
		List<PreguntaEntity> preguntas = payload.getPreguntas()
				.stream()
				.map(pregunta -> Mapper.preguntaRegistroPayloadToEntity(pregunta))
				.collect(Collectors.toList());
		List<PreguntaPayload> preguntasPayload = new ArrayList<>(); 
		preguntas.forEach(pregunta -> {
			if (!pregunta.getTipoPregunta().equalsIgnoreCase(Constantes.ABIERTA) && !pregunta.getTipoPregunta().equalsIgnoreCase(Constantes.UNICA)) {
				throw new BadRequestException("El tipo de pregunta no puede ser distinto a " + Constantes.ABIERTA + " o " + Constantes.UNICA);
			}
			pregunta.setIdEncuesta(payloadResponse.getId());
			preguntasPayload.add(Mapper.preguntaEntityToPayload(servicePregunta.save(pregunta)));
		});
		payloadResponse.setPreguntas(preguntasPayload);
		return payloadResponse;
	}

	@Transactional
	@PutMapping("/modificar")
	public EncuestaPayload update(@RequestBody EncuestaPayload payload) {

		String rol = (String) request.getAttribute("_ROL");
		int idPlanEstudios = (int) request.getAttribute("_IDPLANESTUDIOS");

		if (!rol.equalsIgnoreCase(Constantes.ADMINISTRADOR)) {
			throw new BadRequestException("No tienes permisos para modificar encuestas");
		}

		if (payload.getPreguntas() == null || payload.getPreguntas().isEmpty()) {
			throw new BadRequestException("Debes adjuntar preguntas.");
		}

		EncuestaPayload encuesta = Mapper.encuestaEntityToPayload(service.getById(payload.getId()));
		if (encuesta == null) {
			throw new NotFoundException("Encuesta no encontrada");
		}

		if (encuesta.getIdPlanEstudio() != idPlanEstudios || payload.getIdPlanEstudio() != idPlanEstudios) {
			throw new BadRequestException("No puedes modificar encuestas de otros planes de estudio.");
		}

		List<RespuestaEncuestaPayload> respuestas = serviceRespuestaEncuesta.findAllByIdEncuesta(payload.getId())
				.stream()
				.map(respuesta -> Mapper.respuestaEncuestaEntityToPayload(respuesta))
				.collect(Collectors.toList());
		if (respuestas != null && !respuestas.isEmpty()) {
			throw new BadRequestException("No puedes modificar encuestas con respuestas");
		}

		List<PreguntaEntity> preguntas = payload.getPreguntas()
				.stream()
				.map(pregunta -> Mapper.preguntaPayloadToEntity(pregunta))
				.collect(Collectors.toList());
		//Update pregunta
		EncuestaEntity encu = Mapper.encuestaPayloadToEntity(payload);
		EncuestaPayload payloadResponse = Mapper.encuestaEntityToPayload(service.update(encu));
		
		//Saco las lista de evidencias y las mapeo a lista de evidencias entity y las voy guardando
		List<PreguntaPayload> preguntasPayload = new ArrayList<>(); 
		for (PreguntaEntity pregunta : preguntas) {
			if (!pregunta.getTipoPregunta().equalsIgnoreCase(Constantes.ABIERTA) && !pregunta.getTipoPregunta().equalsIgnoreCase(Constantes.UNICA)) {
				throw new BadRequestException("El tipo de pregunta no puede ser distinto a " + Constantes.ABIERTA + " o " + Constantes.UNICA);
			}
			if (pregunta.getId() != null && pregunta.getId() != 0) {
				preguntasPayload.add(Mapper.preguntaEntityToPayload(servicePregunta.update(pregunta)));	
			} else {
				pregunta.setIdEncuesta(payload.getId());
				preguntasPayload.add(Mapper.preguntaEntityToPayload(servicePregunta.save(pregunta)));
			}
		}
		payloadResponse.setPreguntas(preguntasPayload);
		return payloadResponse;
	}

	@Transactional
	@DeleteMapping("/eliminar/{id}")
	public void delete(@PathVariable("id") int id) {

		String rol = (String) request.getAttribute("_ROL");
		int idPlanEstudios = (int) request.getAttribute("_IDPLANESTUDIOS");

		if (!rol.equalsIgnoreCase(Constantes.ADMINISTRADOR) && !rol.equalsIgnoreCase(Constantes.SUPER_USUARIO)) {
			throw new BadRequestException("No tienes permisos para eliminar encuestas");
		}
		
		EncuestaPayload payload = Mapper.encuestaEntityToPayload(service.getById(id));
		if (payload == null) {
			throw new NotFoundException("Encuesta no encontrada");
		}

		if (rol.equalsIgnoreCase(Constantes.ADMINISTRADOR)) {
			if (payload.getIdPlanEstudio() != idPlanEstudios) {
				throw new BadRequestException("No puedes eliminar encuestas a otros planes de estudio.");
			}
		}

		List<RespuestaEncuestaPayload> respuestas = serviceRespuestaEncuesta.findAllByIdEncuesta(payload.getId())
				.stream()
				.map(respuesta -> Mapper.respuestaEncuestaEntityToPayload(respuesta))
				.collect(Collectors.toList());
		if (respuestas != null && !respuestas.isEmpty()) {
			throw new BadRequestException("No puedes eliminar encuestas con respuestas");
		}

		List<PreguntaEntity> preguntas = servicePregunta.findAllByIdEncuesta(payload.getId());
		if (preguntas == null || preguntas.isEmpty()) {
			throw new BadRequestException("No se encontraron preguntas para agregar");
		}
	
		preguntas.forEach(pregunta -> servicePregunta.delete(pregunta.getId()));
		service.delete(payload.getId());
	}
	
	@GetMapping(path = "/listar-respuestas/{id}", produces = "application/json")
	public List<RespuestaEncuestaPayload> listarRespuestas(@PathVariable("id") int id) {
		String rol = (String) request.getAttribute("_ROL");

		if (!rol.equalsIgnoreCase(Constantes.ADMINISTRADOR)) {
			throw new BadRequestException("No tienes permisos cargar las respuestas encuestas");
		}
		
		EncuestaPayload payload = Mapper.encuestaEntityToPayload(service.getById(id));
		if (payload == null) {
			throw new NotFoundException("Encuesta no encontrada");
		}

		List<RespuestaEncuestaPayload> respuestas = serviceRespuestaEncuesta.findAllByIdEncuesta(id)
				.stream()
				.map(respuesta -> Mapper.respuestaEncuestaEntityToPayload(respuesta))
				.collect(Collectors.toList());
		if (respuestas == null || respuestas.isEmpty()) {
			throw new BadRequestException("Aún no se ha respondido esta encuenta.");
		}
		
		for (RespuestaEncuestaPayload respuesta: respuestas) {
			ProyectoEntity proyecto = serviceProyecto.getById(respuesta.getIdProyecto());
			if (proyecto == null) {
				throw new NotFoundException("Proyecto no encontrado");
			}
			respuesta.setNombreProyecto(proyecto.getNombre());
		};
		return respuestas;
	}
	
	@GetMapping(path = "/cargar-respuesta/{id}",  produces = "application/json")
	public List<PreguntaPayload> cargarRespuesta(@PathVariable("id") int id) {
		String rol = (String) request.getAttribute("_ROL");

		if (!rol.equalsIgnoreCase(Constantes.ADMINISTRADOR)) {
			throw new BadRequestException("No tienes permisos cargar las respuestas encuestas");
		}
		RespuestaEncuestaPayload respuestaEncuesta = Mapper.respuestaEncuestaEntityToPayload(serviceRespuestaEncuesta.getById(id));

		if (respuestaEncuesta == null ) {
			throw new NotFoundException("No se encontro la respuesta");
		}
		
		EncuestaPayload encuesta = Mapper.encuestaEntityToPayload(service.getById(respuestaEncuesta.getIdEncuesta()));
		if (encuesta == null) {
			throw new NotFoundException("Encuesta no encontrada");
		}

		List<PreguntaPayload> preguntas = servicePregunta.findAllByIdEncuesta(encuesta.getId())
				.stream()
				.map(pregunta -> Mapper.preguntaEntityToPayload(pregunta))
				.collect(Collectors.toList());
		if (preguntas == null || preguntas.isEmpty()) {
			throw new NotFoundException("Preguntas no encontradas");
		}

		for (PreguntaPayload pregunta : preguntas) {
			RespuestaPreguntaEntity respuestaPregunta = serviceRespuestaPregunta.findByIdRespuestaEncuestaAndIdPregunta(respuestaEncuesta.getId(), pregunta.getId()).orElse(null);
			if (respuestaPregunta == null) {
				throw new NotFoundException("Respuesta de pregunta no encontrada");
			}
			pregunta.setRespuesta(respuestaPregunta.getRespuestas());
		}

		return preguntas;
	}
}
