package com.ufps.api;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ufps.exceptions.BadRequestException;
import com.ufps.exceptions.NotFoundException;
import com.ufps.payload.EncuestaPayload;
import com.ufps.payload.InformacionInstitucionalRegistroPayload;
import com.ufps.payload.PlanEstudiosPayload;
import com.ufps.payload.PlanEstudiosRegistroPayload;
import com.ufps.payload.ProyectoPayload;
import com.ufps.service.EncuestaInterface;
import com.ufps.service.InformacionInstitucionalInterface;
import com.ufps.service.PlanEstudiosInterface;
import com.ufps.service.ProyectoInterface;
import com.ufps.utilities.Constantes;
import com.ufps.utilities.Mapper;
import com.ufps.utilities.Util;


@RestController
@CrossOrigin(origins="*", methods= {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
@RequestMapping(value="/plan-estudios")
public class PlanEstudiosApi {

	@Autowired
	private PlanEstudiosInterface service;

	@Autowired
	private HttpSession request;

	@Autowired
	private ProyectoInterface serviceProyecto;
	
	@Autowired
	private InformacionInstitucionalInterface serviceInformacion;
	
	@Autowired
	private EncuestaInterface serviceEncuesta;

	@GetMapping(produces = "application/json")
	public List<PlanEstudiosPayload> getAll(){
		return service.getAll()
				.stream()
				.map(ples -> Mapper.planEstudioEntityToPayload(ples))
				.collect(Collectors.toList());
	}

	@GetMapping(path = "/listar", produces = "application/json")
	public List<PlanEstudiosPayload> getAllPublico(){
		return service.getAll()
				.stream()
				.map(ples -> Mapper.planEstudioEntityToPayload(ples))
				.collect(Collectors.toList());
	}
	
	@GetMapping(path="/{id}", produces = "application/json")
	public PlanEstudiosPayload getById(@PathVariable("id") int id) {
		PlanEstudiosPayload payload = Mapper.planEstudioEntityToPayload(service.getById(id));
		if (payload == null) {
			throw new NotFoundException("Error!!\nPlan de estudios no encontrado.");
		}
		return payload;
	}

	@PostMapping(path = "/insertar", consumes = "application/json", produces = "application/json")
	public PlanEstudiosPayload save(@RequestBody PlanEstudiosRegistroPayload payload) {
		String rol = (String) request.getAttribute("_ROL");
		if (!rol.equalsIgnoreCase(Constantes.SUPER_USUARIO)) {
			throw new BadRequestException("Error!!\\nNo cuentas con los permisos para agregar planes de estudio");
		}
		
		if (service.existeUrl(payload.getUrl())) {
			throw new BadRequestException("Ya hay un plan de estudios con esa url");		
		}
		
		Util util = new Util();
		PlanEstudiosPayload payloadResponse = Mapper.planEstudioEntityToPayload(service.save(Mapper.planEstudioRegistroPayloadToEntity(payload)));
		//Guardo la Información institucional base para cada plan de estudio creado
		List<InformacionInstitucionalRegistroPayload> ininLista = util.cargarInformacionInstitucionalDefault(payloadResponse.getId());
		ininLista.forEach(inin -> serviceInformacion.save(Mapper.informacionInstitucionalRegistroPayloadToEntity(inin)));

		return payloadResponse;
	}

	@PutMapping("/modificar")
	public PlanEstudiosPayload modificar(@RequestBody PlanEstudiosPayload payload) {
		String rol = (String) request.getAttribute("_ROL");
		if (!rol.equalsIgnoreCase(Constantes.SUPER_USUARIO)) {
			throw new BadRequestException("Error!!\\nNo cuentas con los permisos para agregar planes de estudio");
		}
		PlanEstudiosPayload ples = Mapper.planEstudioEntityToPayload(service.getById(payload.getId()));
		if (ples == null) {
			throw new NotFoundException("Error!!\\nPlan de estudios no encontrado");
		}
		if (ples.getUrl() != payload.getUrl()) {
			if (service.existeUrl(payload.getUrl())) {
				throw new BadRequestException("Error!!\\nYa hay un plan de estudios con esa url");		
			}
		}
		return Mapper.planEstudioEntityToPayload(service.update(Mapper.planEstudioPayloadToEntity(payload)));
	}
	
	@DeleteMapping("/eliminar/{id}")
	public void delete(@PathVariable("id") int id) {
		String rol = (String) request.getAttribute("_ROL");
		if (!rol.equalsIgnoreCase(Constantes.SUPER_USUARIO)) {
			throw new BadRequestException("Error!!\\nNo cuentas con los permisos para agregar planes de estudio");
		}

		PlanEstudiosPayload ples = Mapper.planEstudioEntityToPayload(service.getById(id));
		if (ples == null) {
			throw new NotFoundException("Error!!\\nPlan de estudios no encontrado");
		}

		List<ProyectoPayload> proyectos = serviceProyecto.getAllIdPlanEstudios(id).stream()
				.map(proyecto -> Mapper.proyectoEntityToPayload(proyecto))
				.collect(Collectors.toList());
		if (proyectos != null && !proyectos.isEmpty()) {
			throw new BadRequestException("Error!!\\nNo puedes eliminar el plan de estudios ya que tiene proyectos asociados.");
		}
		
		List<EncuestaPayload> encuestas = serviceEncuesta.findAllByIdPlanEstudio(ples.getId())
				.stream()
				.map(encuesta -> Mapper.encuestaEntityToPayload(encuesta))
				.collect(Collectors.toList());
		if (encuestas != null && !encuestas.isEmpty()) {
			throw new BadRequestException("Error!!\\nNo puedes eliminar el plan de estudios ya que tiene encuestas asociadas.");
		}
		serviceInformacion.findAllByIdPlanEstudio(id).forEach(inin -> serviceInformacion.delete(inin.getId()));
		service.delete(id);
	}

}
