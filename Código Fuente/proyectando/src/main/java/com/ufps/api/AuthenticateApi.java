package com.ufps.api;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ufps.entity.PlanEstudiosEntity;
import com.ufps.entity.RolEntity;
import com.ufps.entity.UsuarioEntity;
import com.ufps.exceptions.BadRequestException;
import com.ufps.payload.JwtResponse;
import com.ufps.payload.LoginPayload;
import com.ufps.payload.MessageResponse;
import com.ufps.payload.UsuarioRegistroPayload;
import com.ufps.security.jwt.JwtUtils;
import com.ufps.service.PlanEstudiosInterface;
import com.ufps.service.RolInterface;
import com.ufps.service.UsuarioInterface;
import com.ufps.utilities.Constantes;
import com.ufps.utilities.Mapper;
import com.ufps.utilities.SimpleEmail;
import com.ufps.utilities.Util;

@RestController
@CrossOrigin(origins="*", methods= {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
@RequestMapping(value="/authenticate")
public class AuthenticateApi {

	@Autowired
	private UsuarioInterface service;
	
	@Autowired
	private RolInterface rolService;
	
	@Autowired
	private PlanEstudiosInterface planEstudiosService;

	@Autowired
	JwtUtils jwtUtils;

	@Autowired
	AuthenticationManager authenticationManager;

	@PostMapping(path="/login", consumes = "application/json", produces = "application/json")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginPayload loginPayload) {

		UsuarioEntity entity = service.findByCorreo(loginPayload.getCorreo()).orElse(null);

		if (entity == null) {
			throw new BadRequestException("Error!!\n Credenciales Incorrectas o Usuario Inactivo.");
		}

		if (entity.getEstado().equalsIgnoreCase(Constantes.INACTIVO)) {
			throw new BadRequestException("Error!!\n Credenciales Incorrectas o Usuario Inactivo.");
		}

		Util util = new Util();
		if (!util.validatePassword(entity, loginPayload.getContrasena())) {
			throw new BadRequestException("Error!!\\n Credenciales Incorrectas o Usuario Inactivo.");
		}

		String jwt = jwtUtils.generateJwtToken(entity.getCorreo());
			
		List<String> roles = entity.getRoles().stream()
				.map(item -> item.getNombre())
				.collect(Collectors.toList());

		JwtResponse resp = new JwtResponse(jwt, (long) entity.getId(), entity.getCorreo(), entity.getIdPlanEstudio(), roles, entity.getNombres());
		return ResponseEntity.ok(resp);
	}

	@PostMapping(path="/registrar", consumes = "application/json", produces = "application/json")
	public boolean registrar(@Valid @RequestBody UsuarioRegistroPayload user) {

		boolean resul = false;

		if (service.existsByCorreo(user.getCorreo())) {
			throw new BadRequestException("Error!!\nEl correo ya se encuentra registrado");
		}

		RolEntity rol = rolService.getById(user.getIdRol());
		if (rol == null) {
			throw new BadRequestException("Error!!\nEl rol asignado no es válido.");
		}

		PlanEstudiosEntity ples = planEstudiosService.getById(user.getIdPlanEstudio());
		if (ples == null) {
			throw new BadRequestException("Error!!\nEl plan de estudios asignado no es válido.");
		}

		if (rol.getNombre().equalsIgnoreCase(Constantes.ADMINISTRADOR)) {
			UsuarioEntity usuarioEntity = service.findByidPlanEstudioandidRol(user.getIdPlanEstudio(), user.getIdRol()).orElse(null);
			if (usuarioEntity != null && usuarioEntity.getEstado().equalsIgnoreCase(Constantes.ACTIVO)) {
				throw new BadRequestException(
						"Error!!\nYa hay un usuario administrador para el plan de estudios " + ples.getNombre() + ".");
			}
		}

		if (rol.getNombre().equalsIgnoreCase(Constantes.SUPER_USUARIO)) {
			throw new BadRequestException("Error!!\n No es posible registrarte con el rol seleccionado.");
		}

		UsuarioEntity entity = Mapper.usuarioRegistryPayloadToEntity(user);
		Util util = new Util();
		util.passwordEncrypted(user, entity);
		HashSet<RolEntity> roles = new HashSet<RolEntity>();
		roles.add(rol);
		entity.setRoles(roles);

		//Preparamos el cuerpo del mensaje
		Map<String, String> parameterContent = new HashMap<>();
		parameterContent.put("content-user", entity.getApellidos() + " " + entity.getNombres());
		parameterContent.put("content-email", entity.getCorreo());
		parameterContent.put("content-pass", user.getContrasena());
		String msj = util.AjustarContenidoHtml(Constantes.CONTENIDO_HTML_REGISTRO, parameterContent);

		if (service.save(entity) != null) {
			resul = true;
		}

		//Envio de mensajes 
		SimpleEmail sEmail = new SimpleEmail();
		Map<String, String> parameterEmail = new HashMap<>();
		parameterEmail.put("to", entity.getCorreo());
		parameterEmail.put("subject", Constantes.SUBJECT_REGISTRO);
		parameterEmail.put("message", util.contentMensaje(Constantes.ARCHIVO, msj));
		if (!sEmail.sendEmail(parameterEmail)) {
			throw new BadRequestException("Error!!\nEl email no pudo ser enviado. Pero su Usuario fue registrado exitosamente ");
		}
		return resul;
	}

	@GetMapping(path="/validar/{email}")
	public ResponseEntity<MessageResponse> validar(@Valid @PathVariable("email") String email) {

		UsuarioEntity entity = service.findByCorreo(email).orElse(null);
		if (entity == null) {
			throw new BadRequestException("Error!!\nEmail incorrecto");
		}

		if (entity.getEstado().equalsIgnoreCase(Constantes.INACTIVO)) {
			throw new BadRequestException("Error!!\nEl usuario se encuentra inactivo");
		}

		Util util = new Util();

		//Creamos una nueva contrasena
		UsuarioRegistroPayload userRegistry = new UsuarioRegistroPayload();
		userRegistry.setContrasena(String.valueOf((int)(Math.random()*(999999-100000+1)+100000)));
		util.passwordEncrypted(userRegistry, entity);

		//Preparamos el cuerpo del mensaje
		Map<String, String> parameterContent = new HashMap<>();
		parameterContent.put("content-user", entity.getApellidos() + " " + entity.getNombres());
		parameterContent.put("content-email", entity.getCorreo());
		parameterContent.put("content-pass", userRegistry.getContrasena());
		String msj = util.AjustarContenidoHtml(Constantes.CONTENIDO_HTML, parameterContent);

		//Actualizando usuario
		if (service.update(entity) == null) {
			throw new BadRequestException("Error!!\nNo se pudo modificar la cuenta del usuario.");
		}

		//Envio de mensajes 
		SimpleEmail sEmail = new SimpleEmail();
		Map<String, String> parameterEmail = new HashMap<>();
		parameterEmail.put("to", entity.getCorreo());
		parameterEmail.put("subject", Constantes.SUBJECT);
		parameterEmail.put("message", util.contentMensaje(Constantes.ARCHIVO, msj));
		if (!sEmail.sendEmail(parameterEmail)) {
			throw new BadRequestException("Error!!\nEl email no pudo ser enviado");
		}
		MessageResponse msg = new MessageResponse("Se te ha enviado un email con tu nueva contraseña!!");
		return new ResponseEntity<>(msg, HttpStatus.OK);
	}
}
