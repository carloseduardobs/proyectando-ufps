package com.ufps.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ufps.entity.TipoProyectoEntity;
import com.ufps.service.TipoProyectoInterface;


@RestController
@CrossOrigin(origins="*", methods= {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
@RequestMapping(value="/tipo-proyecto")
public class TipoProyectoApi {

	@Autowired
	private TipoProyectoInterface service;

	@GetMapping(produces = "application/json")
	public List<TipoProyectoEntity> getAll(){
		return service.getAll();
	}
	
	@GetMapping(path="/{id}", produces = "application/json")
	public ResponseEntity<TipoProyectoEntity> getById(@PathVariable("id") int id) {
		TipoProyectoEntity ent = service.getById(id);
		if(ent == null) {
			//throw new RecordNotFoundException("Id: ("  + idOpcion + ") not found");
		}
		 return new ResponseEntity<>(ent, HttpStatus.OK);
	}

}
