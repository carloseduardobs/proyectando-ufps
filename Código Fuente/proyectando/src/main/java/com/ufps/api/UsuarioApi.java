package com.ufps.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ufps.entity.RolEntity;
import com.ufps.entity.UsuarioEntity;
import com.ufps.exceptions.BadRequestException;
import com.ufps.payload.ActividadPayload;
import com.ufps.payload.ContrasenaPayload;
import com.ufps.payload.ConvenioPayload;
import com.ufps.payload.EvidenciaActividadPayload;
import com.ufps.payload.EvidenciaPayload;
import com.ufps.payload.EvidenciaProyectoPayload;
import com.ufps.payload.MessageResponse;
import com.ufps.payload.ProyectoPayload;
import com.ufps.payload.UsuarioConsultarPayload;
import com.ufps.payload.UsuarioPayload;
import com.ufps.payload.UsuarioRegistroPayload;
import com.ufps.service.ActividadInterface;
import com.ufps.service.ConvenioInterface;
import com.ufps.service.EvidenciaActividadInterface;
import com.ufps.service.EvidenciaInterface;
import com.ufps.service.EvidenciaProyectoInterface;
import com.ufps.service.ProyectoInterface;
import com.ufps.service.UsuarioInterface;
import com.ufps.utilities.Constantes;
import com.ufps.utilities.Mapper;
import com.ufps.utilities.SimpleEmail;
import com.ufps.utilities.Util;

@RestController
@CrossOrigin(origins="*", methods= {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
@RequestMapping(value="/usuario")
public class UsuarioApi {

	@Autowired
	private UsuarioInterface service;

	@Autowired
	private ProyectoInterface proyectoService;

	@Autowired
	private ActividadInterface actividadService;
	
	@Autowired
	private EvidenciaProyectoInterface evidenciaProyectoService;

	@Autowired
	private EvidenciaActividadInterface evidenciaActividadService;

	@Autowired
	private ConvenioInterface convenioService;
	
	@Autowired
	private EvidenciaInterface evidenciaService;

	@Autowired
	private HttpSession request;	

	@GetMapping(produces = "application/json")
	public List<UsuarioPayload> getAll() {
		String rol = (String) request.getAttribute("_ROL");
		int idPlanEstudios = (int) request.getAttribute("_IDPLANESTUDIOS");

		if (!rol.equalsIgnoreCase(Constantes.ADMINISTRADOR) && !rol.equalsIgnoreCase(Constantes.SUPER_USUARIO)) {
			throw new BadRequestException("Error!!\nEl usuario no tiene permitido ver el listado de usuarios");
		}
		List<UsuarioEntity> lista = null;
		if (rol.equalsIgnoreCase(Constantes.ADMINISTRADOR)) {
			lista  = service.getAllIdPlanEstudios(idPlanEstudios);
		} else {
			lista = service.getAll();
		}
		List<UsuarioPayload> listUser = new ArrayList<>();
		lista.forEach(entity -> {
			List<String> roles = entity.getRoles().stream()
					.map(item -> item.getNombre())
					.collect(Collectors.toList());
			if (!roles.contains(Constantes.SUPER_USUARIO)) {
				listUser.add(Mapper.entityToUsuarioPayload(entity));
			}
		});
		
		return listUser;
	}


	@GetMapping(path="/{id}", produces = "application/json")
	public ResponseEntity<UsuarioConsultarPayload> getById(@Valid @PathVariable("id") int id) {
		String rol = (String) request.getAttribute("_ROL");
		if (!rol.equalsIgnoreCase(Constantes.ADMINISTRADOR) && !rol.equalsIgnoreCase(Constantes.SUPER_USUARIO)) {
			throw new BadRequestException("Error!!\nEl usuario no tiene permitido ver Información de otros usuarios.");
		}
		UsuarioConsultarPayload payload = Mapper.entityToUsuarioConsultarPayload(service.getById(id));

		List<ProyectoPayload> proyectos = proyectoService.findAllByRegistradoPor(id)
				.stream()
				.map(proyecto -> Mapper.proyectoEntityToPayload(proyecto))
				.collect(Collectors.toList());
		payload.setProyectos(proyectos);

		List<ActividadPayload> actividades = actividadService.findAllByRegistradoPor(id)
				.stream()
				.map(actividad -> Mapper.actividadEntityToPayload(actividad))
				.collect(Collectors.toList());
		payload.setActividades(actividades);

		List<ConvenioPayload> convenios = convenioService.findAllByRegistradoPor(id)
				.stream()
				.map(convenio -> Mapper.convenioEntityToPayload(convenio))
				.collect(Collectors.toList());
		payload.setConvenios(convenios);
		
		List<EvidenciaActividadPayload> evidenciasActividad = evidenciaActividadService.findAllByRegistradoPor(id)
				.stream()
				.map(evidencia -> Mapper.evidenciaActividadEntityToPayload(evidencia))
				.collect(Collectors.toList());
		for (EvidenciaActividadPayload evac : evidenciasActividad) {
			List<EvidenciaPayload> evidencias = evidenciaService.getAllByIdEvidenciaActividad(evac.getId())
					.stream()
					.map(evidencia -> Mapper.evidenciaEntityToPayload(evidencia))
					.collect(Collectors.toList());
			evac.setEvidencias(evidencias);
		}
		payload.setEvidenciasActividad(evidenciasActividad);

		List<EvidenciaProyectoPayload> evidenciasProyecto = evidenciaProyectoService.findAllByRegistradoPor(id)
				.stream()
				.map(evidencia -> Mapper.evidenciaProyectoEntityToPayload(evidencia))
				.collect(Collectors.toList());
		for (EvidenciaProyectoPayload evpr : evidenciasProyecto) {
			List<EvidenciaPayload> evidencias = evidenciaService.getAllByIdEvidenciaProyecto(evpr.getId())
					.stream()
					.map(evidencia -> Mapper.evidenciaEntityToPayload(evidencia))
					.collect(Collectors.toList());
			evpr.setEvidencias(evidencias);
		}
		payload.setEvidenciasProyecto(evidenciasProyecto);
		return new ResponseEntity<>(payload, HttpStatus.OK);
	}


	@GetMapping(path="/cosultar", produces = "application/json")
	public ResponseEntity<UsuarioConsultarPayload> consultarUsaurio() {
		long id = (Long) request.getAttribute("_ID");
		UsuarioConsultarPayload payload = Mapper.entityToUsuarioConsultarPayload(service.getById((int) id));

		List<ProyectoPayload> proyectos = proyectoService.findAllByRegistradoPor((int) id)
				.stream()
				.map(proyecto -> Mapper.proyectoEntityToPayload(proyecto))
				.collect(Collectors.toList());
		payload.setProyectos(proyectos);

		List<ActividadPayload> actividades = actividadService.findAllByRegistradoPor((int) id)
				.stream()
				.map(actividad -> Mapper.actividadEntityToPayload(actividad))
				.collect(Collectors.toList());
		payload.setActividades(actividades);

		List<ConvenioPayload> convenios = convenioService.findAllByRegistradoPor((int) id)
				.stream()
				.map(convenio -> Mapper.convenioEntityToPayload(convenio))
				.collect(Collectors.toList());
		payload.setConvenios(convenios);
		
		List<EvidenciaActividadPayload> evidenciasActividad = evidenciaActividadService.findAllByRegistradoPor((int) id)
				.stream()
				.map(evidencia -> Mapper.evidenciaActividadEntityToPayload(evidencia))
				.collect(Collectors.toList());
		for (EvidenciaActividadPayload evac : evidenciasActividad) {
			List<EvidenciaPayload> evidencias = evidenciaService.getAllByIdEvidenciaActividad(evac.getId())
					.stream()
					.map(evidencia -> Mapper.evidenciaEntityToPayload(evidencia))
					.collect(Collectors.toList());
			evac.setEvidencias(evidencias);
		}
		payload.setEvidenciasActividad(evidenciasActividad);

		List<EvidenciaProyectoPayload> evidenciasProyecto = evidenciaProyectoService.findAllByRegistradoPor((int) id)
				.stream()
				.map(evidencia -> Mapper.evidenciaProyectoEntityToPayload(evidencia))
				.collect(Collectors.toList());
		for (EvidenciaProyectoPayload evpr : evidenciasProyecto) {
			List<EvidenciaPayload> evidencias = evidenciaService.getAllByIdEvidenciaProyecto(evpr.getId())
					.stream()
					.map(evidencia -> Mapper.evidenciaEntityToPayload(evidencia))
					.collect(Collectors.toList());
			evpr.setEvidencias(evidencias);
		}
		payload.setEvidenciasProyecto(evidenciasProyecto);
		return new ResponseEntity<>(payload, HttpStatus.OK);
	}	


	@PutMapping("/modificar")
	public boolean update(@Valid @RequestBody UsuarioConsultarPayload user) {
		boolean resul = false;

		String rol = (String) request.getAttribute("_ROL");
		long id = (Long) request.getAttribute("_ID");
		int idPlanEstudios = (int) request.getAttribute("_IDPLANESTUDIOS");

		if (!rol.equalsIgnoreCase(Constantes.ADMINISTRADOR) && !rol.equalsIgnoreCase(Constantes.SUPER_USUARIO)) {
			if ((int) id != user.getId()) {
				throw new BadRequestException("Error!!\nNo puedes modificar Información de otros usuarios.");
			}
		}

		UsuarioEntity entity = service.getById(user.getId());
		if (entity == null) {
			throw new BadRequestException("Error!!\nUsuario no encontrado.");
		}

		if (rol.equalsIgnoreCase(Constantes.ADMINISTRADOR)) {
			if (idPlanEstudios != entity.getIdPlanEstudio()) {
				throw new BadRequestException("Error!!\nNo puedes modificar Información de otros usuarios de otro plan de estudios.");
			}
		}

		if (!entity.getCorreo().equalsIgnoreCase(user.getCorreo())) {
			throw new BadRequestException("Error!!\nNo puedes modificar el correo electronico de un usuario.");
		}

		if (entity.getIdPlanEstudio() != user.getIdPlanEstudio()) {
			throw new BadRequestException("Error!!\nNo puedes modificar el plan de estudio de un usuario.");
		}

		List<RolEntity> roles = entity.getRoles().stream()
				.map(item -> item)
				.collect(Collectors.toList());

		if (roles == null  || roles.isEmpty() ) {
			throw new BadRequestException("Error!!\nEl usuario debe tener roles válidos.");
		}

		if (roles.get(0).getId() != user.getIdRol()) {
			throw new BadRequestException("Error!!\nNo puedes modificar el rol de un usuario.");
		}

		if (entity.getId() != user.getId()) {
			throw new BadRequestException("Error!!\nNo puedes modificar el Id del usuario.");
		}

		entity = Mapper.usuarioPayloadToEntity(entity, user);
		if (service.update(entity) == null) {
			throw new BadRequestException("Error!!\nModificando el usuario.");
		}

		resul = true;
		return resul;
	}


	@DeleteMapping("/eliminar/{id}")
	public ResponseEntity<MessageResponse> delete(@Valid @PathVariable("id") int id) {
		String rol = (String) request.getAttribute("_ROL");
		long idSesion = (Long) request.getAttribute("_ID");

		if (!rol.equalsIgnoreCase(Constantes.SUPER_USUARIO)) {
			throw new BadRequestException("Error!!\nEl usuario no cuenta con permisos para eliminar usuarios");
		}

		if (idSesion == id) {
			throw new BadRequestException("Error!!\nNo puedes eliminarte como super usuario");
		}
		
		if (!evidenciaActividadService.findAllByRegistradoPor(id).isEmpty()) {
			throw new BadRequestException("Error!!\nNo puedes eliminar el usuario tiene evidencias de actividad registradas.");
		} 

		if (!actividadService.findAllByRegistradoPor(id).isEmpty()) {
			throw new BadRequestException("Error!!\nNo puedes eliminar el usuario tiene actividades registradas.");
		} 

		if (!evidenciaProyectoService.findAllByRegistradoPor(id).isEmpty()) {
			throw new BadRequestException("Error!!\nNo puedes eliminar el usuario tiene evidencias de proyecto registradas.");
		} 
		
		if (!convenioService.findAllByRegistradoPor(id).isEmpty()) {
			throw new BadRequestException("Error!!\nNo puedes eliminar el usuario tiene convenios registrados.");
		} 
		
		if (!proyectoService.findAllByRegistradoPor(id).isEmpty()) {
			throw new BadRequestException("Error!!\nNo puedes eliminar el usuario tiene proyectos registrados.");
		} 
		
		service.delete(id);
		MessageResponse msgResponse = new MessageResponse("Usuario eliminado exitosamente!!");
		return new ResponseEntity<>(msgResponse, HttpStatus.OK);
	}


	@PostMapping(path="/cambio-contrasena", consumes = "application/json", produces = "application/json")
	public MessageResponse cambioContrasena(@Valid @RequestBody ContrasenaPayload contrasenaPayload) {
		String correo = (String) request.getAttribute("_CORREO");
		UsuarioEntity entity = service.findByCorreo(contrasenaPayload.getCorreo()).orElse(null);
		if (entity == null) {
			throw new BadRequestException("Error!!\nEl correo no es válidos.");
		}

		if (!correo.equalsIgnoreCase(contrasenaPayload.getCorreo())) {
			throw new BadRequestException("Error!!\nNo puedes cambiar la contraseña de un correo distinto al tuyo.");
		}

		Util util = new Util();
		if (!util.validatePassword(entity, contrasenaPayload.getContrasena())) {
			throw new BadRequestException("Error!!\\nLa contraseña no es valida.");
		}

		if (!contrasenaPayload.getContrasenaNueva()
				.equalsIgnoreCase(contrasenaPayload.getContrasenaVerificacion())) {
			throw new BadRequestException("Error!!\\nLas contraseñas deben ser iguales.");			
		}

		//Creamos una nueva contrasena y nuevo salt
		UsuarioRegistroPayload userRegistry = new UsuarioRegistroPayload();
		userRegistry.setContrasena(contrasenaPayload.getContrasenaNueva());
		util.passwordEncrypted(userRegistry, entity);

		//Actualizando usuario
		if (service.update(entity) == null) {
			throw new BadRequestException("Error!!\nNo se pudo guardar la nueva contraseña.");
		}

		//Preparamos el cuerpo del mensaje
		Map<String, String> parameterContent = new HashMap<>();
		parameterContent.put("content-user", entity.getApellidos() + " " + entity.getNombres());
		parameterContent.put("content-email", entity.getCorreo());
		parameterContent.put("content-pass", contrasenaPayload.getContrasenaNueva());
		String msj = util.AjustarContenidoHtml(Constantes.CONTENIDO_HTML_CAMBIO, parameterContent);


		//Envio de mensajes de confirmacion de cambio
		SimpleEmail sEmail = new SimpleEmail();
		Map<String, String> parameterEmail = new HashMap<>();
		parameterEmail.put("to", entity.getCorreo());
		parameterEmail.put("subject", Constantes.SUBJECT_CAMBIO);
		parameterEmail.put("message", util.contentMensaje(Constantes.ARCHIVO, msj));
		if (!sEmail.sendEmail(parameterEmail)) {
			throw new BadRequestException("Error!!\nEl email no pudo ser enviado");
		}

		return new MessageResponse("Cambio de contraseña exitoso!!");
	}

}
