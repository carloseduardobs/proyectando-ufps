package com.ufps.api;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ufps.exceptions.NotFoundException;
import com.ufps.payload.ActividadDashboardPayload;
import com.ufps.payload.ConveniosDashboardPayload;
import com.ufps.payload.EvidenciaPayload;
import com.ufps.payload.EvidenciaProyectoDashboardPayload;
import com.ufps.payload.EvidenciaActividadDashboardPayload;
import com.ufps.payload.InformacionInstitucionalPayload;
import com.ufps.payload.ProyectoDashboardPayload;
import com.ufps.service.ActividadInterface;
import com.ufps.service.ConvenioInterface;
import com.ufps.service.EvidenciaActividadInterface;
import com.ufps.service.EvidenciaInterface;
import com.ufps.service.EvidenciaProyectoInterface;
import com.ufps.service.InformacionInstitucionalInterface;
import com.ufps.service.PlanEstudiosInterface;
import com.ufps.service.ProyectoInterface;
import com.ufps.utilities.Constantes;
import com.ufps.utilities.Mapper;
import com.ufps.utilities.UtilInformacionInstitucional;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT,
		RequestMethod.DELETE })
@RequestMapping(value = "/dashboard")
public class DashboardApi {

	@Autowired
	private InformacionInstitucionalInterface serviceInformacionInstitucional;

	@Autowired
	private ProyectoInterface servicesProyectos;

	@Autowired
	private ActividadInterface serviceActividades;

	@Autowired
	private ConvenioInterface serviceConvenios;

	@Autowired
	private EvidenciaProyectoInterface serviceEvidenciaProyecto;

	@Autowired
	private EvidenciaActividadInterface serviceEvidenciaActividad;

	@Autowired
	private EvidenciaInterface serviceEvidencia;

	@Autowired
	private PlanEstudiosInterface servicePlanEstudios;

	@GetMapping(path = "/content/{url}/{path}", produces = "application/json")
	public InformacionInstitucionalPayload getById(@PathVariable("url") String url, @PathVariable("path") String path) {
		String key = "";
		switch (path) {
		case "inicio":
			key = Constantes.INICIO;
			break;
		case "politicas":
			key = Constantes.POLITICAS;
			break;
		case "principios":
			key = Constantes.PRINCIPIOS;
			break;
		case "eventos":
			key = Constantes.EVENTOS;
			break;
		case "proyectos":
			key = Constantes.PROYECTOS;
			break;
		case "practicas":
			key = Constantes.PRACTICAS;
			break;
		case "cifras":
			key = Constantes.CIFRAS;
			break;
		default:
			throw new NotFoundException("Contenido no permitido");
		}
		InformacionInstitucionalPayload info = Mapper
				.informacionInstitucionalEntityToPayload(serviceInformacionInstitucional.findAllByUrlAndPath(url, key));
		if (info == null) {
			throw new NotFoundException("Contenido no permitido");
		}
		return info;
	}

	@GetMapping(path = "/nav/{key}", produces = "application/json")
	public List<InformacionInstitucionalPayload> getAllNav(@PathVariable("key") String url) {
		if (!servicePlanEstudios.existeUrl(url)) {
			throw new NotFoundException("Error!!Landing page no encontrada");
		}

		List<InformacionInstitucionalPayload> lista = serviceInformacionInstitucional.findAllByUrl(url, Constantes.LOGO, Constantes.INICIO)
				.stream().map(entity -> Mapper.informacionInstitucionalEntityToPayload(entity))
				.collect(Collectors.toList());
		return lista;
	}

	@GetMapping(path = "/logo/{key}", produces = "application/json")
	public InformacionInstitucionalPayload getLogo(@PathVariable("key") String url) {
		if (!servicePlanEstudios.existeUrl(url)) {
			return UtilInformacionInstitucional.getLogoDefault();
		}

		return Mapper
				.informacionInstitucionalEntityToPayload(serviceInformacionInstitucional.getLogo(url, Constantes.LOGO));
	}

	@GetMapping(path = "/proyectos/{key}", produces = "application/json")
	public List<ProyectoDashboardPayload> getProyectos(@PathVariable("key") String url) {
		if (!servicePlanEstudios.existeUrl(url)) {
			throw new NotFoundException("Error!!Landing page no encontrada");
		}

		List<ProyectoDashboardPayload> proyectos = servicesProyectos.findAllDivulgadosByUrl(url).stream()
				.map(proyecto -> Mapper.proyectoDashboard(proyecto)).collect(Collectors.toList());
		
		proyectos.forEach(proyecto -> {
			List<EvidenciaPayload> evidencias = new ArrayList<>();
			List<ActividadDashboardPayload> actividades = serviceActividades
					.findAllByProyectoAndDivulgado(proyecto.getId()).stream()
					.map(actividad -> Mapper.actividadDashboard(actividad)).collect(Collectors.toList());

			actividades.forEach(actividad -> {
				List<EvidenciaActividadDashboardPayload> evidenciasActividad = serviceEvidenciaActividad
						.findAllByActividadAndDivulgado(actividad.getId()).stream()
						.map(evidencia -> Mapper.evidenciaActividadDashboard(evidencia)).collect(Collectors.toList());
				evidenciasActividad.forEach(evidencia -> {
					List<EvidenciaPayload> archivos = serviceEvidencia.getAllByIdEvidenciaActividad(evidencia.getId())
							.stream().map(archivo -> Mapper.evidenciaEntityToPayload(archivo))
							.collect(Collectors.toList());
					evidencia.setEvidencia(archivos);
					evidencia.getEvidencia().forEach(evi -> {
						evidencias.add(evi);
					});
				});
				actividad.setEvidencias(evidenciasActividad);
			});
			proyecto.setActividadesProyecto(actividades);

			ConveniosDashboardPayload convenio = Mapper
					.convenioDashboard(serviceConvenios.findAllByProyectoAndDivulgado(proyecto.getId()));
			if (convenio != null) {
				proyecto.setConvenio(convenio);
			}

			List<EvidenciaProyectoDashboardPayload> evidenciasProyecto = serviceEvidenciaProyecto
					.findAllByProyectoAndDivulgado(proyecto.getId()).stream()
					.map(evidencia -> Mapper.evidenciaProyectoDashboard(evidencia)).collect(Collectors.toList());
			evidenciasProyecto.forEach(evidencia -> {
				List<EvidenciaPayload> archivos = serviceEvidencia.getAllByIdEvidenciaProyecto(evidencia.getId())
						.stream().map(archivo -> Mapper.evidenciaEntityToPayload(archivo)).collect(Collectors.toList());
				evidencia.setEvidencia(archivos);
				evidencia.getEvidencia().forEach(evi -> {
					evidencias.add(evi);
				});
			});
			proyecto.setEvidenciasProyecto(evidenciasProyecto);
			proyecto.setEvidencias(evidencias);
		});
		return proyectos;
	}

	@GetMapping(path = "/eventos/{key}", produces = "application/json")
	public List<ActividadDashboardPayload> getEventos(@PathVariable("key") String url) {
		if (!servicePlanEstudios.existeUrl(url)) {
			throw new NotFoundException("Error!!Landing page no encontrada");
		}
		List<ActividadDashboardPayload> eventos = new ArrayList<>();
		List<ProyectoDashboardPayload> proyectos = servicesProyectos.findAllDivulgadosAndEventosByUrl(url).stream()
				.map(proyecto -> Mapper.proyectoDashboard(proyecto)).collect(Collectors.toList());

		proyectos.forEach(proyecto -> {
			List<ActividadDashboardPayload> actividades = serviceActividades
					.findAllByProyectoAndDivulgadoEventos(proyecto.getId()).stream()
					.map(actividad -> Mapper.actividadDashboard(actividad)).collect(Collectors.toList());

			actividades.forEach(actividad -> {
				actividad.setNombreProyecto(proyecto.getNombre());
				List<EvidenciaPayload> evidencias = new ArrayList<>();
				List<EvidenciaActividadDashboardPayload> evidenciasActividad = serviceEvidenciaActividad
						.findAllByActividadAndDivulgado(actividad.getId()).stream()
						.map(evidencia -> Mapper.evidenciaActividadDashboard(evidencia)).collect(Collectors.toList());
				evidenciasActividad.forEach(evidencia -> {
					List<EvidenciaPayload> archivos = serviceEvidencia.getAllByIdEvidenciaActividad(evidencia.getId())
							.stream().map(archivo -> Mapper.evidenciaEntityToPayload(archivo))
							.collect(Collectors.toList());
					evidencia.setEvidencia(archivos);
					evidencia.getEvidencia().forEach(evi -> {
						evidencias.add(evi);
					});
				});
				actividad.setEvidencias(evidenciasActividad);
				actividad.setEvidenciasActividad(evidencias);
				eventos.add(actividad);
			});
		});
		return eventos;
	}
}
