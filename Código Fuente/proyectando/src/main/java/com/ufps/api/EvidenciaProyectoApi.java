package com.ufps.api;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ufps.entity.EvidenciaEntity;
import com.ufps.entity.EvidenciaProyectoEntity;
import com.ufps.exceptions.BadRequestException;
import com.ufps.exceptions.NotFoundException;
import com.ufps.payload.EvidenciaPayload;
import com.ufps.payload.EvidenciaProyectoPayload;
import com.ufps.payload.EvidenciaProyectoRegistroPayload;
import com.ufps.payload.ProyectoPayload;
import com.ufps.service.EvidenciaInterface;
import com.ufps.service.EvidenciaProyectoInterface;
import com.ufps.service.ProyectoInterface;
import com.ufps.utilities.Constantes;
import com.ufps.utilities.Mapper;


@RestController
@CrossOrigin(origins="*", methods= {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
@RequestMapping(value="/evidencia-proyecto")
public class EvidenciaProyectoApi {


	@Autowired
	private EvidenciaProyectoInterface service;

	@Autowired
	private EvidenciaInterface evidenciasService;
	
	@Autowired
	private ProyectoInterface proyectoService;

	@Autowired
	private HttpSession request;

	@GetMapping(produces = "application/json")
	public List<EvidenciaProyectoPayload> getAll() {
		String rol = (String) request.getAttribute("_ROL");
		int idPlanEstudios = (int) request.getAttribute("_IDPLANESTUDIOS");

		if (!rol.equalsIgnoreCase(Constantes.ADMINISTRADOR) && !rol.equalsIgnoreCase(Constantes.SUPER_USUARIO)) {
			throw new BadRequestException("No tienes permisos para listar las evidencias de proyectos");
		}
		
		List<EvidenciaProyectoEntity> lista = null;
		if (!rol.equalsIgnoreCase(Constantes.SUPER_USUARIO)) {
			lista = service.getAllIdPlanEstudios(idPlanEstudios);
		} else {
			lista = service.getAll();
		}
		List<EvidenciaProyectoPayload> listaEvidencias = new ArrayList<>();
		lista.forEach(entity -> {
			List<EvidenciaPayload> listaArchivosPayload = new ArrayList<>();
			evidenciasService.getAllByIdEvidenciaProyecto(entity.getId()).forEach(evidenciaEntity -> {
				listaArchivosPayload.add(Mapper.evidenciaEntityToPayload(evidenciaEntity));
			});
			EvidenciaProyectoPayload payload = Mapper.evidenciaProyectoEntityToPayload(entity);
			payload.setEvidencias(listaArchivosPayload);
			listaEvidencias.add(payload);
		});

		return listaEvidencias;
	}

	@GetMapping(path = "/{id}", produces = "application/json")
	public List<EvidenciaProyectoPayload> getById(@PathVariable("id") int id) {

		ProyectoPayload proyecto = Mapper.proyectoEntityToPayload(proyectoService.getById(id));
		if (proyecto == null) {
			throw new NotFoundException("Proyecto no encontrado");
		}

		List<EvidenciaProyectoEntity> lista = service.findAllByIdProyecto(id);
		List<EvidenciaProyectoPayload> listaEvidencias = new ArrayList<>();
		lista.forEach(entity -> {
			List<EvidenciaPayload> listaArchivosPayload = new ArrayList<>();
			evidenciasService.getAllByIdEvidenciaProyecto(entity.getId()).forEach(evidenciaEntity -> {
				listaArchivosPayload.add(Mapper.evidenciaEntityToPayload(evidenciaEntity));
			});
			EvidenciaProyectoPayload payload = Mapper.evidenciaProyectoEntityToPayload(entity);
			payload.setEvidencias(listaArchivosPayload);
			listaEvidencias.add(payload);
		});

		return listaEvidencias;
	}

	@Transactional
	@PostMapping(path = "/insertar", consumes = "application/json", produces = "application/json")
	public EvidenciaProyectoPayload save(@RequestBody EvidenciaProyectoRegistroPayload payload) {
		String rol = (String) request.getAttribute("_ROL");
		int idPlanEstudios = (int) request.getAttribute("_IDPLANESTUDIOS");

		if (payload.getEvidencias() == null || payload.getEvidencias().isEmpty()) {
			throw new BadRequestException("Debes adjuntar evidencias.");
		}

		if (rol.equalsIgnoreCase(Constantes.SUPER_USUARIO)) {
			throw new BadRequestException("No tienes permisos para registrar evidencias a los proyectos");
		}

		ProyectoPayload proyecto = Mapper.proyectoEntityToPayload(proyectoService.getById(payload.getIdProyecto()));
		if (proyecto == null) {
			throw new NotFoundException("Proyecto no encontrado");
		}

		if (proyecto.getAprobar().equalsIgnoreCase(Constantes.NOAPROBAR)) {
			throw new BadRequestException("No puedes registrar evidencias a proyectos que no esten aprobados.");
		}

		if (idPlanEstudios != proyecto.getIdPlanEstudios()) {
			throw new BadRequestException("No puedes registrar evidencias a proyectos de otros planes de estudio.");
		}
		
		//Registro evidencia proyecto
		EvidenciaProyectoEntity evpr = Mapper.evidenciaProyectoRegistroPayloadToEntity(payload);
		long registradoPor = (long)request.getAttribute("_ID");
		evpr.setRegistradoPor((int) registradoPor);
		evpr.setAprobar(Constantes.APROBAR);
		evpr.setDivulgar(Constantes.DIVULGAR);
		EvidenciaProyectoPayload payloadResponse = Mapper.evidenciaProyectoEntityToPayload(service.save(evpr));
		
		//Saco las lista de evidencias y las mapeo a lista de evidencias entity y las voy guardando
		List<EvidenciaEntity> evidencias = payload.getEvidencias()
				.stream()
				.map(evidencia -> Mapper.evidenciaRegistroPayloadToEntity(evidencia))
				.collect(Collectors.toList());
		List<EvidenciaPayload> evidenciasPayload = new ArrayList<>(); 
		evidencias.forEach(evidencia -> {
			evidencia.setIdEvidenciaProyecto(payloadResponse.getId());
			evidenciasPayload.add(Mapper.evidenciaEntityToPayload(evidenciasService.save(evidencia)));
		});
		payloadResponse.setEvidencias(evidenciasPayload);
		return payloadResponse;
	}

	@Transactional
	@PutMapping("/modificar")
	public EvidenciaProyectoPayload update(@RequestBody EvidenciaProyectoPayload payload) {
		String rol = (String) request.getAttribute("_ROL");
		int idPlanEstudios = (int) request.getAttribute("_IDPLANESTUDIOS");
		long registradoPor =(Long) request.getAttribute("_ID");

		if (payload.getEvidencias() == null || payload.getEvidencias().isEmpty()) {
			throw new BadRequestException("Debes adjuntar evidencias.");
		}

		if (rol.equalsIgnoreCase(Constantes.SUPER_USUARIO)) {
			throw new BadRequestException("No tienes permisos para modificar evidencias a las actividades");
		}

		EvidenciaProyectoPayload evidenciaProyecto= Mapper.evidenciaProyectoEntityToPayload(service.getById(payload.getId()));
		if (evidenciaProyecto == null) {
			throw new NotFoundException("Evidencia de proyecto no encontrada");
		}

		ProyectoPayload proyecto = Mapper.proyectoEntityToPayload(proyectoService.getById(payload.getIdProyecto()));
		if (proyecto == null) {
			throw new NotFoundException("Proyecto no encontrado");
		}

		if (payload.getDivulgar().equalsIgnoreCase(Constantes.DIVULGAR) && payload.getAprobar().equalsIgnoreCase(Constantes.NOAPROBAR)) {
			throw new BadRequestException("No puedes divulgar evidencias las cuales no hayan sido aprobadas");
		}

		if (idPlanEstudios != proyecto.getIdPlanEstudios()) {
			throw new BadRequestException("No puedes registrar evidencias a proyectos de otros planes de estudio.");
		}

		if (payload.getRegistradoPor() != evidenciaProyecto.getRegistradoPor()) {
			throw new BadRequestException("No puedes cambiar la persona que registro la evidencia");
		}

		if (!rol.equalsIgnoreCase(Constantes.ADMINISTRADOR)) {
			if (evidenciaProyecto.getRegistradoPor() != (int) registradoPor) {
				throw new BadRequestException("No puedes modificar las evidencias de proyectos que no hayas creado");
			}
			if (!evidenciaProyecto.getAprobar().equalsIgnoreCase(payload.getAprobar())) {
				throw new BadRequestException("No puedes aprobar las evidencias de proyectos");
			}
			
			if (!evidenciaProyecto.getDivulgar().equalsIgnoreCase(payload.getDivulgar())) {
				throw new BadRequestException("No puedes divulgar las evidencias de proyectos");
			}
		}

		List<EvidenciaEntity> evidencias = payload.getEvidencias()
				.stream()
				.map(evidencia -> Mapper.evidenciaPayloadToEntity(evidencia))
				.collect(Collectors.toList());
		//Update evidencia proyecto
		EvidenciaProyectoEntity evpr = Mapper.evidenciaProyectoPayloadToEntity(payload);
		EvidenciaProyectoPayload payloadResponse = Mapper.evidenciaProyectoEntityToPayload(service.update(evpr));
		
		//Saco las lista de evidencias y las mapeo a lista de evidencias entity y las voy guardando
		List<EvidenciaPayload> evidenciasPayload = new ArrayList<>(); 
		for (EvidenciaEntity evidencia : evidencias) {
			if (evidencia.getId() != null && evidencia.getId() != 0) {
				evidenciasPayload.add(Mapper.evidenciaEntityToPayload(evidenciasService.update(evidencia)));	
			} else {
				evidencia.setIdEvidenciaProyecto(payload.getId());
				evidenciasPayload.add(Mapper.evidenciaEntityToPayload(evidenciasService.save(evidencia)));
			}
		}
		payloadResponse.setEvidencias(evidenciasPayload);
		return payloadResponse;
	}

	@Transactional
	@DeleteMapping("/eliminar/{id}")
	public void delete(@PathVariable("id") int id) {
		String rol = (String) request.getAttribute("_ROL");
		int idPlanEstudios = (int) request.getAttribute("_IDPLANESTUDIOS");

		if (!rol.equalsIgnoreCase(Constantes.ADMINISTRADOR) && !rol.equalsIgnoreCase(Constantes.SUPER_USUARIO)) {
			throw new BadRequestException("No tienes permisos para eliminar las evidencias de proyectos");
		}

		EvidenciaProyectoPayload payload = Mapper.evidenciaProyectoEntityToPayload(service.getById(id));
		if (payload == null) {
			throw new NotFoundException("La evidencia del proyecto no fue encontrada.");
		}

		ProyectoPayload proyecto = Mapper.proyectoEntityToPayload(proyectoService.getById(payload.getIdProyecto()));
		if (proyecto == null) {
			throw new NotFoundException("El proyecto no fue encontrado.");
		}

		if (rol.equalsIgnoreCase(Constantes.ADMINISTRADOR)) {
			if (proyecto.getIdPlanEstudios() != idPlanEstudios) {
				throw new BadRequestException("No puedes eliminar las evidencias de prouyectos de otros planes de estudio");
			}
		}
		List<EvidenciaEntity> evidencias = evidenciasService.getAllByIdEvidenciaProyecto(payload.getId());
		if (evidencias == null || evidencias.isEmpty()) {
			throw new NotFoundException("Evidencias no encontradas");
		}
		
		evidencias.forEach(evidencia -> evidenciasService.delete(evidencia.getId()));
		service.delete(id);
	}

	
}
