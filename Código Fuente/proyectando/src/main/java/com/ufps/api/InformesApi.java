package com.ufps.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ufps.exceptions.BadRequestException;
import com.ufps.exceptions.NotFoundException;
import com.ufps.payload.EncuestaPayload;
import com.ufps.payload.InformeActividadPayload;
import com.ufps.payload.InformeConvenioPayload;
import com.ufps.payload.InformeEncuestaPayload;
import com.ufps.payload.InformeProyectoPayload;
import com.ufps.payload.InformeUsuarioPayload;
import com.ufps.payload.InformesPayload;
import com.ufps.payload.PreguntaPayload;
import com.ufps.payload.RespuestaPreguntaPayload;
import com.ufps.service.ActividadInterface;
import com.ufps.service.ConvenioInterface;
import com.ufps.service.EncuestaInterface;
import com.ufps.service.PreguntaInterface;
import com.ufps.service.ProyectoInterface;
import com.ufps.service.RespuestaEncuestaInterface;
import com.ufps.service.RespuestaPreguntaInterface;
import com.ufps.service.UsuarioInterface;
import com.ufps.utilities.Constantes;
import com.ufps.utilities.Mapper;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT,
		RequestMethod.DELETE })
@RequestMapping(value = "/informes")
public class InformesApi {

	@Autowired
	private ProyectoInterface serviceProyecto;

	@Autowired
	private ActividadInterface serviceActividad;

	@Autowired
	private ConvenioInterface serviceConvenios;

	@Autowired
	private UsuarioInterface serviceUsuarios;

	@Autowired
	private RespuestaEncuestaInterface serviceRespuestaEncuesta;

	@Autowired
	private EncuestaInterface serviceEncuesta;

	@Autowired
	private PreguntaInterface servicePregunta;

	@Autowired
	private RespuestaPreguntaInterface serviceRespuestaPregunta;

	@Autowired
	private HttpSession request;

	@PostMapping(path = "/proyectos", consumes = "application/json", produces = "application/json")
	public List<InformeProyectoPayload> informeProyecto(@RequestBody InformesPayload payload) {

		String rol = (String) request.getAttribute("_ROL");
		int idPlanEstudios = (int) request.getAttribute("_IDPLANESTUDIOS");

		if (!rol.equalsIgnoreCase(Constantes.ADMINISTRADOR)) {
			throw new BadRequestException("No tienes permisos para registrar los proyectos");
		}

		if (!payload.getTipoInforme().equalsIgnoreCase("Proyectos")) {
			throw new BadRequestException("Tipo de informe incorrecto.");
		}
		List<Object[]> informe = null;
		if (payload.getAprobados().equalsIgnoreCase("TODOS")) {
			informe = serviceProyecto.getInformeProyecto(payload.getFechaInicio(), payload.getFechaFin(),
					idPlanEstudios);
		} else {
			informe = serviceProyecto.getInformeProyecto(payload.getFechaInicio(), payload.getFechaFin(),
					payload.getAprobados(), idPlanEstudios);
		}

		List<InformeProyectoPayload> informePayload = new ArrayList<>();
		if (informe != null) {
			informe.forEach(info -> {
				informePayload.add(Mapper.objectToInformeProyectoPayload(info));
			});

		}
		return informePayload;
	}

	@PostMapping(path = "/actividades", consumes = "application/json", produces = "application/json")
	public List<InformeActividadPayload> informeActividades(@RequestBody InformesPayload payload) {

		String rol = (String) request.getAttribute("_ROL");
		int idPlanEstudios = (int) request.getAttribute("_IDPLANESTUDIOS");

		if (!rol.equalsIgnoreCase(Constantes.ADMINISTRADOR)) {
			throw new BadRequestException("No tienes permisos para registrar los proyectos");
		}

		if (!payload.getTipoInforme().equalsIgnoreCase("Actividades")) {
			throw new BadRequestException("Tipo de informe incorrecto.");
		}
		List<Object[]> informe = null;
		if (payload.getAprobados().equalsIgnoreCase("TODOS")) {
			informe = serviceActividad.getInformeActividad(payload.getFechaInicio(), payload.getFechaFin(),
					idPlanEstudios);
		} else {
			informe = serviceActividad.getInformeActividad(payload.getFechaInicio(), payload.getFechaFin(),
					payload.getAprobados(), idPlanEstudios);
		}

		List<InformeActividadPayload> informePayload = new ArrayList<>();
		if (informe != null) {
			informe.forEach(info -> {
				informePayload.add(Mapper.objectToInformeActividadPayload(info));
			});

		}
		return informePayload;
	}

	@PostMapping(path = "/convenios", consumes = "application/json", produces = "application/json")
	public List<InformeConvenioPayload> informeConvenios(@RequestBody InformesPayload payload) {

		String rol = (String) request.getAttribute("_ROL");
		int idPlanEstudios = (int) request.getAttribute("_IDPLANESTUDIOS");

		if (!rol.equalsIgnoreCase(Constantes.ADMINISTRADOR)) {
			throw new BadRequestException("No tienes permisos para registrar los proyectos");
		}

		if (!payload.getTipoInforme().equalsIgnoreCase("Convenios")) {
			throw new BadRequestException("Tipo de informe incorrecto.");
		}
		List<Object[]> informe = serviceConvenios.getInformeConvenios(payload.getFechaInicio(), payload.getFechaFin(),
				idPlanEstudios);

		List<InformeConvenioPayload> informePayload = new ArrayList<>();
		if (informe != null) {
			informe.forEach(info -> {
				informePayload.add(Mapper.objectToInformeConvenioPayload(info));
			});

		}
		return informePayload;
	}

	@PostMapping(path = "/usuarios", consumes = "application/json", produces = "application/json")
	public List<InformeUsuarioPayload> informeUsuarios(@RequestBody InformesPayload payload) {

		String rol = (String) request.getAttribute("_ROL");
		int idPlanEstudios = (int) request.getAttribute("_IDPLANESTUDIOS");

		if (!rol.equalsIgnoreCase(Constantes.ADMINISTRADOR)) {
			throw new BadRequestException("No tienes permisos para registrar los proyectos");
		}

		if (!payload.getTipoInforme().equalsIgnoreCase("Usuarios")) {
			throw new BadRequestException("Tipo de informe incorrecto.");
		}
		List<Object[]> informe = null;
		if (payload.getAprobados().equalsIgnoreCase("TODOS")) {
			informe = serviceUsuarios.getInformeUsuarios(payload.getFechaInicio(), payload.getFechaFin(),
					idPlanEstudios);
		} else {
			String estado = payload.getAprobados().equalsIgnoreCase("SI") ? Constantes.ACTIVO : Constantes.INACTIVO;
			informe = serviceUsuarios.getInformeUsuarios(payload.getFechaInicio(), payload.getFechaFin(), estado,
					idPlanEstudios);
		}

		List<InformeUsuarioPayload> informePayload = new ArrayList<>();
		if (informe != null) {
			informe.forEach(info -> {
				informePayload.add(Mapper.objectToInformeUsuariosPayload(info));
			});

		}
		return informePayload;
	}

	@PostMapping(path = "/encuestas", consumes = "application/json", produces = "application/json")
	public Map<String, Object> informeEncuestas(@RequestBody InformesPayload payload) {

		String rol = (String) request.getAttribute("_ROL");
		int idPlanEstudios = (int) request.getAttribute("_IDPLANESTUDIOS");

		if (!rol.equalsIgnoreCase(Constantes.ADMINISTRADOR)) {
			throw new BadRequestException("No tienes permisos para registrar los proyectos");
		}

		if (!payload.getTipoInforme().equalsIgnoreCase("Encuestas")) {
			throw new BadRequestException("Tipo de informe incorrecto.");
		}

		if (payload.getEncuesta() == null || payload.getEncuesta().trim().equalsIgnoreCase("")) {
			throw new BadRequestException("Encuesta seleccionada no encontrada.");
		}

		EncuestaPayload encuesta = Mapper
				.encuestaEntityToPayload(serviceEncuesta.getById(Integer.parseInt(payload.getEncuesta())));
		if (encuesta == null) {
			throw new NotFoundException("Encuesta no encontrada.");
		}

		List<PreguntaPayload> preguntas = servicePregunta.findAllByIdEncuesta(encuesta.getId()).stream()
				.map(pregunta -> Mapper.preguntaEntityToPayload(pregunta)).collect(Collectors.toList());
		if (preguntas == null || preguntas.isEmpty()) {
			throw new NotFoundException("Preguntas no encontradas.");
		}

		Map<String, Object> mapResult = new HashMap<>();
		List<String> columnsToDisplay = this.getColumnsToDisplay(preguntas);// columnas id
		List<String> columns = this.getColumns(preguntas); // Lo que se va a mostrar
		List<Boolean> hiddenColumn = this.getHiddenColumn(preguntas);
		List<Map<String, Object>> rows = new ArrayList<>();

		List<Object[]> informe = serviceRespuestaEncuesta.getInformeEncuesta(payload.getFechaInicio(),
				payload.getFechaFin(), idPlanEstudios, encuesta.getId());
		if (informe != null) {
			informe.forEach(info -> {
				Map<String, Object> mapaRespuesta = new HashMap<>();
				InformeEncuestaPayload informePayload = Mapper.objectToInformeEncuestaPayload(info);
				List<RespuestaPreguntaPayload> listaRespuestaPregunta = serviceRespuestaPregunta
						.findAllByIdRespuestaEncuesta(Integer.parseInt(informePayload.getIdRespuestaEncuesta()))
						.stream().map(repr -> Mapper.respuestaPreguntaEntityToPayload(repr))
						.collect(Collectors.toList());
				if (listaRespuestaPregunta == null || listaRespuestaPregunta.isEmpty()) {
					throw new NotFoundException("Respuesta de pregunta no encontradas.");
				}
				mapaRespuesta.put("proyecto", informePayload.getProyecto());
				mapaRespuesta.put("correo", informePayload.getCorreo());
				mapaRespuesta.put("fechaEnvio", informePayload.getFechaEnvio());
				mapaRespuesta.put("fechaRealizacion", informePayload.getFechaRealizacion());
				for (RespuestaPreguntaPayload repr : listaRespuestaPregunta) {
					mapaRespuesta.put("pregunta_" + repr.getIdPregunta(), repr.getRespuestas());
				}
				rows.add(mapaRespuesta);
			});

		}

		mapResult.put("columnsToDisplay", columnsToDisplay);
		mapResult.put("columns", columns);
		mapResult.put("hiddenColumn", hiddenColumn);
		mapResult.put("rows", rows);
		return mapResult;
	}

	private List<String> getColumnsToDisplay(List<PreguntaPayload> preguntas) {
		List<String> lista = new ArrayList<>();
		lista.add("proyecto");
		lista.add("correo");
		lista.add("fechaEnvio");
		lista.add("fechaRealizacion");
		for (PreguntaPayload payload : preguntas) {
			lista.add("pregunta_" + payload.getId());
		}
		return lista;
	}

	private List<String> getColumns(List<PreguntaPayload> preguntas) {
		List<String> lista = new ArrayList<>();
		lista.add("Proyecto");
		lista.add("Correo");
		lista.add("Fecha Envio");
		lista.add("Fecha Realizacion");
		for (PreguntaPayload payload : preguntas) {
			lista.add(payload.getEnunciado());
		}
		return lista;
	}

	private List<Boolean> getHiddenColumn(List<PreguntaPayload> preguntas) {
		List<Boolean> lista = new ArrayList<>();
		lista.add(true);
		lista.add(true);
		lista.add(true);
		lista.add(true);
		for (PreguntaPayload payload : preguntas) {
			payload.getId();
			lista.add(false);
		}
		return lista;
	}
}
