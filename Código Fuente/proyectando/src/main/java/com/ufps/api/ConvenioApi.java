package com.ufps.api;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ufps.entity.ConvenioEntity;
import com.ufps.exceptions.BadRequestException;
import com.ufps.exceptions.NotFoundException;
import com.ufps.payload.ConvenioPayload;
import com.ufps.payload.ConvenioRegistroPayload;
import com.ufps.payload.ProyectoPayload;
import com.ufps.service.ConvenioInterface;
import com.ufps.service.ProyectoInterface;
import com.ufps.utilities.Constantes;
import com.ufps.utilities.Mapper;


@RestController
@CrossOrigin(origins="*", methods= {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
@RequestMapping(value="/convenio")
public class ConvenioApi {

	
	@Autowired
	private ConvenioInterface service;

	@Autowired
	private ProyectoInterface proyectoSerivice;

	@Autowired
	private HttpSession request;

	@GetMapping(produces = "application/json")
	public List<ConvenioPayload> getAll(){
		String rol = (String) request.getAttribute("_ROL");
		int idPlanEstudios = (int) request.getAttribute("_IDPLANESTUDIOS");

		List<ConvenioEntity> lista = null;
		if (!rol.equalsIgnoreCase(Constantes.SUPER_USUARIO)) {
			lista = service.getAllIdPlanEstudios(idPlanEstudios);
		} else {
			lista = service.getAll();
		}
		List<ConvenioPayload> listaConvenios = new ArrayList<>();
		lista.forEach(entity -> {
			listaConvenios.add(Mapper.convenioEntityToPayload(entity));
		});
		
		return listaConvenios;
	}
	
	@GetMapping(path="/{id}", produces = "application/json")
	public ResponseEntity<ConvenioPayload> getById(@PathVariable("id") int id) {
		ConvenioPayload convenio = Mapper.convenioEntityToPayload(service.getById(id));
		if(convenio == null) {
			//throw new RecordNotFoundException("Id: ("  + idOpcion + ") not found");
		}
		 return new ResponseEntity<>(convenio, HttpStatus.OK);
	}
	
	@PostMapping(path="/insertar", consumes = "application/json", produces = "application/json")
	public ConvenioPayload save(@RequestBody ConvenioRegistroPayload payload) {

		String rol = (String) request.getAttribute("_ROL");
		int idPlanEstudios = (int) request.getAttribute("_IDPLANESTUDIOS");

		if (!rol.equalsIgnoreCase(Constantes.ADMINISTRADOR) && !rol.equalsIgnoreCase(Constantes.DIRECTOR_PROYECTO)) {
			throw new BadRequestException("No tienes permisos para registrar las convenios");
		}

		ProyectoPayload proyecto = Mapper.proyectoEntityToPayload(proyectoSerivice.getById(payload.getIdProyecto()));
		if (proyecto == null) {
			throw new NotFoundException("El proyecto no fue encontrado.");
		}

		if (proyecto.getAprobar().equalsIgnoreCase(Constantes.NOAPROBAR)) {
			throw new BadRequestException("No puedes registrar convenios a proyectos que no esten aprobados.");
		}

		if (service.existsByIdProyecto(proyecto.getId())) {
			throw new BadRequestException("Solo puedes tener un convenio por proyecto.");
		}

		if (proyecto.getIdPlanEstudios() != idPlanEstudios) {
			throw new BadRequestException("No puedes registrar convenios a proyectos de otros planes de estudio");
		}

		ConvenioEntity entity =Mapper.convenioRegistroPayloadToEntity(payload);
		long registradoPor =(Long) request.getAttribute("_ID");
		entity.setRegistradoPor((int) registradoPor);
		return Mapper.convenioEntityToPayload(service.save(entity));
	}
	
	@PutMapping("/modificar")
	public ConvenioPayload update(@RequestBody ConvenioPayload payload) {

		String rol = (String) request.getAttribute("_ROL");
		int idPlanEstudios = (int) request.getAttribute("_IDPLANESTUDIOS");
		long registradoPor =(Long) request.getAttribute("_ID");

		if (!rol.equalsIgnoreCase(Constantes.ADMINISTRADOR) && !rol.equalsIgnoreCase(Constantes.DIRECTOR_PROYECTO)) {
			throw new BadRequestException("No tienes permisos para modificar los convenios");
		}

		ConvenioPayload convenio = Mapper.convenioEntityToPayload(service.getById(payload.getId()));
		if (convenio == null) {
			throw new NotFoundException("El convenio no fue encontrado.");
		}

		ProyectoPayload proyecto = Mapper.proyectoEntityToPayload(proyectoSerivice.getById(payload.getIdProyecto()));
		if (proyecto == null) {
			throw new NotFoundException("El proyecto no fue encontrado.");
		}

		if (payload.getDivulgar().equalsIgnoreCase(Constantes.DIVULGAR) && payload.getAprobar().equalsIgnoreCase(Constantes.NOAPROBAR)) {
			throw new BadRequestException("No puedes divulgar convenios los cuales no hayan sido aprobados");
		}

		if (proyecto.getIdPlanEstudios() != idPlanEstudios) {
			throw new BadRequestException("No puedes modificar convenios de otros planes de estudio");
		}

		if (convenio.getRegistradoPor() != payload.getRegistradoPor()) {
			throw new BadRequestException("No puedes cambiar la persona que registro el convenio");
		}

		if (rol.equalsIgnoreCase(Constantes.DIRECTOR_PROYECTO)) {
			if (convenio.getRegistradoPor() != (int) registradoPor) {
				throw new BadRequestException("No puedes modificar los convenios que no hayas creado");
			}
			if (!convenio.getAprobar().equalsIgnoreCase(payload.getAprobar())) {
				throw new BadRequestException("No puedes aprobar convenios");
			}
			
			if (!convenio.getDivulgar().equalsIgnoreCase(payload.getDivulgar())) {
				throw new BadRequestException("No puedes divulgar convenios");
			}
		}

		return Mapper.convenioEntityToPayload(service.save(Mapper.convenioPayloadToEntity(payload)));
	}
	
	@DeleteMapping("/eliminar/{id}")
	public void delete(@PathVariable("id") int id) {
		String rol = (String) request.getAttribute("_ROL");
		int idPlanEstudios = (int) request.getAttribute("_IDPLANESTUDIOS");

		if (!rol.equalsIgnoreCase(Constantes.ADMINISTRADOR) && !rol.equalsIgnoreCase(Constantes.SUPER_USUARIO)) {
			throw new BadRequestException("No tienes permisos para eliminar los proyectos");
		}

		ConvenioPayload payload = Mapper.convenioEntityToPayload(service.getById(id));
		if (payload == null) {
			throw new NotFoundException("El convenio no fue encontrado.");
		}

		ProyectoPayload proyecto = Mapper.proyectoEntityToPayload(proyectoSerivice.getById(payload.getIdProyecto()));
		if (proyecto == null) {
			throw new NotFoundException("El proyecto no fue encontrado.");
		}

		if (rol.equalsIgnoreCase(Constantes.ADMINISTRADOR)) {
			if (proyecto.getIdPlanEstudios() != idPlanEstudios) {
				throw new BadRequestException("No puedes eliminar convenios de otros planes de estudio");
			}
		}

		service.delete(id);
	}

}
