package com.ufps.api;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ufps.entity.EvidenciaActividadEntity;
import com.ufps.entity.EvidenciaEntity;
import com.ufps.exceptions.BadRequestException;
import com.ufps.exceptions.NotFoundException;
import com.ufps.payload.ActividadPayload;
import com.ufps.payload.EvidenciaActividadPayload;
import com.ufps.payload.EvidenciaActividadRegistroPayload;
import com.ufps.payload.EvidenciaPayload;
import com.ufps.payload.ProyectoPayload;
import com.ufps.service.ActividadInterface;
import com.ufps.service.EvidenciaActividadInterface;
import com.ufps.service.EvidenciaInterface;
import com.ufps.service.ProyectoInterface;
import com.ufps.utilities.Constantes;
import com.ufps.utilities.Mapper;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT,
		RequestMethod.DELETE })
@RequestMapping(value = "/evidencia-actividad")
public class EvidenciaActividadApi {

	@Autowired
	private EvidenciaActividadInterface service;

	@Autowired
	private EvidenciaInterface evidenciasService;
	
	@Autowired 
	private ActividadInterface actividadService;

	@Autowired
	private ProyectoInterface proyectoService;

	@Autowired
	private HttpSession request;

	@GetMapping(produces = "application/json")
	public List<EvidenciaActividadPayload> getAll() {
		String rol = (String) request.getAttribute("_ROL");
		int idPlanEstudios = (int) request.getAttribute("_IDPLANESTUDIOS");

		if (!rol.equalsIgnoreCase(Constantes.ADMINISTRADOR) && !rol.equalsIgnoreCase(Constantes.SUPER_USUARIO)) {
			throw new BadRequestException("No tienes permisos para listar las evidencias de actividades");
		}

		List<EvidenciaActividadEntity> lista = null;
		if (!rol.equalsIgnoreCase(Constantes.SUPER_USUARIO)) {
			lista = service.getAllIdPlanEstudios(idPlanEstudios);
		} else {
			lista = service.getAll();
		}
		List<EvidenciaActividadPayload> listaEvidencias = new ArrayList<>();
		lista.forEach(entity -> {
			List<EvidenciaPayload> listaArchivosPayload = new ArrayList<>();
			evidenciasService.getAllByIdEvidenciaProyecto(entity.getId()).forEach(evidenciaEntity -> {
				listaArchivosPayload.add(Mapper.evidenciaEntityToPayload(evidenciaEntity));
			});
			EvidenciaActividadPayload payload = Mapper.evidenciaActividadEntityToPayload(entity);
			payload.setEvidencias(listaArchivosPayload);
			listaEvidencias.add(payload);
		});

		return listaEvidencias;
	}

	@GetMapping(path = "/{id}", produces = "application/json")
	public List<EvidenciaActividadPayload> getById(@PathVariable("id") int id) {

		ActividadPayload actividad = Mapper.actividadEntityToPayload(actividadService.getById(id));
		if (actividad == null) {
			throw new NotFoundException("Actividad no encontrada");
		}

		List<EvidenciaActividadEntity> lista = service.findAllByIdActividad(id);
		List<EvidenciaActividadPayload> listaEvidencias = new ArrayList<>();
		lista.forEach(entity -> {
			List<EvidenciaPayload> listaArchivosPayload = new ArrayList<>();
			evidenciasService.getAllByIdEvidenciaProyecto(entity.getId()).forEach(evidenciaEntity -> {
				listaArchivosPayload.add(Mapper.evidenciaEntityToPayload(evidenciaEntity));
			});
			EvidenciaActividadPayload payload = Mapper.evidenciaActividadEntityToPayload(entity);
			payload.setEvidencias(listaArchivosPayload);
			listaEvidencias.add(payload);
		});

		return listaEvidencias;
	}

	@Transactional
	@PostMapping(path = "/insertar", consumes = "application/json", produces = "application/json")
	public EvidenciaActividadPayload save(@RequestBody EvidenciaActividadRegistroPayload payload) {
		String rol = (String) request.getAttribute("_ROL");
		int idPlanEstudios = (int) request.getAttribute("_IDPLANESTUDIOS");

		if (payload.getEvidencias() == null || payload.getEvidencias().isEmpty()) {
			throw new BadRequestException("Debes adjuntar evidencias.");
		}

		if (rol.equalsIgnoreCase(Constantes.SUPER_USUARIO)) {
			throw new BadRequestException("No tienes permisos para registrar evidencias a las actividades");
		}
		
		ActividadPayload actividad = Mapper.actividadEntityToPayload(actividadService.getById(payload.getIdActividad()));
		if (actividad == null) {
			throw new NotFoundException("Actividad no encontrada");
		}

		if (actividad.getAprobar().equalsIgnoreCase(Constantes.NOAPROBAR)) {
			throw new BadRequestException("No puedes registrar evidencias a actividades que no esten aprobadas.");
		}

		ProyectoPayload proyecto = Mapper.proyectoEntityToPayload(proyectoService.getById(actividad.getIdProyecto()));
		if (proyecto == null) {
			throw new NotFoundException("Proyecto no encontrado");
		}

		if (idPlanEstudios != proyecto.getIdPlanEstudios()) {
			throw new BadRequestException("No puedes registrar evidencias a actividades de otros planes de estudio.");
		}

		//Registro evidencia actividad
		EvidenciaActividadEntity evpr = Mapper.evidenciaActividadRegistroPayloadToEntity(payload);
		long registradoPor = (long)request.getAttribute("_ID");
		evpr.setRegistradoPor((int) registradoPor);
		evpr.setAprobar(Constantes.APROBAR);
		evpr.setDivulgar(Constantes.DIVULGAR);
		EvidenciaActividadPayload payloadResponse = Mapper.evidenciaActividadEntityToPayload(service.save(evpr));
		
		//Saco las lista de evidencias y las mapeo a lista de evidencias entity y las voy guardando
		List<EvidenciaEntity> evidencias = payload.getEvidencias()
				.stream()
				.map(evidencia -> Mapper.evidenciaRegistroPayloadToEntity(evidencia))
				.collect(Collectors.toList());
		List<EvidenciaPayload> evidenciasPayload = new ArrayList<>(); 
		evidencias.forEach(evidencia -> {
			evidencia.setIdEvidenciaActividad(payloadResponse.getId());
			evidenciasPayload.add(Mapper.evidenciaEntityToPayload(evidenciasService.save(evidencia)));
		});
		payloadResponse.setEvidencias(evidenciasPayload);
		return payloadResponse;
	}

	@Transactional
	@PutMapping("/modificar")
	public EvidenciaActividadPayload update(@RequestBody EvidenciaActividadPayload payload) {
		String rol = (String) request.getAttribute("_ROL");
		int idPlanEstudios = (int) request.getAttribute("_IDPLANESTUDIOS");
		long registradoPor =(Long) request.getAttribute("_ID");

		if (payload.getEvidencias() == null || payload.getEvidencias().isEmpty()) {
			throw new BadRequestException("Debes adjuntar evidencias.");
		}

		if (rol.equalsIgnoreCase(Constantes.SUPER_USUARIO)) {
			throw new BadRequestException("No tienes permisos para registrar evidencias a las actividades");
		}

		EvidenciaActividadPayload evidenciaActividad = Mapper.evidenciaActividadEntityToPayload(service.getById(payload.getId()));
		if (evidenciaActividad == null) {
			throw new NotFoundException("Evidencia de actividad no encontrada");
		}

		ActividadPayload actividad = Mapper.actividadEntityToPayload(actividadService.getById(payload.getIdActividad()));
		if (actividad == null) {
			throw new NotFoundException("Actividad no encontrada");
		}

		if (payload.getDivulgar().equalsIgnoreCase(Constantes.DIVULGAR) && payload.getAprobar().equalsIgnoreCase(Constantes.NOAPROBAR)) {
			throw new BadRequestException("No puedes divulgar evidencias las cuales no hayan sido aprobadas");
		}

		ProyectoPayload proyecto = Mapper.proyectoEntityToPayload(proyectoService.getById(actividad.getIdProyecto()));
		if (proyecto == null) {
			throw new NotFoundException("Proyecto no encontrado");
		}

		if (idPlanEstudios != proyecto.getIdPlanEstudios()) {
			throw new BadRequestException("No puedes modificar evidencias a actividades de otros planes de estudio.");
		}

		if (payload.getRegistradoPor() != evidenciaActividad.getRegistradoPor()) {
			throw new BadRequestException("No puedes cambiar la persona que registro la evidencia");
		}

		if (!rol.equalsIgnoreCase(Constantes.ADMINISTRADOR)) {
			if (evidenciaActividad.getRegistradoPor() != (int) registradoPor) {
				throw new BadRequestException("No puedes modificar las evidencias de actividad que no hayas creado");
			}
			if (!evidenciaActividad.getAprobar().equalsIgnoreCase(payload.getAprobar())) {
				throw new BadRequestException("No puedes aprobar las evidencias de actividad");
			}
			
			if (!evidenciaActividad.getDivulgar().equalsIgnoreCase(payload.getDivulgar())) {
				throw new BadRequestException("No puedes divulgar las evidencias de actividad");
			}
		}
		
		List<EvidenciaEntity> evidencias = payload.getEvidencias()
				.stream()
				.map(evidencia -> Mapper.evidenciaPayloadToEntity(evidencia))
				.collect(Collectors.toList());
		//Update evidencia actividad
		EvidenciaActividadEntity evpr = Mapper.evidenciaActividadPayloadToEntity(payload);
		EvidenciaActividadPayload payloadResponse = Mapper.evidenciaActividadEntityToPayload(service.update(evpr));
		
		//Saco las lista de evidencias y las mapeo a lista de evidencias entity y las voy guardando
		List<EvidenciaPayload> evidenciasPayload = new ArrayList<>(); 
		for (EvidenciaEntity evidencia : evidencias) {
			if (evidencia.getId() != null && evidencia.getId() != 0) {
				evidenciasPayload.add(Mapper.evidenciaEntityToPayload(evidenciasService.update(evidencia)));	
			} else {
				evidencia.setIdEvidenciaActividad(payload.getId());
				evidenciasPayload.add(Mapper.evidenciaEntityToPayload(evidenciasService.save(evidencia)));
			}
		}
		payloadResponse.setEvidencias(evidenciasPayload);
		return payloadResponse;
	}

	@Transactional
	@DeleteMapping("/eliminar/{id}")
	public void delete(@PathVariable("id") int id) {
		String rol = (String) request.getAttribute("_ROL");
		int idPlanEstudios = (int) request.getAttribute("_IDPLANESTUDIOS");

		if (!rol.equalsIgnoreCase(Constantes.ADMINISTRADOR) && !rol.equalsIgnoreCase(Constantes.SUPER_USUARIO)) {
			throw new BadRequestException("No tienes permisos para eliminar las evidencias de actividad");
		}

		EvidenciaActividadPayload payload = Mapper.evidenciaActividadEntityToPayload(service.getById(id));
		if (payload == null) {
			throw new NotFoundException("La evidencia de la actividad no fue encontrada.");
		}

		ActividadPayload actividad = Mapper.actividadEntityToPayload(actividadService.getById(payload.getIdActividad()));
		if (actividad == null) {
			throw new NotFoundException("Actividad no encontrada");
		}

		ProyectoPayload proyecto = Mapper.proyectoEntityToPayload(proyectoService.getById(actividad.getIdProyecto()));
		if (proyecto == null) {
			throw new NotFoundException("El proyecto no fue encontrado.");
		}

		if (rol.equalsIgnoreCase(Constantes.ADMINISTRADOR)) {
			if (proyecto.getIdPlanEstudios() != idPlanEstudios) {
				throw new BadRequestException("No puedes eliminar las evidencias de actividad de otros planes de estudio");
			}
		}

		List<EvidenciaEntity> evidencias = evidenciasService.getAllByIdEvidenciaActividad(payload.getId());
		if (evidencias == null || evidencias.isEmpty()) {
			throw new NotFoundException("Evidencias no encontradas");
		}
		
		evidencias.forEach(evidencia -> evidenciasService.delete(evidencia.getId()));
		service.delete(id);
	}

}
