package com.ufps.api;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ufps.exceptions.BadRequestException;
import com.ufps.exceptions.NotFoundException;
import com.ufps.payload.EncuestaPayload;
import com.ufps.payload.PreguntaPayload;
import com.ufps.payload.RespuestaEncuestaPayload;
import com.ufps.service.EncuestaInterface;
import com.ufps.service.PreguntaInterface;
import com.ufps.service.RespuestaEncuestaInterface;
import com.ufps.utilities.Constantes;
import com.ufps.utilities.Mapper;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE })
@RequestMapping(value = "/pregunta")
public class PreguntaApi {

	@Autowired
	private PreguntaInterface service;

	@Autowired
	private EncuestaInterface serviceEncuesta;

	@Autowired
	private RespuestaEncuestaInterface serviceRespuestaEncuesta;

	@Autowired
	private HttpSession request;

	@DeleteMapping("/eliminar/{id}")
	public void delete(@PathVariable("id") int id) {

		String rol = (String) request.getAttribute("_ROL");
		int idPlanEstudios = (int) request.getAttribute("_IDPLANESTUDIOS");

		if (!rol.equalsIgnoreCase(Constantes.ADMINISTRADOR) && !rol.equalsIgnoreCase(Constantes.SUPER_USUARIO)) {
			throw new BadRequestException("No tienes permisos para eliminar encuestas");
		}

		PreguntaPayload payload = Mapper.preguntaEntityToPayload(service.getById(id));
		if (payload == null) {
			throw new NotFoundException("Pregunta no encontrada");
		}
		
		EncuestaPayload encuesta = Mapper.encuestaEntityToPayload(serviceEncuesta.getById(payload.getIdEncuesta()));
		if (encuesta == null) {
			throw new NotFoundException("Encuesta no encontrada");
		}

		List<RespuestaEncuestaPayload> respuestas = serviceRespuestaEncuesta.findAllByIdEncuesta(payload.getId())
				.stream()
				.map(respuesta -> Mapper.respuestaEncuestaEntityToPayload(respuesta))
				.collect(Collectors.toList());
		if (respuestas != null && !respuestas.isEmpty()) {
			throw new BadRequestException("No puedes eliminar preguntas de encuestas con respuestas");
		}

		if (rol.equalsIgnoreCase(Constantes.ADMINISTRADOR)) {
			if (encuesta.getIdPlanEstudio() != idPlanEstudios) {
				throw new BadRequestException("No puedes eliminar preguntas de encuestas a otros planes de estudio.");
			}
		}
		
		service.delete(payload.getId());
	}
	
}
