# Proyectando-UFPS

Proyectando-UFPS es un aplicativo web para la gestión de la proyección social de la programa Ingeniería de Sistemas de la Universidad Francisco de Paula Santander.

## Índice
1. <a href="#caracter%C3%ADsticas">Características</a>
2. <a href="#contenido-del-proyecto">Contenido del proyecto</a>
3. <a href="#arquitectura">Arquitectura</a>
3. <a href="#tecnolog%C3%ADas">Tecnologías</a>
4. <a href="#ide">IDE</a>
5. <a href="#instalaci%C3%B3n">Instalación</a>
6. <a href="#demo">Demo</a>
7. <a href="#autores">Autor(es)</a>
8. <a href="#instituci%C3%B3n-acad%C3%A9mica">Institución Académica</a>

### Características

- El aplicativo web Proyectando - UFPS permite gestionar los diferentes tipos de proyectos que se trabajan en el ambito de la proyeccion social de la universidad
- Permite gestionar todas las actividades que se llevan a cabo durante un proyecto y que ayudan a que cumpla con sus objetivos. 
- Permite gestionar diferentes evidencias que se puedan tomar durante las actividades realizadas asi como durante la ejecucion del proyecto
- Se pueden adjuntar convenios que se establezcan con los posibles beneficiarios de los proyectos
- Se generan encuestas para conocer la opinion de los beneficiarios de dichos proyectos
- Permite la gestion de los usuarios que interactuan con la vista administrativa del aplicativo.
- Permite el manejo de la informacion que se puede mostrar a los usuarios que ingresen a la pagina de proyeccion social (LandingPage)

### Contenido del proyecto
#### Frontend
EL proyecto Frontend de Proyectando-UFPS llamado [Frontend-Proyectando](https://gitlab.com/carloseduardobs/proyectando-ufps/-/tree/main/C%C3%B3digo%20Fuente/Frontend-Proyectando) Se encuentra compuesto por los paquetes que contienen los componentes Angular que conforman la capa de presentación de la aplicación
|Paquete | Descripción |
|:---      |:---        |
|app|Este paquete contiene el componente app.component el cual renderiza todas las vistas de la aplicación.|
|public|Este paquete contiene los componentes inicio.component, mision-vision.component, politicas.component, estadisticas-landing.component,  proyectos.component, eventos.component, login.component, register.component, contacto.component, los cuales se encargan de renderizar todas las vistas a las cuales cualquier usuario puede acceder y que muestra toda la información pública sobre la proyección social.|
|admin|Este paquete contiene los componentes dashboard.component, proyecto.component, crear-proyecto.component, consultar-proyectos.component, actividad.component, crear-actividad.component, consultar-actividades.component, convenios.component, consultar-convenios.component, usuario.component, consultar-usuarios.component, adjuntar-evidencia.component, informes.component, estadisticas.component, los cuales se encargan de renderizar todas las vistas de administración del aplicativo y a las cuales solo los usuarios registrados pueden acceder y que muestra todas las opciones que se disponen para la gestión de los proyectos de proyección social.|
|models|Este paquete contiene los modelos proyecto.ts, convenio.ts, evidencia.ts, tipoProyecto.ts, user.ts, actividad.ts los cuales se encargan de crear las interfaces para definir la estructura de los datos.|
|material|Este paquete contiene el módulo material.module el cual se encarga de importar y exportar todos los módulos que se implementaron en el aplicativo para el uso de angular material.|
|services|Este paquete contiene los servicios autenticacion.service, actividad.service, proyecto.service, convenios.service, notificacion.service, usuario.service, landing-page.service, los cuales se encargan de la comunicación con los servicios expuestos externamente (Backend) a partir del uso de un cliente http con el cual se realizan las diferentes peticiones RestApi, también algunos servicios son usados para el manejo de datos entre componentes.|
|shared|Este paquete contiene los componentes dialog.component, sidenav.component, footer.component, header.component los cuales son algunos reusables y de uso compartido entre toda la aplicación.|

#### Backend
EL proyecto Backend de Proyectando-UFPS llamado [proyectando](https://gitlab.com/carloseduardobs/proyectando-ufps/-/tree/main/C%C3%B3digo%20Fuente/proyectando) es una api construida con Spring Boot el cual se utiliza para la implementación de nuestra lógica de negocio, para la comunicación con la capa de datos a través de Spring JPA, exponer nuestras Apis para para que sean consumidas por la capa de presentación y para consumir servicios externos como Amazon simple email servicio.

|Paquete | Descripción |
|:---      |:---        |
|ufps(root)|Es el paquete en el cual se inicializa la aplicación ProyectandoApplication.|
|api|Es el paquete encargado de las solicitudes las cuales implementa la lógica de negocio, compuesta por los archivos ActividadApi, AuthenticateApi, ConvenioApi, DashboardApi, EvidenciaActividadApi, EvidenciaApi EvidenciaProyectoApi, InformacionInstitucionalApi, LandingPageApi, PlanEstudiosApi, ProcesoMisionalApi, ProyectoApi, ReportesApi, RolApi, TipoProyectoApi, UsuarioApi y UsuarioRolApi.|
|config|Compuesta por los archivos entre los cuales tenemos la configuración de excepciones ExceptionConfig y la configuración para utiliza swagger para documentar nuestras apis SwaggerConfiguration.|
|entity|Compuesta por los archivos los cuales son representación de la base de datos los cuales son ActividadEntity, ConvenioEntity, EvidenciaActividadEntity, EvidenciaEntity, EvidenciaProyectoEntity, InformacionInstitucionalEntity, PlanEstudiosEntity, ProcesoMisionalEntity, ProyectoEntity, RolEntity, TipoProyectoEntity, UsuarioEntity, UsuarioRolEntity.|
|exception|Compuesta por los archivos de excepciones los cuales extienden de RuntimeException entre los cuales tenemos BadRequestException y NotFoundException.|
|payload|Compuesta por los archivos en los cuales mapeamos los campos que recibiremos y enviaremos en las peticiones esto con el fin de no exponer las entitys al usuario final; los archivos son ActividadPayload, ActividadRegistroPayload, ContrasenaPayload, ConvenioPayload, ConvenioRegistroPayload, EvidenciaActividadPayload, EvidenciaActividadRegistroPayload, EvidenciaPayload, EvidenciaProyectoPayload, EvidenciaProyectoRegistroPayload, EvidenciaRegistroActividadPayload, EvidenciaRegistroProyectoPayload, LoginPayload, ProyectoPayload, ProyectoRegistroPayload, UsuarioPayload, UsuarioRegistroPayload, JwtResponse, MessageResponse.|
|repository|Su característica más atractiva es la capacidad de crear implementaciones de repositorio automáticamente, en tiempo de ejecución, desde una interfaz los archivos son ActividadRepository, ConvenioRepository, EvidenciaActividadRepository, EvidenciaProyectoRepository, EvidenciaRepository, InformacionInstitucionalRepository, PlanEstudiosRepository, ProcesoMisionalRepository, ProyectoRepository, RolRepository, TipoProyectoRepository, UsuarioRepository, UsuarioRolRepository.|
|security|Este paquete contiene todo lo necesario para la autenticación de usuarios por medio de JWT estos archivos son WebSecurityConfig, AuthEntryPointJwt, AuthTokenFilter, JwtUtils, UserDetailsImpl, UserDetailsServiceImpl.|
|service|Estos archivos de clase son los encargados de conectar repository con api, los archivos que la componen son ActividadInterface, ActividadService, ConvenioInterface, ConvenioService, EvidenciaActividadInterface, EvidenciaActividadService, EvidenciaInterface, EvidenciaProyectoInterface, EvidenciaProyectoService, EvidenciaService, InformacionInstitucionalInterface, InformacionInstitucionalService, PlanEstudiosInterface, PlanEstudiosService, ProcesoMisionalInterface, ProcesoMisionalService, ProyectoInterface, ProyectoService, RolInterface, RolService, TipoProyectoInterface, TipoProyectoService, UsuarioInterface, UsuarioRolInterface, UsuarioRolService, UsuarioService.|
|utilities|Esta clases son las cuales podamos reutilizar con facilidad; clases con las que podamos pasar de un objeto payload a entity (Mapper), clase para la encriptación de contraseña, validación de contraseña y demás métodos reutilizables(Util), constantes creadas para el sistema (Constantes), clase para él envió de email(SimpleEmail), interface utilizada en el paquete repository para crea los métodos de acceso a datos básicos para cada entidad(BaseServiceInterface) y la clase encargada de manejar los estatus de error(ApiError).|

#### Base de Datos
Este [archivo SQL](https://gitlab.com/carloseduardobs/proyectando-ufps/-/blob/main/C%C3%B3digo%20Fuente/BD_PROYECTANDO.sql) llamado BD_PROYECTANDO.sql contine las sentencias DDL para generar nuestra Base de Datos así como tambien las sentencias DML para la insercion de los datos necesarios para el funcionamiento de proyectando - UFPS
![Esquema BD Proyectando - UFPS](https://gitlab.com/carloseduardobs/proyectando-ufps/-/raw/main/Documentaci%C3%B3n/Anexos/bd_squema.png)
### Arquitectura
proyectando-UFPS está basado en una arquitectura basada en capas. Cada capa del patrón de arquitectura en capas tiene un papel y una responsabilidad específicas dentro de la aplicación.

![Arquitectura Proyectando - UFPS](https://gitlab.com/carloseduardobs/proyectando-ufps/-/raw/main/Documentaci%C3%B3n/Anexos/Arquitectura%20Proyectando%20-UFPS%20(1).jpg)

- <b>Capa de presentación: </b>Es la capa en la cual están las interfaces de usuario la cual permite la interacción del usuario con la aplicación 
- <b>Capa de negocio: </b>También denominada Lógica de Dominio, esta capa contiene la funcionalidad que implementa la aplicación. Involucra cálculos basados en la información dada por el usuario y datos almacenados y validaciones. Controla la ejecución de la capa de acceso a datos y servicios externos. En proyectando UFPS utilizamos Spring Boot para la implementación de nuestra lógica de negocio, para la comunicación con la capa de datos a través de Spring JPA, exponer nuestras Apis para para que sean consumidas por la capa de presentación y para consumir servicios externos como Amazon simple email servicio.
- <b>Capa de datos: </b> es donde residen los datos y es la encargada de acceder a los mismos. Está formada por uno o más gestores de bases de datos que realizan todo el almacenamiento de datos, reciben solicitudes de almacenamiento o recuperación de información desde la capa de negocio. En proyectando UFPS hacemos uso de MySQL como gestor de base de datos para darle persistencia a nuestra aplicación.
### Tecnologías

* [Angular](https://angular.io/) - Framework para aplicaciones web desarrollado en TypeScript
* [Java - SpringBoot](https://spring.io/projects/spring-boot) - Framework para el desarrollo de aplicaciones, de código abierto para la plataforma Java.
* [MySQL](https://www.mysql.com/) - Motor base de datos.

### IDE

El proyecto se desarrolla usando los siguientes IDEs: 

- Visual Studio Code para trabajar el Frontend con Angular
- SpringToolSuite 4 para trabajar el Backend
- DBeaver para la administración de bases de datos

### Instalación

#### Pre-requisitos

Debes tener instalado
 
* [NodeJs](https://nodejs.org/es/) - Entorno en tiempo de ejecución multiplataforma, de código abierto, para la capa del servidor basado en el lenguaje de programación JavaScript
* [Java v8](https://www.oracle.com/co/java/technologies/javase/javase-jdk8-downloads.html) - Java Development Kit es un software que provee herramientas de desarrollo para la creación de programas en Java
* [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/welcome-versions.html#welcome-versions-v2) - La interfaz de línea de comandos (CLI) es una herramienta unificada para administrar los productos de AWS.

#### Instalación Angular

Desde una consola de comandos ejecutamos el siguiente comando para comenzar a descargar las dependencias necesarias.

```
npm install
```

luego ejecutaremos este comando para iniciar la aplicacion de manera local

```
ng serve
```

con esto tendremos desplegado el frontend del aplicativo, podemos ingresar desde el navegador en la siguiente ruta: http://localhost:4200/

#### Instalacion SpringBoot

Lo primero es importar el codigo como un proyecto Gradle existente.
Una vez importado podremos ejecutar desde el IDE dando click derecho sobre el proyecto, elegimos la opcion Run as> SpringBoot App
Se iniciara nuestro servidor local en la ruta: http://localhost:5000/

#### Adicionar archivos de credenciales AWS

Una vez instalado AWS CLI debemos configurar las credenciales para que la aplicacion pueda hacer uso del servicio de correo SES (Simple Email Services) de AWS, para eso abriremos una terminal CMD y ejecutamos el siguiente comando:
```
aws configure
```
Nos pedira que indiquemos las siguientes credenciales como se observa en el siguiente ejemplo:
```
aws_access_key_id=AKIAIOSFODNN7EXAMPLE
aws_secret_access_key=wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY
region=us-west-2
output=json
```
El aws_access_key_id y el aws_secret_access_key lo obtenemos del servicio IAM de AWS. [AWS](https://docs.aws.amazon.com/es_es/cli/latest/userguide/cli-configure-files.html)

### Demo

Para ver el demo de la aplicación puede dirigirse a: [Proyectando](http://proyectando-front.s3-website.us-east-2.amazonaws.com/proyectando/ingSistemas).

#### Usuarios de prueba
Cualquier persona con un correo de dominio [@ufps.edu.co](@ufps.edu.co) puede registrarse a Proyectando - UFPS en cualquiera de los roles mencionado a continuación excepto el rol Super Usuario
| Usuario | Contraseña | Rol | Descripción |
|:---     |:---        |:--- |:---         |
|proyectandoufps@gmail.com|proyectando2021|Super Usuario|El super usuario es el encargado de la administración de los distintos usuarios creados en el sistema; este rol es creado directamente en Base de Datos.|
|adminsistemas@ufps.edu.co|Admin|Administrador|El rol Administrador, es el rol asignado al director del programa académico;  este usuario es el encargado de la gestión de la proyección social del programa académico al cual pertenece, así como la administración de su landing page.  Este rol solo es permitido uno por programa académico.|
|||Director de proyecto|Este rol es el asignado a los docentes encargados de los distintos proyectos de proyección social de su  programa académico|
|||Estudiante|El rol estudiante puede cargar las evidencias que puedan ser tomar durante las actividades realizadas en la ejecución de un proyecto|



### Autor(es)

Proyecto desarrollado por:

* **Carlos Eduardo Barrera Silva** - *Desarrollo Backend y Documentación* - (<carloseduardobs@ufps.edu.co>).
* **James Garcia Sanchez** - *Desarrollo Frontend y Documentación* - (<jamesgs@ufps.edu.co>).
* **Samir Alexis Parada** - *Desarrollo Backend y Documentación* - (<samiralexispo@ufps.edu.co>).

### Institución Académica   
Proyecto desarrollado en el curso de profundización Ingenieria de Software para el [Programa de Ingeniería de Sistemas] de la [Universidad Francisco de Paula Santander]


   [Programa de Ingeniería de Sistemas]:<https://ingsistemas.cloud.ufps.edu.co/>
   [Universidad Francisco de Paula Santander]:<https://ww2.ufps.edu.co/>
